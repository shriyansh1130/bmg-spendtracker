# BMG Spend Tracker

## General information & briefing
- BMG Spend Tracker is a Single-Page App consisting of an Angular 5^ client and a Java/Spring Boot 2^ backend, authentication via Okta SSO 
- Environments and intentional use:
    - dev (local): Used locally to mimick BMG's server infrastructure, i.e. for pre-PR testing, migration script testing, etc.  
    - test (bmg): Initial test station on BMG's premises. Basically, this is your "bombing test field" and theoretically you can do whatever you like. Data losses cause 
    no harm and aren't expected to be persistent at all times - neither is availability. However: Think before you deploy. 
    - staging (bmg): Pre-production environment which should be treated as the production environment itself. Also, this is the place where UAT takes place. Staging is 
    expected to be available at all times. 
    - production (bmg): Self-explanatory
- A word about deployments:
    - Deployments are performed manually until CI/CD measures are taken into consideration
    - Both backend application and frontend client are wrapped in a .war archive, hosted by a maintained Tomcat server hosted on BMG's premises
    - Embedded Tomcat is used for local development
- git usage, branching, commit message conventions: 
    - We like to use the git-flow approach as much as possible. If in doubt, ask yourself: "What would git-flow do?"
    - Production branch: master
    - Development branch: develop
    - Feature branch prefix and pattern: feature/BST-{TICKETID}-{SHORTDESCRIPTIONINKEBAPCASE}
    - Example feature branch: feature/BSD-123-info-popover-animation-enhancement
    - Commit messages should have the pattern: {IMPLEMENT | FIX | DESCRIPTIVE_ACTION} {TICKETID} - {commit msg}
    - Sample message: "IMPLEMENT BST-123 - Removed Hibernate dependency"
- Speaking of git: Please use a code formatter prior to committing or abide by the rules of existing (formatted) files. For now, the default IntelliJ IDEA formatting rules are official since that's what most of us use


The following lists show the technological stack and requirements that Syndicats either _has_ to use as per BMG/Bertelsmann company policies or _has decided_ to use: 
### Backend
- Spring Boot 2^, Java 8, gradle
- MS SQL Server/Hibernate 
- Flyway/Javers   
- Elasticsearch v5.6^
- Project Lombok 
- Auth via Okta SSO & Spring Security
- REST API served as HAL/JSON (HATEOAS approach)
 
### Frontend
- Angular 5^ (w/SCSS enabled)
- Bootstrap 
- angular-cli, npm, webpack

### ...and if I want to use library x? 
You're allowed and highly encouraged to use whatever backend or frontend library you want as long as a) it makes sense to use it and b) the library's license is compatible or you paid for a commercial licence and won't charge neither BMG nor Syndicats :)  

## Development

### Prerequisites (yes, you need these)
- Java JDK 8+, 
- node & npm
- Lombok for backend development: https://projectlombok.org/
- Docker (including docker-compose)
- Optional but recommmended: Apache Gradle
- Recommended: Angular CLI - makes life easier
- Ideally: IntelliJ IDEA on a Mac (that's what most of us at Syndicats use at least)

### Development infrastructure and dependencies
- BMG Spend Tracker depends on:
    - MS SQL Server
    - Elasticsearch
    - Okta SSO (soon)
- With the exception of Okta these are mimicked by the docker-compose setup provided 
- In the project's root directory, execute the following, depending on what you'd like to do
```shell
$ docker-compose up         // startup infrastructure components
$ docker-compose stop       // stop components
$ docker-compose down -v    // stop components, remove containers (-v to remove volumes as well, data loss/reset!)  
```

### Deployment Process:
- Pull master
- Pull development
- Terminal in project root
```shell
$ docker-compose up
```
- Terminal in frontend
```shell
$ npm install
$ npm start
```
- Run Spring Boot Application with dev profile
This example uses bmg-test, following Server Data can vary:
    - bst-test
    - ggbmgttrack
    - ASTPBMG70
- test running application
- test Production Build
```shell
$ ./gradlew clean war
```
- merge development into master
```shell
./gradlew clean war
scp backend/build/libs/bmg-spendtracker-0.3.2.war bst-test:/tmp
ssh bst-test
bash
sudo -u ggbmgttrack -i //login as technical user (serverdependant) //ggbmg+serverlettercode+track
/DBA/nest/tomcat/ASTPBMG70/xbin/stop_tomcat.sh //reverse search good!
less /DBA/nest/tomcat/ASTPBMG70/logs/catalina.out.20180703 //check server log if stopped (OR open 2nd tab and open in follow mode(Shift f))
rm -rf /DBA/nest/tomcat/ASTPBMG70/webapps/ROOT.war /DBA/nest/tomcat/ASTPBMG70/webapps/ROOT // remove old war
cp /tmp/bmg-spendtracker-0.3.2.war /DBA/nest/tomcat/ASTPBMG70/webapps/ROOT.war
/DBA/nest/tomcat/ASTPBMG70/xbin/start_tomcat.sh
less /DBA/nest/tomcat/ASTPBMG70/logs/catalina.out.20180703 //check server log
exit //switch to personal user
rm -f /tmp/bmg-spendtracker-0.3.2.war // cleanup so another person can do the deployment without creating a new tmp file
```
### Backend startup
```shell
$ git clone ... (d'uh)
$ cd bmg-spendtracker
$ ./gradlew bootRun
```
- Backend should be up and running @ http://localhost:8080 (check e.g. http://localhost:8080/api )

### Frontend startup
```shell
$ cd bmg-spendtracker/frontend
$ npm install
$ npm start 
//alternatively: $ ng serve --proxy-config proxy.conf.json
```
- Frontend should be up and running @ http://localhost:4200
- (Note: Requests to localhost:4200/api/* will be redirected to backend port 8080)

### Frontend E2E testing
```shell
$ cd bmg-spendtracker/frontend
$ npm run test
```
- Or for concurrent testing with 4 workers
```shell
$ cd bmg-spendtracker/frontend
$ npm run testc4
```
- For test creation use testcafe-live which watches file changes and reruns tests automatically
```shell
$ cd bmg-spendtracker/frontend
$ testcafe-live chrome testcafe/tests/your-test-file.js
```
## Servers & Infrastructure
- Login to servers via personal users, change to technical user (Tomcat) via
```shell
sudo -u <TECHNICAL_USER> -i
```

### Test
- URL: https://spendtrackertest.bmg.com
- Technical User: ggbmgttrack
- Server: gtunxlvd04424 (10.6.209.89)
- Tomcat: ASTPBMG70 (Port 22002)
- Elasticsearch cluster: xxx 
- Master ES node: xxx
- Data ES node: xxx

### Staging
- URL: https://spendtrackerstag.bmg.com
- Technical User: ggbmgsttrack
- Server: gtunxlve04425 (10.6.209.90)
- Tomcat: ASTPBMG71 (Port 22002)
- Elasticsearch cluster: xxx
- Master ES node: xxx
- Data ES node: xxx
 
### Production
- URL: https://spendtracker.bmg.com
- Technical User: ggbmgptrack
- Server: gtunxlvf04426 (10.6.209.91)
- Tomcat: ASTPBMG72 (Port 22002)
- Elasticsearch cluster: xxx
- Master ES node: xxx
- Data ES node: xxx

## Database servers

### Test
- Server Name: gtlnmiwvm1671
- Port: 1433
- Server IP: 10.6.238.142
- DB Component: DBMPBMG7
- Database name: bmg_spend_test
- User name: bmg_spend_test
- Password: abz7WiEBVwvoM852D7qI
- Type: SQL Server 2014

### Staging
- Server Name: DEZIRWSB
- Port: 5555
- Server IP: 10.6.238.152
- DB Component: DBMIBMG14
- Database name: bmg_spend_stag
- User name: bmg_spend_stag
- Password: LtbG4hqZYIs1iwc7rYNo
- Type: SQL Server 2014