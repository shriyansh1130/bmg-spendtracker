# BMG Spend Tracker

### Getting started

To work with the project you need:

- [JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [gradle](https://gradle.org/)

Then you can start the application like this:

```
./gradlew bootRun
```

or with `dev` profile which provides a connection to a real ms-sql database:

```
./gradlew bootRun -Dspring.profiles.active=dev
```

### Running tests

The build defines several tasks for running tests:

```
./gradlew test
```

### API Documentation

To generate an api documentation execute:

```
./gradlew test build asciidoctor
```

### HAL Browser (for Spring Data REST)

To browse the api you can start the application:

```
./gradlew bootRun
```

afterwards open [a link](http://localhost:8080/api/browser/index.html#/api) with a browser 
and you can call the rest endpoints.
