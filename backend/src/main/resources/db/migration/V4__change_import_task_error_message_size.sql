alter table import_task_error_messages alter column error_messages varchar(MAX)
go

alter table import_task alter column error_message varchar(MAX)
go