create table application_user
(
	id bigint identity
		primary key,
	created_at datetime2,
	created_by varchar(255),
	modified_by varchar(255),
	modified_at datetime2,
	email varchar(255),
	fullname varchar(255),
	user_id varchar(255)
		constraint UK_93ev5eqh25ajuay7q4lxibi64
			unique,
	default_currency_id bigint
)
go

create table budget_category
(
	id bigint identity
		primary key,
	created_at datetime2,
	created_by varchar(255),
	modified_by varchar(255),
	modified_at datetime2,
	display_order bigint not null
		constraint UK_ogjoy58ek48fu9rpo3368gygr
			unique,
	budget_category_name nvarchar(150) not null
		constraint UK_88q4axij2ywrae177f741t5r9
			unique
)
go

create table budget_spend_type
(
	id bigint identity
		primary key,
	created_at datetime2,
	created_by varchar(255),
	modified_by varchar(255),
	modified_at datetime2,
	budget_spend_type_code varchar(255) not null
		constraint UK_21n0ax5dbfbawrerrm088qg6c
			unique
		check ([budget_spend_type_code]>=0 AND [budget_spend_type_code]<=99999),
	gl_account varchar(255) not null,
	ppd_recoupable bit not null,
	ppd_recoupment_pct numeric(6,3),
	title nvarchar(150) not null
		constraint UK_l9qb6d89rwub8ehtjkk600d23
			unique,
	budget_sub_category_id bigint not null
)
go

create table budget_sub_category
(
	id bigint identity
		primary key,
	created_at datetime2,
	created_by varchar(255),
	modified_by varchar(255),
	modified_at datetime2,
	display_order bigint not null
		constraint UK_cdrlbg0bdqoa2fwecllml44cx
			unique,
	title nvarchar(150) not null
		constraint UK_oogeqywcvol7c9dptgmskeg5b
			unique,
	budget_category_id bigint not null
		constraint FKhqf5u7u7obtv1lcle4ewtt22u
			references budget_category
)
go

alter table budget_spend_type
	add constraint FKoodpkc5dkvcg369sd3fbdxjpe
		foreign key (budget_sub_category_id) references budget_sub_category
go

create table company
(
	id bigint identity
		primary key,
	created_at datetime2,
	created_by varchar(255),
	modified_by varchar(255),
	modified_at datetime2,
	company_code nvarchar(4),
	company_name nvarchar(150) not null,
	international bit not null,
	sap_company_code nvarchar(255) not null,
	country_id bigint not null
)
go

create table country
(
	id bigint identity
		primary key,
	created_at datetime2,
	created_by varchar(255),
	modified_by varchar(255),
	modified_at datetime2,
	affiliate_budget varchar(255),
	country_code nvarchar(2) not null
		constraint UK_oqixmig4k8qxc8oba3fl4gqkr
			unique,
	country_name nvarchar(150) not null
		constraint UK_qrkn270121aljmucrdbnmd35p
			unique,
	po_country bit not null,
	sap_country bit not null,
	currency_id bigint
)
go

alter table company
	add constraint FKaa85rotlnir4w4xlj1nkilnws
		foreign key (country_id) references country
go

create table currency
(
	id bigint identity
		primary key,
	created_at datetime2,
	created_by varchar(255),
	modified_by varchar(255),
	modified_at datetime2,
	currency_code nvarchar(3) not null
		constraint UK_7n17ygajjchsso2n0lyxrsyif
			unique,
	currency_name nvarchar(150) not null,
	currency_symbol nvarchar(3) not null
)
go

alter table application_user
	add constraint FK4ioqd2b80iqe4kjurhy5qgoqu
		foreign key (default_currency_id) references currency
go

alter table country
	add constraint FKfnsa0kdnu5cg50irup7h29tmj
		foreign key (currency_id) references currency
go

create table exchange_rate
(
	id bigint identity
		primary key,
	created_at datetime2,
	created_by varchar(255),
	modified_by varchar(255),
	modified_at datetime2,
	exchange_rate_name nvarchar(150) not null
		constraint UK_d90gr6oao22mdy3dkqr2b2wbk
			unique,
	exchange_rate_value numeric(8,5) not null,
	export_date varchar(255),
	from_currency_id bigint not null
		constraint FKsqp62p3y14idtrqa2w6763oj9
			references currency,
	to_currency_id bigint not null
		constraint FKgn1n0nulvm9b138l5dqixcp7v
			references currency
)
go

create table project
(
	id bigint identity
		primary key,
	created_at datetime2,
	created_by varchar(255),
	modified_by varchar(255),
	modified_at datetime2,
	assignment nvarchar(40) not null,
	deal_type nvarchar(25) not null,
	ion_sap nvarchar(12) not null,
	label nvarchar(25) not null,
	priority int not null
		check ([priority]<=99),
	project_name nvarchar(75) not null,
	sap_client_code nvarchar(12) not null,
	segment nvarchar(10) not null,
	unique_code varchar(255) not null,
	repertoire_owner_company_id bigint not null
		constraint FKg9iaklt0e0smeg22d9hfkhvdm
			references company
)
go

create table purchase_order
(
	id bigint identity
		primary key,
	created_at datetime2,
	created_by varchar(255),
	modified_by varchar(255),
	modified_at datetime2,
	purchase_order_number varchar(255),
	status varchar(255) not null,
	country_id bigint not null
		constraint FK6yey93knikxpay98skxj9h84d
			references country,
	currency_id bigint not null
		constraint FKijpaw9ak0pnsorfdl9dej85kx
			references currency,
	vendor_id bigint not null
)
go

create table purchase_order_number
(
	country_code varchar(2) not null,
	year_of_century int not null,
	sequential_number bigint not null
		check ([sequential_number]>=0 AND [sequential_number]<=99999),
	primary key (country_code, year_of_century),
	constraint UKg09h1fawsf20uhbp1va8aclh9
		unique (country_code, year_of_century, sequential_number)
)
go

create table purchase_order_position
(
	id bigint identity
		primary key,
	created_at datetime2,
	created_by varchar(255),
	modified_by varchar(255),
	modified_at datetime2,
	description varchar(255),
	notes varchar(255),
	positions_order int,
	price numeric(12,3),
	quantity numeric(19,2),
	service_month datetime2 not null,
	po_country_id bigint
		constraint FKp90p8q5wsh0dym4jrroy4xmmt
			references country,
	project_id bigint not null
		constraint FKg7dy7spsvyhkqt8iyo1r8yoi9
			references project,
	purchase_order_id bigint
		constraint FKqtc6njlc5dh2t4e93xvdbgs3k
			references purchase_order,
	budget_spend_type_id bigint
		constraint FKgt3sjtimq3xiou4m6rwvwlggp
			references budget_spend_type
)
go

create table purchase_order_position_attachment
(
	id bigint identity
		primary key,
	created_at datetime2,
	created_by varchar(255),
	modified_by varchar(255),
	modified_at datetime2,
	blob varbinary(max),
	content_type varchar(255),
	filename varchar(255),
	size int,
	uuid varchar(255)
		constraint UK_t3s7bvthgkjo3ipkils5x92ls
			unique,
	purchase_order_position_id bigint
		constraint FKdi93wibkm4hanua9ub31y6rd8
			references purchase_order_position
)
go


create table vendor
(
	id bigint identity
		primary key,
	created_at datetime2,
	created_by varchar(255),
	modified_by varchar(255),
	modified_at datetime2,
	account_number numeric(7),
	city nvarchar(255),
	contact_name nvarchar(50),
	email nvarchar(255),
	grp nvarchar(4),
	house_number nvarchar(50),
	name1 nvarchar(50) not null,
	name2 nvarchar(50),
	name3 nvarchar(50),
	phone_number nvarchar(255),
	postcode nvarchar(50),
	reconciliation_account_number varchar(255),
	region nvarchar(255),
	street nvarchar(50),
	vat_registration_number nvarchar(20),
	company_id bigint not null
		constraint FKsfx6gf0an0bo7kwscuhi1hoya
			references company
)
go

alter table purchase_order
	add constraint FK20jcn7pw6hvx0uo0sh4y1d9xv
		foreign key (vendor_id) references vendor
go

