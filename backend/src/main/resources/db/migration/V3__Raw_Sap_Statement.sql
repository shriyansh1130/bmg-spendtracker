create table raw_sap_statement
(
	id bigint identity
		primary key,
	created_at datetime2,
	created_by varchar(255),
	modified_by varchar(255),
	modified_at datetime2,
	amount_in_local_currency varchar(255),
	budget_territory_code varchar(255),
	currency_code varchar(255),
	document_no varchar(255),
	file_import_date varchar(255),
	file_name varchar(255),
	file_type varchar(255),
	gl_assignment varchar(255),
	gl_cost_ctr varchar(255),
	gl_order varchar(255),
	gl_order_type varchar(255),
	gl_sap_ion_code varchar(255),
	gl_spend_type_code varchar(255),
	item_no varchar(255),
	po_number varchar(255),
	posting_date varchar(255),
	royalty_cost_description varchar(255),
	sap_company_code varchar(255),
	sap_territory_code varchar(255),
	text varchar(255),
	unique_key varchar(255),
	year varchar(255),
	year_month varchar(255)
)
go



