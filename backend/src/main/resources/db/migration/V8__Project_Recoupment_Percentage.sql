CREATE TABLE project_recoupment_percentage (
   recoupment_percentage NUMERIC(6, 3) NOT NULL,
   budget_spend_type_id BIGINT NOT NULL,
   project_id BIGINT NOT NULL,
   PRIMARY KEY (budget_spend_type_id, project_id)
)
GO

ALTER TABLE project_recoupment_percentage
  ADD CONSTRAINT fkh47bq0madjloeq3626o4mjk4q
  FOREIGN KEY (budget_spend_type_id) REFERENCES budget_spend_type
GO

ALTER TABLE project_recoupment_percentage
  ADD CONSTRAINT fkm89e42wtcg8v8allefaaeycn6
  FOREIGN KEY (project_id) REFERENCES project
GO