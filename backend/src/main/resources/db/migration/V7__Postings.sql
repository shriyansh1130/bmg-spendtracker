CREATE TABLE raw_posting_sync_task (
  id bigint identity primary key,
  created_at datetime2,
  created_by varchar(255),
  modified_by varchar(255),
  modified_at datetime2,
  matched_po int NOT NULL,
  processed_items int NOT NULL,
  sync_status varchar(255),
  total_items int NOT NULL,
  unmatched_po int NOT NULL
)
go

CREATE TABLE raw_posting_sync_task_error_messages (
  raw_posting_sync_task_id bigint NOT NULL
    CONSTRAINT FKcfwsi6subn3tl16f0fetjpb3h
    references raw_posting_sync_task,
  error_messages varchar(max)
)
go

CREATE TABLE sap_posting (
  id bigint identity primary key,
  created_at datetime2,
  created_by varchar(255),
  modified_by varchar(255),
  modified_at datetime2,
  amount_in_local_currency numeric(19,2),
  document_no varchar(255),
  file_import_date date,
  file_name varchar(255),
  file_type varchar(255),
  gl_assignment varchar(255),
  gl_cost_ctr varchar(255),
  gl_order varchar(255),
  gl_order_type varchar(255),
  gl_sap_ion varchar(255),
  item_no varchar(255),
  po_number varchar(255),
  posting_date date,
  royalty_cost_description varchar(255),
  sap_client_code varchar(255),
  text varchar(255),
  unique_key varchar(255),
  [year] varchar(255),
  year_month varchar(255),
  budget_spend_type_id bigint CONSTRAINT FKqmudbvj3ges7c3vdd6sv3am38 REFERENCES budget_spend_type,
  budget_territory_id bigint NOT NULL CONSTRAINT FK7xho13g9es0o5quupd7yn6eor REFERENCES country,
  company_id bigint NOT NULL CONSTRAINT FKg5yhwkrta12ourtie5yyt4ji9 REFERENCES company,
  currency_id bigint NOT NULL CONSTRAINT FKsq84yvcivfe4ydypvvvvr3hba REFERENCES currency,
  project_id bigint NOT NULL CONSTRAINT FKqfx6caxjp7695t19xmsy1aemn REFERENCES project,
  purchase_order_position_id bigint CONSTRAINT FKsproh5ss72u2ibd72r12pyqou REFERENCES purchase_order_position,
  sap_territory_id bigint NOT NULL CONSTRAINT FKan3ruitq2nk84lnnlw53kromt REFERENCES country
)
go