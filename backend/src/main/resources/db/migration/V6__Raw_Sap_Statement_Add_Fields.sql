ALTER TABLE raw_sap_statement ADD sap_client_code VARCHAR(255);
go

ALTER TABLE raw_sap_statement ADD process_status VARCHAR(255);
go

ALTER TABLE raw_sap_statement ADD project_id VARCHAR(255);
go

