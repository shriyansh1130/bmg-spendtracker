create table import_task
(
	id bigint identity
		primary key,
	created_at datetime2,
	created_by varchar(255),
	modified_by varchar(255),
	modified_at datetime2,
	error_message varchar(255),
	file_name varchar(250) not null,
	file_path varchar(250) not null,
	deleted_count int not null,
	error_count int not null,
	inserted_count int not null,
	updated_count int not null,
	import_status int not null,
	importer_name varchar(50) not null
)
go

create table import_task_error_messages
(
	import_task_id bigint not null
		constraint FKahcnmjw894syr2g94u5mfxegb
			references import_task,
	error_messages varchar(255)
)
go

