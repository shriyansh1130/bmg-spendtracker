create table import_task_warning_messages
(
	import_task_id bigint not null
		constraint  importtaskwarningmessagefk
			references import_task,
	warning_messages varchar(max)
)
go