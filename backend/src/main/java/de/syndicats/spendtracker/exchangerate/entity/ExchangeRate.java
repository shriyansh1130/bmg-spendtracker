package de.syndicats.spendtracker.exchangerate.entity;

import de.syndicats.spendtracker.application.entity.AbstractBaseModel;
import de.syndicats.spendtracker.currency.entity.Currency;
import lombok.*;
import org.hibernate.annotations.Nationalized;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Table(name = "exchange_rate")
@Entity
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public class ExchangeRate extends AbstractBaseModel {

    @Nationalized
    @Column(name = "exchange_rate_name", unique = true, nullable = false, length = 150)
    @Size(max = 150)
    @NonNull
    private String exchangeRateName;

    @Column(name = "exchange_rate_value", nullable = false)
    @NumberFormat(pattern = "#000.00000")
    @DecimalMin("0")
    @DecimalMax("999.99999")
    @Digits(integer = 3, fraction = 5)
    @NonNull
    private BigDecimal exchangeRateValue;

    @ManyToOne(optional = false)
    @JoinColumn(name = "from_currency_id", referencedColumnName = "id", nullable = false)
    @NonNull
    private Currency fromCurrency;

    @ManyToOne(optional = false)
    @JoinColumn(name = "to_currency_id", referencedColumnName = "id", nullable = false)
    @NonNull
    private Currency toCurrency;

    private String exportDate;

}