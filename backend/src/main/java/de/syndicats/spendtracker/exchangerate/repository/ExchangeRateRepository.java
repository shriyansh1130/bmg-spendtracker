package de.syndicats.spendtracker.exchangerate.repository;

import de.syndicats.spendtracker.exchangerate.entity.ExchangeRate;
import de.syndicats.spendtracker.exchangerate.entity.projection.ExchangeRateProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

//recommended to use kebab-case (which is highlighted by RFC3986)
@RepositoryRestResource(collectionResourceRel = "exchange-rates", path = "exchange-rates", excerptProjection = ExchangeRateProjection.class)
public interface ExchangeRateRepository extends JpaRepository<ExchangeRate, Long> {
    Page<ExchangeRate> findByExchangeRateNameIgnoreCaseContaining(@Param("exchangeRateName") String exchangeRateName, Pageable p);

    Optional<ExchangeRate> findByFromCurrency_CurrencyCodeAndToCurrency_CurrencyCode(@Param("fromCurrencyCode") String fromCurrencyCode, @Param("toCurrencyCode") String toCurrencyCode );

    boolean existsByExchangeRateName(String name);
}
