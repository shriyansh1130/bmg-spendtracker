package de.syndicats.spendtracker.exchangerate.entity.projection;

import de.syndicats.spendtracker.currency.entity.projection.CurrencyProjection;
import de.syndicats.spendtracker.exchangerate.entity.ExchangeRate;
import org.springframework.data.rest.core.config.Projection;

import java.math.BigDecimal;


@Projection(name = "exchangeRateProjection", types = {ExchangeRate.class})
public interface ExchangeRateProjection {
    String getExchangeRateName();
    BigDecimal getExchangeRateValue();
    CurrencyProjection getFromCurrency();
    CurrencyProjection getToCurrency();
}

