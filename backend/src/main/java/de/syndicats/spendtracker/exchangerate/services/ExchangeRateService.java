package de.syndicats.spendtracker.exchangerate.services;

import de.syndicats.spendtracker.currency.entity.Currency;
import de.syndicats.spendtracker.exchangerate.entity.ExchangeRate;
import de.syndicats.spendtracker.exchangerate.repository.ExchangeRateRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ExchangeRateService {
    private ExchangeRateRepository exchangeRateRepository;

    public BigDecimal calculateAmountInTargetCurrency(Currency sourceCurrency, BigDecimal sourceAmount, Currency targetCurrency) {

        final String fromCurrencyCode = sourceCurrency.getCurrencyCode();
        final String toCurrencyCode = targetCurrency.getCurrencyCode();
        Optional<ExchangeRate> rate = this.exchangeRateRepository.findByFromCurrency_CurrencyCodeAndToCurrency_CurrencyCode(fromCurrencyCode, toCurrencyCode);
        if (rate.isPresent()) {
            return sourceAmount.multiply(rate.get().getExchangeRateValue());
        } else {
            throw new RuntimeException(String.format("Tried to calculate amount for non-existing exchange-rate: FROM: %s, TO: %s", fromCurrencyCode, toCurrencyCode));
        }
    }
}
