package de.syndicats.spendtracker.exchangerate.data;

import de.syndicats.spendtracker.currency.entity.Currency;
import de.syndicats.spendtracker.currency.repository.CurrencyRepository;
import de.syndicats.spendtracker.exchangerate.entity.ExchangeRate;
import de.syndicats.spendtracker.importer.data.AbstractCSVManager;
import de.syndicats.spendtracker.importer.data.ImportResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;


@Component("exchangerate")
@AllArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class ExchangeRateImporter extends AbstractCSVManager<ExchangeRate, Long> {

    private static final String ID = "id";
    private static final String FROM = "From Currency";
    private static final String TO = "To Currency";
    private static final String RATE = "Rate";
    private static final String DATE = "Date";
    private static final String DELETE = "Delete";
    private static final String[] HEADERS = {ID, FROM, TO, RATE, DATE, DELETE};

    private CurrencyRepository currencyRepository;

    @Override
    protected String[] getHeaders() {
        return HEADERS;
    }

    @Override
    protected ExchangeRate createEntityFromRecord(CSVRecord record) {
        return ExchangeRate.builder()
                .exchangeRateName(getExchangeRateName(record))
                .exchangeRateValue(getExchangeRateValue(record))
                .exportDate(getExportDate(record))
                .fromCurrency(getFromCurrency(record))
                .toCurrency(getToCurrency(record))
                .build();
    }

    private String getExportDate(CSVRecord record) {
        return record.get(DATE);
    }

    private BigDecimal getExchangeRateValue(CSVRecord record) {
        return new BigDecimal(record.get(RATE));
    }

    private Currency getFromCurrency(CSVRecord record) {
        return currencyRepository.findByCurrencyCode(getFromCurrencyCode(record));
    }

    private Currency getToCurrency(CSVRecord record) {
        return currencyRepository.findByCurrencyCode(getToCurrencyCode(record));
    }

    private String getFromCurrencyCode(CSVRecord record) {
        return record.get(FROM);
    }

    private String getToCurrencyCode(CSVRecord record) {
        return record.get(TO);
    }

    private String getExchangeRateName(CSVRecord record) {
        return getFromCurrencyCode(record) + "-" + getToCurrencyCode(record);
    }

    @Override
    protected boolean validateAgainstAllRecords(CSVRecord recordToValidate, List<CSVRecord> allRecords, ImportResult result, int line) {
        return true;
    }

    @Override
    protected boolean validateRecord(CSVRecord recordToValidate, ImportResult result, int line) {
        if (getToCurrency(recordToValidate) == null) {
            String errorMessage = String.format("To-Currency %s does not exist. Skipping import of %s", getToCurrencyCode(recordToValidate), getExchangeRateName(recordToValidate), getExchangeRateName(recordToValidate));
            result.addErrorMessage(errorMessage);
            result.incrementErrorCount();
            return false;
        }

        if (getFromCurrency(recordToValidate) == null) {
            String errorMessage = String.format("From-Currency %s does not exist. Skipping import of %s", getFromCurrencyCode(recordToValidate), getExchangeRateName(recordToValidate));
            result.addErrorMessage(errorMessage);
            result.incrementErrorCount();
            return false;
        }
        return true;
    }

    @Override
    protected String getEntityName() {
        return "Exchange Rate";
    }

    @Override
    protected String getInstanceIdentifier(ExchangeRate entity) {
        return entity.getExchangeRateName();
    }

    @Override
    protected boolean shouldDelete(CSVRecord currentRecord) {
        return currentRecord.isMapped(DELETE) && StringUtils.isNotEmpty(currentRecord.get(DELETE));
    }

    @Override
    protected Long getEntityId(CSVRecord currentRecord) {
        return StringUtils.isEmpty(currentRecord.get(ID)) ? null : Long.valueOf(currentRecord.get(ID));
    }

    @Override
    protected ExchangeRate updateEntityFromRecord(ExchangeRate exchangeRate, CSVRecord currentRecord) {
        exchangeRate.setExchangeRateValue(getExchangeRateValue(currentRecord));
        exchangeRate.setExportDate(getExportDate(currentRecord));
        return exchangeRate;
    }

    @Override
    protected String getIdAsString(Long aLong) {
        return String.valueOf(aLong);
    }

    @Override
    protected String getExportSortField() {
        return "id";
    }

    @Override
    protected Iterable<?> getExportRow(ExchangeRate exchangeRate) {
        return Arrays.asList(
                exchangeRate.getId(),
                exchangeRate.getFromCurrency().getCurrencyCode(),
                exchangeRate.getToCurrency().getCurrencyCode(),
                exchangeRate.getExchangeRateValue(),
                exchangeRate.getExportDate(),
                ""
        );
    }
}