package de.syndicats.spendtracker.budget.data;

import de.syndicats.spendtracker.budget.entity.BudgetSpendType;
import de.syndicats.spendtracker.budget.entity.BudgetSubCategory;
import de.syndicats.spendtracker.budget.repository.BudgetSubCategoryRepository;
import de.syndicats.spendtracker.importer.data.AbstractCSVManager;
import de.syndicats.spendtracker.importer.data.ImportResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component("budgetSpendType")
@AllArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class BudgetSpendTypeImporter extends AbstractCSVManager<BudgetSpendType, Long> {
    private static final String ID = "id";
    private static final String TITLE = "title";
    private static final String CODE = "code";
    private static final String GL_ACCOUNT = "glAccount";
    private static final String PPD_RECOUPMENT_PCT = "ppdRecoupmentPct";
    private static final String BUDGET_SUB_CATEGORY_ID = "budgetSubCategoryId";
    private static final String DELETE = "delete";
    private static final String[] HEADERS = {ID, TITLE, CODE, PPD_RECOUPMENT_PCT, GL_ACCOUNT, BUDGET_SUB_CATEGORY_ID, DELETE};

    private BudgetSubCategoryRepository budgetSubCategoryRepository;

    @Override
    protected String[] getHeaders() {
        return HEADERS;
    }

    @Override
    protected BudgetSpendType createEntityFromRecord(CSVRecord record) {
        BudgetSubCategory subCategory = getSubCategory(record);
        return BudgetSpendType.builder()
                .title(getTitle(record))
                .budgetSubCategory(subCategory)
                .budgetSpendTypeCode(getBudgetSpendTypeCode(record))
                .glAccount(getGlAccount(record))
                .ppdRecoupmentPct(getPpdRecoupmentPct(record))
                .build();
    }

    private BudgetSubCategory getSubCategory(CSVRecord record) {
        return budgetSubCategoryRepository.findById(getSubCategoryId(record)).orElseThrow(() -> new RuntimeException("Invalid State: No Subcategory for given id found. Should have been catched in validation already."));
    }

    private String getTitle(CSVRecord record) {
        return record.get(TITLE);
    }

    private BigDecimal getPpdRecoupmentPct(CSVRecord record) {
        return record.isSet(PPD_RECOUPMENT_PCT) && record.get(PPD_RECOUPMENT_PCT).isEmpty() ? BigDecimal.ZERO : new BigDecimal(record.get(PPD_RECOUPMENT_PCT));
    }

    @Override
    protected boolean validateAgainstAllRecords(CSVRecord recordToValidate, List<CSVRecord> allRecords, ImportResult result, int line) {
        return true;
    }

    @Override
    protected boolean validateRecord(CSVRecord recordToValidate, ImportResult result, int line) {
        // Pre-Checks / Requirements
        long subCategoryId = getSubCategoryId(recordToValidate);
        Optional<BudgetSubCategory> optionalSubCategory = budgetSubCategoryRepository.findById(subCategoryId);
        if (!optionalSubCategory.isPresent()) {
            String errorMessage = String.format("Line %d: Can't import spend-type %s, because sub-category with ID: %d is missing", line, getTitle(recordToValidate), subCategoryId);
            result.addErrorMessage(errorMessage);
            result.incrementErrorCount();
            return false;
        }
        return true;
    }

    private Long getSubCategoryId(CSVRecord recordToValidate) {
        return Long.valueOf(recordToValidate.get(BUDGET_SUB_CATEGORY_ID));
    }

    @Override
    protected String getEntityName() {
        return "Budget Spend Type";
    }

    @Override
    protected String getInstanceIdentifier(BudgetSpendType entity) {
        return entity.getBudgetSpendTypeCode();
    }

    @Override
    protected boolean shouldDelete(CSVRecord currentRecord) {
        return false;
    }

    @Override
    protected Long getEntityId(CSVRecord currentRecord) {
        return StringUtils.isEmpty(currentRecord.get(ID)) ? null : Long.valueOf(currentRecord.get(ID));
    }

    @Override
    protected BudgetSpendType updateEntityFromRecord(BudgetSpendType budgetSpendType, CSVRecord currentRecord) {
        budgetSpendType.setTitle(getTitle(currentRecord));
        budgetSpendType.setBudgetSpendTypeCode(getBudgetSpendTypeCode(currentRecord));
        budgetSpendType.setBudgetSubCategory(getSubCategory(currentRecord));
        budgetSpendType.setPpdRecoupmentPct(getPpdRecoupmentPct(currentRecord));
        budgetSpendType.setGlAccount(getGlAccount(currentRecord));
        return budgetSpendType;
    }

    private String getGlAccount(CSVRecord currentRecord) {
        return currentRecord.get(GL_ACCOUNT);
    }

    private String getBudgetSpendTypeCode(CSVRecord currentRecord) {
        return currentRecord.get(CODE);
    }

    @Override
    protected String getIdAsString(Long aLong) {
        return String.valueOf(aLong);
    }

    @Override
    protected String getExportSortField() {
        return "id";
    }

    @Override
    protected Iterable<?> getExportRow(BudgetSpendType st) {
        return Arrays.asList(
                st.getId(),
                st.getTitle(),
                st.getBudgetSpendTypeCode(),
                st.getPpdRecoupmentPct(),
                st.getGlAccount(),
                st.getBudgetSubCategory().getId(),
                "");
    }


}
