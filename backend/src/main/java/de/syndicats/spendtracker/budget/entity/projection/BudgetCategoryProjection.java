package de.syndicats.spendtracker.budget.entity.projection;

import de.syndicats.spendtracker.budget.entity.BudgetCategory;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;

@Projection(name = "budgetCategoryProjection", types = BudgetCategory.class)
public interface BudgetCategoryProjection {
    String getTitle();

    Long getDisplayOrder();

    List<BudgetSubCategoryProjection> getBudgetSubCategories();

}


