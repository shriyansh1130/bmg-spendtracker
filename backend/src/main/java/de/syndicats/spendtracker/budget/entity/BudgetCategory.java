package de.syndicats.spendtracker.budget.entity;

import de.syndicats.spendtracker.application.entity.AbstractBaseModel;
import lombok.*;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "budget_category")
@Entity
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(exclude = "budgetSubCategories")
public class BudgetCategory extends AbstractBaseModel {

    @Nationalized
    @Column(name = "budget_category_name", unique = true, nullable = false, length = 150)
    @NonNull
    private String title;

    @Column(name = "display_order", unique = true, nullable = false)
    @NonNull
    private Long displayOrder;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "budgetCategory")
    @OrderBy("displayOrder asc")
    @Builder.Default
    private List<BudgetSubCategory> budgetSubCategories = new ArrayList<>();

}