package de.syndicats.spendtracker.budget.data;

import de.syndicats.spendtracker.budget.entity.BudgetCategory;
import de.syndicats.spendtracker.budget.entity.BudgetSubCategory;
import de.syndicats.spendtracker.budget.repository.BudgetCategoryRepository;
import de.syndicats.spendtracker.importer.data.AbstractCSVManager;
import de.syndicats.spendtracker.importer.data.ImportResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component("budgetSubCategory")
@AllArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class BudgetSubCategoryImporter extends AbstractCSVManager<BudgetSubCategory, Long> {
    private static final String ID = "id";
    private static final String TITLE = "title";
    private static final String DISPLAY_ORDER = "displayOrder";
    private static final String BUDGET_CATEGORY_ID = "budgetCategoryId";
    private static final String DELETE = "delete";
    private static final String[] HEADERS = {ID, TITLE, DISPLAY_ORDER, BUDGET_CATEGORY_ID, DELETE};

    private BudgetCategoryRepository budgetCategoryRepository;

    @Override
    protected String[] getHeaders() {
        return HEADERS;
    }

    @Override
    protected BudgetSubCategory createEntityFromRecord(CSVRecord record) {
        String title = getTitle(record);
        BudgetCategory category = getCategory(record);
        return BudgetSubCategory.builder()
                .title(title)
                .budgetCategory(category)
                .displayOrder(getDisplayOrder(record))
                .build();
    }

    private Long getDisplayOrder(CSVRecord record) {
        return Long.valueOf(record.get(DISPLAY_ORDER));
    }

    private String getTitle(CSVRecord record) {
        return record.get(TITLE);
    }

    private Long getCategoryId(CSVRecord recordToValidate) {
        return Long.valueOf(recordToValidate.get(BUDGET_CATEGORY_ID));
    }

    private BudgetCategory getCategory(CSVRecord record) {
        return budgetCategoryRepository.findById(getCategoryId(record)).orElseThrow(() -> new RuntimeException("Invalid State: No Category for given id found. Should have been catched in validation already."));
    }

    @Override
    protected boolean validateAgainstAllRecords(CSVRecord recordToValidate, List<CSVRecord> allRecords, ImportResult result, int line) {
        return true;
    }

    @Override
    protected boolean validateRecord(CSVRecord recordToValidate, ImportResult result, int line) {
        // Pre-Checks / Requirements
        Optional<BudgetCategory> optionalCategory = budgetCategoryRepository.findById(getCategoryId(recordToValidate));
        if (!optionalCategory.isPresent()) {
            String errorMessage = String.format("Line %d: Can't import Budget Subcategory %s, because Category with ID: %d is missing", line, getTitle(recordToValidate), getCategoryId(recordToValidate));
            result.addErrorMessage(errorMessage);
            result.incrementErrorCount();
            return false;
        }

        return true;
    }

    @Override
    protected String getEntityName() {
        return "Budget Sub Category";
    }

    @Override
    protected String getInstanceIdentifier(BudgetSubCategory entity) {
        return entity.getTitle();
    }

    @Override
    protected boolean shouldDelete(CSVRecord currentRecord) {
        return currentRecord.isMapped(DELETE) && StringUtils.isNotEmpty(currentRecord.get(DELETE));
    }

    @Override
    protected Long getEntityId(CSVRecord currentRecord) {
        return StringUtils.isEmpty(currentRecord.get(ID)) ? null : Long.valueOf(currentRecord.get(ID));
    }

    @Override
    protected BudgetSubCategory updateEntityFromRecord(BudgetSubCategory budgetSubCategory, CSVRecord currentRecord) {
        budgetSubCategory.setTitle(getTitle(currentRecord));
        budgetSubCategory.setDisplayOrder(getDisplayOrder(currentRecord));
        return budgetSubCategory;
    }

    @Override
    protected String getIdAsString(Long aLong) {
        return String.valueOf(aLong);
    }

    @Override
    protected String getExportSortField() {
        return "id";
    }

    @Override
    protected Iterable<?> getExportRow(BudgetSubCategory budgetSubCategory) {
        return Arrays.asList(budgetSubCategory.getId(),
                budgetSubCategory.getTitle(),
                budgetSubCategory.getDisplayOrder(),
                budgetSubCategory.getBudgetCategory().getId(),
                "");
    }
}
