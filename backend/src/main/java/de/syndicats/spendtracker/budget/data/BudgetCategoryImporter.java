package de.syndicats.spendtracker.budget.data;

import de.syndicats.spendtracker.budget.entity.BudgetCategory;
import de.syndicats.spendtracker.importer.data.AbstractCSVManager;
import de.syndicats.spendtracker.importer.data.ImportResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component("budgetCategory")
@AllArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class BudgetCategoryImporter extends AbstractCSVManager<BudgetCategory, Long> {
    private static final String ID = "id";
    private static final String TITLE = "title";
    private static final String DISPLAY_ORDER = "displayOrder";
    private static final String DELETE = "delete";
    private static final String[] HEADERS = {ID, TITLE, DISPLAY_ORDER, DELETE};

    @Override
    protected String[] getHeaders() {
        return HEADERS;
    }

    @Override
    protected BudgetCategory createEntityFromRecord(CSVRecord record) {
        return BudgetCategory.builder()
                .title(getTitle(record))
                .displayOrder(getDisplayOrder(record))
                .build();
    }

    @Override
    protected boolean validateAgainstAllRecords(CSVRecord recordToValidate, List<CSVRecord> allRecords, ImportResult result, int line) {
        return true;
    }

    @Override
    protected boolean validateRecord(CSVRecord recordToValidate, ImportResult result, int line) {
        return true;
    }

    @Override
    protected String getEntityName() {
        return "Budget Category";
    }

    @Override
    protected String getInstanceIdentifier(BudgetCategory entity) {
        return entity.getTitle();
    }

    @Override
    protected boolean shouldDelete(CSVRecord currentRecord) {
        return currentRecord.isMapped(DELETE) && StringUtils.isNotEmpty(currentRecord.get(DELETE));
    }

    @Override
    protected Long getEntityId(CSVRecord currentRecord) {
        return StringUtils.isEmpty(currentRecord.get(ID)) ? null : Long.valueOf(currentRecord.get(ID));
    }

    @Override
    protected BudgetCategory updateEntityFromRecord(BudgetCategory category, CSVRecord currentRecord) {
        category.setTitle(getTitle(currentRecord));
        category.setDisplayOrder(getDisplayOrder(currentRecord));
        return category;
    }

    private String getTitle(CSVRecord currentRecord) {
        return currentRecord.get(TITLE);
    }

    private Long getDisplayOrder(CSVRecord currentRecord) {
        return Long.valueOf(currentRecord.get(DISPLAY_ORDER));
    }

    @Override
    protected String getIdAsString(Long aLong) {
        return String.valueOf(aLong);
    }

    @Override
    protected String getExportSortField() {
        return "id";
    }

    @Override
    protected Iterable<?> getExportRow(BudgetCategory entity) {
        return Arrays.asList(entity.getId(),
                entity.getTitle(),
                entity.getDisplayOrder(),
                "");
    }
}
