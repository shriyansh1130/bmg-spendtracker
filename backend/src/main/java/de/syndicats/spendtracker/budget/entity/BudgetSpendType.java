package de.syndicats.spendtracker.budget.entity;

import de.syndicats.spendtracker.application.entity.AbstractBaseModel;
import lombok.*;
import org.hibernate.annotations.Nationalized;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;

@Table(name = "budget_spend_type")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public class BudgetSpendType extends AbstractBaseModel {

    @Nationalized
    @Column(name = "title", unique = true, nullable = false, length = 150)
    @Size(max = 150)
    @NonNull
    private String title;

    @ManyToOne(optional = false)
    @JoinColumn(name = "budget_sub_category_id", referencedColumnName = "id", nullable = false)
    @NonNull
    private BudgetSubCategory budgetSubCategory;

    @Column(name = "budget_spend_type_code", unique = true, nullable = false)
    @Min(0)
    @Max(99999)
    @NonNull
    private String budgetSpendTypeCode;

    @Column(name = "gl_account", nullable = false)
    @NonNull
    private String glAccount;

    @Column(name = "ppd_recoupment_pct", nullable = true)
    @NumberFormat(pattern = "#000.000")
    @DecimalMin("0")
    @DecimalMax("100")
    @Digits(integer = 3, fraction = 3)
    private BigDecimal ppdRecoupmentPct;


}
