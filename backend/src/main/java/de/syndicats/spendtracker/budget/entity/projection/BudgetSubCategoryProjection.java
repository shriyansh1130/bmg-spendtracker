package de.syndicats.spendtracker.budget.entity.projection;

import de.syndicats.spendtracker.budget.entity.BudgetSubCategory;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;

@Projection(name = "budgetSubCategoryProjection", types = BudgetSubCategory.class)
public interface BudgetSubCategoryProjection {
    String getTitle();

    Long getDisplayOrder();

    List<BudgetSpendTypeProjection> getBudgetSpendTypes();
}
