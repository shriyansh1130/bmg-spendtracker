package de.syndicats.spendtracker.budget.repository;

import de.syndicats.spendtracker.budget.entity.BudgetSpendType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

//recommended to use kebab-case (which is highlighted by RFC3986)
@RepositoryRestResource(collectionResourceRel = "budget-spend-types", path = "budget-spend-types")
public interface BudgetSpendTypeRepository extends JpaRepository<BudgetSpendType, Long> {
    Page<BudgetSpendType> findByBudgetSpendTypeCodeContaining(@Param("budgetSpendTypeCode") String budgetSpendTypeCode, Pageable p);
    Page<BudgetSpendType> findByTitleIgnoreCaseContaining(@Param("title") String title, Pageable p);
    Optional<BudgetSpendType> findByBudgetSpendTypeCode(String spendTypeCode);
}