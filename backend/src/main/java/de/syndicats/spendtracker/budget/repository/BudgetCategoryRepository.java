package de.syndicats.spendtracker.budget.repository;

import de.syndicats.spendtracker.budget.entity.BudgetCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

//recommended to use kebab-case (which is highlighted by RFC3986)
@RepositoryRestResource(collectionResourceRel = "budget-categories", path = "budget-categories")
public interface BudgetCategoryRepository extends JpaRepository<BudgetCategory, Long> {
}
