package de.syndicats.spendtracker.budget.entity.projection;

import de.syndicats.spendtracker.budget.entity.BudgetSpendType;
import org.springframework.data.rest.core.config.Projection;

import java.math.BigDecimal;

@Projection(name = "budgetSpendTypeProjection", types = BudgetSpendType.class)
public interface BudgetSpendTypeProjection {
    String getTitle();

    Integer getBudgetSpendTypeCode();

    BigDecimal getPpdRecoupmentPct();

    String getGlAccount();

}
