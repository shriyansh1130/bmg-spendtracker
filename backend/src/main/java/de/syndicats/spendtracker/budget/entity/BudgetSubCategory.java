package de.syndicats.spendtracker.budget.entity;

import de.syndicats.spendtracker.application.entity.AbstractBaseModel;
import lombok.*;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Table(name = "budget_sub_category")
@Entity
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public class BudgetSubCategory extends AbstractBaseModel {

    @Nationalized
    @Column(name = "title", unique = true, nullable = false, length = 150)
    @Size(max = 150)
    @NonNull
    private String title;

    @Column(name = "display_order", unique = true, nullable = false)
    @NonNull
    private Long displayOrder;

    @ManyToOne(optional = false)
    @JoinColumn(name = "budget_category_id", referencedColumnName = "id", nullable = false)
    @NonNull
    private BudgetCategory budgetCategory;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "budgetSubCategory")
    @OrderBy("title asc")
    @Builder.Default
    private List<BudgetSpendType> budgetSpendTypes = new ArrayList<>();
}
