package de.syndicats.spendtracker.importer.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.csv.CSVRecord;

@Getter
@AllArgsConstructor
public class ImportItem {

    private int index;
    private int bucket;
    private CSVRecord csvRecord;


    public static ImportItem from(int index, int bucket, CSVRecord csvRecord) {
        return new ImportItem(index, bucket, csvRecord);
    }

    public int getLine() {
        return index + 1;
    }
}
