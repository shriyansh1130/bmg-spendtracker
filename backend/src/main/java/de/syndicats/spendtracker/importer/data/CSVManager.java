package de.syndicats.spendtracker.importer.data;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public interface CSVManager {


    /**
     * Imports Records from an Inputstream containing CSV Data
     *
     * @param inputStream the designated Inputstream
     * @param createOnly  only new datasets should be imported
     * @return An Instance of ImportResult with statistics and error Information
     * @throws IOException in case of any IO-related failures
     */
    ImportResult importRecords(long importTaskId, InputStream inputStream, boolean createOnly) throws IOException;


    /**
     * Exports Records to a (temporary) CSV-File.
     *
     * @return File-Pointer to the (temporary) CSV-File.
     * @throws IOException in case of any IO-related failures
     */
    File exportRecords() throws IOException;

}
