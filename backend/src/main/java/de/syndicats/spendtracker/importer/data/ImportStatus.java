package de.syndicats.spendtracker.importer.data;

public enum ImportStatus {

    UPLOADED, IN_PROGRESS, COMPLETED, FAILED


}
