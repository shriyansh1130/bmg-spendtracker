package de.syndicats.spendtracker.importer.service;

import de.syndicats.spendtracker.importer.data.CSVManager;
import de.syndicats.spendtracker.importer.data.ImportResult;
import de.syndicats.spendtracker.importer.data.ImportStatus;
import de.syndicats.spendtracker.importer.entity.ImportTask;
import de.syndicats.spendtracker.importer.repository.ImportTaskRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.ByteOrderMark;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BOMInputStream;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.util.Optional;


@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class ImportService implements ApplicationContextAware {

    private ImportTaskRepository importTaskRepository;

    private ApplicationContext applicationContext;

    @Transactional
    public ImportTask createImportTaskFromStream(InputStream i, String fileName, String importerName) {

        ImportTask importTask = ImportTask.builder()
                .filePath("Not Assigned")
                .fileName(fileName)
                .importerName(importerName)
                .importStatus(ImportStatus.UPLOADED)
                .importResult(new ImportResult())
                .build();

        try {
            File tempFile = File.createTempFile(importerName, "_" + System.currentTimeMillis());
            importTask.setFilePath(tempFile.getAbsolutePath());
            try (FileOutputStream o = new FileOutputStream(tempFile)) {
                IOUtils.copy(i, o);
            }

        } catch (Throwable t) {
            importTask.setImportStatus(ImportStatus.FAILED);
            importTask.setErrorMessage(t.getMessage());
            log.error("Error creating ImportTask", t);
        } finally {
            updateImportTask(importTask);
        }
        return importTask;
    }


    @Async()
    public void importRecordsFromTask(Long importTaskId) {

        Optional<ImportTask> importTaskOptional = importTaskRepository.findById(importTaskId);
        ImportTask importTask = importTaskOptional.orElseThrow(() -> new IllegalArgumentException("No ImportTask found " + importTaskId));

        try {
            String filePath = importTask.getFilePath();
            File file = new File(filePath);
            FileInputStream fileInputStream = new FileInputStream(file);


            log.info("Start Importing {}", importTask);

            importTask.setImportStatus(ImportStatus.IN_PROGRESS);
            updateImportTask(importTask);

            ImportResult importResult = processInputStream(importTaskId, importTask.getImporterName(), fileInputStream);

            importTask.setImportResult(importResult);
            importTask.setImportStatus(ImportStatus.COMPLETED);

        } catch (Throwable t) {

            importTask.setImportStatus(ImportStatus.FAILED);
            importTask.setErrorMessage(t.getMessage());

            log.error("Error During Import", t);
            throw new RuntimeException(t);
        } finally {
            updateImportTask(importTask);
        }
    }

    protected ImportResult processInputStream(long importTaskId, String importerName, FileInputStream fileInputStream) throws IOException {
        CSVManager importer = (CSVManager) applicationContext.getBean(importerName);
        return importer.importRecords(importTaskId, new BOMInputStream(fileInputStream,
                ByteOrderMark.UTF_8,
                ByteOrderMark.UTF_16BE,
                ByteOrderMark.UTF_16LE, ByteOrderMark.UTF_32BE, ByteOrderMark.UTF_32LE), false);
    }


    @Transactional()
    protected void updateImportTask(ImportTask importTask) {
        importTaskRepository.save(importTask);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
