package de.syndicats.spendtracker.importer.repository;

import de.syndicats.spendtracker.importer.entity.ImportTask;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "import-tasks", path = "import-tasks")
public interface ImportTaskRepository extends JpaRepository<ImportTask, Long> {

    Page<ImportTask> findByImporterNameOrderByCreatedAtDesc(@Param("importerName") String importerName, Pageable page);

}
