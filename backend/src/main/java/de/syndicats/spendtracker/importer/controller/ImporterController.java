package de.syndicats.spendtracker.importer.controller;

import de.syndicats.spendtracker.importer.data.CSVManager;
import de.syndicats.spendtracker.importer.service.ImportService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

@Controller
@RequestMapping("/api")
@AllArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class ImporterController implements ApplicationContextAware {

    private ImportService importService;

    private ApplicationContext applicationContext;

    @GetMapping(value = "export")
    @ResponseBody
    public ResponseEntity<InputStreamResource> exportCompanies(@RequestParam("importer") String importerParam) {
        try {

            CSVManager importer = (CSVManager) applicationContext.getBean(importerParam);

            File file = importer.exportRecords();
            InputStream is = new FileInputStream(file);
            InputStreamResource resource = new InputStreamResource(is);
            String fileName = file.getName();

            HttpHeaders headers = new HttpHeaders();
            headers.setContentLength(file.length());
            headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
            headers.set(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);

            return new ResponseEntity<>(resource, headers, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error Exporting:", e);
            return ResponseEntity.badRequest()
                    .build();
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
