package de.syndicats.spendtracker.importer.data;

import lombok.Getter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

@Embeddable
@Getter
public class ImportResult {

    @ElementCollection(targetClass = String.class, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @Lob
    private List<String> errorMessages = Collections.synchronizedList(new ArrayList<String>());

    @ElementCollection(targetClass = String.class, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @Lob
    private List<String> warningMessages = Collections.synchronizedList(new ArrayList<String>());

    private int insertedCount;
    private int updatedCount;
    private int deletedCount;
    private int errorCount;


    public ImportResult addErrorMessage(String errorMessage) {
        this.errorMessages.add(errorMessage);
        return this;
    }

    public ImportResult addWarningMessage(String warningMessage) {
        this.warningMessages.add(warningMessage);
        return this;
    }

    public boolean hasErrorMessages() {
        return !this.errorMessages.isEmpty();
    }

    public boolean hasWarningMessages() {
        return !this.warningMessages.isEmpty();
    }

    public void incrementInsertedCount() {
        this.insertedCount++;
    }

    public void incrementUpdatedCount() {
        this.updatedCount++;
    }

    public void incrementErrorCount() {
        this.errorCount++;
    }

    public void incrementDeletedCount() {
        this.deletedCount++;
    }

    @Override
    public String  toString() {
        final StringBuilder sb = new StringBuilder();

        sb.append("Not imported: ")
                .append(errorCount)
                .append("\n");
        sb.append("Inserted: ")
                .append(insertedCount)
                .append("\n");
        sb.append("Updated: ")
                .append(updatedCount)
                .append("\n");
        sb.append("Deleted: ")
                .append(deletedCount)
                .append("\n");
        sb.append("Violations:\n");
        errorMessages.forEach(msg -> sb.append(msg)
                .append("\n"));
        sb.append("Warnings:\n");
        warningMessages.forEach(msg -> sb.append(msg)
                .append("\n"));

        return sb.toString();

    }
}
