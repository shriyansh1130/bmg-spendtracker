package de.syndicats.spendtracker.importer.data;

import de.syndicats.spendtracker.importer.entity.ImportTask;
import de.syndicats.spendtracker.importer.repository.ImportTaskRepository;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public abstract class AbstractCSVManager<T, ID> implements CSVManager {

    private final static int BUCKET_SIZE = 100;

    @Autowired
    @Setter
    protected JpaRepository<T, ID> entityRepository;

    @Setter
    @Autowired
    private ImportTaskRepository importTaskRepository;

    @Autowired
    @Setter
    private Validator validator;

    protected abstract String[] getHeaders();

    protected abstract T createEntityFromRecord(CSVRecord record);

    protected abstract boolean validateAgainstAllRecords(CSVRecord recordToValidate, List<CSVRecord> allRecords, ImportResult result, int line);

    protected abstract boolean validateRecord(CSVRecord recordToValidate, ImportResult result, int line);

    protected abstract String getEntityName();

    protected abstract String getInstanceIdentifier(T entity);

    protected abstract boolean shouldDelete(CSVRecord currentRecord);

    protected abstract ID getEntityId(CSVRecord currentRecord);

    protected abstract T updateEntityFromRecord(T t, CSVRecord currentRecord);

    protected abstract String getIdAsString(ID id);

    protected abstract String getExportSortField();

    protected abstract Iterable<?> getExportRow(T entity);

    @Override
    public ImportResult importRecords(long importTaskId, InputStream inputStream, boolean createOnly) throws IOException {
        log.info("Importing {} data ...", getEntityName());
        ImportResult result = new ImportResult();


        List<ImportItem> importItems = getImportItems(inputStream);
        List<CSVRecord> records = new ArrayList<>();

        importItems.stream().map(ImportItem::getCsvRecord).forEach(records::add);
        importItems.stream()
                .collect(Collectors.groupingBy(ImportItem::getBucket))
                .forEach((bucketNr, bucket) -> importBucket(importTaskId, result, records, bucket));

        log.info("{} import done. {}", getEntityName(), result);
        return result;

    }

    @Transactional
    protected void importBucket(long importTaskId, ImportResult result, List<CSVRecord> records, List<ImportItem> bucket) {
        long start = System.currentTimeMillis();

        Optional<ImportTask> importTaskOptional = importTaskRepository.findById(importTaskId);

        bucket.forEach(item -> importItem(result, records, item));

        importTaskOptional.ifPresent(i -> {
            i.setImportResult(result);
            importTaskRepository.save(i);
        });

        long duration = System.currentTimeMillis() - start;
        log.debug("Import Bucket with size {} took {} ms", bucket.size(), duration);

    }

    private void importItem(ImportResult result, List<CSVRecord> records, ImportItem importItem) {
        int line = importItem.getLine();
        try {
            CSVRecord currentRecord = importItem.getCsvRecord();
            if (validateAgainstAllRecords(currentRecord, records, result, line)) {
                if (shouldDelete(currentRecord)) {
                    this.handleDelete(result, line, this.getEntityId(currentRecord));
                } else if (validateRecord(currentRecord, result, line)) {
                    ID id = getEntityId(currentRecord);
                    if (shouldCreate(id)) {
                        handleCreate(result, line, currentRecord);
                    } else {
                        handleUpdate(result, line, currentRecord, id);
                    }
                }
            }
        } catch (Exception e) {
            String errorMessage = String.format("Line %d: Can't import. Exception: %s error-message %s", line, e.getClass(), e.getMessage());
            result.addErrorMessage(errorMessage);
            result.incrementErrorCount();
        }

    }


    protected boolean shouldCreate(ID id) {
        return id == null;
    }

    private void handleCreate(ImportResult result, int line, CSVRecord currentRecord) {
        T entityToSave;
        entityToSave = createEntityFromRecord(currentRecord);
        if (validateEntity(entityToSave, line, result)) {
            beforeCreate(entityToSave);
            entityRepository.save(entityToSave);
            result.incrementInsertedCount();
        }
    }

    protected void beforeCreate(T entityToSave) {
    }

    private void handleUpdate(ImportResult result, int line, CSVRecord currentRecord, ID id) {
        T entityToSave;
        Optional<T> entity = entityRepository.findById(id);
        if (entity.isPresent()) {
            entityToSave = updateEntityFromRecord(entity.get(), currentRecord);
            if (validateEntity(entityToSave, line, result)) {
                entityRepository.save(entityToSave);
                result.incrementUpdatedCount();
            }
        } else {
            String errorMessage = String.format("Skipping Line %d: import skipped. No Data found for given ID: %s", line, getIdAsString(id));
            result.addErrorMessage(errorMessage);
            result.incrementErrorCount();
        }
    }


    private List<ImportItem> getImportItems(InputStream inputStream) throws IOException {
        List<ImportItem> records = new ArrayList<>();
        try (CSVParser parser = CSVFormat.newFormat(';')
                .withHeader(getHeaders())
                .withFirstRecordAsHeader()
                .withQuote('"')
                .parse(new InputStreamReader(inputStream))) {
            int i = 0;
            int bucketNr = 0;
            for (CSVRecord record : parser) {

                ImportItem importItem = ImportItem.from(i, bucketNr, record);
                records.add(importItem);
                i++;

                if (i % BUCKET_SIZE == 0) {
                    bucketNr++;
                }

            }
        }
        return records;
    }

    @Override
    public File exportRecords() throws IOException {
        File targetFile = File.createTempFile(getFilename(), ".csv");
        return writeCSVToFile(targetFile, getHeaders());
    }

    private String getFilename() {
        return getEntityName() + "_" + System.currentTimeMillis();
    }

    public void printToCSV(CSVPrinter csvPrinter) throws IOException {
        List<T> entities = this.entityRepository.findAll(getExportSortOrder());
        for (T entity : entities) {
            csvPrinter.printRecord(getExportRow(entity));
        }
    }

    private Sort getExportSortOrder() {
        return Sort.by(new Sort.Order(Sort.Direction.ASC, getExportSortField()));
    }

    protected boolean validateEntity(T entity, int lineNumber, ImportResult result) {
        Set<ConstraintViolation<T>> constraintViolations = this.validator.validate(entity);

        //No Violations...
        if (constraintViolations.isEmpty()) {
            return true;

        } else {
            String violations = constraintViolations.stream()
                    .map(con -> con.getPropertyPath()
                            .toString() + " " + con.getMessage())
                    .collect(Collectors.joining(";\n"));
            String errorMessage = String.format("Line %d: Can't import %s %s, because %s", lineNumber, getEntityName(), getInstanceIdentifier(entity), violations);
            result.addErrorMessage(errorMessage);
            result.incrementErrorCount();
            return false;
        }
    }


    private void writeBOM(FileOutputStream stream) throws IOException {
        stream.write(0xEF);
        stream.write(0xBB);
        stream.write(0xBF);
    }

    private File writeCSVToFile(File targetFile, String[] HEADERS) throws IOException {
        try (FileOutputStream stream = new FileOutputStream(targetFile);
             BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stream, StandardCharsets.UTF_8));
             final CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withQuote(null)
                     .withDelimiter(';')
                     .withCommentMarker(null)
                     .withHeader(HEADERS))) {
            writeBOM(stream);
            printToCSV(csvPrinter);
        }
        return targetFile;
    }

    private void handleDelete(ImportResult result, int line, ID id) {
        try {
            Optional<T> elementToDelete = entityRepository.findById(id);
            if (elementToDelete.isPresent()) {
                entityRepository.delete(elementToDelete.get());
                result.incrementDeletedCount();
            } else {
                result.incrementErrorCount();
                result.addErrorMessage(String.format("Skipping Line %d: DELETE skipped for ID: %s. Element does not exist", line, getIdAsString(id)));
            }
        } catch (DataIntegrityViolationException dive) {
            result.incrementErrorCount();
            result.addErrorMessage(String.format("Skipping Line %d: DELETE skipped for ID: %s. Element already has references", line, getIdAsString(id)));
        } catch (Exception e) {
            result.incrementErrorCount();
            result.addErrorMessage(String.format("Skipping Line %d: DELETE skipped for ID: %s. Error-Message: %s", line, getIdAsString(id), e.getMessage()));
        }
    }


}
