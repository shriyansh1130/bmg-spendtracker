package de.syndicats.spendtracker.importer.controller;

import de.syndicats.spendtracker.importer.entity.ImportTask;
import de.syndicats.spendtracker.importer.service.ImportService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

@RepositoryRestController
@RequestMapping("/api")
@AllArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class ImportTaskController {

    private ImportService importService;

    private ApplicationContext applicationContext;

    @PostMapping(value = "import", produces = "application/json")
    public ResponseEntity<PersistentEntityResource> importTask(@RequestPart("file") MultipartFile file, @RequestParam("importer") String importerParam, PersistentEntityResourceAssembler persistentEntityResourceAssembler) {
        try {
            //Creating import Task
            String fileName = Optional.of(file.getOriginalFilename())
                    .orElse(importerParam + ".csv");

            ImportTask importTask = importService.createImportTaskFromStream(file.getInputStream(), fileName, importerParam);

            //Asynchronous import
            importService.importRecordsFromTask(importTask.getId());

            return ResponseEntity.ok(persistentEntityResourceAssembler.toFullResource(importTask));
        } catch (Exception e) {
            return ResponseEntity.badRequest()
                    .build();
        }
    }
}