package de.syndicats.spendtracker.importer.entity;

import de.syndicats.spendtracker.application.entity.AbstractBaseModel;
import de.syndicats.spendtracker.importer.data.ImportResult;
import de.syndicats.spendtracker.importer.data.ImportStatus;
import lombok.*;

import javax.persistence.*;

@Table(name = "import_task")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public class ImportTask extends AbstractBaseModel {

    @Column(name = "importer_name", nullable = false, length = 50)
    @NonNull
    private String importerName;

    @Column(name = "file_path", nullable = false, length = 250)
    @NonNull
    private String filePath;

    @Column(name = "file_name", nullable = false, length = 250)
    @NonNull
    private String fileName;

    @Column(name = "import_status", nullable = false)
    @NonNull
    private ImportStatus importStatus;

    @Embedded
    private ImportResult importResult;

    @Column(name = "error_message")
    @Lob
    private String errorMessage;
}
