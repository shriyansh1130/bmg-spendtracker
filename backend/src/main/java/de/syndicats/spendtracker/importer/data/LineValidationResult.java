package de.syndicats.spendtracker.importer.data;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LineValidationResult {
    private int line;
    private String warningMessage;
    private String errorMessage;

    public String getErrorMessage() {
        return String.format("Line %d: %s", line, errorMessage);
    }

    public String getWarningMessage() {
        return String.format("Line %d: %s", line, warningMessage);
    }

    public boolean hasError() {
        return this.errorMessage != null;
    }

    public boolean hasWarning() {
        return this.warningMessage != null;
    }
}
