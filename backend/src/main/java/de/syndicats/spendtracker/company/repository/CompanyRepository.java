package de.syndicats.spendtracker.company.repository;

import de.syndicats.spendtracker.company.entity.Company;
import de.syndicats.spendtracker.company.entity.projection.CompanyProjection;
import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.vendor.entity.Vendor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "companies", path = "companies", excerptProjection = CompanyProjection.class)
public interface CompanyRepository extends JpaRepository<Company, Long> {

    Company findBySapCompanyCode(String s);

    boolean existsBySapCompanyCode(String sapCompanyCode);

    Company findByCountry_IdAndInternationalIsTrue(@Param("countryId") Long countryId);
}
