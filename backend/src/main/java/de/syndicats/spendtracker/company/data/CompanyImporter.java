package de.syndicats.spendtracker.company.data;

import de.syndicats.spendtracker.company.entity.Company;
import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.country.repository.CountryRepository;
import de.syndicats.spendtracker.importer.data.AbstractCSVManager;
import de.syndicats.spendtracker.importer.data.ImportResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component("company")
@AllArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
@SuppressWarnings("squid:S1075")
public class CompanyImporter extends AbstractCSVManager<Company, Long> {


    private static final String ID = "id";
    private static final String NAME = "CompanyName";
    private static final String CODE = "code";
    private static final String COUNTRY_NAME = "country";
    private static final String SAP_CODE = "SAPCompanyCode";
    private static final String IS_INTERNATIONAL = "isInternational";
    private static final String DELETE = "Delete";
    private static final String[] HEADERS = {ID, NAME, CODE, COUNTRY_NAME, SAP_CODE, IS_INTERNATIONAL, DELETE};

    private static final String INTERNATIONAL_TRUE = "Yes";

    private CountryRepository countryRepository;

    @Override
    protected String[] getHeaders() {
        return HEADERS;
    }

    @Override
    protected Company createEntityFromRecord(CSVRecord record) {

        return Company.builder()
                .companyName(getCompanyName(record))
                .companyCode(getCompanyCode(record))
                .sapCompanyCode(getSapCompanyCode(record))
                .country(getCountry(record))
                .international(isInternational(record))
                .build();
    }

    private boolean isInternational(CSVRecord record) {
        return INTERNATIONAL_TRUE.equalsIgnoreCase(record.get(IS_INTERNATIONAL));
    }

    private Country getCountry(CSVRecord record) {
        return countryRepository.findByCountryName(getCountryName(record));
    }

    private String getSapCompanyCode(CSVRecord record) {
        return record.get(SAP_CODE);
    }

    private String getCompanyCode(CSVRecord record) {
        return record.get(CODE);
    }

    private String getCountryName(CSVRecord record) {
        return record.get(COUNTRY_NAME);
    }

    private String getCompanyName(CSVRecord record) {
        return record.get(NAME);
    }

    @Override
    protected boolean validateAgainstAllRecords(CSVRecord recordToValidate, List<CSVRecord> allRecords, ImportResult result, int line) {
        return true;
    }

    @Override
    protected boolean validateRecord(CSVRecord recordToValidate, ImportResult result, int line) {
        // Pre-Checks / Requirements
        Country country = getCountry(recordToValidate);
        if (country == null) {
            String errorMessage = String.format("Line %d: Can't import company %s, because country %s is missing", line, getSapCompanyCode(recordToValidate), getCountryName(recordToValidate));
            result.addErrorMessage(errorMessage);
            result.incrementErrorCount();
            return false;
        }
        return true;
    }

    @Override
    protected String getEntityName() {
        return "Country";
    }

    @Override
    protected String getInstanceIdentifier(Company entity) {
        return entity.getCompanyName();
    }

    @Override
    protected boolean shouldDelete(CSVRecord currentRecord) {
        return currentRecord.isMapped(DELETE) && StringUtils.isNotEmpty(currentRecord.get(DELETE));
    }

    @Override
    protected Long getEntityId(CSVRecord currentRecord) {
        return StringUtils.isEmpty(currentRecord.get(ID)) ? null : Long.valueOf(currentRecord.get(ID));
    }

    @Override
    protected Company updateEntityFromRecord(Company company, CSVRecord currentRecord) {
        company.setCompanyName(getCompanyName(currentRecord));
        company.setCompanyCode(getCompanyCode(currentRecord));
        company.setCountry(getCountry(currentRecord));
        company.setSapCompanyCode(getSapCompanyCode(currentRecord));
        company.setInternational(isInternational(currentRecord));
        return company;
    }

    @Override
    protected String getIdAsString(Long aLong) {
        return String.valueOf(aLong);
    }

    @Override
    protected String getExportSortField() {
        return "id";
    }

    @Override
    protected Iterable<?> getExportRow(Company company) {
        return Arrays.asList(
                company.getId(),
                company.getCompanyName(),
                company.getCompanyCode(),
                company.getCountry()
                        .getCountryName(),
                company.getSapCompanyCode(),
                company.isInternational() ? "Yes" : "No",
                ""
        );
    }
}