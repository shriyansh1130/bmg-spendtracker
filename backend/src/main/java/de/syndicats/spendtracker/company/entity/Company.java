package de.syndicats.spendtracker.company.entity;

import de.syndicats.spendtracker.application.entity.AbstractBaseModel;
import de.syndicats.spendtracker.country.entity.Country;
import lombok.*;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Table(name = "company")
@Entity
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public class Company extends AbstractBaseModel {

    @Nationalized
    @Column(name = "company_name", nullable = false, length = 150)
    @Size(max = 150)
    @NonNull
    private String companyName;

    @Nationalized
    @Column(name = "company_code", length = 4)
    @Size(max = 4)
    private String companyCode;

    @ManyToOne
    @JoinColumn(name = "country_id", referencedColumnName = "id", nullable = false)
    @NonNull
    private Country country;

    @Nationalized
    @Column(name = "sap_company_code", nullable = false)
    @NonNull
    private String sapCompanyCode;

    @Column(name = "international", nullable = false)
    @Builder.Default
    private boolean international = false;

}