package de.syndicats.spendtracker.company.entity.projection;

import de.syndicats.spendtracker.company.entity.Company;
import de.syndicats.spendtracker.country.entity.projection.CountryProjection;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "companyProjection", types = {Company.class})
public interface CompanyProjection {
    String getCompanyName();

    String getCompanyCode();

    CountryProjection getCountry();

    String getSapCompanyCode();

    boolean getInternational();
}
