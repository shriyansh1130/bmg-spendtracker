package de.syndicats.spendtracker.purchaseorder.validators;

import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrder;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component("beforeDeletePurchaseOrderValidator")
public class DeletePurchaseOrderValidator implements Validator{


    @Override
    public boolean supports(Class<?> clazz) {
        return PurchaseOrder.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        PurchaseOrder po = (PurchaseOrder) target;
        if (!PurchaseOrderStatus.Draft.equals(po.getStatus())) {
            errors.reject("purchaseorder.delete.error.status");
        }
    }
}
