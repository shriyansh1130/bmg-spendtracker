package de.syndicats.spendtracker.purchaseorder.service;

import de.syndicats.spendtracker.project.entity.Project;
import de.syndicats.spendtracker.project.service.ProjectRecoupmentPercentageService;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderCoding;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderCodingType;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import de.syndicats.spendtracker.purchaseorder.util.PurchaseOrderUtil;
import de.syndicats.spendtracker.vendor.entity.Vendor;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@AllArgsConstructor
@Slf4j
@Service
public class SapCodingRulesService {
    private static final String ORDERTYPE_MAST = "MAST";
    private static final String ORDERTYPE_NEWR = "NEWR";
    private static final String ION_SAP_CAT = "CAT";
    private static final String CAT_COSTCTR_POSTFIX = "5050";
    private static final String NON_CAT_COSTCTR_POSTFIX = "5080";
    private static final String TEXT_DELIMITER = "-";
    private static final int SAP_TEXT_MAX_CHARACTERS = 50;
    public static final int MIN_DESCRIPTION_CHARACTERS = 18;

    private PurchaseOrderPositionService purchaseOrderPositionService;
    private ProjectRecoupmentPercentageService projectRecoupmentPercentageService;

    public PurchaseOrderCoding createCoding(PurchaseOrderPosition position) {
        BigDecimal recoupmentPct = purchaseOrderPositionService.getRecoupmentPct(position);
        if (isRecharge(position)) {
            return calculateRechargeCoding(position);
        } else if (projectRecoupmentPercentageService.hasProjectSpecificRecoupment(position.getProject(), position.getSpendType()) || isTraditionalModel(position)) {
            if (BigDecimal.ZERO.compareTo(recoupmentPct) == 0) {
                return calculateZeroRecoupabilityCoding(position);
            } else if (BigDecimal.valueOf(100).compareTo(recoupmentPct) == 0) {
                return calculateFullRecoupabilityCoding(position);
            } else {
                return calculateCombinedCoding(position, recoupmentPct);
            }
        } else {
            return calculateFullRecoupabilityCoding(position);
        }

    }

    private PurchaseOrderCoding calculateCombinedCoding(PurchaseOrderPosition position, BigDecimal recoupmentPct) {
        String recoupableAmount = recoupmentPct.stripTrailingZeros().toPlainString() + "%";
        String nonRecoupableAmount = new BigDecimal("100").subtract(recoupmentPct).stripTrailingZeros().toPlainString() + "%";
        return PurchaseOrderCoding.builder()
                .codingType(PurchaseOrderCodingType.MIXED_RECOUPABILITY)
                .recoupmentPct(recoupmentPct)
                .nonRecoupableAccount(position.getSpendType().getGlAccount())
                .nonRecoupableAssignment(position.getProject().getSapClientCode())
                .nonRecoupableOrder(position.getProject().getIonSap())
                .nonRecoupableReferenceKey2(PurchaseOrderUtil.createPositionReferenceString(position))
                .nonRecoupableReferenceKey3(position.getSpendType().getBudgetSpendTypeCode() + position.getPoCountry().getCountryCode())
                .nonRecoupableCostCenter(getCostCenter(position))
                .nonRecoupableOrderType(getOrderType(position))
                .nonRecoupableText(calculateVariableSapText(position, SAP_TEXT_MAX_CHARACTERS))
                .nonRecoupableAmount(nonRecoupableAmount)
                .recoupableAccount(position.getProject().getSapClientCode())
                .recoupableAssignment(position.getSpendType().getBudgetSpendTypeCode() + position.getPoCountry().getCountryCode())
                .recoupableText(calculateVariableSapText(position, SAP_TEXT_MAX_CHARACTERS))
                .recoupableReferenceKey2(PurchaseOrderUtil.createPositionReferenceString(position))
                .recoupableAmount(recoupableAmount)
                .build();
    }

    private PurchaseOrderCoding calculateFullRecoupabilityCoding(PurchaseOrderPosition position) {
        return PurchaseOrderCoding.builder()
                .codingType(PurchaseOrderCodingType.FULL_RECOUPABILITY)
                .recoupmentPct(new BigDecimal("100.00"))
                .recoupableAccount(position.getProject().getSapClientCode())
                .recoupableAssignment(position.getSpendType().getBudgetSpendTypeCode() + position.getPoCountry().getCountryCode())
                .recoupableText(calculateVariableSapText(position, SAP_TEXT_MAX_CHARACTERS))
                .recoupableReferenceKey2(PurchaseOrderUtil.createPositionReferenceString(position))
                .recoupableAmount("100%")
                .build();
    }

    private PurchaseOrderCoding calculateZeroRecoupabilityCoding(PurchaseOrderPosition position) {
        return PurchaseOrderCoding.builder()
                .codingType(PurchaseOrderCodingType.ZERO_RECOUPABILITY)
                .recoupmentPct(BigDecimal.ZERO)
                .nonRecoupableAccount(position.getSpendType().getGlAccount())
                .nonRecoupableAssignment(position.getProject().getSapClientCode())
                .nonRecoupableOrder(position.getProject().getIonSap())
                .nonRecoupableReferenceKey2(PurchaseOrderUtil.createPositionReferenceString(position))
                .nonRecoupableReferenceKey3(position.getSpendType().getBudgetSpendTypeCode() + position.getPoCountry().getCountryCode())
                .nonRecoupableCostCenter(getCostCenter(position))
                .nonRecoupableOrderType(getOrderType(position))
                .nonRecoupableText(calculateVariableSapText(position, SAP_TEXT_MAX_CHARACTERS))
                .nonRecoupableAmount("100%")
                .build();
    }

    private PurchaseOrderCoding calculateRechargeCoding(PurchaseOrderPosition position) {
        return PurchaseOrderCoding.builder()
                .codingType(PurchaseOrderCodingType.RECHARGE)
                .recoupmentPct(new BigDecimal("100.00"))
                .recoupableAccount("")
                .recoupableCustomerAccount("")
                .recoupableAssignment(position.getProject().getProjectName())
                .recoupableText(calculateRechargeSapText(position))
                .recoupableReferenceKey2(PurchaseOrderUtil.createPositionReferenceString(position))
                .recoupableReferenceKey3(position.getProject().getSapClientCode())
                .recoupableAmount("100%")
                .build();
    }

    private String calculateVariableSapText(PurchaseOrderPosition position, int availableCharacters) {
        final String vendorShortName = getVendorShortName(position.getPurchaseOrder().getVendor());
        final String prefix = vendorShortName + TEXT_DELIMITER;
        int availableDescriptionLength = availableCharacters - prefix.length();
        String shortDescription = getShortDescription(position.getDescription(), availableDescriptionLength);
        return prefix + shortDescription;
    }

    private String calculateRechargeSapText(PurchaseOrderPosition position) {
        final String budgetSpendTypeCode = position.getSpendType().getBudgetSpendTypeCode();
        final String countryCode = position.getPoCountry().getCountryCode();
        final String prefix = budgetSpendTypeCode + countryCode + TEXT_DELIMITER;
        int availableVariableTextLength = SAP_TEXT_MAX_CHARACTERS - prefix.length();
        return prefix + this.calculateVariableSapText(position, availableVariableTextLength);
    }

    private String getCostCenter(PurchaseOrderPosition position) {
        String postfix = ION_SAP_CAT.equals(position.getProject().getIonSap()) ? CAT_COSTCTR_POSTFIX : NON_CAT_COSTCTR_POSTFIX;
        return position.getProject().getRepertoireOwnerCompany().getSapCompanyCode() + postfix;
    }

    private String getOrderType(PurchaseOrderPosition position) {
        return ION_SAP_CAT.equals(position.getProject().getIonSap()) ? ORDERTYPE_MAST : ORDERTYPE_NEWR;
    }

    private String getShortDescription(String description, int availableDescriptionLength) {
        if (availableDescriptionLength < MIN_DESCRIPTION_CHARACTERS) {
            log.warn("Could not use needed {} characters of description for coding rule and will use shorter description. Available charaters: {}, Original Description: {}", MIN_DESCRIPTION_CHARACTERS, availableDescriptionLength, description);
        }
        return availableDescriptionLength > description.length() ? description : description.substring(0, availableDescriptionLength);
    }

    private String getVendorShortName(Vendor vendor) {
        return vendor.getName1().length() > 25 ? vendor.getName1().substring(0, 25) : vendor.getName1();
    }

    private boolean isTraditionalModel(PurchaseOrderPosition position) {
        return Project.DEAL_TYPE_TRADITIONAL_MODEL.equals(position.getProject().getDealType());

    }

    private boolean isRecharge(PurchaseOrderPosition position) {
        return !position.getPurchaseOrder().getCountry().equals(position.getProject().getRepertoireOwnerCompany().getCountry());
    }
}
