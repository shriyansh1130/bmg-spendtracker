package de.syndicats.spendtracker.purchaseorder.entity.projection;

import de.syndicats.spendtracker.country.entity.projection.CountryProjection;
import de.syndicats.spendtracker.currency.entity.projection.CurrencyProjection;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrder;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderStatus;
import de.syndicats.spendtracker.vendor.entity.projection.VendorProjection;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;


@Projection(name = "purchaseOrderProjection", types = {PurchaseOrder.class})
public interface PurchaseOrderProjection {

    String getPurchaseOrderNumber();

    PurchaseOrderStatus getStatus();

    CountryProjection getCountry();

    CurrencyProjection getCurrency();

    VendorProjection getVendor();

    List<PurchaseOrderPositionProjection> getPositions();
}
