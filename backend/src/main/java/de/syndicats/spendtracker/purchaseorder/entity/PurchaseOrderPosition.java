package de.syndicats.spendtracker.purchaseorder.entity;

import de.syndicats.spendtracker.application.entity.AbstractBaseModel;
import de.syndicats.spendtracker.budget.entity.BudgetSpendType;
import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.posting.entity.Posting;
import de.syndicats.spendtracker.project.entity.Project;
import lombok.*;
import org.hibernate.annotations.Formula;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Table(name = "purchase_order_position")
@Entity
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@Builder
@Data
@EqualsAndHashCode(callSuper = true, exclude = "purchaseOrder")
@ToString
public class PurchaseOrderPosition extends AbstractBaseModel {
    @ManyToOne
    @JoinColumn(name = "po_country_id", referencedColumnName = "id")
    private Country poCountry;

    @ManyToOne
    @JoinColumn(name = "budget_spend_type_id", referencedColumnName = "id")
    @NonNull
    private BudgetSpendType spendType;

    @Column(name = "price")
    @NumberFormat(pattern = "#000000000.00")
    @DecimalMin("0.01")
    @DecimalMax("999999999.999")
    @Digits(integer = 9, fraction = 3, message = "{javax.validation.constraints.Digits.message}")
    private BigDecimal price;

    @Column(name = "quantity")
    @DecimalMin("1")
    @DecimalMax("999999")
    private BigDecimal quantity;

    @Column(name = "description")
    @NonNull
    private String description;

    @Column(name = "service_month", nullable = false)
    @NonNull
    private LocalDateTime serviceMonth;

    @Lob
    @Column(name = "notes")
    private String notes;

    @ManyToOne
    @JoinColumn(name = "project_id", referencedColumnName = "id", nullable = false)
    @NonNull
    private Project project;

    @Column(name = "positions_order")
    private int orderNumber;

    @ManyToOne
    @JoinColumn(name = "purchase_order_id")
    private PurchaseOrder purchaseOrder;

    @Formula("quantity * price")
    private BigDecimal total;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "purchaseOrderPosition")
    @Builder.Default
    private List<PurchaseOrderPositionAttachment> attachments = new ArrayList<>();

    @OneToMany(mappedBy = "purchaseOrderPosition", fetch = FetchType.EAGER)
    @Builder.Default
    private List<Posting> postings = new ArrayList<>();

    @PrePersist
    public void onPrePersist() {
        if (this.getPoCountry() == null && this.getPurchaseOrder() != null && this.getPurchaseOrder().getCountry() != null) {
            this.setPoCountry(this.getPurchaseOrder().getCountry());
        }
    }

    @Transient
    public String getServiceMonthString() {
        DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .parseCaseInsensitive()
                .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
                .appendPattern("MMM-yyyy").toFormatter(Locale.ENGLISH);
        return formatter.format(this.serviceMonth);
    }

    @Transient
    public BigDecimal getOpenPo() {
        return calculateOpenPo(getPaid());
    }

    @Transient
    public BigDecimal getVariance() {
        return calculateVariance(getPaid());
    }

    @Transient
    public BigDecimal getPaid() {
        return this.postings.stream().map(Posting::getAmountInTargetCurrency).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
    }

    @Transient
    public BigDecimal calculateOpenPo(BigDecimal invoiced) {
        BigDecimal variance = calculateVariance(invoiced);
        return !this.getPurchaseOrder()
                .getStatus()
                .equals(PurchaseOrderStatus.Open) ||
                isOverCompanyThreshold(invoiced) ||
                variance.compareTo(BigDecimal.ZERO) < 0  ? new  BigDecimal("0.00") : variance;
    }

    private boolean isOverCompanyThreshold(BigDecimal invoiced) {
        //TODO: use threshold on company as soon as it is stored there. For now: constant value
        BigDecimal COMPANY_THRESHOLD = new BigDecimal("0.95");
        return this.calculateTotal().multiply(COMPANY_THRESHOLD).compareTo(invoiced) < 0;
    }

    @Transient
    public BigDecimal calculateVariance(BigDecimal invoiced) {
        return this.calculateTotal().subtract(invoiced);
    }

    @Transient
    public BigDecimal calculateTotal() {
        // Fallback calculation if @Formula Annotation is not working
        return this.getTotal() != null ? this.getTotal() : this.getPrice().multiply(this.getQuantity());
    }
}
