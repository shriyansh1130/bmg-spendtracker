package de.syndicats.spendtracker.purchaseorder.validators;

import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrder;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component("beforeSavePurchaseOrderValidator")
public class SavePurchaseOrderValidator implements Validator{


    @Override
    public boolean supports(Class<?> clazz) {
        return PurchaseOrder.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        PurchaseOrder po = (PurchaseOrder) target;
        if (PurchaseOrderStatus.Closed.equals(po.getStatus())) {
            errors.reject("purchaseorder.save.error.status");
        }
    }
}
