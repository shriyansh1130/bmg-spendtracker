package de.syndicats.spendtracker.purchaseorder.repository;

import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrder;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@JaversSpringDataAuditable
@RepositoryRestResource(collectionResourceRel = "purchase-orders", path = "purchase-orders")
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, Long> {
    Optional<PurchaseOrder> findByPurchaseOrderNumber(String poNumber);
}
