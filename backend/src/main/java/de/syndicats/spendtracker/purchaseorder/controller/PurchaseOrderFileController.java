package de.syndicats.spendtracker.purchaseorder.controller;

import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPositionAttachment;
import de.syndicats.spendtracker.purchaseorder.service.PurchaseOrderPositionAttachmentService;
import de.syndicats.spendtracker.purchaseorder.service.PurchaseOrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.transaction.Transactional;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Controller
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("/api")
@Slf4j
public class PurchaseOrderFileController {

    @Autowired
    private PurchaseOrderService purchaseOrderService;


    @Autowired
    private PurchaseOrderPositionAttachmentService purchaseOrderPositionAttachmentService;

    @GetMapping(value = "orders/excel")
    @ResponseBody
    public ResponseEntity<InputStreamResource> excelPoOrderExport(@RequestParam("purchaseOrderId") List<Long> purchaseOrderIds) throws Exception {
        File file = purchaseOrderService.excelPoOrderExport(purchaseOrderIds).get();
        InputStream is = new FileInputStream(file);
        InputStreamResource resource = new InputStreamResource(is);
        String currentDateTime = new SimpleDateFormat("yyyy-MM-dd_HH-mm").format(new Date());
        String fileName = String.format("spend-tracker-all-pos-%s.xls", currentDateTime);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentLength(file.length());
        headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);

        return new ResponseEntity<>(resource, headers, HttpStatus.OK);
    }

    @GetMapping("/purchase-order-position-attachments/download")
    @ResponseBody
    @Transactional
    public ResponseEntity<?> uploadFile(@RequestParam("uuid") UUID uuid) throws IOException {
        Optional<PurchaseOrderPositionAttachment> optionalAttachment = purchaseOrderPositionAttachmentService.load(uuid);
        if (!optionalAttachment.isPresent()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        PurchaseOrderPositionAttachment attachment = optionalAttachment.get();
        log.debug("Image served: {}", uuid);
        byte[] blob = attachment.getBlob();
        String contentType = attachment.getContentType();
        Resource resource = new ByteArrayResource(blob, contentType);

        return ResponseEntity.ok()
                .contentType(MediaType.valueOf(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + attachment.getFilename())
                .body(resource);
    }

}
