package de.syndicats.spendtracker.purchaseorder.controller.dto;

import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderStatus;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class PurchaseOrderDto {
    private String id;
    private String purchaseOrderNumber;
    private PurchaseOrderStatus status;
    private String countryId;
    private String vendorId;
    private String currencyId;
    private List<PurchaseOrderPositionDto> positions;

}
