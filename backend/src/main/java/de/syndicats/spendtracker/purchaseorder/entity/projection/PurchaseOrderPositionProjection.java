package de.syndicats.spendtracker.purchaseorder.entity.projection;

import de.syndicats.spendtracker.budget.entity.projection.BudgetSpendTypeProjection;
import de.syndicats.spendtracker.country.entity.projection.CountryProjection;
import de.syndicats.spendtracker.project.entity.projection.ProjectProjection;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.math.BigDecimal;
import java.util.List;

@Projection(name = "purchaseOrderPositionProjection", types = {PurchaseOrderPosition.class})
interface PurchaseOrderPositionProjection {

    BudgetSpendTypeProjection getSpendType();

    CountryProjection getPoCountry();

    BigDecimal getPrice();

    BigDecimal getQuantity();

    String getDescription();

    @Value("#{target.serviceMonthString}")
    String getServiceMonth();

    ProjectProjection getProject();

    BigDecimal getTotal();

    List<PurchaseOrderPositionAttachmentProjection> getAttachments();

    @Value("#{target.postings.size() == 0 && target.orderNumber != 0 }")
    boolean isDeletable();

    String getNotes();
}
