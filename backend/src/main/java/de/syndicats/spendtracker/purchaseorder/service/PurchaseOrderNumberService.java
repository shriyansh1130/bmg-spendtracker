package de.syndicats.spendtracker.purchaseorder.service;

import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderNumber;
import de.syndicats.spendtracker.purchaseorder.repository.PurchaseOrderNumberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class PurchaseOrderNumberService {

    public static final String PRURCHASE_ORDER_NUMER_FORMAT = "%s%d%05d";

    private final PurchaseOrderNumberRepository purchaseOrderNumberRepository;

    @Autowired
    PurchaseOrderNumberService(PurchaseOrderNumberRepository purchaseOrderNumberRepository) {
        this.purchaseOrderNumberRepository = purchaseOrderNumberRepository;
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public String createNewPurchaseOrderNumber(String countryCode, Integer yearOfCentury) {
        Optional<PurchaseOrderNumber> result = purchaseOrderNumberRepository.findById(
                PurchaseOrderNumber.Id.builder().countryCode(countryCode).yearOfCentury(yearOfCentury).build());

        final PurchaseOrderNumber purchaseOrderNumber;
        final Long nextSequentialPurchaseOrderNumber;
        if (result.isPresent()) {
            purchaseOrderNumber = result.get();
            nextSequentialPurchaseOrderNumber = Long
                    .valueOf(purchaseOrderNumber.getSequentialNumberCounter().longValue() + 1);
        } else {
            purchaseOrderNumber = PurchaseOrderNumber.builder()
                    .id(PurchaseOrderNumber.Id.builder().countryCode(countryCode).yearOfCentury(yearOfCentury).build())
                    .build();
            nextSequentialPurchaseOrderNumber = Long.valueOf(0);
        }

        purchaseOrderNumber.setSequentialNumberCounter(nextSequentialPurchaseOrderNumber);
        purchaseOrderNumberRepository.save(purchaseOrderNumber);

        return String.format(PRURCHASE_ORDER_NUMER_FORMAT, countryCode, yearOfCentury,
                nextSequentialPurchaseOrderNumber);
    }
}
