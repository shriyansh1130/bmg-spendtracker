package de.syndicats.spendtracker.purchaseorder.entity;

import de.syndicats.spendtracker.application.entity.AbstractBaseModel;
import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.currency.entity.Currency;
import de.syndicats.spendtracker.vendor.entity.Vendor;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "purchase_order")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public class PurchaseOrder extends AbstractBaseModel {

    @Column(name = "purchase_order_number")
    private String purchaseOrderNumber;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    @NonNull
    private PurchaseOrderStatus status;

    @ManyToOne
    @JoinColumn(name = "country_id", referencedColumnName = "id", nullable = false)
    @NonNull
    private Country country;

    @ManyToOne
    @JoinColumn(name = "currency_id", referencedColumnName = "id", nullable = false)
    @NonNull
    private Currency currency;

    @ManyToOne
    @JoinColumn(name = "vendor_id", referencedColumnName = "id", nullable = false)
    @NonNull
    private Vendor vendor;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "purchase_order_id")
    @OrderColumn()
    @Builder.Default
    private List<PurchaseOrderPosition> positions = new ArrayList<>();

}
