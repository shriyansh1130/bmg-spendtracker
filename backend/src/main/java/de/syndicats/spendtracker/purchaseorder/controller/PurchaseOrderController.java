package de.syndicats.spendtracker.purchaseorder.controller;

import de.syndicats.spendtracker.purchaseorder.controller.dto.PurchaseOrderDto;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrder;
import de.syndicats.spendtracker.purchaseorder.service.PurchaseOrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@RepositoryRestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("/api")
public class PurchaseOrderController {

    @Autowired
    private PurchaseOrderService purchaseOrderService;

    @PostMapping(value = "orders/upsert")
    @ResponseBody
    public ResponseEntity<PersistentEntityResource> updateOrInsertPurchaseOrder(@RequestBody PurchaseOrderDto purchaseOrderToSave, PersistentEntityResourceAssembler persistentEntityResourceAssembler) {
        PurchaseOrder po = this.purchaseOrderService.upsert(purchaseOrderToSave);
        return ResponseEntity.ok(persistentEntityResourceAssembler.toFullResource(po));
    }


}
