package de.syndicats.spendtracker.purchaseorder.repository;

import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPositionAttachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "purchase-order-position-attachments", path = "purchase-order-position-attachments")
public interface PurchaseOrderPositionAttachmentRepository extends JpaRepository<PurchaseOrderPositionAttachment, Long> {

    List<PurchaseOrderPositionAttachment> findAllByPurchaseOrderPosition_Id(@Param("purchaseOrderPositionId") Long purchaseOrderPositionId);

    Optional<PurchaseOrderPositionAttachment> findByUuid(@Param("uuid")String uuid);

    List<PurchaseOrderPositionAttachment> findManyByUuidIsIn(@Param("uuids") List<String> uuids);
}
