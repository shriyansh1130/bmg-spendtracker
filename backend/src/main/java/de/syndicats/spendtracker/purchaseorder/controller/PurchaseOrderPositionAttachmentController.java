package de.syndicats.spendtracker.purchaseorder.controller;

import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPositionAttachment;
import de.syndicats.spendtracker.purchaseorder.repository.PurchaseOrderPositionAttachmentRepository;
import de.syndicats.spendtracker.purchaseorder.repository.PurchaseOrderPositionRepository;
import de.syndicats.spendtracker.purchaseorder.service.PurchaseOrderPositionAttachmentService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

@RepositoryRestController
@RequestMapping("/api")
@Slf4j
public class PurchaseOrderPositionAttachmentController {


    @Autowired
    private PurchaseOrderPositionRepository purchaseOrderPositionRepository;

    @Autowired
    private PurchaseOrderPositionAttachmentService purchaseOrderPositionAttachmentService;

    @PostMapping("/purchase-order-positions/{id}/attachments")
    @ResponseBody
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED)
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile uploadfile,
                                        @PathVariable("id") Long poPositionId,
                                        PersistentEntityResourceAssembler persistentEntityResourceAssembler) throws IOException {
        log.debug("Uploading file... {}", uploadfile.getName());
        Optional<PurchaseOrderPosition> position = purchaseOrderPositionRepository.findById(poPositionId);

        if (!position.isPresent()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        if (uploadfile.isEmpty()) {
            return new ResponseEntity("please select a file!", HttpStatus.OK);
        }

        PurchaseOrderPositionAttachment attachment = this.purchaseOrderPositionAttachmentService.store(uploadfile, position.get());
        return new ResponseEntity<>(persistentEntityResourceAssembler.toResource(attachment), HttpStatus.CREATED);
    }

    @PostMapping("/purchase-order-positions/unassigned/attachments")
    @ResponseBody
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED)
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile uploadfile,
                                        PersistentEntityResourceAssembler persistentEntityResourceAssembler) throws IOException {
        log.debug("Uploading file... {}", uploadfile.getOriginalFilename());

        if (uploadfile.isEmpty()) {
            return new ResponseEntity("please select a file!", HttpStatus.OK);
        }

        PurchaseOrderPositionAttachment attachment = this.purchaseOrderPositionAttachmentService.store(uploadfile);
        return new ResponseEntity<>(persistentEntityResourceAssembler.toResource(attachment), HttpStatus.CREATED);
    }

}
