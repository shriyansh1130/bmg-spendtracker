package de.syndicats.spendtracker.purchaseorder.entity;

public enum PurchaseOrderCodingType {
    RECHARGE, ZERO_RECOUPABILITY, MIXED_RECOUPABILITY, FULL_RECOUPABILITY
}
