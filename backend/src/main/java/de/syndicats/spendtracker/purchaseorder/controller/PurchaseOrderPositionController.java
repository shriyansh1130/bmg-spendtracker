package de.syndicats.spendtracker.purchaseorder.controller;


import de.syndicats.spendtracker.posting.services.PostingService;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderCoding;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import de.syndicats.spendtracker.purchaseorder.entity.projection.PurchaseOrderPositionSummaryProjection;
import de.syndicats.spendtracker.purchaseorder.repository.PurchaseOrderPositionRepository;
import de.syndicats.spendtracker.purchaseorder.service.PurchaseOrderPositionService;
import de.syndicats.spendtracker.purchaseorder.service.SapCodingRulesService;
import de.syndicats.spendtracker.user.entity.User;
import de.syndicats.spendtracker.user.repository.UserRepository;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/purchase-order-positions")
public class PurchaseOrderPositionController {

    @Autowired
    private PurchaseOrderPositionRepository purchaseOrderPositionRepository;

    @Autowired
    private PurchaseOrderPositionService purchaseOrderPositionService;

    @Autowired
    private PostingService postingService;

    @Autowired
    private ProjectionFactory projectionFactory;

    @Autowired
    private PagedResourcesAssembler<PurchaseOrderPositionSummaryProjection> resourceAssembler;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SapCodingRulesService sapCodingRulesService;

    @Autowired
    private EntityManager em;

    @GetMapping("/summary")
    public PagedResources<Resource<PurchaseOrderPositionSummaryProjection>> getSummary(
            @RequestParam("status") String status,
            @RequestParam("spendType") String spendType,
            @RequestParam("createdBy") String createdBy,
            @RequestParam("poNumber") String poNumber,
            @RequestParam("countryName") String countryName,
            @RequestParam("vendorName") String vendorName,
            @RequestParam("projectName") String projectName,
            Pageable page)  {

        Page<PurchaseOrderPosition> positions = purchaseOrderPositionRepository.findPage(
                status, spendType, createdBy, poNumber, projectName, countryName, vendorName, page);


        return resourceAssembler.toResource(positions.map(this::parsePurchaseOrderPositionToProjection));

    }

    private PurchaseOrderPositionSummaryProjection parsePurchaseOrderPositionToProjection(PurchaseOrderPosition pos) {
        Hibernate.initialize(pos.getPostings());
        em.detach(pos);
        Optional<User> optionalUser = userRepository.findByUserId(pos.getCreatedBy());
        optionalUser.ifPresent(user -> pos.setCreatedBy(user.getFullname()));
        return this.projectionFactory.createProjection(PurchaseOrderPositionSummaryProjection.class, pos);
    }

    @GetMapping("/{id}/warnings")
    @ResponseBody
    public List<String> getWarnings(@PathVariable("id") Long poPositionId)  {
        Optional<PurchaseOrderPosition> optionalPurchaseOrderPosition = purchaseOrderPositionRepository.findById(poPositionId);
        PurchaseOrderPosition position = optionalPurchaseOrderPosition.orElseThrow(ResourceNotFoundException::new);
        return this.postingService.getErrorsForPurchaseOrderPosition(position);
    }

    @GetMapping("/{id}/recoupment-percentage")
    @ResponseBody
    public BigDecimal getRecoupmentPercentage(@PathVariable("id") Long poPositionId) {
        Optional<PurchaseOrderPosition> optionalPurchaseOrderPosition = purchaseOrderPositionRepository.findById(poPositionId);
        PurchaseOrderPosition position = optionalPurchaseOrderPosition.orElseThrow(ResourceNotFoundException::new);
        return purchaseOrderPositionService.getRecoupmentPct(position);
    }

    @GetMapping("/{id}/coding-rules")
    @ResponseBody
    public PurchaseOrderCoding getCodingRules(@PathVariable("id") Long poPositionId) {
        Optional<PurchaseOrderPosition> optionalPurchaseOrderPosition = purchaseOrderPositionRepository.findById(poPositionId);
        PurchaseOrderPosition position = optionalPurchaseOrderPosition.orElseThrow(ResourceNotFoundException::new);
        return sapCodingRulesService.createCoding(position);
    }

}
