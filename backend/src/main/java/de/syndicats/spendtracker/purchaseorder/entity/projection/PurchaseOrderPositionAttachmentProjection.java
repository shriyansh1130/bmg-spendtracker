package de.syndicats.spendtracker.purchaseorder.entity.projection;

import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPositionAttachment;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "purchaseOrderPositionAttachmentProjection", types = {PurchaseOrderPositionAttachment.class})
interface PurchaseOrderPositionAttachmentProjection {
   String getFilename();
   Integer getSize();
   String getContentType();
   String getUuid();
}
