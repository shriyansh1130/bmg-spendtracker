package de.syndicats.spendtracker.purchaseorder.entity;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class PurchaseOrderCoding {
    private PurchaseOrderCodingType codingType;
    private BigDecimal recoupmentPct;
    private String recoupableAccount;
    private String recoupableCustomerAccount;
    private String recoupableAssignment;
    private String recoupableText;
    private String recoupableReferenceKey2;
    private String recoupableReferenceKey3;
    private String recoupableAmount;
    private String nonRecoupableAccount;
    private String nonRecoupableAssignment;
    private String nonRecoupableOrder;
    private String nonRecoupableReferenceKey2;
    private String nonRecoupableReferenceKey3;
    private String nonRecoupableCostCenter;
    private String nonRecoupableOrderType;
    private String nonRecoupableText;
    private String nonRecoupableAmount;
}
