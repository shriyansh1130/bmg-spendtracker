package de.syndicats.spendtracker.purchaseorder.repository;

import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderNumber;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PurchaseOrderNumberRepository extends JpaRepository<PurchaseOrderNumber, PurchaseOrderNumber.Id> {
}
