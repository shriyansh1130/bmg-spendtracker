package de.syndicats.spendtracker.purchaseorder.processor;

import de.syndicats.spendtracker.purchaseorder.controller.PurchaseOrderController;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.webmvc.RepositoryLinksResource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Component;

@Component
public class PurchaseOrderResourceProcessor {
    @Bean
    public ResourceProcessor<RepositoryLinksResource> purchaseOrderProcessor() {
        return new ResourceProcessor<RepositoryLinksResource>( ) {
            @Override
            public RepositoryLinksResource process(RepositoryLinksResource resource) {
                resource.add(ControllerLinkBuilder.linkTo(PurchaseOrderController.class).slash("orders").slash("upsert").withRel("orders/upsert"));
                return resource;
            }
        };

    }
}
