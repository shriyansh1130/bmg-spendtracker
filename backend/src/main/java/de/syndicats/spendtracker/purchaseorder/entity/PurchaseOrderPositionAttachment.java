package de.syndicats.spendtracker.purchaseorder.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.syndicats.spendtracker.application.entity.AbstractBaseModel;
import lombok.*;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Blob;

@Table(name = "purchase_order_position_attachment")
@Entity
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@Builder
@Data
@EqualsAndHashCode
@ToString
public class PurchaseOrderPositionAttachment extends AbstractBaseModel {

    @ManyToOne
    private PurchaseOrderPosition purchaseOrderPosition;

    @Column
    @NonNull
    private String filename;

    @Column(unique = true)
    @NonNull
    private String uuid;

    @Column
    @Lob
    @JsonIgnore
    private byte[] blob;

    @Column
    private int size;

    @Column
    private String contentType;


}
