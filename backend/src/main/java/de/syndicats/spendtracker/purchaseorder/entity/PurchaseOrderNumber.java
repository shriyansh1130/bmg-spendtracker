package de.syndicats.spendtracker.purchaseorder.entity;

import lombok.*;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Table(name = "purchase_order_number",
        uniqueConstraints = @UniqueConstraint(columnNames = { "country_code", "year_of_century", "sequential_number" }))
@Entity
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@Builder
@Data
@EqualsAndHashCode
@ToString
public class PurchaseOrderNumber {

    @Embeddable
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    @AllArgsConstructor
    @Builder
    @Data
    @EqualsAndHashCode
    @ToString
    public static class Id implements Serializable {

        private static final long serialVersionUID = -9135580491804758865L;

        @Column(name = "country_code", nullable = false, length = 2)
        @Size(max = 2)
        @NonNull
        private String countryCode;

        @Column(name = "year_of_century", nullable = false)
        @NumberFormat(pattern = "#00")
        @Min(0)
        @Max(99)
        @NonNull
        private Integer yearOfCentury;
    }

    @EmbeddedId
    private PurchaseOrderNumber.Id id;

    @Column(name = "sequential_number", nullable = false)
    @NumberFormat(pattern = "#00000")
    @Min(0)
    @Max(99999)
    @Builder.Default
    private Long sequentialNumberCounter = 0L;
}
