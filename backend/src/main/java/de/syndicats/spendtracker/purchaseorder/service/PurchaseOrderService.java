package de.syndicats.spendtracker.purchaseorder.service;

import de.syndicats.spendtracker.application.entity.AbstractBaseModel;
import de.syndicats.spendtracker.budget.entity.BudgetSpendType;
import de.syndicats.spendtracker.budget.repository.BudgetSpendTypeRepository;
import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.country.repository.CountryRepository;
import de.syndicats.spendtracker.currency.entity.Currency;
import de.syndicats.spendtracker.currency.repository.CurrencyRepository;
import de.syndicats.spendtracker.posting.services.PostingService;
import de.syndicats.spendtracker.project.entity.Project;
import de.syndicats.spendtracker.project.repository.ProjectRepository;
import de.syndicats.spendtracker.purchaseorder.controller.dto.PurchaseOrderDto;
import de.syndicats.spendtracker.purchaseorder.controller.dto.PurchaseOrderPositionDto;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrder;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPositionAttachment;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderStatus;
import de.syndicats.spendtracker.purchaseorder.repository.PurchaseOrderPositionRepository;
import de.syndicats.spendtracker.purchaseorder.repository.PurchaseOrderRepository;
import de.syndicats.spendtracker.user.repository.UserRepository;
import de.syndicats.spendtracker.vendor.entity.Vendor;
import de.syndicats.spendtracker.vendor.repository.VendorRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.LocalDate;
import org.springframework.beans.BeanUtils;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class PurchaseOrderService {

    private PurchaseOrderRepository purchaseOrderRepository;
    private CountryRepository countryRepository;
    private CurrencyRepository currencyRepository;
    private VendorRepository vendorRepository;
    private ProjectRepository projectRepository;
    private BudgetSpendTypeRepository budgetSpendTypeRepository;
    private PurchaseOrderPositionRepository purchaseOrderPositionRepository;
    private PurchaseOrderNumberService purchaseOrderNumberService;
    private UserRepository userRepository;
    private PurchaseOrderPositionAttachmentService purchaseOrderPositionAttachmentService;
    private PostingService postingService;
    private SapCodingRulesService codingRulesService;

    @Transactional
    public CompletableFuture<File> excelPoOrderExport(List<Long> purchaseOrderIds) throws Exception {

        List<PurchaseOrderPosition> purchaseOrderPositions = purchaseOrderPositionRepository.findByIdIn(purchaseOrderIds);

        ExcelGenerator.PurchaseOrderExcelContext poContext = ExcelGenerator.PurchaseOrderExcelContext.builder()
                .purchaseOrderPositions(purchaseOrderPositions)
                .sheetName("All POs")
                .userRepository(userRepository)
                .targetFile(File.createTempFile("ExcelExport_PurchaseOrderPositions_", ".xls"))
                .sapCodingRulesService(codingRulesService)
                .build();

        return ExcelGenerator.createExcel(poContext);
    }

    @Transactional
    public PurchaseOrder upsert(PurchaseOrderDto purchaseOrderToSave) {
        if (purchaseOrderToSave.getId() == null) {
            return this.createNewPurchaseOrder(purchaseOrderToSave);
        } else {
            Optional<PurchaseOrder> optionalPurchaseOrder = this.purchaseOrderRepository.findById(Long.valueOf(purchaseOrderToSave.getId()));
            if (optionalPurchaseOrder.isPresent()) {
                PurchaseOrder purchaseOrder = optionalPurchaseOrder.get();
                if (!isChangeValid(purchaseOrder, purchaseOrderToSave)) {
                    log.error("Invalid change detected. Return unmodified. Current-ID: {}, Change: {}", purchaseOrder.getId(), purchaseOrderToSave);
                    return purchaseOrder;
                }
                this.updatePurchaseOrder(purchaseOrder, purchaseOrderToSave);
                return purchaseOrder;
            } else {
                throw new EntityNotFoundException();
            }
        }
    }

    private boolean isChangeValid(PurchaseOrder existing, PurchaseOrderDto changed) {
        if (PurchaseOrderStatus.Closed.equals(existing.getStatus())) {
            log.error("Tried to update closed purchase order.");
            return false;
        }

        if (!PurchaseOrderStatus.Draft.equals(existing.getStatus()) && !Long.valueOf(changed.getCurrencyId()).equals(existing.getCurrency().getId())) {
            log.error("Tried to change currency on non-draft purchase order.");
            return false;
        }

        return true;
    }

    private PurchaseOrder updatePurchaseOrder(PurchaseOrder purchaseOrder, PurchaseOrderDto purchaseOrderToSave) {
        BeanUtils.copyProperties(purchaseOrderToSave, purchaseOrder, "country", "currency", "vendor", "id", "positions");
        updateCountry(purchaseOrder, purchaseOrderToSave);
        updateCurrency(purchaseOrder, purchaseOrderToSave);
        updateVendor(purchaseOrder, purchaseOrderToSave);
        deleteRemovedPositions(purchaseOrder.getPositions(), purchaseOrderToSave.getPositions());
        List<PurchaseOrderPosition> positionsToSave = purchaseOrderToSave.getPositions().stream().map(this::upsertPosition).collect(Collectors.toList());
        purchaseOrder.getPositions().clear();
        purchaseOrder.getPositions().addAll(positionsToSave);
        purchaseOrder.setPurchaseOrderNumber(getPurchaseOrderNumber(purchaseOrder.getCountry(), purchaseOrderToSave));
        return purchaseOrder;
    }

    private String getPurchaseOrderNumber(Country country, PurchaseOrderDto purchaseOrderToSave) {
        if (purchaseOrderToSave.getStatus() == PurchaseOrderStatus.Draft || !"DRAFT".equals(purchaseOrderToSave.getPurchaseOrderNumber())) {
            return purchaseOrderToSave.getPurchaseOrderNumber();
        }

        int year = LocalDate.now().getYearOfCentury();

        return purchaseOrderNumberService.createNewPurchaseOrderNumber(country.getCountryCode(), Integer.valueOf(year));
    }

    private void deleteRemovedPositions(List<PurchaseOrderPosition> positions, List<PurchaseOrderPositionDto> positionsToSave) {
        Set<PurchaseOrderPosition> toDelete = positions.stream()
                .filter(position -> positionsToSave.stream()
                        .noneMatch(dto -> dto.getId() != null && Long.valueOf(dto.getId()).equals(position.getId())))
                .collect(Collectors.toSet());

        purchaseOrderPositionRepository.deleteAll(toDelete);

    }

    private void updateVendor(PurchaseOrder purchaseOrder, PurchaseOrderDto purchaseOrderToSave) {
        Optional<Vendor> newVendor = this.getUpdateEntityRelation(purchaseOrder.getVendor(), purchaseOrderToSave.getVendorId(), vendorRepository);
        newVendor.ifPresent(purchaseOrder::setVendor);
    }

    private void updateCountry(PurchaseOrder purchaseOrder, PurchaseOrderDto purchaseOrderToSave) {
        Optional<Country> newCountry = this.getUpdateEntityRelation(purchaseOrder.getCountry(), purchaseOrderToSave.getCountryId(), countryRepository);
        newCountry.ifPresent(purchaseOrder::setCountry);
    }

    private void updateCurrency(PurchaseOrder purchaseOrder, PurchaseOrderDto purchaseOrderToSave) {
        Optional<Currency> newCurrency = this.getUpdateEntityRelation(purchaseOrder.getCurrency(), purchaseOrderToSave.getCurrencyId(), currencyRepository);
        newCurrency.ifPresent(purchaseOrder::setCurrency);
    }

    private <T extends AbstractBaseModel> Optional<T> getUpdateEntityRelation(AbstractBaseModel currentRelationEntity, String newRelationEntityId, JpaRepository<T, Long> repository) {
        Long newId = Long.valueOf(newRelationEntityId);
        if (needsUpdate(currentRelationEntity, newId)) {
            return repository.findById(newId);
        }
        return Optional.empty();
    }

    private boolean needsUpdate(AbstractBaseModel relationModel, Long newID) {
        return relationModel == null || newID != null && !relationModel.getId().equals(newID);

    }

    private PurchaseOrderPosition upsertPosition(PurchaseOrderPositionDto positionToSave) {
        if (positionToSave.getId() == null) {
            return this.createNewPurchaseOrderPosition(positionToSave);
        } else {
            return this.updatePurchaseOrderPosition(positionToSave);
        }
    }

    private PurchaseOrderPosition updatePurchaseOrderPosition(PurchaseOrderPositionDto positionToSave) {
        PurchaseOrderPosition position = purchaseOrderPositionRepository.getOne(Long.valueOf(positionToSave.getId()));
        BeanUtils.copyProperties(positionToSave, position, "id", "project", "spendType", "attachments", "serviceMonth");
        position.setServiceMonth(positionToSave.deriveServiceMonthDateTime());
        updateProject(position, positionToSave);
        updateSpendType(position, positionToSave);
        updatePoCountry(position, positionToSave);
        //attachments are already saved for existing positions
        return position;
    }

    private void updateSpendType(PurchaseOrderPosition position, PurchaseOrderPositionDto positionToSave) {
        Optional<BudgetSpendType> newSpendType = this.getUpdateEntityRelation(position.getSpendType(), positionToSave.getSpendTypeId(), budgetSpendTypeRepository);
        newSpendType.ifPresent(position::setSpendType);
    }

    private void updateProject(PurchaseOrderPosition position, PurchaseOrderPositionDto positionToSave) {
        Optional<Project> newProject = this.getUpdateEntityRelation(position.getProject(), positionToSave.getProjectId(), projectRepository);
        newProject.ifPresent(position::setProject);
    }

    private void updatePoCountry(PurchaseOrderPosition position, PurchaseOrderPositionDto positionToSave) {
        Optional<Country> newCountry = this.getUpdateEntityRelation(position.getPoCountry(), positionToSave.getPoCountryId(), countryRepository);
        newCountry.ifPresent(position::setPoCountry);
    }

    private PurchaseOrderPosition createNewPurchaseOrderPosition(PurchaseOrderPositionDto positionToSave) {
        PurchaseOrderPosition newPosition = PurchaseOrderPosition.builder()
                .description(positionToSave.getDescription())
                .project(projectRepository.getOne(Long.valueOf(positionToSave.getProjectId())))
                .serviceMonth(positionToSave.deriveServiceMonthDateTime())
                .spendType(budgetSpendTypeRepository.getOne(Long.valueOf(positionToSave.getSpendTypeId())))
                .poCountry(countryRepository.getOne(Long.valueOf(positionToSave.getPoCountryId())))
                .price(positionToSave.getPrice())
                .quantity(positionToSave.getQuantity())
                .notes(positionToSave.getNotes())
                .build();

        return newPosition;
    }


    private PurchaseOrder createNewPurchaseOrder(PurchaseOrderDto purchaseOrderToSave) {


        PurchaseOrder po = PurchaseOrder.builder()
                .status(purchaseOrderToSave.getStatus())
                .country(countryRepository.getOne(Long.valueOf(purchaseOrderToSave.getCountryId())))
                .currency(currencyRepository.getOne(Long.valueOf(purchaseOrderToSave.getCurrencyId())))
                .vendor(vendorRepository.getOne(Long.valueOf(purchaseOrderToSave.getVendorId())))
                .positions(purchaseOrderToSave.getPositions().stream().map(this::upsertPosition).collect(Collectors.toList()))
                .build();

        po.setPurchaseOrderNumber(getPurchaseOrderNumber(po.getCountry(), purchaseOrderToSave));
        PurchaseOrder persistentPo = this.purchaseOrderRepository.save(po);
        this.addAttachments(purchaseOrderToSave, persistentPo);

        return persistentPo;
    }

    private void addAttachments(PurchaseOrderDto purchaseOrderToSave, PurchaseOrder persistentPo) {
        for (int i = 0; i < persistentPo.getPositions().size(); i++) {
            PurchaseOrderPosition pos = persistentPo.getPositions().get(i);
            List <String> attachmentUuids = purchaseOrderToSave.getPositions().get(i).getAttachments();
            List<PurchaseOrderPositionAttachment> attachments = purchaseOrderPositionAttachmentService.loadMultiple(attachmentUuids);
            attachments.forEach(purchaseOrderPositionAttachment -> purchaseOrderPositionAttachment.setPurchaseOrderPosition(pos));
        }
    }
}
