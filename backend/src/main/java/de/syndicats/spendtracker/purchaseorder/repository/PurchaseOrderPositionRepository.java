package de.syndicats.spendtracker.purchaseorder.repository;

import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.project.entity.Project;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderStatus;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@JaversSpringDataAuditable
@RepositoryRestResource(collectionResourceRel = "purchase-order-positions", path = "purchase-order-positions")
public interface PurchaseOrderPositionRepository extends JpaRepository<PurchaseOrderPosition, Long> {
    @Query("Select p FROM PurchaseOrderPosition p LEFT JOIN User u ON p.createdBy=u.userId where lower(p.purchaseOrder.status) like lower(concat('%', :status , '%')) " +
            " AND lower(p.spendType.budgetSpendTypeCode) like lower(concat('%', :spendType , '%')) " +
            " AND (lower(u.fullname) like lower(concat('%', :createdBy , '%')) OR lower(p.createdBy) like lower(concat('%', :createdBy , '%'))) " +
            " AND lower(p.purchaseOrder.purchaseOrderNumber) like lower(concat('%', :poNumber , '%')) " +
            " AND lower(p.purchaseOrder.country.countryName) like lower(concat('%', :countryName , '%')) "+
            " AND lower(p.purchaseOrder.vendor.name1) like lower(concat('%', :vendorName , '%')) "+
            " AND (lower(p.project.projectName) like lower(concat('%', :projectName , '%')) OR lower(p.project.sapClientCode) like lower(concat('%', :projectName , '%')))" +
            " AND p.createdBy like ?#{hasAuthority('bmg-spendtracker-user-restricted') ? principal : '%'}")
    Page<PurchaseOrderPosition> findPage(@Param("status") String status, @Param("spendType") String spendType, @Param("createdBy") String createdBy, @Param("poNumber") String poNumber, @Param("projectName") String projectName, @Param("countryName") String countryName, @Param("vendorName") String vendorName, @Param("pageable") Pageable pageable);

    @Query("Select p FROM PurchaseOrderPosition p LEFT JOIN User u ON p.createdBy=u.userId where lower(p.purchaseOrder.status) like lower(concat('%', :status , '%')) " +
            " AND lower(p.spendType.budgetSpendTypeCode) like lower(concat('%', :spendType , '%')) " +
            " AND (lower(u.fullname) like lower(concat('%', :createdBy , '%')) OR lower(p.createdBy) like lower(concat('%', :createdBy , '%'))) " +
            " AND lower(p.purchaseOrder.purchaseOrderNumber) like lower(concat('%', :poNumber , '%')) " +
            " AND lower(p.purchaseOrder.country.countryName) like lower(concat('%', :countryName , '%')) "+
            " AND lower(p.purchaseOrder.vendor.name1) like lower(concat('%', :vendorName , '%')) "+
            " AND (lower(p.project.projectName) like lower(concat('%', :projectName , '%')) OR lower(p.project.sapClientCode) like lower(concat('%', :projectName , '%')))" +
            " ORDER BY p.project.projectName ASC")
    List<PurchaseOrderPosition> findAll(@Param("status") String status, @Param("spendType") String spendType, @Param("createdBy") String createdBy, @Param("poNumber") String poNumber, @Param("projectName") String project, @Param("countryName") String country, @Param("vendorName") String vendor);


    List<PurchaseOrderPosition> findByIdIn(List<Long> ids);

    List<PurchaseOrderPosition> findByProjectAndPoCountry(Project project, Country countryId);

    boolean existsByProjectAndPoCountryAndPurchaseOrder_StatusIn(Project project, Country country, List<PurchaseOrderStatus> purchaseOrderStatus);

}
