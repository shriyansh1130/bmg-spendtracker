package de.syndicats.spendtracker.purchaseorder.util;

import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;

public final class PurchaseOrderUtil {
    public static final String PO_NUMBER_LINE_DELIMITER = "#";

    private PurchaseOrderUtil() {
    }

    public static String createPositionReferenceString(PurchaseOrderPosition position) {
        return position.getPurchaseOrder().getPurchaseOrderNumber() + PO_NUMBER_LINE_DELIMITER + (position.getOrderNumber() + 1);
    }


}
