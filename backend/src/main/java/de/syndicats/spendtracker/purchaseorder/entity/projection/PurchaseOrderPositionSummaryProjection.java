package de.syndicats.spendtracker.purchaseorder.entity.projection;

import de.syndicats.spendtracker.budget.entity.projection.BudgetSpendTypeProjection;
import de.syndicats.spendtracker.country.entity.projection.CountryNameAndCodeProjection;
import de.syndicats.spendtracker.currency.entity.Currency;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.math.BigDecimal;

@Projection(name = "purchaseOrderPositionSummaryProjection", types = {PurchaseOrderPosition.class})
public interface PurchaseOrderPositionSummaryProjection {

    BudgetSpendTypeProjection getSpendType();

    @Value("#{target.purchaseOrder.currency}")
    Currency getCurrency();

    @Value("#{target.purchaseOrder.purchaseOrderNumber}")
    String getPurchaseOrderNumber();

    @Value("#{target.purchaseOrder.vendor.name1}")
    String getVendorName();

    CountryNameAndCodeProjection getPoCountry();

    @Value("#{target.purchaseOrder.country.countryName}")
    String getCountryName();

    @Value("#{target.purchaseOrder.status}")
    PurchaseOrderStatus getPoStatus();

    BigDecimal getPrice();

    BigDecimal getQuantity();

    BigDecimal getTotal();

    String getDescription();

    @Value("#{target.serviceMonthString}")
    String getServiceMonth();

    @Value("#{target.project.projectName.concat(' (').concat(target.project.sapClientCode).concat(')')}")
    String getProjectName();

    Integer getOrderNumber();

    String getCreatedBy();

    BigDecimal getOpenPo();

    BigDecimal getPaid();
}
