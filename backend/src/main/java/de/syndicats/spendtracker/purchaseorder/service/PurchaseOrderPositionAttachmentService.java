package de.syndicats.spendtracker.purchaseorder.service;

import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPositionAttachment;
import de.syndicats.spendtracker.purchaseorder.repository.PurchaseOrderPositionAttachmentRepository;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service
public class PurchaseOrderPositionAttachmentService {

    @Autowired
    private PurchaseOrderPositionAttachmentRepository purchaseOrderPositionAttachmentRepository;

    public PurchaseOrderPositionAttachment store(MultipartFile file, PurchaseOrderPosition position) throws IOException {
        UUID uuid = UUID.randomUUID();
        byte[] bytes = IOUtils.toByteArray(file.getInputStream());

        PurchaseOrderPositionAttachment attachment = PurchaseOrderPositionAttachment.builder()
                .uuid(uuid.toString())
                .filename(file.getOriginalFilename())
                .purchaseOrderPosition(position)
                .contentType(file.getContentType())
                .build();

        PurchaseOrderPositionAttachment persistentAttachment = purchaseOrderPositionAttachmentRepository.save(attachment);

        CompletableFuture.supplyAsync(() -> this.appendBlob(persistentAttachment, bytes));

        return persistentAttachment;
    }

    public PurchaseOrderPositionAttachment store(MultipartFile file) throws IOException {
        return this.store(file, null);
    }

    public Optional<PurchaseOrderPositionAttachment> load(UUID uuid) {
        return this.purchaseOrderPositionAttachmentRepository.findByUuid(uuid.toString());
    }

    public List<PurchaseOrderPositionAttachment> loadMultiple(List<String> uuids) {
        return this.purchaseOrderPositionAttachmentRepository.findManyByUuidIsIn(uuids);
    }

    private PurchaseOrderPositionAttachment appendBlob(PurchaseOrderPositionAttachment attachment, byte[] bytes) {
        attachment.setBlob(bytes);
        return purchaseOrderPositionAttachmentRepository.save(attachment);
    }
}
