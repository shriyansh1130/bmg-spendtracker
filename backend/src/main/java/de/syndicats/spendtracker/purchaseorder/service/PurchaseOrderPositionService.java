package de.syndicats.spendtracker.purchaseorder.service;


import de.syndicats.spendtracker.project.entity.ProjectRecoupmentPercentage;
import de.syndicats.spendtracker.project.entity.ProjectRecoupmentPercentageId;
import de.syndicats.spendtracker.project.repository.ProjectRecoupmentPercentageRepository;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class PurchaseOrderPositionService {

    @Autowired
    private ProjectRecoupmentPercentageRepository projectRecoupmentPercentageRepository;


    public BigDecimal getRecoupmentPct(PurchaseOrderPosition position) {
        Optional<ProjectRecoupmentPercentage> optionalPercentage = projectRecoupmentPercentageRepository.findById(new ProjectRecoupmentPercentageId(position.getProject(), position.getSpendType()));
        if (optionalPercentage.isPresent()) {
            return optionalPercentage.get().getRecoupmentPercentage();
        }
        return position.getSpendType().getPpdRecoupmentPct();
    }
}
