package de.syndicats.spendtracker.purchaseorder.controller.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.chrono.Chronology;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.List;
import java.util.Locale;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class PurchaseOrderPositionDto {
    private String id;
    private String projectId;
    private String spendTypeId;
    private String poCountryId;
    private BigDecimal price;
    private BigDecimal quantity;
    private String description;
    private String serviceMonth;
    private List<String> attachments;
    private String notes;

    @JsonIgnore
    public LocalDateTime deriveServiceMonthDateTime() {
            DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                    .parseCaseInsensitive()
                    .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
                    .appendPattern("MMM-yyyy").toFormatter(Locale.ENGLISH);
        return LocalDate.parse(this.serviceMonth, formatter).atStartOfDay();
    }
}
