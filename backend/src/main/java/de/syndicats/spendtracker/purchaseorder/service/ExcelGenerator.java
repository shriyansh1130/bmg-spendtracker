package de.syndicats.spendtracker.purchaseorder.service;

import de.syndicats.spendtracker.project.entity.Project;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderCoding;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import de.syndicats.spendtracker.purchaseorder.util.PurchaseOrderUtil;
import de.syndicats.spendtracker.user.entity.User;
import de.syndicats.spendtracker.user.repository.UserRepository;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.DateFormatConverter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.CompletableFuture;

@Slf4j
public class ExcelGenerator {

    private ExcelGenerator() {
    }

    @Getter
    @ToString
    public static class PurchaseOrderExcelContext extends BaseExcelContext {

        private UserRepository userRepository;
        // ------ Coding Rules ------
        private static final String RECOUPABLE_ACCOUNT = "R Account";

        private static final String LINE_NUMBER = "Line #";
        private static final String STATUS = "Status";
        private static final String PURCHASE_ORDER_NUMBER = "PO #";
        private static final String PURCHASE_ORDER_LINE_NUMBER = "PO Line #";
        private static final String PROJECT_CODE = "Project Code";
        private static final String PROJECT_NAME = "Project Name";
        private static final String SPEND_TYPE = "Spend Type";
        private static final String SUPPLIER_NAME = "Supplier Name";
        private static final String PO_DESCRIPTION = "PO Description";
        private static final String CURRENCY_CODE = "Currency Code";
        private static final String PO_VALUE = "PO Value (ex. VAT) / Price";
        private static final String QUANTITY = "Quantity";
        private static final String PO_AMOUNT = "PO Amount";
        private static final String INVOICED = "Invoiced";
        private static final String VARIANCE = "Variance";
        private static final String OPEN_PO = "Open PO";
        private static final String SERVICE_MONTH = "Service Month";
        private static final String WHO_CREATED = "Who Created";
        private static final String CREATED_DATE = "Created Date";
        private static final String SPEND_TYPE_CODE = "Spend Type Code";
        private static final String PO_COUNTRY = "PO Country";
        private static final String CURRENT_TERRITORY = "Current Territory";
        private static final String SAP_COMPANY_CODE = "SAP Company #";
        private static final String SAP_ION = "SAP ION";
        private static final String RO_TERRITORY = "RO Territory";
        private static final String RECOUPABLE_CUSTOMER_ACCOUNT = "R Customer Account";
        private static final String RECOUPABLE_ASSIGNMENT = "R Assignment";
        private static final String RECOUPABLE_TEXT = "R Text";
        private static final String RECOUPABLE_REFERENCE_KEY_2 = "R Reference Key 2";
        private static final String RECOUPABLE_REFERENCE_KEY_3 = "R Reference Key 3";
        private static final String RECOUPABLE_AMOUNT = "R Amount";
        private static final String NON_RECOUPABLE_ACCOUNT = "NR Account";
        private static final String NON_RECOUPABLE_ASSIGNMENT = "NR Assignment";
        private static final String NON_RECOUPABLE_ORDER = "NR Order";
        private static final String NON_RECOUPABLE_REFERENCE_KEY_2 = "NR Reference Key 2";
        private static final String NON_RECOUPABLE_REFERENCE_KEY_3 = "NR Reference Key 3";
        private static final String NON_RECOUPABLE_COSTCENTER = "NR Cost Ctr";
        private static final String NON_RECOUPABLE_ORDER_TYPE = "NR Order Type";
        private static final String NON_RECOUPABLE_TEXT = "NR Text";
        private static final String NON_RECOUPABLE_AMOUNT = "NR Amount";
        protected static final List<String> HEADER_NAMES = new ArrayList<>(
                Arrays.asList(STATUS,
                        PURCHASE_ORDER_NUMBER,
                        LINE_NUMBER,
                        PURCHASE_ORDER_LINE_NUMBER,
                        PROJECT_CODE,
                        PROJECT_NAME,
                        SPEND_TYPE,
                        PO_COUNTRY,
                        SUPPLIER_NAME,
                        PO_DESCRIPTION,
                        CURRENCY_CODE,
                        PO_VALUE,
                        QUANTITY,
                        PO_AMOUNT,
                        INVOICED,
                        VARIANCE,
                        OPEN_PO,
                        SERVICE_MONTH,
                        WHO_CREATED,
                        CREATED_DATE,
                        SPEND_TYPE_CODE,
                        CURRENT_TERRITORY,
                        SAP_COMPANY_CODE,
                        SAP_ION,
                        RO_TERRITORY,
                        RECOUPABLE_ACCOUNT,
                        RECOUPABLE_CUSTOMER_ACCOUNT,
                        RECOUPABLE_ASSIGNMENT,
                        RECOUPABLE_TEXT,
                        RECOUPABLE_REFERENCE_KEY_2,
                        RECOUPABLE_REFERENCE_KEY_3,
                        RECOUPABLE_AMOUNT,
                        NON_RECOUPABLE_ACCOUNT,
                        NON_RECOUPABLE_ASSIGNMENT,
                        NON_RECOUPABLE_ORDER,
                        NON_RECOUPABLE_REFERENCE_KEY_2,
                        NON_RECOUPABLE_REFERENCE_KEY_3,
                        NON_RECOUPABLE_COSTCENTER,
                        NON_RECOUPABLE_ORDER_TYPE,
                        NON_RECOUPABLE_TEXT,
                        NON_RECOUPABLE_AMOUNT
                ));


        private static final BigDecimal TEMPORARY_INVOICED = new BigDecimal("0");
        private SapCodingRulesService sapCodingRulesService;

        @NotNull
        private List<PurchaseOrderPosition> purchaseOrderPositions;

        @Builder
        private PurchaseOrderExcelContext(@NotNull String sheetName,
                                          @NotNull File targetFile,
                                          List<PurchaseOrderPosition> purchaseOrderPositions,
                                          @NotNull UserRepository userRepository,
                                          @NotNull SapCodingRulesService sapCodingRulesService
        ) {

            super(sheetName, targetFile);
            this.purchaseOrderPositions = purchaseOrderPositions;
            this.userRepository = userRepository;
            this.sapCodingRulesService = sapCodingRulesService;
        }

        @Override
        void processWorkbook() {
            createHeaderRow();
            createDataRows();
            formatSheet();
        }

        private void createHeaderRow() {
            Row headerRow = sheet.createRow(0);
            headerRow.setHeightInPoints(12.75f);
            for (int i = 0; i < HEADER_NAMES.size(); i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(HEADER_NAMES.get(i));
            }
        }


        private float parsePercentageAsFloat(String percentage) {
            if (StringUtils.isEmpty(percentage)) {
                return 0.0F;
            }
            return Float.parseFloat(percentage.substring(0, percentage.indexOf('%'))) / 100.0F;
        }

        private void createDataRows() {
            int rownum = 1;
            CellStyle numericValueStyle = workbook.createCellStyle();
            numericValueStyle.setDataFormat(workbook.createDataFormat()
                    .getFormat("0.00"));

            CellStyle dateValueStyle = workbook.createCellStyle();
            String excelDateFormatPattern = DateFormatConverter.convert(Locale.ENGLISH, "dd-MM-yyyy");
            dateValueStyle.setDataFormat(workbook.createDataFormat()
                    .getFormat(excelDateFormatPattern));

            CellStyle dateAsMonthValueStyle = workbook.createCellStyle();
            String dateAsMonthDateFormatPattern = DateFormatConverter.convert(Locale.ENGLISH, "MMM-yyyy");
            dateAsMonthValueStyle.setDataFormat(workbook.createDataFormat()
                    .getFormat(dateAsMonthDateFormatPattern));

            CellStyle percentageValueStyle = workbook.createCellStyle();
            percentageValueStyle.setDataFormat(workbook.createDataFormat()
                    .getFormat("0%"));

            for (PurchaseOrderPosition purchaseOrderPosition : this.purchaseOrderPositions) {
                Row row = sheet.createRow(rownum);
                Cell cell;

                cell = row.createCell(HEADER_NAMES.indexOf(STATUS));
                cell.setCellValue(purchaseOrderPosition.getPurchaseOrder()
                        .getStatus() != null ? purchaseOrderPosition.getPurchaseOrder()
                        .getStatus()
                        .name() : "");

                cell = row.createCell(HEADER_NAMES.indexOf(PURCHASE_ORDER_NUMBER));
                cell.setCellValue(purchaseOrderPosition.getPurchaseOrder()
                        .getPurchaseOrderNumber());

                int lineNumber = purchaseOrderPosition.getOrderNumber() + 1;

                cell = row.createCell(HEADER_NAMES.indexOf(LINE_NUMBER));
                cell.setCellValue(lineNumber);

                cell = row.createCell(HEADER_NAMES.indexOf(PURCHASE_ORDER_LINE_NUMBER));
                cell.setCellValue(PurchaseOrderUtil.createPositionReferenceString(purchaseOrderPosition));

                Project project = purchaseOrderPosition.getProject();
                if (project != null) {
                    cell = row.createCell(HEADER_NAMES.indexOf(PROJECT_CODE));
                    cell.setCellValue(project.getSapClientCode());

                    cell = row.createCell(HEADER_NAMES.indexOf(PROJECT_NAME));
                    cell.setCellValue(project.getProjectName());
                }

                cell = row.createCell(HEADER_NAMES.indexOf(SPEND_TYPE));
                cell.setCellValue(
                        purchaseOrderPosition.getSpendType() != null ? purchaseOrderPosition.getSpendType()
                                .getTitle()
                                : "");

                cell = row.createCell(HEADER_NAMES.indexOf(SUPPLIER_NAME));
                cell.setCellValue(purchaseOrderPosition.getPurchaseOrder()
                        .getVendor()
                        .getName1());

                cell = row.createCell(HEADER_NAMES.indexOf(PO_DESCRIPTION));
                cell.setCellValue(purchaseOrderPosition.getDescription());

                cell = row.createCell(HEADER_NAMES.indexOf(CURRENCY_CODE));
                cell.setCellValue(purchaseOrderPosition.getPurchaseOrder()
                        .getCurrency()
                        .getCurrencyCode());

                cell = row.createCell(HEADER_NAMES.indexOf(PO_VALUE));
                cell.setCellStyle(numericValueStyle);
                cell.setCellValue(toRoundedDouble(purchaseOrderPosition.getPrice()));

                cell = row.createCell(HEADER_NAMES.indexOf(QUANTITY));
                cell.setCellStyle(numericValueStyle);
                cell.setCellValue(toRoundedDouble(purchaseOrderPosition.getQuantity()));

                cell = row.createCell(HEADER_NAMES.indexOf(PO_AMOUNT));
                cell.setCellStyle(numericValueStyle);
                cell.setCellValue(toRoundedDouble(purchaseOrderPosition.getTotal()));

                cell = row.createCell(HEADER_NAMES.indexOf(INVOICED));
                cell.setCellStyle(numericValueStyle);
                cell.setCellValue(toRoundedDouble(purchaseOrderPosition.getPaid()));

                cell = row.createCell(HEADER_NAMES.indexOf(VARIANCE));
                cell.setCellStyle(numericValueStyle);
                cell.setCellValue(toRoundedDouble(purchaseOrderPosition.getVariance()));

                cell = row.createCell(HEADER_NAMES.indexOf(OPEN_PO));
                cell.setCellStyle(numericValueStyle);
                cell.setCellValue(toRoundedDouble(purchaseOrderPosition.getOpenPo()));

                cell = row.createCell(HEADER_NAMES.indexOf(SERVICE_MONTH));
                cell.setCellStyle(dateAsMonthValueStyle);
                cell.setCellValue(java.sql.Timestamp.valueOf(purchaseOrderPosition.getServiceMonth()));

                final Cell createdByCell = row.createCell(HEADER_NAMES.indexOf(WHO_CREATED));

                if (purchaseOrderPosition.getCreatedBy() == null || "system".equals(purchaseOrderPosition.getCreatedBy())) {
                    createdByCell.setCellValue(purchaseOrderPosition.getCreatedBy());
                } else {
                    Optional<User> optionalUser = this.userRepository.findByUserId(purchaseOrderPosition.getCreatedBy());
                    optionalUser.ifPresent(user -> createdByCell.setCellValue(user.getFullname()));
                }

                cell = row.createCell(HEADER_NAMES.indexOf(CREATED_DATE));
                cell.setCellStyle(dateValueStyle);
                cell.setCellValue(purchaseOrderPosition.getCreatedAt());

                cell = row.createCell(HEADER_NAMES.indexOf(SPEND_TYPE_CODE));
                cell.setCellValue(purchaseOrderPosition.getSpendType()
                        .getBudgetSpendTypeCode());

                cell = row.createCell(HEADER_NAMES.indexOf(PO_COUNTRY));
                cell.setCellValue(purchaseOrderPosition.getPoCountry() != null ? purchaseOrderPosition.getPoCountry()
                        .getCountryCode() : "");

                cell = row.createCell(HEADER_NAMES.indexOf(CURRENT_TERRITORY));
                cell.setCellValue(purchaseOrderPosition.getPurchaseOrder()
                        .getCountry()
                        .getCountryCode());

                cell = row.createCell(HEADER_NAMES.indexOf(SAP_COMPANY_CODE));
                cell.setCellValue(purchaseOrderPosition.getPurchaseOrder()
                        .getVendor()
                        .getCompany()
                        .getSapCompanyCode());

                cell = row.createCell(HEADER_NAMES.indexOf(SAP_ION));
                cell.setCellValue(purchaseOrderPosition.getProject()
                        .getIonSap());

                cell = row.createCell(HEADER_NAMES.indexOf(RO_TERRITORY));
                cell.setCellValue(purchaseOrderPosition.getProject()
                        .getRepertoireOwnerCompany()
                        .getCountry()
                        .getCountryCode());

                addSapCodingRuleCells(purchaseOrderPosition, row, percentageValueStyle);

                rownum++;
            }
        }

        private void addSapCodingRuleCells(PurchaseOrderPosition purchaseOrderPosition, Row row, CellStyle percentageValueStyle) {
            if (sapCodingRulesService != null) {
                Cell cell;
                PurchaseOrderCoding coding = sapCodingRulesService.createCoding(purchaseOrderPosition);

                cell = row.createCell(HEADER_NAMES.indexOf(RECOUPABLE_ACCOUNT));
                cell.setCellValue(coding.getRecoupableAccount());

                cell = row.createCell(HEADER_NAMES.indexOf(RECOUPABLE_CUSTOMER_ACCOUNT));
                cell.setCellValue(coding.getRecoupableCustomerAccount());

                cell = row.createCell(HEADER_NAMES.indexOf(RECOUPABLE_ASSIGNMENT));
                cell.setCellValue(coding.getRecoupableAssignment());

                cell = row.createCell(HEADER_NAMES.indexOf(RECOUPABLE_TEXT));
                cell.setCellValue(coding.getRecoupableText());

                cell = row.createCell(HEADER_NAMES.indexOf(RECOUPABLE_REFERENCE_KEY_2));
                cell.setCellValue(coding.getRecoupableReferenceKey2());

                cell = row.createCell(HEADER_NAMES.indexOf(RECOUPABLE_REFERENCE_KEY_3));
                cell.setCellValue(coding.getRecoupableReferenceKey3());

                cell = row.createCell(HEADER_NAMES.indexOf(RECOUPABLE_AMOUNT));
                cell.setCellValue(parsePercentageAsFloat(coding.getRecoupableAmount()));
                cell.setCellStyle(percentageValueStyle);

                cell = row.createCell(HEADER_NAMES.indexOf(NON_RECOUPABLE_ACCOUNT));
                cell.setCellValue(coding.getNonRecoupableAccount());

                cell = row.createCell(HEADER_NAMES.indexOf(NON_RECOUPABLE_ASSIGNMENT));
                cell.setCellValue(coding.getNonRecoupableAssignment());

                cell = row.createCell(HEADER_NAMES.indexOf(NON_RECOUPABLE_ORDER));
                cell.setCellValue(coding.getNonRecoupableOrder());

                cell = row.createCell(HEADER_NAMES.indexOf(NON_RECOUPABLE_REFERENCE_KEY_2));
                cell.setCellValue(coding.getNonRecoupableReferenceKey2());

                cell = row.createCell(HEADER_NAMES.indexOf(NON_RECOUPABLE_REFERENCE_KEY_3));
                cell.setCellValue(coding.getNonRecoupableReferenceKey3());

                cell = row.createCell(HEADER_NAMES.indexOf(NON_RECOUPABLE_COSTCENTER));
                cell.setCellValue(coding.getNonRecoupableCostCenter());

                cell = row.createCell(HEADER_NAMES.indexOf(NON_RECOUPABLE_ORDER_TYPE));
                cell.setCellValue(coding.getNonRecoupableOrderType());

                cell = row.createCell(HEADER_NAMES.indexOf(NON_RECOUPABLE_TEXT));
                cell.setCellValue(coding.getNonRecoupableText());

                cell = row.createCell(HEADER_NAMES.indexOf(NON_RECOUPABLE_AMOUNT));
                cell.setCellValue(parsePercentageAsFloat(coding.getNonRecoupableAmount()));
                cell.setCellStyle(percentageValueStyle);
            } else {
                log.error("No SAP Coding Rules Service given. Coding Rule related Columns will be empty");
            }
        }


        private Double toRoundedDouble(BigDecimal bigDecimal) {
            if (bigDecimal == null) {
                return new Double("0.00");
            }
            return bigDecimal.setScale(2, RoundingMode.HALF_UP)
                    .doubleValue();
        }

        private void formatSheet() {
            for (int i = 0; i < HEADER_NAMES.size(); i++) {
                sheet.autoSizeColumn(i);
            }
            sheet.setAutoFilter(new CellRangeAddress(0, 0, 0, HEADER_NAMES.size() - 1));
        }

        private BigDecimal calculateTotal(PurchaseOrderPosition purchaseOrderPosition) {
            return purchaseOrderPosition.getQuantity()
                    .multiply(purchaseOrderPosition.getPrice());
        }
    }

    static abstract class BaseExcelContext {

        protected final String sheetName;

        protected final File targetFile;

        protected Workbook workbook = null;

        protected Sheet sheet = null;

        protected Map<String, CellStyle> styles = null;

        protected BaseExcelContext(String sheetName, File targetFile) {
            this.sheetName = sheetName;
            this.targetFile = targetFile;
        }

        void openWorkbook() {
            workbook = new XSSFWorkbook();
            initStyles();
            initSheet();
        }

        abstract void processWorkbook();

        void closeWorkbook() throws IOException {
            if (workbook == null) {
                throw new RuntimeException("Workbook is null, open workbook first.");
            }

            try (FileOutputStream os = new FileOutputStream(targetFile)) {
                workbook.write(os);
            } finally {
                workbook.close();
            }

            workbook = null;
            styles = null;
            sheet = null;
        }

        protected void initStyles() {
            styles = new HashMap<>();
        }

        protected void initSheet() {
            sheet = workbook.createSheet(sheetName);
        }
    }

    public static CompletableFuture<File> createExcel(BaseExcelContext excelContext) throws IOException {

        excelContext.openWorkbook();
        excelContext.processWorkbook();
        excelContext.closeWorkbook();

        return CompletableFuture.completedFuture(excelContext.targetFile);
    }
}
