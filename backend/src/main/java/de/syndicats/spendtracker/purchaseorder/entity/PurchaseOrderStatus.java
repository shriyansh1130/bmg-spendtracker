package de.syndicats.spendtracker.purchaseorder.entity;

@SuppressWarnings("squid:S00115")
public enum PurchaseOrderStatus {
    /**
     * default status until first submitted
     */
    Draft,

    /**
     * status after submitted
     * editing the PO changes the status back to "Draft"
     */
    Open,

    /**
     * status after set to closed
     * can no longer be edited
     */
    Closed
}
