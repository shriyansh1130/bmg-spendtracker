package de.syndicats.spendtracker.application.bootstrap;

import de.syndicats.spendtracker.budget.data.BudgetCategoryImporter;
import de.syndicats.spendtracker.budget.data.BudgetSpendTypeImporter;
import de.syndicats.spendtracker.budget.data.BudgetSubCategoryImporter;
import de.syndicats.spendtracker.company.data.CompanyImporter;
import de.syndicats.spendtracker.country.data.CountryImporter;
import de.syndicats.spendtracker.currency.data.CurrencyImporter;
import de.syndicats.spendtracker.exchangerate.data.ExchangeRateImporter;
import de.syndicats.spendtracker.project.data.ProjectImporter;
import de.syndicats.spendtracker.vendor.data.VendorImporter;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty("syndicats.initialDataGen")
@AllArgsConstructor
@Slf4j
@Order(1)
public class InitialDataGenerator implements CommandLineRunner {
    private static final String BUDGET_CATEGORY_IMPORT_PATH = "/db/import/budget_categories.csv";
    private static final String BUDGET_SUB_CATEGORY_IMPORT_PATH = "/db/import/budget_sub_categories.csv";
    private static final String BUDGET_SPEND_TYPE_IMPORT_PATH = "/db/import/budget_spend_types.csv";
    private static final String COMPANY_IMPORT_PATH = "/db/import/companies.csv";
    private static final String COUNTRIES_IMPORT_PATH = "/db/import/countries.csv";
    private static final String CURRENCIES_IMPORT_PATH = "/db/import/currencies.csv";
    private static final String EXCHANGE_RATE_IMPORT_PATH = "/db/import/exchange_rates.csv";
    private static final String PROJECT_IMPORT_PATH = "/db/import/projects.csv";
    private static final String VENDOR_IMPORT_PATH = "/db/import/vendors.csv";


    private BudgetCategoryImporter budgetCategoryImporter;
    private BudgetSubCategoryImporter budgetSubCategoryImporter;
    private BudgetSpendTypeImporter budgetSpendTypeImporter;
    private CompanyImporter companyImporter;
    private CountryImporter countryImporter;
    private CurrencyImporter currencyImporter;
    private ExchangeRateImporter exchangeRateImporter;
    private ProjectImporter projectImporter;
    private VendorImporter vendorImporter;


    @Override
    public void run(String... args) throws Exception {
        budgetCategoryImporter.importRecords(-1, new ClassPathResource(BUDGET_CATEGORY_IMPORT_PATH).getInputStream(), true);
        budgetSubCategoryImporter.importRecords(-1, new ClassPathResource(BUDGET_SUB_CATEGORY_IMPORT_PATH).getInputStream(), true);
        budgetSpendTypeImporter.importRecords(-1, new ClassPathResource(BUDGET_SPEND_TYPE_IMPORT_PATH).getInputStream(), true);
        currencyImporter.importRecords(-1, new ClassPathResource(CURRENCIES_IMPORT_PATH).getInputStream(), true);
        countryImporter.importRecords(-1, new ClassPathResource(COUNTRIES_IMPORT_PATH).getInputStream(), true);
        companyImporter.importRecords(-1, new ClassPathResource(COMPANY_IMPORT_PATH).getInputStream(), true);
        exchangeRateImporter.importRecords(-1, new ClassPathResource(EXCHANGE_RATE_IMPORT_PATH).getInputStream(), true);
        projectImporter.importRecords(-1, new ClassPathResource(PROJECT_IMPORT_PATH).getInputStream(), true);
        vendorImporter.importRecords(-1, new ClassPathResource(VENDOR_IMPORT_PATH).getInputStream(), true);
    }
}
