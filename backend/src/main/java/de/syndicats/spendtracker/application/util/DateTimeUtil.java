package de.syndicats.spendtracker.application.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Locale;

public class DateTimeUtil {

    public static LocalDateTime getLocalDateTimeFromYearMonthString(String yearMonthString) {
        DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .parseCaseInsensitive()
                .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
                .appendPattern("MMM-yyyy").toFormatter(Locale.ENGLISH);

        LocalDateTime dateTime = LocalDateTime.parse(yearMonthString, formatter);
        return dateTime;
    }
}
