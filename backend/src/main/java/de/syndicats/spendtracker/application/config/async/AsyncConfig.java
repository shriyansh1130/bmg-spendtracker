package de.syndicats.spendtracker.application.config.async;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * Created by kailehmann on 16.10.17.
 */
@Configuration
@EnableAsync
public class AsyncConfig {

	@Value("${syndicats.threadpool.corePoolSize:2}")
	private int corePoolSize;

	@Value("${syndicats.threadpool.maxPoolSize:20}")
	private int maxPoolSize;

	@Value("${syndicats.threadpool.queueCapacity:500}")
	private int queueCapacity;

	@Value("${syndicats.threadpool.namePrefix:ThreadPool}")
	private String threadNamePrefix;

	@Bean
	public Executor asyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(corePoolSize);
		executor.setMaxPoolSize(maxPoolSize);
		executor.setQueueCapacity(queueCapacity);
		executor.setThreadNamePrefix(threadNamePrefix + "-");
		executor.initialize();
		return executor;
	}

}
