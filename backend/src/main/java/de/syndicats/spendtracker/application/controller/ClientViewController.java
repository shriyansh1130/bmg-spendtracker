package de.syndicats.spendtracker.application.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * ClientController that forwards non-api requests to index.html so that
 * Angular can take care of routing
 */
@Controller
public class ClientViewController {

    //New Angular-Routes need to be added to this list
    @RequestMapping({
            "/login",
            "/login/**",
            "/projects",
            "/projects/**",
            "/projects/**/**",
            "/logout",
            "/accessDenied",
            "/vendors/**",
            "/orders",
            "/orders/**",
            "/exchange-rates",
            "/exchange-rates/**",
            "/currencies",
            "/countries",
            "/companies",
            "/budget-categories",
            "/budget-categories/**"
    })
    public String index() {
        return "forward:/index.html";
    }

}
