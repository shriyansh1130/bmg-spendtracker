package de.syndicats.spendtracker.application.bootstrap;

import de.syndicats.spendtracker.budget.data.BudgetCategoryImporter;
import de.syndicats.spendtracker.budget.data.BudgetSpendTypeImporter;
import de.syndicats.spendtracker.budget.data.BudgetSubCategoryImporter;
import de.syndicats.spendtracker.budget.entity.BudgetSpendType;
import de.syndicats.spendtracker.budget.repository.BudgetSpendTypeRepository;
import de.syndicats.spendtracker.company.data.CompanyImporter;
import de.syndicats.spendtracker.country.data.CountryImporter;
import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.country.repository.CountryRepository;
import de.syndicats.spendtracker.currency.data.CurrencyImporter;
import de.syndicats.spendtracker.currency.entity.Currency;
import de.syndicats.spendtracker.currency.repository.CurrencyRepository;
import de.syndicats.spendtracker.exchangerate.data.ExchangeRateImporter;
import de.syndicats.spendtracker.project.data.ProjectImporter;
import de.syndicats.spendtracker.project.entity.Project;
import de.syndicats.spendtracker.project.repository.ProjectRepository;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrder;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderStatus;
import de.syndicats.spendtracker.purchaseorder.repository.PurchaseOrderRepository;
import de.syndicats.spendtracker.purchaseorder.service.PurchaseOrderNumberService;
import de.syndicats.spendtracker.vendor.data.VendorImporter;
import de.syndicats.spendtracker.vendor.entity.Vendor;
import de.syndicats.spendtracker.vendor.repository.VendorRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
@ConditionalOnProperty("syndicats.devTestDataGen")
@RequiredArgsConstructor
@Slf4j
@Order(100)
public class DevTestDataGenerator implements CommandLineRunner {

    private static final String BUDGET_CATEGORY_IMPORT_PATH = "/db/import/budget_categories.csv";
    private static final String BUDGET_SUB_CATEGORY_IMPORT_PATH = "/db/import/budget_sub_categories.csv";
    private static final String BUDGET_SPEND_TYPE_IMPORT_PATH = "/db/import/budget_spend_types.csv";
    private static final String COMPANY_IMPORT_PATH = "/db/import/companies.csv";
    private static final String COUNTRIES_IMPORT_PATH = "/db/import/countries.csv";
    private static final String CURRENCIES_IMPORT_PATH = "/db/import/currencies.csv";
    private static final String EXCHANGE_RATE_IMPORT_PATH = "/db/import/exchange_rates.csv";
    private static final String PROJECT_IMPORT_PATH = "/db/import/projects_short.csv";
    private static final String VENDOR_IMPORT_PATH = "/db/import/vendors_short.csv";

    @NonNull
    private BudgetCategoryImporter budgetCategoryImporter;
    @NonNull
    private BudgetSubCategoryImporter budgetSubCategoryImporter;
    @NonNull
    private BudgetSpendTypeImporter budgetSpendTypeImporter;
    @NonNull
    private CompanyImporter companyImporter;
    @NonNull
    private CountryImporter countryImporter;
    @NonNull
    private CurrencyImporter currencyImporter;
    @NonNull
    private ExchangeRateImporter exchangeRateImporter;
    @NonNull
    private ProjectImporter projectImporter;
    @NonNull
    private VendorImporter vendorImporter;

    @NonNull
    private CountryRepository countryRepository;
    @NonNull
    private ProjectRepository projectRepository;
    @NonNull
    private CurrencyRepository currencyRepository;
    @NonNull
    private VendorRepository vendorRepository;
    @NonNull
    private BudgetSpendTypeRepository budgetSpendTypeRepository;
    @NonNull
    private PurchaseOrderRepository purchaseOrderRepository;

    private Project project;
    private Country country;
    private Currency currency;
    private Vendor vendor;
    private BudgetSpendType budgetSpendType;

    @Override
    @Transactional
    public void run(String... args) throws IOException {
        log.info("Creating dev test data...");

        importRecords();
        createPurchaseOrders();

        log.info("...done.");
    }

    @SuppressWarnings("Duplicates")
    void importRecords() throws IOException {
        budgetCategoryImporter.importRecords(-1, new ClassPathResource(BUDGET_CATEGORY_IMPORT_PATH).getInputStream(), true);
        budgetSubCategoryImporter.importRecords(-1, new ClassPathResource(BUDGET_SUB_CATEGORY_IMPORT_PATH).getInputStream(), true);
        budgetSpendTypeImporter.importRecords(-1, new ClassPathResource(BUDGET_SPEND_TYPE_IMPORT_PATH).getInputStream(), true);
        currencyImporter.importRecords(-1, new ClassPathResource(CURRENCIES_IMPORT_PATH).getInputStream(), true);
        countryImporter.importRecords(-1, new ClassPathResource(COUNTRIES_IMPORT_PATH).getInputStream(), true);
        companyImporter.importRecords(-1, new ClassPathResource(COMPANY_IMPORT_PATH).getInputStream(), true);
        exchangeRateImporter.importRecords(-1, new ClassPathResource(EXCHANGE_RATE_IMPORT_PATH).getInputStream(), true);
        projectImporter.importRecords(-1, new ClassPathResource(PROJECT_IMPORT_PATH).getInputStream(), true);
        vendorImporter.importRecords(-1, new ClassPathResource(VENDOR_IMPORT_PATH).getInputStream(), true);
    }

    void createPurchaseOrders() {
        budgetSpendType = budgetSpendTypeRepository.findAll().get(0);
        country = countryRepository.findBySapCountryIsTrue(null).iterator().next();
        currency = currencyRepository.findAll().get(0);
        vendor = vendorRepository.findAll().get(0);
        project = projectRepository.findAll().get(0);

        purchaseOrderRepository.save(createPurchaseOrder(PurchaseOrderStatus.Open, 1, 7));
        purchaseOrderRepository.save(createPurchaseOrder(PurchaseOrderStatus.Closed, 2, 29));
        purchaseOrderRepository.save(createPurchaseOrder(PurchaseOrderStatus.Draft, 3, 23));

    }

    private PurchaseOrder createPurchaseOrder(PurchaseOrderStatus purchaseOrderStatus, int index, int numOfPositions) {

        List<PurchaseOrderPosition> purchaseOrderPositions = new ArrayList<>();
        for (int i = 0; i < numOfPositions; i++) {
            purchaseOrderPositions.add(createPurchaseOrderPosition(i));
        }

        String purchaseOrderNumber = String.format(PurchaseOrderNumberService.PRURCHASE_ORDER_NUMER_FORMAT, country.getCountryCode(),
                LocalDate.now().getYear(), index);

        PurchaseOrder po = PurchaseOrder.builder()
                .purchaseOrderNumber(purchaseOrderNumber)
                .status(purchaseOrderStatus)
                .currency(currency)
                .country(country)
                .vendor(vendor)
                .positions(purchaseOrderPositions)
                .build();

        purchaseOrderPositions.forEach(purchaseOrderPosition -> purchaseOrderPosition.setPurchaseOrder(po));

        return po;

    }

    private PurchaseOrderPosition createPurchaseOrderPosition(int i) {

        return PurchaseOrderPosition.builder()
                .project(project)
                .price(new BigDecimal(10))
                .quantity(new BigDecimal(1))
                .description("purchaseOrderPosition_description" + i)
                .serviceMonth(LocalDate.now().withDayOfMonth(1).atStartOfDay())
                .spendType(budgetSpendType)
                .build();
    }

}