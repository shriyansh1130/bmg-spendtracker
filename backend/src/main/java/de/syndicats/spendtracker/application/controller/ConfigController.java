package de.syndicats.spendtracker.application.controller;

import lombok.RequiredArgsConstructor;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ConfigController {

	public static final String LIVE = "live";
	public static final String STAGING = "staging";
	public static final String TEST = "test";
	public static final String DEV = "dev";

	@Value("${okta.oauth.redirectUri}")
	private String redirectUri;

	@Value("${okta.oauth.issuer}")
	private String issuer;

	@Value("${okta.oauth.clientId}")
	private String clientId;

	@Autowired
	private Environment environment;

	@GetMapping(value = "config", produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> config() {
		ObjectNode configNode = new ObjectMapper().createObjectNode();
		configNode.put("redirectUri", redirectUri);
		configNode.put("issuer", issuer);
		configNode.put("clientId", clientId);
		configNode.put("env", getEnv());

		return ResponseEntity.ok(configNode.toString());
	}

	private String getEnv() {
		List<String> profiles = Arrays.asList(environment.getActiveProfiles());

		if (profiles.contains(LIVE)) {
			return LIVE;
		}

		if (profiles.contains(STAGING)) {
			return STAGING;
		}

		if (profiles.contains(TEST)) {
			return TEST;
		}

		return DEV;
	}
}
