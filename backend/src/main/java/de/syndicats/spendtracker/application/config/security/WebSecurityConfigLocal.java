package de.syndicats.spendtracker.application.config.security;

import de.syndicats.spendtracker.user.entity.Authority;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@ComponentScan("de.syndicats.spendtracker.application.config.security")
@Profile({"local", "default"})
public class WebSecurityConfigLocal {

    @Bean(name = "testAdmin")
    public Authentication testadmin() {
        return new UsernamePasswordAuthenticationToken("testAdmin", null,
                AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_ADMIN"));
    }

    @Bean
    protected ResourceServerConfigurerAdapter resourceServerConfigurerAdapter() {

        return new ResourceServerConfigurerAdapter() {
            @Override
            public void configure(HttpSecurity http) throws Exception {
                http.headers()
                        .frameOptions()
                        .sameOrigin();
                http.csrf()
                        .ignoringAntMatchers("/**");

                http
                        .authorizeRequests()
                        .antMatchers("/", "/index.html")
                        .permitAll()
                        .antMatchers("/login/**")
                        .permitAll()
                        .antMatchers("/oauth2/*/v1/keys") // Okta workaround
                        .permitAll()
                        .antMatchers("/config")
                        .permitAll()
                        .antMatchers("/h2-console/**")
                        .permitAll()
                        .antMatchers("/api/**")
                        .permitAll()
                        .antMatchers("/*")
                        .permitAll()
                        .antMatchers("/assets/**")
                        .permitAll()
                        .anyRequest()
                        .authenticated();
            }
        };
    }

}
