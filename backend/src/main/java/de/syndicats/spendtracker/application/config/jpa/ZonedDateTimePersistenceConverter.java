package de.syndicats.spendtracker.application.config.jpa;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

@Converter(autoApply = true)
public class ZonedDateTimePersistenceConverter implements AttributeConverter<ZonedDateTime, Date> {

	@Override
	public Date convertToDatabaseColumn(ZonedDateTime attribute) {
		return attribute != null ? java.sql.Date.from(attribute.toInstant()) : null;
	}

	@Override
	public ZonedDateTime convertToEntityAttribute(Date dbData) {
		return dbData != null ? ZonedDateTime.ofInstant(dbData.toInstant(), ZoneId.systemDefault()) : null;
	}
}
