package de.syndicats.spendtracker.application.config.security;

import de.syndicats.spendtracker.user.entity.Authority;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@ComponentScan("de.syndicats.spendtracker.application.config.security")
@Profile("!local")
public class WebSecurityConfig {

    @Bean(name = "testAdmin")
    @Profile("!live")
    public Authentication testadmin() {
        return new UsernamePasswordAuthenticationToken("testAdmin", null,
                AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_ADMIN"));
    }

    @Bean
    protected ResourceServerConfigurerAdapter resourceServerConfigurerAdapter() {
        return new ResourceServerConfigurerAdapter() {
            @Override
            public void configure(HttpSecurity http) throws Exception {
                http.authorizeRequests()
                        .antMatchers("/", "/index.html")
                        .permitAll()
                        .antMatchers("/login/**")
                        .permitAll()
                        .antMatchers("/oauth2/*/v1/keys") // Okta workaround
                        .permitAll()
                        .antMatchers("/config")
                        .permitAll()
                        .antMatchers(HttpMethod.GET, "/api/**").hasAnyAuthority(Authority.USER, Authority.READONLY, Authority.ADMIN)
                        // Projects
                        .antMatchers(HttpMethod.POST,"/api/projects/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.PUT,"/api/projects/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.DELETE,"/api/projects/**").hasAuthority(Authority.ADMIN)
                        // Categories etc.
                        .antMatchers(HttpMethod.POST, "/api/budget-categories/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.POST,"/api/budget-sub-categories/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.POST,"/api/budget-spend-types/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.PUT, "/api/budget-categories/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.PUT,"/api/budget-sub-categories/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.PUT,"/api/budget-spend-types/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.DELETE, "/api/budget-categories/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.DELETE,"/api/budget-sub-categories/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.DELETE,"/api/budget-spend-types/**").hasAuthority(Authority.ADMIN)
                        // Countries
                        .antMatchers(HttpMethod.POST,"/api/countries/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.PUT,"/api/countries/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.DELETE,"/api/countries/**").hasAuthority(Authority.ADMIN)
                        // Companies
                        .antMatchers(HttpMethod.POST,"/api/companies/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.PUT,"/api/companies/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.DELETE,"/api/companies/**").hasAuthority(Authority.ADMIN)
                        // Currencies
                        .antMatchers(HttpMethod.POST, "/api/currencies/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.PUT, "/api/currencies/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.DELETE, "/api/currencies/**").hasAuthority(Authority.ADMIN)
                        // Vendors
                        .antMatchers(HttpMethod.POST, "/api/vendors/**").hasAnyAuthority(Authority.ADMIN, Authority.USER)
                        .antMatchers(HttpMethod.PUT, "/api/vendors/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.DELETE, "/api/vendors/**").hasAuthority(Authority.ADMIN)
                        // Exchange-Rates
                        .antMatchers(HttpMethod.POST, "/api/exchange-rates/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.PUT, "/api/exchange-rates/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.DELETE, "/api/exchange-rates/**").hasAuthority(Authority.ADMIN)
                        // Profile
                        .antMatchers(HttpMethod.POST, "/api/profile/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.PUT, "/api/profile/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.DELETE, "/api/profile/**").hasAuthority(Authority.ADMIN)
                        // Purchase Order Attachments
                        .antMatchers(HttpMethod.POST, "/api/purchase-order-position-attachments/**").hasAuthority(Authority.USER)
                        .antMatchers(HttpMethod.PUT, "/api/purchase-order-position-attachments/**").hasAuthority(Authority.USER)
                        .antMatchers(HttpMethod.DELETE, "/api/purchase-order-position-attachments/**").hasAuthority(Authority.USER)
                        // Purchase Order
                        .antMatchers(HttpMethod.POST, "/api/orders/upsert").hasAuthority(Authority.USER)
                        .antMatchers(HttpMethod.POST, "/api/purchase-orders/**").hasAuthority(Authority.USER)
                        .antMatchers(HttpMethod.PUT, "/api/purchase-orders/**").hasAuthority(Authority.USER)
                        .antMatchers(HttpMethod.DELETE, "/api/purchase-orders/**").hasAuthority(Authority.USER)
                        // Purchase Order Positions
                        .antMatchers(HttpMethod.POST, "/api/purchase-order-positions/**").hasAuthority(Authority.USER)
                        .antMatchers(HttpMethod.PUT, "/api/purchase-order-positions/**").hasAuthority(Authority.USER)
                        .antMatchers(HttpMethod.DELETE, "/api/purchase-order-positions/**").hasAuthority(Authority.USER)
                        // Profile
                        .antMatchers(HttpMethod.PUT, "/api/profile/**").hasAuthority(Authority.ADMIN)
                        .antMatchers(HttpMethod.DELETE, "/api/profile/**").hasAuthority(Authority.ADMIN)
                        .antMatchers("/*")
                        .permitAll()
                        .antMatchers("/assets/**")
                        .permitAll()
                        .anyRequest()
                        .authenticated();
            }
        };
    }

}
