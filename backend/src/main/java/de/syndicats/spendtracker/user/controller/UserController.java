package de.syndicats.spendtracker.user.controller;

import de.syndicats.spendtracker.user.entity.User;
import de.syndicats.spendtracker.user.repository.UserRepository;
import de.syndicats.spendtracker.user.service.OktaService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

/**
 * Created by kailehmann on 04.02.18.
 */
@RestController
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("/api/users")
public class UserController {

    @NonNull
    private OktaService oktaService;

    @NonNull
    private UserRepository userRepository;

    @PostMapping(value = "syncUserFromOkta")
    @ResponseBody
    //TODO: Secure with proper authority
    public ResponseEntity<ResponseMessage> syncUserFromOkta(@RequestBody String userId) {
        try {
            log.info("Sync User {} From Okta", userId);
            oktaService.syncUserFromOkta(userId);
            return ResponseEntity.ok(ResponseMessage.createSuccess("User successfully synced from Okta"));
        } catch (Exception t) {
            return ResponseEntity.badRequest()
                    .body(ResponseMessage.createError(t.getMessage()));
        }
    }

    @PostMapping(value = "syncUsersFromOkta")
    @ResponseBody
    //TODO: Secure with proper authority
    public ResponseEntity<ResponseMessage> syncUsersFromOkta() {
        try {
            log.info("Sync Users From Okta");
            oktaService.syncUsersFromOkta();
            return ResponseEntity.ok(ResponseMessage.createSuccess("Users successfully synced from Okta"));
        } catch (Exception t) {
            return ResponseEntity.badRequest()
                    .body(ResponseMessage.createError(t.getMessage()));
        }
    }

    @GetMapping(value = "me")
    @ResponseBody
    public ResponseEntity<User> me() {
        OAuth2Authentication auth = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
        String oktaId = (String)auth.getUserAuthentication().getPrincipal();
        User me = userRepository.findByUserId(oktaId).orElse(oktaService.syncUserFromOkta(oktaId));
        return ResponseEntity.ok(me);
    }
}
