package de.syndicats.spendtracker.user.repository;

import de.syndicats.spendtracker.user.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource(exported = false, collectionResourceRel = "users", path = "users")
public interface UserRepository extends PagingAndSortingRepository<User, Long> {

    /**
     * Finds user by it's AuthorizationProvider ID (i.g. Okta ID)
     *
     * @param userId
     * @return
     */
    Optional<User> findByUserId(@Param("userId") String userId);

}
