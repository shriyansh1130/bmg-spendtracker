package de.syndicats.spendtracker.user.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/*
 * "Caches" request to https://{OktaAuthIssuerURL}/v1/keys...
 */
@RestController
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class OktaStaticController {

    /**
     * DEV & TEST
     */
    @GetMapping(value = "/oauth2/aused6b3457wws7m70h7/v1/keys", produces = "application/json")
    public Object keysTest() throws IOException {
        log.info("Request for Okta JWK set re-routed. [DEV & TEST]");
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue("{\"keys\":[{\"alg\":\"RS256\",\"e\":\"AQAB\",\"n\":\"vA9uWPhY-hzdDWj37Hfn3vY_Qg5z9QJrWeLk4LU0fF9VKyrJRk39xN3CC3kzpkGMJ15DcMD6SHAevtoUugY7QWkgrNbVPCy7ZjFbhNW_yUKxlcptDyCf-8AyIZyFx0dbPLpBQn3fC2hAJZKC7vdguSgkoJDgxdzOqYUoyw_1Um43C5NL5wNqDoxNLemGbrlOpJ9muTa9fA2sQ6IQtSgRFI1dv97NzE4fmJElLoI6GbzBH6LVh0Bvc8qMQgr625ue07hmasneGN-Uwxrqw5V-ZwpD2jCMhamOs2WqbKloW9nU9gcA733JB5f4PSBh_G0HEq8wb4cj58OSjHWCQQCklQ\",\"kid\":\"RQybale5kSx7DGtjOtULlQVvVc42p73heLqlyq7wdZQ\",\"kty\":\"RSA\",\"use\":\"sig\"}]}", Object.class);
    }

    /**
     * Staging & Live
     */
    @GetMapping(value = "/oauth2/aus1w576e1L0kyx3l0i7/v1/keys", produces = "application/json")
    public Object keysProd() throws IOException {
        log.info("Request for Okta JWK set re-routed. [STAG & PROD]");
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue("{\"keys\":[{\"alg\":\"RS256\",\"e\":\"AQAB\",\"n\":\"h6v-u44o-8o2FMVW2-0g-kMpJ3FOP-Nha-21Ma-AlVCRyFZ1xFceWxfr9GJ3g887P5w3g74Fveir82L8gxFmKw53kyLAMFWtQL2PH5ZFRiRhKcvDYBwutY4J4qfgBVZaN197j1kwM__bcdUizvE7w-RsQGQU8Zks0loYMBPx1CnALUNoio8_trCOeexzpj6bFLMV8HRq2WRvTQ7GFN6HNc9bX3CIvQDFQu5Uq0CCcbxVCxECXLrHrF-ye3dXrVGnqrqlJFOL6KD52TfmCgcG3ZyYvIL3rdNbtJS_2V9YEDVP2Z4FE4V1cbdhJElCc4m1zJ-9wqgxlK4ZfSmIMuesow\",\"kid\":\"0Zql3wTEz1IUwBOfI8C_61gnrjFsPKKR647ykzxC6vc\",\"kty\":\"RSA\",\"use\":\"sig\"},{\"alg\":\"RS256\",\"e\":\"AQAB\",\"n\":\"0rXQn8eG3wUX9PGljgMA_Sk4t0cgdEnhvakbpRp-2MThTj7QEeEdghnj-z6iZNoAf1H1ztOtxk6dJnu3Gw31nKLBlTv1NL0IoC-lRLF_XDhot_l7QjO7G_uNb2SJHl28A1BsMCEDGjtqWSNrCsxhIMMXXAy-th7FOACOttxNg-pMsTZfuAJ76wMMB1qLXYFT6TE_qtLY6Tm9kMvJ7fbzWHOUGjhOQiKBFagXdhCU1vxp2QyZZLOdNZq3EWCib9-q9LhIu4SbHMy4g0iq_5mPd_Cp4vu-6qDGUUPijkv_3lC5KYZGS1L65aWoM5kZnLINmp6FmtplaKo3ya7kWbjoXw\",\"kid\":\"xvnZdECoH4TP04JoyLr-T9MVbR_ZuMguc5G61P5UULk\",\"kty\":\"RSA\",\"use\":\"sig\"}]}Ø", Object.class);
    }

}
