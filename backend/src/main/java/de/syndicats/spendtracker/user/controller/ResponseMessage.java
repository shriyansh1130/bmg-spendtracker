package de.syndicats.spendtracker.user.controller;

import java.util.Objects;

/**
 * Created by kailehmann on 26.09.17.
 * <p>
 * Simple ResponseMessage POJO
 */
public class ResponseMessage {

    private final Type type;
    private final String message;

    private ResponseMessage(Type type, String message) {
        this.type = type;
        this.message = message;
    }

    public static ResponseMessage createSuccess(String message) {
        return new ResponseMessage(Type.SUCCESS, Objects.requireNonNull(message));
    }

    public static ResponseMessage createError(String message) {
        return new ResponseMessage(Type.ERROR, Objects.requireNonNull(message));
    }

    public Type getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "ResponseMessage{" +
                "type=" + type +
                ", message='" + message + '\'' +
                '}';
    }

    private enum Type {
        ERROR, SUCCESS
    }

}
