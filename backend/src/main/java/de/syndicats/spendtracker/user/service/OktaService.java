package de.syndicats.spendtracker.user.service;

import com.okta.spring.oauth.OktaOAuthProperties;
import de.syndicats.spendtracker.currency.entity.Currency;
import de.syndicats.spendtracker.currency.repository.CurrencyRepository;
import de.syndicats.spendtracker.user.entity.User;
import de.syndicats.spendtracker.user.repository.UserRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.codehaus.jackson.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Map;

/**
 * Created by kailehmann on 04.02.18.
 */
@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class OktaService {

    @SuppressWarnings("unused")
    private static final String USER_ENDPOINT_TEMPLATE = "%s/api/v1/apps/%s/users";

    @Value("${okta.oauth.apikey}")
    public String oktaApiKey;

    @Value("${okta.oauth.url}")
    public String oktaUrl;

    @SuppressWarnings("unused")
    @Autowired
    private OktaOAuthProperties oktaProperties;

    @NonNull
    private UserRepository userRepository;

    @NonNull
    private CurrencyRepository currencyRepository;

    @Autowired
    private TokenStore tokenStore;


    @Transactional(isolation = Isolation.SERIALIZABLE)
    public User syncUserFromOkta(String oktaUserId) {

        Authentication authentication = SecurityContextHolder.getContext()
                .getAuthentication();

        Map<String, Object> extraInfo = getExtraInfo((OAuth2Authentication) authentication);

        User user = userRepository.findByUserId(oktaUserId).orElse(User.builder().userId(oktaUserId).build());

        user.setFullname(extraInfo.get("name")
                .toString());
        user.setEmail(extraInfo.get("email")
                .toString());

        Currency userCurrency = extractUserCurrency(extraInfo);
        user.setDefaultCurrency(userCurrency);

        return userRepository.save(user);

        // String clientId = oktaProperties.getClientId();
        //
        // String usersEndpoint = String.format(USER_ENDPOINT_TEMPLATE, oktaUrl,
        // clientId) + "/" + oktaUserId;
        //
        // HttpHeaders headers = new HttpHeaders();
        // headers.add(HttpHeaders.AUTHORIZATION, "SSWS " + oktaApiKey);
        // headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        // headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        //
        //
        // HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        // RestTemplate restTemplate = new RestTemplate();
        // ResponseEntity<String> respEntity = restTemplate.exchange(usersEndpoint,
        // HttpMethod.GET, entity, String.class);
        // JsonNode userNode = new ObjectMapper().readTree(respEntity.getBody());
        //
        //
        // User user = updateUserFromNode(userNode);
        // return user;
    }

    private Currency extractUserCurrency(Map<String, Object> extraInfo) {
        Currency currency = null;
        if (extraInfo.containsKey("currency")) {
            String currencyAttr = extraInfo.get("currency")
                    .toString();
            currency = currencyRepository.findByCurrencyCode(currencyAttr);
        }
        if (null == currency) {
            currency = currencyRepository.findByCurrencyCode("USD");
        }
        return currency;
    }

    @Transactional
    public void syncUsersFromOkta() throws IOException {

        return;

        // String clientId = oktaProperties.getClientId();
        //
        // String usersEndpoint = String.format(USER_ENDPOINT_TEMPLATE, oktaUrl,
        // clientId);
        //
        // HttpHeaders headers = new HttpHeaders();
        // headers.add(HttpHeaders.AUTHORIZATION, "SSWS " + oktaApiKey);
        // headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        // headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        //
        //
        // HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        // RestTemplate restTemplate = new RestTemplate();
        // ResponseEntity<String> respEntity = restTemplate.exchange(usersEndpoint,
        // HttpMethod.GET, entity, String.class);
        //
        //
        // JsonNode userNodes = new ObjectMapper().readTree(respEntity.getBody());
        //
        //
        // userNodes.getElements()
        // .forEachRemaining(this::updateUserFromNode);
    }

    @SuppressWarnings("unused")
    private User updateUserFromNode(JsonNode userNode) {
        String userId = userNode.get("id")
                .asText();

        JsonNode profileNode = userNode.get("profile");
        String name = profileNode.get("name")
                .asText();
        String email = profileNode.get("email")
                .asText();


        User user = userRepository.findByUserId(userId).orElse(User.builder().userId(userId).build());
        user.setFullname(name);
        user.setEmail(email);

        log.info("Synced User {}", user);

        userRepository.save(user);
        return user;

    }

    public Map<String, Object> getExtraInfo(OAuth2Authentication auth) {
        OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) auth.getDetails();
        OAuth2AccessToken accessToken = tokenStore.readAccessToken(details.getTokenValue());
        return accessToken.getAdditionalInformation();
    }

}
