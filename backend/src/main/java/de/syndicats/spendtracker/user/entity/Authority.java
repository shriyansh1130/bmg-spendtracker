package de.syndicats.spendtracker.user.entity;

public class Authority {
    public static final String ADMIN = "bmg-spendtracker-admin";
    public static final String USER_RESTRICTED = "bmg-spendtracker-user-restricted";
    public static final String USER = "bmg-spendtracker-user";
    public static final String READONLY = "bmg-spendtracker-readonly";
}
