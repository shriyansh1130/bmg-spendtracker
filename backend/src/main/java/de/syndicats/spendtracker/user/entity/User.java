package de.syndicats.spendtracker.user.entity;

import de.syndicats.spendtracker.application.entity.AbstractBaseModel;
import de.syndicats.spendtracker.currency.entity.Currency;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "ApplicationUser")
@ToString()
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder()
public class User extends AbstractBaseModel {

    @NonNull
    @Column(unique = true)
    private String userId;

    private String fullname;

    private String email;

    @ManyToOne
    private Currency defaultCurrency;

}
