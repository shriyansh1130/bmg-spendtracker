package de.syndicats.spendtracker.vendor.data;

import de.syndicats.spendtracker.company.entity.Company;
import de.syndicats.spendtracker.company.repository.CompanyRepository;
import de.syndicats.spendtracker.importer.data.AbstractCSVManager;
import de.syndicats.spendtracker.importer.data.ImportResult;
import de.syndicats.spendtracker.vendor.entity.Vendor;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@Component("vendor")
@AllArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
@SuppressWarnings("squid:S1075")
public class VendorImporter extends AbstractCSVManager<Vendor, Long> {
    private static final String ID = "id";
    private static final String CITY = "city";
    private static final String STREET = "street";
    private static final String DELETE = "delete";
    private static final String SAP_COMPANY_CODE = "sapCompanyCode";
    private static final String RECONCILIATION_ACCOUNT_NUMBER = "reconciliationAccountNumber";
    private static final String ACCOUNT_NUMBER = "accountNumber";
    private static final String NAME = "name";
    private static final String POSTAL_CODE = "postalCode";
    private static final String HOUSE_NUMBER = "houseNumber";
    private static final String REGION = "region";
    private static final String CONTACT_NAME = "contactName";
    private static final String EMAIL = "email";
    private static final String PHONE_NUMBER = "phoneNumber";
    private static final String GROUP = "group";
    private static final String VAT_REGISTRATION_NUMBER = "vatRegistrationNumber";


    private static final String[] HEADERS = {ID, SAP_COMPANY_CODE, RECONCILIATION_ACCOUNT_NUMBER, ACCOUNT_NUMBER, NAME, CITY, POSTAL_CODE, STREET, HOUSE_NUMBER, REGION, CONTACT_NAME, EMAIL, PHONE_NUMBER, GROUP, VAT_REGISTRATION_NUMBER, DELETE};

    private CompanyRepository companyRepository;

    @Override
    protected String[] getHeaders() {
        return HEADERS;
    }

    @Override
    protected Vendor createEntityFromRecord(CSVRecord record) {
        return Vendor.builder()
                .accountNumber(getAccountNumber(record))
                .name1(getName(record))
                .postcode(getPostcode(record))
                .city(getCity(record))
                .street(getStreet(record))
                .houseNumber(getHouseNumber(record))
                .region(getRegion(record))
                .contactName(getContactName(record))
                .email(getEmail(record))
                .phoneNumber(getPhoneNumber(record))
                .group(getGroup(record))
                .company(getCompany(record))
                .reconciliationAccountNumber(getReconciliationAccountNumber(record))
                .vatRegistrationNumber(getVatRegistrationNumber(record))
                .build();
    }

    private Company getCompany(CSVRecord record) {
        return companyRepository.findBySapCompanyCode(getCompanyCode(record));
    }

    private String getCompanyCode(CSVRecord record) {
        return record.get(SAP_COMPANY_CODE);
    }

    private String getVatRegistrationNumber(CSVRecord record) {
        return record.get(VAT_REGISTRATION_NUMBER);
    }

    private String getGroup(CSVRecord record) {
        return record.get(GROUP);
    }

    private String getPhoneNumber(CSVRecord record) {
        return record.get(PHONE_NUMBER);
    }

    private String getEmail(CSVRecord record) {
        return record.get(EMAIL);
    }

    private String getContactName(CSVRecord record) {
        return record.get(CONTACT_NAME);
    }

    private String getRegion(CSVRecord record) {
        return record.get(REGION);
    }

    private String getHouseNumber(CSVRecord record) {
        return record.get(HOUSE_NUMBER);
    }

    private String getStreet(CSVRecord record) {
        return record.get(STREET);
    }

    private String getPostcode(CSVRecord record) {
        return record.get(POSTAL_CODE);
    }

    private String getCity(CSVRecord record) {
        return record.get(CITY);
    }

    private String getName(CSVRecord record) {
        return record.get(NAME);
    }

    private BigDecimal getAccountNumber(CSVRecord record) {
        return new BigDecimal(record.get(ACCOUNT_NUMBER));
    }

    private String getReconciliationAccountNumber(CSVRecord record) {
        return record.get(RECONCILIATION_ACCOUNT_NUMBER);
    }

    @Override
    protected boolean validateAgainstAllRecords(CSVRecord recordToValidate, List<CSVRecord> allRecords, ImportResult result, int line) {
        return true;
    }

    @Override
    protected boolean validateRecord(CSVRecord recordToValidate, ImportResult result, int line) {
        // Pre-Checks / Requirements
        Company company = getCompany(recordToValidate);
        if (company == null) {
            String errorMessage = String.format("Line %d: Can't import vendor %s, because company %s does not exist", line, getAccountNumber(recordToValidate), getCompanyCode(recordToValidate));
            result.addErrorMessage(errorMessage);
            result.incrementErrorCount();
            return false;
        }
        return true;
    }

    @Override
    protected String getEntityName() {
        return "Vendor";
    }

    @Override
    protected String getInstanceIdentifier(Vendor entity) {
        return entity.getName1();
    }

    @Override
    protected boolean shouldDelete(CSVRecord currentRecord) {
        return currentRecord.isMapped(DELETE) && StringUtils.isNotEmpty(currentRecord.get(DELETE));
    }

    @Override
    protected Long getEntityId(CSVRecord currentRecord) {
        return StringUtils.isEmpty(currentRecord.get(ID)) ? null : Long.valueOf(currentRecord.get(ID));
    }

    @Override
    protected Vendor updateEntityFromRecord(Vendor vendor, CSVRecord currentRecord) {
        vendor.setAccountNumber(getAccountNumber(currentRecord));
        vendor.setName1(getName(currentRecord));
        vendor.setPostcode(getPostcode(currentRecord));
        vendor.setCity(getCity(currentRecord));
        vendor.setStreet(getStreet(currentRecord));
        vendor.setHouseNumber(getHouseNumber(currentRecord));
        vendor.setRegion(getRegion(currentRecord));
        vendor.setContactName(getContactName(currentRecord));
        vendor.setEmail(getEmail(currentRecord));
        vendor.setPhoneNumber(getPhoneNumber(currentRecord));
        vendor.setGroup(getGroup(currentRecord));
        vendor.setCompany(getCompany(currentRecord));
        vendor.setReconciliationAccountNumber(getReconciliationAccountNumber(currentRecord));
        vendor.setVatRegistrationNumber(getVatRegistrationNumber(currentRecord));
        return vendor;
    }

    @Override
    protected String getIdAsString(Long aLong) {
        return String.valueOf(aLong);
    }

    @Override
    protected String getExportSortField() {
        return "id";
    }

    @Override
    protected Iterable<?> getExportRow(Vendor v) {
        return Arrays.asList(
                v.getId(),
                v.getCompany().getSapCompanyCode(),
                v.getReconciliationAccountNumber(),
                v.getAccountNumber(),
                v.getName1(),
                v.getCity(),
                v.getPostcode(),
                v.getStreet(),
                v.getHouseNumber(),
                v.getRegion(),
                v.getContactName(),
                v.getEmail(),
                v.getPhoneNumber(),
                v.getGroup(),
                v.getVatRegistrationNumber(),
                ""
        );
    }
}