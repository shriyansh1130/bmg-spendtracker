package de.syndicats.spendtracker.vendor.entity;

import de.syndicats.spendtracker.application.entity.AbstractBaseModel;
import de.syndicats.spendtracker.company.entity.Company;
import de.syndicats.spendtracker.country.entity.Country;
import lombok.*;
import org.hibernate.annotations.Nationalized;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Table(name = "vendor")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public class Vendor extends AbstractBaseModel {

    @Column(name = "account_number")
    @NumberFormat(pattern = "#0000000")
    @DecimalMin("0")
    @DecimalMax("9999999")
    @Digits(integer = 7, fraction = 0)
    private BigDecimal accountNumber;

    @Nationalized
    @Column(name = "name1", nullable = false, length = 50)
    @Size(max = 50)
    @NonNull
    private String name1;

    @Nationalized
    @Column(name = "name2", length = 50)
    @Size(max = 50)
    private String name2;

    @Nationalized
    @Column(name = "name3", length = 50)
    @Size(max = 50)
    private String name3;

    @Nationalized
    @Column(name = "contact_name", length = 50)
    @Size(max = 50)
    private String contactName;

    @Nationalized
    @Column(name = "street", length = 50)
    @Size(max = 50)
    private String street;

    @Nationalized
    @Column(name = "house_number", length = 50)
    @Size(max = 50)
    private String houseNumber;

    @Nationalized
    @Column(name = "city")
    private String city;

    @Nationalized
    @Column(name = "region")
    private String region;

    @Nationalized
    @Column(name = "postcode", length = 50)
    @Size(max = 50)
    private String postcode;

    @Nationalized
    @Column(name = "vat_registration_number", length = 20)
    @Size(max = 20)
    private String vatRegistrationNumber;

    @Nationalized
    @Column(name = "phone_number")
    private String phoneNumber;

    @Nationalized
    @Column(name = "email")
    private String email;

    @Column(name = "reconciliation_account_number")
    @NonNull
    private String reconciliationAccountNumber;

    @ManyToOne(optional = false)
    @JoinColumn(name = "company_id", referencedColumnName = "id", nullable = false)
    @NonNull
    private Company company;

    /* 'group' is used by sql, therefore column name 'grp' */
    @Nationalized
    @Column(name = "grp", length = 4)
    @Size(max = 4)
    private String group;

    public String getUniqueId(){
        return company.getSapCompanyCode() + accountNumber;
    }
}
