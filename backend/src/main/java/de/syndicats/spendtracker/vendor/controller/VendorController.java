package de.syndicats.spendtracker.vendor.controller;

import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.country.repository.CountryRepository;
import de.syndicats.spendtracker.project.entity.Project;
import de.syndicats.spendtracker.project.repository.ProjectRepository;
import de.syndicats.spendtracker.vendor.entity.Vendor;
import de.syndicats.spendtracker.vendor.repository.VendorRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Collectors;

@RepositoryRestController
@RequestMapping("/api/vendors")
@AllArgsConstructor
public class VendorController {


    private VendorRepository vendorRepository;

    private ProjectRepository projectRepository;

    private CountryRepository countryRepository;

    private PagedResourcesAssembler<Vendor> pagedResourcesAssembler;

    @SuppressWarnings("unchecked")
    @GetMapping(value = "search/validVendors")
    public ResponseEntity<PagedResources<PersistentEntityResource>> validVendors(
            @RequestParam("countryId") Long countryId,
            @RequestParam("projectId") Long projectId,
            @RequestParam(value="name", required = false) String name,
            PersistentEntityResourceAssembler persistentEntityResourceAssembler
            ) {
        Page<Vendor> vendors;
        if (name == null) {
            name = "%";
        }

        Country country = countryRepository.findById(countryId).orElseThrow(() -> new RuntimeException("No country found with id=" + countryId));
        Project project = projectRepository.findById(projectId).orElseThrow(() -> new RuntimeException("No project found with id=" + projectId));

        if (country.equals(project.getRepertoireOwnerCompany().getCountry())) {
            vendors = vendorRepository.findByCompanyAndName1IgnoreCaseContaining(project.getRepertoireOwnerCompany(), name, PageRequest.of(0,25));
        } else {
            vendors = vendorRepository.findByCompany_CountryAndCompany_InternationalAndName1IgnoreCaseContaining(country, true, name, PageRequest.of(0,25));
        }

        return new ResponseEntity<>(pagedResourcesAssembler.toResource(vendors, persistentEntityResourceAssembler::toResource), HttpStatus.OK);
    }


    @SuppressWarnings("unchecked")
    @GetMapping(value = "search/similarVendors")
    public ResponseEntity<List<Vendor>> validVendors(
            @RequestParam("vendorName") String vendorName,
            @RequestParam("street") String street,
            @RequestParam("city") String city,
            @RequestParam("postcode") String postcode,
            @RequestParam("linkedCompanyId") Long companyId,
            PersistentEntityResourceAssembler persistentEntityResourceAssembler
    ) {
        List<Vendor> vendors = vendorRepository.findSimilar(vendorName, street, city, postcode, companyId);
        return new ResponseEntity<>(vendors, HttpStatus.OK);
    }
}

