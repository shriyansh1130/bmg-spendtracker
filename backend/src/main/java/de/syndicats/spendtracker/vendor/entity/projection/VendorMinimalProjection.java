package de.syndicats.spendtracker.vendor.entity.projection;

import de.syndicats.spendtracker.vendor.entity.Vendor;
import org.springframework.data.rest.core.config.Projection;


@Projection(name = "vendorMinimalProjection", types = {Vendor.class})
public interface VendorMinimalProjection {

    String getName1();

}
