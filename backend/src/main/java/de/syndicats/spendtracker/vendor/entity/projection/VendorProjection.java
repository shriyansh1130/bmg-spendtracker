package de.syndicats.spendtracker.vendor.entity.projection;

import de.syndicats.spendtracker.company.entity.projection.CompanyProjection;
import de.syndicats.spendtracker.country.entity.projection.CountryProjection;
import de.syndicats.spendtracker.vendor.entity.Vendor;
import org.springframework.data.rest.core.config.Projection;

import java.math.BigDecimal;


@Projection(name = "vendorProjection", types = {Vendor.class})
public interface VendorProjection {

    BigDecimal getAccountNumber();

    String getName1();

    String getName2();

    String getName3();

    String getContactName();

    String getStreet();

    String getHouseNumber();

    String getCity();

    String getRegion();

    String getPostcode();

    String getVatRegistrationNumber();

    String getReconciliationAccountNumber();

    CompanyProjection getCompany();

    String getGroup();
}
