package de.syndicats.spendtracker.vendor.repository;

import de.syndicats.spendtracker.company.entity.Company;
import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.vendor.entity.Vendor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "vendors", path = "vendors")
@SuppressWarnings("squid:S00100")
public interface VendorRepository extends JpaRepository<Vendor, Long> {

    Page<Vendor> findByCompanyAndName1IgnoreCaseContaining(@Param("company") Company company, @Param("name1") String name1, Pageable pageable);

    Page<Vendor> findByCompany_CountryAndCompany_InternationalAndName1IgnoreCaseContaining(@Param("country") Country country, @Param("international") boolean international, @Param("name1") String name1, Pageable pageable);

    Page<Vendor> findByCompany_Country_IdAndName1IgnoreCaseContaining(@Param("countryId") Long countryId, @Param("name1") String name1, Pageable pageable);

    List<Vendor> findByVatRegistrationNumberAndCompany_SapCompanyCode(@Param("vatRegistrationNumber") String vatRegistrationNumber, @Param("sapCompanyCode") String sapCompanyCode);

    @Query("SELECT v FROM Vendor v WHERE (v.city = :city OR v.postcode = :postcode) AND ((v.name1 <> '' AND v.name1 = :vendorName) OR (v.street <> '' AND v.street = :street)) AND (v.company.id = :companyId)")
    List<Vendor> findSimilar(@Param("vendorName") String vendorName, @Param("street") String street, @Param("city") String city, @Param("postcode") String postcode, @Param("companyId") Long companyId);
}