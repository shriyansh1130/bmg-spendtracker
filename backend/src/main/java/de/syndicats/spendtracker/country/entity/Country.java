package de.syndicats.spendtracker.country.entity;

import de.syndicats.spendtracker.application.entity.AbstractBaseModel;
import de.syndicats.spendtracker.currency.entity.Currency;
import lombok.*;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Table(name = "country")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public class Country extends AbstractBaseModel {

    @Nationalized
    @Column(name = "country_name", unique = true, nullable = false, length = 150)
    @Size(max = 150)
    @NonNull
    private String countryName;

    @Nationalized
    @Column(name = "country_code", unique = true, nullable = false, length = 2)
    @Size(max = 2)
    @NonNull
    private String countryCode;

    @ManyToOne
    @JoinColumn(name = "currency_id", referencedColumnName = "id")
    private Currency currency;

    private boolean sapCountry;

    private boolean poCountry;

    private String affiliateBudget;

}