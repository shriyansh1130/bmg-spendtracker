package de.syndicats.spendtracker.country.entity.projection;

import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.currency.entity.projection.CurrencyProjection;
import org.springframework.data.rest.core.config.Projection;


@Projection(name = "countryCurrencyProjection", types = {Country.class})
public interface CountryCurrencyProjection {
    String getCountryName();
    CurrencyProjection getCurrency();
}
