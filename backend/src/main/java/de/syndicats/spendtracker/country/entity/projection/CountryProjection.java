package de.syndicats.spendtracker.country.entity.projection;

import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.currency.entity.projection.CurrencyProjection;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "countryProjection", types = {Country.class})
public interface CountryProjection {

    String getCountryName();

    String getCountryCode();

    CurrencyProjection getCurrency();

    boolean isSapCountry();

    boolean isPoCountry();

    String getAffiliateBudget();

}
