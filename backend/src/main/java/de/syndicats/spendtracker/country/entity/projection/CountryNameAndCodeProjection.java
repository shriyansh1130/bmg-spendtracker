package de.syndicats.spendtracker.country.entity.projection;

import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.currency.entity.projection.CurrencyProjection;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "countryNameAndCodeProjection", types = {Country.class})
public interface CountryNameAndCodeProjection {

    String getCountryName();

    String getCountryCode();
}
