package de.syndicats.spendtracker.country.data;

import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.currency.entity.Currency;
import de.syndicats.spendtracker.currency.repository.CurrencyRepository;
import de.syndicats.spendtracker.importer.data.AbstractCSVManager;
import de.syndicats.spendtracker.importer.data.ImportResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;


@Component("country")
@AllArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class CountryImporter extends AbstractCSVManager<Country, Long> {
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String CODE = "code";
    private static final String CURRENCY = "currency";
    private static final String SAP_COUNTRY = "SAP Country";
    private static final String PO_COUNTRY = "PO Country";
    private static final String AFFILIATE_BUDGET = "Affiliate Budget";
    private static final String CURRENCY_CODE = "Curency Code";
    private static final String CURRENCY_CODE_DESCRIPTION = "Currency Code Description";
    private static final String DELETE = "delete";

    private static final String[] HEADERS = {ID, NAME, CODE, CURRENCY, SAP_COUNTRY, PO_COUNTRY, AFFILIATE_BUDGET, CURRENCY_CODE, CURRENCY_CODE_DESCRIPTION, DELETE};

    private CurrencyRepository currencyRepository;

    @Override
    protected String[] getHeaders() {
        return HEADERS;
    }

    @Override
    protected Country createEntityFromRecord(CSVRecord record) {
        Currency currency = currencyRepository.findByCurrencyCode(record.get(CURRENCY_CODE));
        return Country.builder()
                .countryCode(record.get(CODE))
                .countryName(record.get(NAME))
                .poCountry("Yes".equalsIgnoreCase(record.get(PO_COUNTRY)))
                .sapCountry("Yes".equalsIgnoreCase(record.get(SAP_COUNTRY)))
                .currency(currency)
                .affiliateBudget(record.get(AFFILIATE_BUDGET))
                .build();
    }

    @Override
    protected boolean validateAgainstAllRecords(CSVRecord recordToValidate, List<CSVRecord> allRecords, ImportResult result, int line) {
        return true;
    }

    @Override
    protected boolean validateRecord(CSVRecord recordToValidate, ImportResult result, int line) {
        return true;
    }

    @Override
    protected String getEntityName() {
        return "Country";
    }

    @Override
    protected String getInstanceIdentifier(Country entity) {
        return entity.getCountryName();
    }

    @Override
    protected boolean shouldDelete(CSVRecord currentRecord) {
        return currentRecord.isMapped(DELETE) && StringUtils.isNotEmpty(currentRecord.get(DELETE));
    }

    @Override
    protected Long getEntityId(CSVRecord currentRecord) {
        return StringUtils.isEmpty(currentRecord.get(ID)) ? null : Long.valueOf(currentRecord.get(ID));
    }

    @Override
    protected Country updateEntityFromRecord(Country country, CSVRecord currentRecord) {
        Currency currency = currencyRepository.findByCurrencyCode(currentRecord.get(CURRENCY_CODE));
        if (currency != null) {
            country.setCurrency(currency);
        }

        country.setCountryCode(currentRecord.get(CODE));
        country.setCountryName(currentRecord.get(NAME));
        country.setPoCountry("Yes".equalsIgnoreCase(currentRecord.get(PO_COUNTRY)));
        country.setSapCountry("Yes".equalsIgnoreCase(currentRecord.get(SAP_COUNTRY)));
        country.setAffiliateBudget(currentRecord.get(AFFILIATE_BUDGET));
        return country;
    }

    @Override
    protected String getIdAsString(Long aLong) {
        return String.valueOf(aLong);
    }

    @Override
    protected String getExportSortField() {
        return "id";
    }

    @Override
    protected Iterable<?> getExportRow(Country c) {
        Currency currency = c.getCurrency();
        String currencyName = currency != null ? currency.getCurrencyName() : "";
        String currencyCode = currency != null ? currency.getCurrencyCode() : "";
        String currencySymbol = currency != null ? currency.getCurrencySymbol() : "";

        return Arrays.asList(
                c.getId(),
                c.getCountryName(),
                c.getCountryCode(),
                currencyName,
                c.isSapCountry() ? "Yes" : "No",
                c.isPoCountry() ? "Yes" : "No",
                c.getAffiliateBudget(),
                currencyCode,
                currencySymbol,
                "");
    }
}