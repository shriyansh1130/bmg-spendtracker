package de.syndicats.spendtracker.country.repository;

import de.syndicats.spendtracker.country.entity.Country;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "countries", path = "countries")
public interface CountryRepository extends JpaRepository<Country, Long> {

    // for masterdata we don't allow this method
    @Override
    @RestResource(exported = false)
    <S extends Country> S save(S entity);

    // for masterdata we don't allow this method
    @Override
    @RestResource(exported = false)
    void delete(Country entity);

    Country findByCountryCode(@Param("countryCode") String countryCode);

    Country findByCountryName(String s);

    Page<Country> findBySapCountryIsTrue(Pageable pageable);

    Page<Country> findByPoCountryIsTrue(Pageable pageable);

    Page<Country> findByCountryNameIgnoreCaseContaining(@Param("countryName") String countryName, Pageable p);

    List<Country> findAllByCountryNameIgnoreCaseContaining(@Param("countryName") String countryName);
}