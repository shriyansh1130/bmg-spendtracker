package de.syndicats.spendtracker.country.controller;

import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.country.repository.CountryRepository;
import de.syndicats.spendtracker.posting.repository.PostingRepository;
import de.syndicats.spendtracker.project.entity.Project;
import de.syndicats.spendtracker.project.repository.ProjectRepository;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderStatus;
import de.syndicats.spendtracker.purchaseorder.repository.PurchaseOrderPositionRepository;
import lombok.AllArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@RepositoryRestController
@RequestMapping("/api/countries")
public class CountryCustomSearchController {

    private CountryRepository countryRepository;
    private ProjectRepository projectRepository;
    private PostingRepository postingRepository;
    private PurchaseOrderPositionRepository purchaseOrderPositionRepository;

    @GetMapping("search/findForProjectWithData")
    public @ResponseBody
    Resources<PersistentEntityResource> findForProjectWithData(@RequestParam("projectId") Long projectId, @RequestParam("countryName") String countryName, PersistentEntityResourceAssembler persistentEntityResourceAssembler) {
        if (countryName == null || StringUtils.isEmpty(countryName)) {
            countryName = "%";
        }

        Project project = projectRepository.findById(projectId).orElseThrow(() -> new RuntimeException("No project found with id=" + projectId));

        List<Country> countries = countryRepository.findAllByCountryNameIgnoreCaseContaining(countryName);

        List<PersistentEntityResource> resources = countries.stream().filter((country) ->
                postingRepository.existsByProjectAndBudgetTerritory(project, country) ||
                        purchaseOrderPositionRepository.existsByProjectAndPoCountryAndPurchaseOrder_StatusIn(project, country, Arrays.asList(PurchaseOrderStatus.Closed, PurchaseOrderStatus.Open)))
                .map(persistentEntityResourceAssembler::toResource)
                .collect(Collectors.toList());

        return new Resources<>(resources);
    }

}
