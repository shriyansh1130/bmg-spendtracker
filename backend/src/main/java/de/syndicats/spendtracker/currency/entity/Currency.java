package de.syndicats.spendtracker.currency.entity;

import de.syndicats.spendtracker.application.entity.AbstractBaseModel;
import lombok.*;
import org.hibernate.annotations.Nationalized;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Table(name = "currency")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public class Currency extends AbstractBaseModel {

    @Nationalized
    @Column(name = "currency_name", nullable = false, length = 150)
    @Size(max = 150)
    @NonNull
    private String currencyName;

    @Nationalized
    @Column(name = "currency_code", unique = true, nullable = false, length = 3)
    @Size(max = 3)
    @NonNull
    private String currencyCode;

    @Nationalized
    @Column(name = "currency_symbol", nullable = false, length = 3)
    @Size(max = 3)
    @NonNull
    private String currencySymbol;

}
