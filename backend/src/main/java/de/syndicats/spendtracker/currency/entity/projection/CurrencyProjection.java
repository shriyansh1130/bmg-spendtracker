package de.syndicats.spendtracker.currency.entity.projection;

import de.syndicats.spendtracker.currency.entity.Currency;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "currencyProjection", types = {Currency.class})
public interface CurrencyProjection {
    String getCurrencyName();

    String getCurrencyCode();

    String getCurrencySymbol();

}
