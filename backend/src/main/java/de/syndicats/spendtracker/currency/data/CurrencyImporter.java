package de.syndicats.spendtracker.currency.data;

import de.syndicats.spendtracker.currency.entity.Currency;
import de.syndicats.spendtracker.importer.data.AbstractCSVManager;
import de.syndicats.spendtracker.importer.data.ImportResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component("currency")
@AllArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class CurrencyImporter extends AbstractCSVManager<Currency, Long> {


    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String CODE = "code";
    private static final String SYMBOL = "symbol";
    private static final String DELETE = "delete";
    private static final String[] HEADERS = {ID, CODE, NAME, SYMBOL, DELETE};

    @Override
    protected String[] getHeaders() {
        return HEADERS;
    }

    @Override
    protected Currency createEntityFromRecord(CSVRecord record) {
        return Currency.builder()
                .currencyCode(getCode(record))
                .currencyName(getName(record))
                .currencySymbol(getSymbol(record)).build();
    }

    private String getSymbol(CSVRecord record) {
        return record.get(SYMBOL);
    }

    private String getName(CSVRecord record) {
        return record.get(NAME);
    }

    private String getCode(CSVRecord record) {
        return record.get(CODE);
    }

    @Override
    protected boolean validateAgainstAllRecords(CSVRecord recordToValidate, List<CSVRecord> allRecords, ImportResult result, int line) {
        return true;
    }

    @Override
    protected boolean validateRecord(CSVRecord recordToValidate, ImportResult result, int line) {
        return true;
    }

    @Override
    protected String getEntityName() {
        return "Currency";
    }

    @Override
    protected String getInstanceIdentifier(Currency entity) {
        return entity.getCurrencyName();
    }

    @Override
    protected boolean shouldDelete(CSVRecord currentRecord) {
        return currentRecord.isMapped(DELETE) && StringUtils.isNotEmpty(currentRecord.get(DELETE));
    }

    @Override
    protected Long getEntityId(CSVRecord currentRecord) {
        return StringUtils.isEmpty(currentRecord.get(ID)) ? null : Long.valueOf(currentRecord.get(ID));
    }

    @Override
    protected Currency updateEntityFromRecord(Currency currency, CSVRecord currentRecord) {
        currency.setCurrencyCode(getCode(currentRecord));
        currency.setCurrencyName(getName(currentRecord));
        currency.setCurrencySymbol(getSymbol(currentRecord));
        return currency;
    }

    @Override
    protected String getIdAsString(Long aLong) {
        return String.valueOf(aLong);
    }

    @Override
    protected String getExportSortField() {
        return "id";
    }

    @Override
    protected Iterable<?> getExportRow(Currency currency) {
        return Arrays.asList(
                currency.getId(),
                currency.getCurrencyCode(),
                currency.getCurrencyName(),
                currency.getCurrencySymbol(),
                ""
        );
    }
}
