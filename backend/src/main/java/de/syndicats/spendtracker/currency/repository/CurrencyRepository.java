package de.syndicats.spendtracker.currency.repository;

import de.syndicats.spendtracker.currency.entity.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.validation.constraints.Size;

@RepositoryRestResource(collectionResourceRel = "currencies", path = "currencies")
public interface CurrencyRepository extends JpaRepository<Currency, Long> {

    Currency findByCurrencyCode(String currencyCode);

    // for masterdata we don't allow this method
    @Override
    @RestResource(exported = false)
    <S extends Currency> S save(S entity);

    // for masterdata we don't allow this method
    @Override
    @RestResource(exported = false)
    void delete(Currency entity);

    boolean existsByCurrencyCode(@Size(max = 3) String currencyCode);
}