package de.syndicats.spendtracker.project.service;

import de.syndicats.spendtracker.budget.entity.BudgetCategory;
import de.syndicats.spendtracker.budget.entity.BudgetSpendType;
import de.syndicats.spendtracker.budget.entity.BudgetSubCategory;
import de.syndicats.spendtracker.budget.repository.BudgetSpendTypeRepository;
import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.currency.entity.Currency;
import de.syndicats.spendtracker.exchangerate.services.ExchangeRateService;
import de.syndicats.spendtracker.posting.entity.Posting;
import de.syndicats.spendtracker.posting.repository.PostingRepository;
import de.syndicats.spendtracker.project.entity.Project;
import de.syndicats.spendtracker.project.service.dto.ProjectBudgetCategoryDto;
import de.syndicats.spendtracker.project.service.dto.ProjectBudgetContainerDto;
import de.syndicats.spendtracker.project.service.dto.ProjectBudgetSpendTypeDto;
import de.syndicats.spendtracker.project.service.dto.ProjectBudgetSubCategoryDto;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderStatus;
import de.syndicats.spendtracker.purchaseorder.repository.PurchaseOrderPositionRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ProjectService {

    private BudgetSpendTypeRepository budgetSpendTypeRepository;
    private PostingRepository postingRepository;
    private PurchaseOrderPositionRepository purchaseOrderPositionRepository;
    private ExchangeRateService exchangeRateService;

    /**
     * Gathers all budget information for the specified project and country.
     *
     * @param project the project for which the budgets are returned
     * @param country country for which the budgets are returned
     * @return the collection of {@link ProjectBudgetCategoryDto} with its children for the given project filtered by undrafted purchase order positions.*
     * @implNote We go through ALL BudgetSpendTypes here because we want empty values too. Also, we build the tree bottom-up, as all calculations are done on leaf level and higher levels are just aggregations.
     */
    public ProjectBudgetContainerDto getProjectBudgetDtos(Project project, Country country) {
        List<BudgetSpendType> budgetSpendTypes = budgetSpendTypeRepository.findAll(Sort.by("budgetSpendTypeCode"));

        List<Posting> postings = postingRepository.findByProject(project).stream()
                .filter(posting -> posting.getPurchaseOrderPosition() == null ? country.equals(posting.getBudgetTerritory()) : posting.getPurchaseOrderPosition().getPoCountry().equals(country))
                .collect(Collectors.toList());

        List<PurchaseOrderPosition> positions = purchaseOrderPositionRepository.findByProjectAndPoCountry(project, country);

        Map<BudgetSpendType, List<Posting>> postingsPerSpendType = postings.stream().collect(Collectors.groupingBy(Posting::getBudgetSpendType, Collectors.toList()));

        //We only need undrafted purchase order positions
        Map<BudgetSpendType, List<PurchaseOrderPosition>> positionsPerSpendType = positions.stream()
                .filter(position -> !PurchaseOrderStatus.Draft.equals(position.getPurchaseOrder().getStatus())).collect(Collectors.groupingBy(PurchaseOrderPosition::getSpendType, Collectors.toList()));

        Map<BudgetSubCategory, ProjectBudgetSubCategoryDto> budgetSubCategoryBudgets = new HashMap<>();
        Map<BudgetCategory, ProjectBudgetCategoryDto> budgetCategoryBudgets = new HashMap<>();

        for (BudgetSpendType budgetSpendType : budgetSpendTypes) {
            BudgetSubCategory budgetSubCategory = budgetSpendType.getBudgetSubCategory();
            BudgetCategory budgetCategory = budgetSubCategory.getBudgetCategory();

            budgetCategoryBudgets.putIfAbsent(budgetCategory, new ProjectBudgetCategoryDto(budgetCategory.getId(), budgetCategory.getTitle()));
            budgetSubCategoryBudgets.putIfAbsent(budgetSubCategory, new ProjectBudgetSubCategoryDto(budgetSubCategory.getId(), budgetSubCategory.getTitle()));

            BigDecimal paid = Optional.ofNullable(postingsPerSpendType.get(budgetSpendType)).orElse(new ArrayList<>()).stream().map(posting -> this.getPostingAmount(posting, country)).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
            BigDecimal openPo = Optional.ofNullable(positionsPerSpendType.get(budgetSpendType)).orElse(new ArrayList<>()).stream().map(position -> this.getPositionOpenPo(position, country)).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
            BigDecimal committedSpend = paid.add(openPo);

            if (paid.compareTo(BigDecimal.ZERO) != 0 || openPo.compareTo(BigDecimal.ZERO) != 0 || committedSpend.compareTo(BigDecimal.ZERO) != 0) {
                ProjectBudgetSpendTypeDto spendTypeDto = new ProjectBudgetSpendTypeDto(budgetSpendType.getId(), budgetSpendType.getTitle(), paid, openPo, committedSpend);
                ProjectBudgetSubCategoryDto budgetSubCategoryDto = budgetSubCategoryBudgets.get(budgetSubCategory);
                budgetSubCategoryDto.addChild(spendTypeDto);
                budgetCategoryBudgets.get(budgetCategory).addChild(budgetSubCategoryDto);
            }
        }

        List<ProjectBudgetCategoryDto> categoryBudgets = budgetCategoryBudgets.values().stream().filter(projectBudgetCategoryDto -> projectBudgetCategoryDto.getChildren().size() > 0).collect(Collectors.toList());
        return new ProjectBudgetContainerDto(categoryBudgets);
    }

    private BigDecimal getPositionOpenPo(PurchaseOrderPosition position, Country country) {
        return getInCountryCurrency(country, position.getPurchaseOrder().getCurrency(), position.getOpenPo());
    }

    private BigDecimal getInCountryCurrency(Country country, Currency sourceCurrency, BigDecimal sourceOpenPo) {
        if (sourceCurrency.equals(country.getCurrency())) {
            return sourceOpenPo;
        } else {
            return exchangeRateService.calculateAmountInTargetCurrency(sourceCurrency, sourceOpenPo, country.getCurrency());
        }
    }

    private BigDecimal getPostingAmount(Posting posting, Country country) {
        return getInCountryCurrency(country, posting.getCurrency(), posting.getAmountInLocalCurrency());
    }

}