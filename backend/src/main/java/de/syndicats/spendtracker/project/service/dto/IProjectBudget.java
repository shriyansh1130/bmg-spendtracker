package de.syndicats.spendtracker.project.service.dto;

import java.math.BigDecimal;

public interface IProjectBudget {

    Long getId();

    String getTitle();

    BigDecimal getPaid();

    BigDecimal getOpenPo();

    BigDecimal getCommittedSpend();
}
