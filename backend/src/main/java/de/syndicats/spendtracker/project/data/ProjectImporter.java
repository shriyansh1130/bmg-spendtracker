package de.syndicats.spendtracker.project.data;

import de.syndicats.spendtracker.company.entity.Company;
import de.syndicats.spendtracker.company.repository.CompanyRepository;
import de.syndicats.spendtracker.importer.data.AbstractCSVManager;
import de.syndicats.spendtracker.importer.data.ImportResult;
import de.syndicats.spendtracker.project.entity.Project;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component("project")
@AllArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
@SuppressWarnings("squid:S1075")
public class ProjectImporter extends AbstractCSVManager<Project, Long> {
    private static final String ID = "id";
    private static final String SAP_CLIENT_CODE = "SAP Client Code";
    private static final String PROJECT_NAME = "Project Name";
    private static final String RO_COMPANY_CODE = "RO Company Code";
    private static final String ION_SAP = "ION SAP";
    private static final String ASSIGNMENT = "Assignment";
    private static final String LABEL = "Label";
    private static final String DEAL_TYPE = "DealType";
    private static final String SEGMENT = "Segment";
    private static final String PRIORITY = "Priority";
    private static final String UNIQUE_CODE = "Unique Code";
    private static final String DELETE = "delete";
    private static final String[] HEADERS = {ID, SAP_CLIENT_CODE, PROJECT_NAME, RO_COMPANY_CODE, ION_SAP, ASSIGNMENT, LABEL, DEAL_TYPE, SEGMENT, PRIORITY, UNIQUE_CODE, DELETE};


    private CompanyRepository companyRepository;

    @Override
    protected String[] getHeaders() {
        return HEADERS;
    }

    @Override
    protected Project createEntityFromRecord(CSVRecord record) {
        return Project.builder()
                .uniqueCode(getUniqueCode(record))
                .sapClientCode(getSapClientCode(record))
                .projectName(getProjectName(record))
                .repertoireOwnerCompany(getCompany(record))
                .ionSap(getIonSap(record))
                .assignment(getAssignment(record))
                .label(getLabel(record))
                .dealType(getDealType(record))
                .segment(getSegment(record))
                .priority(getPriority(record))
                .build();
    }

    private int getPriority(CSVRecord record) {
        return Integer.parseInt(record.get(PRIORITY));
    }

    private String getSegment(CSVRecord record) {
        return record.get(SEGMENT);
    }

    private String getDealType(CSVRecord record) {
        return record.get(DEAL_TYPE);
    }

    private String getLabel(CSVRecord record) {
        return record.get(LABEL);
    }

    private String getAssignment(CSVRecord record) {
        return record.get(ASSIGNMENT);
    }

    private String getIonSap(CSVRecord record) {
        return record.get(ION_SAP);
    }

    private String getProjectName(CSVRecord record) {
        return record.get(PROJECT_NAME).trim();
    }

    private String getSapClientCode(CSVRecord record) {
        return record.get(SAP_CLIENT_CODE);
    }

    private String getUniqueCode(CSVRecord record) {
        return record.get(UNIQUE_CODE);
    }

    @Override
    protected boolean validateAgainstAllRecords(CSVRecord recordToValidate, List<CSVRecord> allRecords, ImportResult result, int line) {
        return true;
    }

    @Override
    protected boolean validateRecord(CSVRecord recordToValidate, ImportResult result, int line) {
        // Pre-Checks / Requirements
        Company company = getCompany(recordToValidate);
        if (company == null) {
            String errorMessage = String.format("Line %d: Can't import project %s %s, because company %s does not exist", getSapClientCode(recordToValidate), getProjectName(recordToValidate), getCompanyCode(recordToValidate));
            result.addErrorMessage(errorMessage);
            result.incrementErrorCount();
            return false;
        }

        return true;
    }

    private Company getCompany(CSVRecord recordToValidate) {
        return companyRepository.findBySapCompanyCode(getCompanyCode(recordToValidate));
    }

    private String getCompanyCode(CSVRecord recordToValidate) {
        return recordToValidate.get(RO_COMPANY_CODE);
    }

    @Override
    protected String getEntityName() {
        return "Project";
    }

    @Override
    protected String getInstanceIdentifier(Project entity) {
        return entity.getProjectName();
    }

    @Override
    protected boolean shouldDelete(CSVRecord currentRecord) {
        return currentRecord.isMapped(DELETE) && StringUtils.isNotEmpty(currentRecord.get(DELETE));
    }

    @Override
    protected Long getEntityId(CSVRecord currentRecord) {
        return StringUtils.isEmpty(currentRecord.get(ID)) ? null : Long.valueOf(currentRecord.get(ID));
    }

    @Override
    protected Project updateEntityFromRecord(Project project, CSVRecord currentRecord) {
        project.setUniqueCode(getUniqueCode(currentRecord));
        project.setSapClientCode(getSapClientCode(currentRecord));
        project.setProjectName(getProjectName(currentRecord));
        project.setRepertoireOwnerCompany(getCompany(currentRecord));
        project.setIonSap(getIonSap(currentRecord));
        project.setAssignment(getAssignment(currentRecord));
        project.setLabel(getLabel(currentRecord));
        project.setDealType(getDealType(currentRecord));
        project.setSegment(getSegment(currentRecord));
        project.setPriority(getPriority(currentRecord));
        return project;
    }

    @Override
    protected String getIdAsString(Long aLong) {
        return String.valueOf(aLong);
    }

    @Override
    protected String getExportSortField() {
        return "id";
    }

    @Override
    protected Iterable<?> getExportRow(Project p) {
        return Arrays.asList(
                p.getId(),
                p.getSapClientCode(),
                p.getProjectName(),
                p.getRepertoireOwnerCompany().getSapCompanyCode(),
                p.getIonSap(),
                p.getAssignment(),
                p.getLabel(),
                p.getDealType(),
                p.getSegment(),
                p.getPriority(),
                p.getUniqueCode(),
                ""
        );
    }
}