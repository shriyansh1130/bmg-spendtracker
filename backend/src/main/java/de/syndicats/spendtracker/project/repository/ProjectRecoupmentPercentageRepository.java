package de.syndicats.spendtracker.project.repository;

import de.syndicats.spendtracker.project.entity.ProjectRecoupmentPercentage;
import de.syndicats.spendtracker.project.entity.ProjectRecoupmentPercentageId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRecoupmentPercentageRepository extends JpaRepository<ProjectRecoupmentPercentage, ProjectRecoupmentPercentageId> {
    
}