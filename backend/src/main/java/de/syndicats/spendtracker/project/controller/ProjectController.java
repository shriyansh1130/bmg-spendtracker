package de.syndicats.spendtracker.project.controller;

import de.syndicats.spendtracker.company.entity.Company;
import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.country.repository.CountryRepository;
import de.syndicats.spendtracker.project.entity.Project;
import de.syndicats.spendtracker.project.repository.ProjectRepository;
import de.syndicats.spendtracker.project.service.ProjectService;
import de.syndicats.spendtracker.project.service.dto.ProjectBudgetContainerDto;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@RepositoryRestController
@RequestMapping("/api/projects")
@AllArgsConstructor
public class ProjectController {

    private ProjectRepository projectRepository;

    private CountryRepository countryRepository;

    private PagedResourcesAssembler<Project> pagedResourcesAssembler;

    private ProjectService projectService;

    @SuppressWarnings("unchecked")
    @GetMapping(value = "search/validProjects")
    public ResponseEntity<PagedResources<PersistentEntityResource>> validProjects(@RequestParam("countryId") Long countryId,
                                                                                  @RequestParam("projectId") Long referenceProjectId,
                                                                                  @RequestParam(value = "name", required = false) String name,
                                                                                  PersistentEntityResourceAssembler persistentEntityResourceAssembler) {
        Page<Project> projects = Page.empty();
        if (name == null) {
            name = "%";
        }
        Country country = countryRepository.findById(countryId).orElseThrow(() -> new RuntimeException("No country found with id=" + countryId));
        Project project = projectRepository.findById(referenceProjectId).orElseThrow(() -> new RuntimeException("No project found with id=" + referenceProjectId));

        Company roCompany = project.getRepertoireOwnerCompany();
        Country roTerritory = roCompany.getCountry();

        if (country.equals(roTerritory) && !roCompany.isInternational()) {
            // only allow projects of roCompany
            projects = projectRepository.findByRepertoireOwnerCompanyAndProjectNameIgnoreCaseContaining(roCompany, name, PageRequest.of(0, 25));
        } else {
            if (roCompany.isInternational()) {
                // allow projects of RoCompany OR projects with RoTerritory != roTerriory AND roCompany is international
                projects = projectRepository
                        .findValidProjectsForInternationalROCompanyAndROTerritoryAndProjectName(roCompany, roTerritory, name, PageRequest.of(0, 25));
            }
        }
        return new ResponseEntity<>(pagedResourcesAssembler.toResource(projects, persistentEntityResourceAssembler::toResource), HttpStatus.OK);
    }

    @GetMapping(value = "budgets")
    @Transactional
    public ResponseEntity<ProjectBudgetContainerDto> allBudgets(@RequestParam("projectId") Long projectId, @RequestParam("countryId") Long countryId) {
        Country country = countryRepository.findById(countryId).orElseThrow(() -> new IllegalArgumentException("Invalid Country Id"));
        Project project = projectRepository.findById(projectId).orElseThrow(() -> new IllegalArgumentException("Invalid Project Id"));
        return new ResponseEntity<>(projectService.getProjectBudgetDtos(project, country), HttpStatus.OK);
    }


}

