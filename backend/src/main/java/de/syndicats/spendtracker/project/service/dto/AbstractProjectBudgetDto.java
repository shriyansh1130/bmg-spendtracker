package de.syndicats.spendtracker.project.service.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
abstract class AbstractProjectBudgetDto {

    private Long id;

    private String title;


    public AbstractProjectBudgetDto(Long id, String title) {
        this.id = id;
        this.title = title;
    }

}
