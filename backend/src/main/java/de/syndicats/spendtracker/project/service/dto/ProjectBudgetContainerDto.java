package de.syndicats.spendtracker.project.service.dto;

import lombok.Value;

import java.math.BigDecimal;
import java.util.List;

@Value
public class ProjectBudgetContainerDto {
    private List<ProjectBudgetCategoryDto> categoryBudgets;

    public BigDecimal getCommittedSpend() {
        return categoryBudgets.stream().map(ProjectBudgetCategoryDto::getCommittedSpend).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
    }

    public BigDecimal getOpenPo() {
        return categoryBudgets.stream().map(ProjectBudgetCategoryDto::getOpenPo).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
    }

    public BigDecimal getPaid() {
        return categoryBudgets.stream().map(ProjectBudgetCategoryDto::getPaid).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
    }
}


