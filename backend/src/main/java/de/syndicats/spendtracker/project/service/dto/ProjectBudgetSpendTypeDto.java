package de.syndicats.spendtracker.project.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ProjectBudgetSpendTypeDto extends AbstractProjectBudgetDto implements IProjectBudgetChild<ProjectBudgetSubCategoryDto> {

    private BigDecimal paid;

    private BigDecimal openPo;

    private BigDecimal committedSpend;

    @JsonIgnore
    private ProjectBudgetSubCategoryDto parent;

    public ProjectBudgetSpendTypeDto(Long id, String title, BigDecimal invoiced, BigDecimal openPo, BigDecimal committedSpend) {
        super(id, title);
        this.paid = invoiced;
        this.openPo = openPo;
        this.committedSpend = committedSpend;
    }

}
