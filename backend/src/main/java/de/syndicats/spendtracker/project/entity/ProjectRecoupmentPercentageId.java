package de.syndicats.spendtracker.project.entity;

import de.syndicats.spendtracker.budget.entity.BudgetSpendType;
import lombok.*;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@ToString
@EqualsAndHashCode
public class ProjectRecoupmentPercentageId implements Serializable {

    @ManyToOne(optional = false)
    private Project project;

    @ManyToOne(optional = false)
    private BudgetSpendType budgetSpendType;

}
