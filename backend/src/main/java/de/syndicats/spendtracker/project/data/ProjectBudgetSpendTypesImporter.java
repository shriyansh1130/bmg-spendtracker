package de.syndicats.spendtracker.project.data;

import de.syndicats.spendtracker.budget.entity.BudgetSpendType;
import de.syndicats.spendtracker.budget.repository.BudgetSpendTypeRepository;
import de.syndicats.spendtracker.importer.data.AbstractCSVManager;
import de.syndicats.spendtracker.importer.data.ImportResult;
import de.syndicats.spendtracker.project.entity.Project;
import de.syndicats.spendtracker.project.entity.ProjectRecoupmentPercentage;
import de.syndicats.spendtracker.project.entity.ProjectRecoupmentPercentageId;
import de.syndicats.spendtracker.project.repository.ProjectRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component("projectBudgetSpendType")
@AllArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class ProjectBudgetSpendTypesImporter extends AbstractCSVManager<ProjectRecoupmentPercentage, ProjectRecoupmentPercentageId> {
    private static final String UNIQUE_CODE = "Unique Code";//Unique Code
    private static final String RECOUP = "Recoup%";//Recoup%
    private static final String SPEND_TYPE_CODE = "SpendTypeCode";//SpendTypeCode
    private static final String DELETE = "Delete";
    private static final String[] HEADERS = {UNIQUE_CODE, SPEND_TYPE_CODE, RECOUP, DELETE};

    private BudgetSpendTypeRepository budgetSpendTypeRepository;
    private ProjectRepository projectRepository;

    @Override
    protected String[] getHeaders() {
        return HEADERS;
    }

    @Override
    protected ProjectRecoupmentPercentage createEntityFromRecord(CSVRecord record) {
        Project project = getProject(record);
        BudgetSpendType budgetSpendType = getBudgetSpendType(record);

        return ProjectRecoupmentPercentage.builder()
                .projectRecoupmentPercentageId(new ProjectRecoupmentPercentageId(project, budgetSpendType))
                .recoupmentPercentage(getRecoupmentBigD(record))
                .build();
    }

    private BudgetSpendType getBudgetSpendType(CSVRecord record) {
        return this.budgetSpendTypeRepository.findByBudgetSpendTypeCode(getSpendTypeCode(record))
                .orElseThrow(() ->
                        new RuntimeException(String.format("Invalid State: No Spend Type for given code (%s) found. Should have been catched in validation already.", getSpendTypeCode(record))));
    }

    private Project getProject(CSVRecord record) {
        return this.projectRepository.findByUniqueCode(getProjectUniqueCode(record))
                .orElseThrow(() ->
                        new RuntimeException(String.format("Invalid State: No project for given unique code (%s)  found. Should have been catched in validation already.", getProjectUniqueCode(record))));
    }

    private String getSpendTypeCode(CSVRecord record) {
        return record.get(SPEND_TYPE_CODE);
    }

    private String getProjectUniqueCode(CSVRecord record) {
        return record.get(UNIQUE_CODE);
    }

    private BigDecimal getRecoupmentBigD(CSVRecord record) {
        return new BigDecimal(getRecoupmentPct(record).trim().replace("%", ""));
    }

    private String getRecoupmentPct(CSVRecord record) {
        return record.get(RECOUP);
    }

    @Override
    protected boolean validateAgainstAllRecords(CSVRecord recordToValidate, List<CSVRecord> allRecords, ImportResult result, int line) {
        return true;
    }

    @Override
    protected boolean validateRecord(CSVRecord recordToValidate, ImportResult result, int line) {
        Optional<Project> optionalProject = this.projectRepository.findByUniqueCode(getProjectUniqueCode(recordToValidate));
        Optional<BudgetSpendType> optionalBudgetSpendType = this.budgetSpendTypeRepository.findByBudgetSpendTypeCode(getSpendTypeCode(recordToValidate));

        if (!optionalProject.isPresent()) {
            String errorMessage = String.format("Line %d: Can't import project spend-type, because project: %s was not found", line, getProjectUniqueCode(recordToValidate));
            result.addErrorMessage(errorMessage);
            result.incrementErrorCount();
            return false;
        }

        if (!optionalBudgetSpendType.isPresent()) {
            String errorMessage = String.format("Line %d: Can't import project spend-type, because spend type code: %s was not found", line, getSpendTypeCode(recordToValidate));
            result.addErrorMessage(errorMessage);
            result.incrementErrorCount();
            return false;
        }

        if (StringUtils.isEmpty(getRecoupmentPct(recordToValidate))) {
            String errorMessage = String.format("Line %d: Can't import project spend-type, because recoupment pecentage is empty", line);
            result.addErrorMessage(errorMessage);
            result.incrementErrorCount();
            return false;
        }
        return true;
    }

    @Override
    protected String getEntityName() {
        return "Project Recoupment";
    }

    @Override
    protected String getInstanceIdentifier(ProjectRecoupmentPercentage entity) {
        return entity.getProjectRecoupmentPercentageId().getProject().getProjectName() + " - " + entity.getProjectRecoupmentPercentageId().getBudgetSpendType().getBudgetSpendTypeCode();
    }

    @Override
    protected boolean shouldDelete(CSVRecord currentRecord) {
        return currentRecord.isMapped(DELETE) && StringUtils.isNotEmpty(currentRecord.get(DELETE));
    }

    @Override
    protected ProjectRecoupmentPercentageId getEntityId(CSVRecord currentRecord) {
        Optional<Project> optionalProject = this.projectRepository.findByUniqueCode(getProjectUniqueCode(currentRecord));
        Optional<BudgetSpendType> optionalBudgetSpendType = this.budgetSpendTypeRepository.findByBudgetSpendTypeCode(getSpendTypeCode(currentRecord));

        if (optionalProject.isPresent() && optionalBudgetSpendType.isPresent()) {
            return new ProjectRecoupmentPercentageId(optionalProject.get(), optionalBudgetSpendType.get());
        } else {
            log.error("Tried to create ProjectRecoupmentPercentageId for non-existing project ({}) or spendType ({})", getProjectUniqueCode(currentRecord), getSpendTypeCode(currentRecord));
            return null;
        }
    }

    @Override
    protected ProjectRecoupmentPercentage updateEntityFromRecord(ProjectRecoupmentPercentage projectRecoupmentPercentage, CSVRecord currentRecord) {
        projectRecoupmentPercentage.setRecoupmentPercentage(getRecoupmentBigD(currentRecord));
        return projectRecoupmentPercentage;
    }

    @Override
    protected String getIdAsString(ProjectRecoupmentPercentageId projectRecoupmentPercentageId) {
        return projectRecoupmentPercentageId.getProject().getProjectName() + " - " + projectRecoupmentPercentageId.getBudgetSpendType().getBudgetSpendTypeCode();
    }

    @Override
    protected String getExportSortField() {
        return "projectRecoupmentPercentageId.project.uniqueCode";
    }

    @Override
    protected Iterable<?> getExportRow(ProjectRecoupmentPercentage projectRecoupmentPercentage) {
        return Arrays.asList(
                projectRecoupmentPercentage.getProjectRecoupmentPercentageId().getProject().getUniqueCode(),
                projectRecoupmentPercentage.getProjectRecoupmentPercentageId().getBudgetSpendType().getBudgetSpendTypeCode(),
                projectRecoupmentPercentage.getRecoupmentPercentage()
        );
    }

    @Override
    protected boolean shouldCreate(ProjectRecoupmentPercentageId projectRecoupmentPercentageId) {
        return !this.entityRepository.existsById(projectRecoupmentPercentageId);
    }
}
