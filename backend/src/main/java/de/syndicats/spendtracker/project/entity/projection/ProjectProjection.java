package de.syndicats.spendtracker.project.entity.projection;

import de.syndicats.spendtracker.company.entity.projection.CompanyProjection;
import de.syndicats.spendtracker.country.entity.projection.CountryProjection;
import de.syndicats.spendtracker.project.entity.Project;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "projectProjection", types = Project.class)
public interface ProjectProjection {
    String getSapClientCode();

    String getProjectName();

    CompanyProjection getRepertoireOwnerCompany();

    String getIonSap();

    String getAssignment();

    String getLabel();

    String getDealType();

    String getSegment();

    int getPriority();
}
