package de.syndicats.spendtracker.project.entity;

import de.syndicats.spendtracker.application.entity.AbstractBaseModel;
import de.syndicats.spendtracker.company.entity.Company;
import lombok.*;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;

@Table(name = "project")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public class Project extends AbstractBaseModel {
    public static final String DEAL_TYPE_TRADITIONAL_MODEL = "TRADITIONAL MODEL";

    @Column(name = "unique_code", nullable = false, unique = true)
    @NonNull
    private String uniqueCode;

    @Nationalized
    @Column(name = "sap_client_code", nullable = false, length = 12)
    @Size(max = 12)
    @NonNull
    private String sapClientCode;

    @Nationalized
    @Column(name = "project_name", nullable = false, length = 150)
    @Size(max = 75)
    @NonNull
    private String projectName;

    @ManyToOne(optional = false)
    @JoinColumn(name = "repertoire_owner_company_id", referencedColumnName = "id", nullable = false)
    @NonNull
    private Company repertoireOwnerCompany;

    @Nationalized
    @Column(name = "ion_sap", nullable = false, length = 12)
    @Size(max = 12)
    @NonNull
    private String ionSap;

    @Nationalized
    @Column(name = "assignment", nullable = false, length = 40)
    @Size(max = 40)
    @NonNull
    private String assignment;

    @Nationalized
    @Column(name = "label", nullable = false, length = 25)
    @Size(max = 25)
    @NonNull
    private String label;

    @Nationalized
    @Column(name = "deal_type", nullable = false, length = 25)
    @Size(max = 25)
    @NonNull
    private String dealType;

    @Nationalized
    @Column(name = "segment", nullable = false, length = 10)
    @Size(max = 10)
    @NonNull
    private String segment;

    @Column(name = "priority", nullable = false)
    @Max(99)
    @Builder.Default
    private int priority = 0;

}