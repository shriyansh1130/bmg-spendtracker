package de.syndicats.spendtracker.project.entity;

import lombok.*;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;

@Table(name = "project_recoupment_percentage")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ToString
public class ProjectRecoupmentPercentage {

    @EmbeddedId
    private  ProjectRecoupmentPercentageId projectRecoupmentPercentageId;

    @Column(name = "recoupment_percentage", nullable = false)
    @NumberFormat(pattern = "#000.000")
    @DecimalMin("0")
    @DecimalMax("100")
    @Digits(integer = 3, fraction = 3)
    private  BigDecimal recoupmentPercentage;

}