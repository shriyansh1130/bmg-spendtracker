package de.syndicats.spendtracker.project.service.dto;

import java.math.BigDecimal;
import java.util.Set;

public interface IProjectBudgetParent<T extends IProjectBudgetChild> extends IProjectBudget {

    default void addChild(T child) {
        this.getChildren().add(child);
        child.setParent(this);
    }

    Set<T> getChildren();

    default BigDecimal getPaid() {
        return this.getChildren().stream().map(IProjectBudget::getPaid).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
    }

    default BigDecimal getOpenPo() {
        return this.getChildren().stream().map(IProjectBudget::getOpenPo).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
    }

    default BigDecimal getCommittedSpend() {
        return this.getChildren().stream().map(IProjectBudget::getCommittedSpend).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
    }

}
