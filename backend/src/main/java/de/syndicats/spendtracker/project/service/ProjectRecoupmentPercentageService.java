package de.syndicats.spendtracker.project.service;


import de.syndicats.spendtracker.budget.entity.BudgetSpendType;
import de.syndicats.spendtracker.project.entity.Project;
import de.syndicats.spendtracker.project.entity.ProjectRecoupmentPercentageId;
import de.syndicats.spendtracker.project.repository.ProjectRecoupmentPercentageRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ProjectRecoupmentPercentageService {
    private ProjectRecoupmentPercentageRepository projectRecoupmentPercentageRepository;

    public boolean hasProjectSpecificRecoupment(Project project, BudgetSpendType spendType) {
        return projectRecoupmentPercentageRepository.existsById(new ProjectRecoupmentPercentageId(project, spendType));
    }
}
