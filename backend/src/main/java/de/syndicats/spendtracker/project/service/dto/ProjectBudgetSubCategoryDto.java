package de.syndicats.spendtracker.project.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class ProjectBudgetSubCategoryDto extends AbstractProjectBudgetDto implements IProjectBudgetParent<ProjectBudgetSpendTypeDto>, IProjectBudgetChild<ProjectBudgetCategoryDto> {

    @JsonIgnore
    private ProjectBudgetCategoryDto parent;

    private Set<ProjectBudgetSpendTypeDto> children = new HashSet<>();

    public ProjectBudgetSubCategoryDto(Long id, String title) {
        super(id, title);
    }
}