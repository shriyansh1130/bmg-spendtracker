package de.syndicats.spendtracker.project.service.dto;

public interface IProjectBudgetChild<T extends IProjectBudgetParent> extends IProjectBudget {

    T getParent();

    void setParent(T parent);

}
