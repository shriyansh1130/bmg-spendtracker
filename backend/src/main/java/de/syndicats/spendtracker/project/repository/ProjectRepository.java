package de.syndicats.spendtracker.project.repository;

import de.syndicats.spendtracker.company.entity.Company;
import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.project.entity.Project;
import de.syndicats.spendtracker.project.entity.projection.ProjectProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "projects", path = "projects", excerptProjection = ProjectProjection.class)
public interface ProjectRepository extends JpaRepository<Project, Long> {
    Page<Project> findByProjectNameIgnoreCaseContaining(@Param("projectName") String projectName, Pageable p);

    Page<Project> findByRepertoireOwnerCompanyAndProjectNameIgnoreCaseContaining(
            @Param("repertoireOwnerCompany") Company repertoireOwnerCompany,
            @Param("projectName") String projectName,
            Pageable pageable
    );

    Page<Project> findByProjectNameIgnoreCaseContainingAndSapClientCodeIgnoreCaseContaining(
            @Param("projectName") String projectName,
            @Param("sapClientCode") String sapClientCode,
            Pageable p
    );

    @Query("Select p FROM Project p where (p.repertoireOwnerCompany = :repertoireOwnerCompany " +
            "OR (p.repertoireOwnerCompany.country <> :repertoireOwnerTerritory " +
            "   AND p.repertoireOwnerCompany.international = true)" +
            ") AND lower(p.projectName) like lower(concat('%', :projectName , '%')) "+
            "ORDER BY p.projectName ASC")
    Page<Project> findValidProjectsForInternationalROCompanyAndROTerritoryAndProjectName(
            @Param("repertoireOwnerCompany") Company repertoireOwnerCompany,
            @Param("repertoireOwnerTerritory") Country repertoireOwnerTerritory,
            @Param("projectName") String projectName,
            Pageable pageable
    );

    Optional<Project> findByIonSapAndAssignment(String sapIonCode, String glAssignment);

    Optional<Project> findByUniqueCode(String uniqueCode);
}