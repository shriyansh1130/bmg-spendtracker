package de.syndicats.spendtracker.project.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class ProjectBudgetCategoryDto extends AbstractProjectBudgetDto implements IProjectBudgetParent<ProjectBudgetSubCategoryDto> {

    private Set<ProjectBudgetSubCategoryDto> children = new HashSet<>();

    public ProjectBudgetCategoryDto(Long id, String title) {
        super(id, title);
    }
}