package de.syndicats.spendtracker.posting.repository;

import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.posting.entity.Posting;
import de.syndicats.spendtracker.project.entity.Project;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PostingRepository extends JpaRepository<Posting, Long> {

    Optional<Posting> findByUniqueKey(String uniqueKey);

    List<Posting> findByPurchaseOrderPositionIsNotNull();

    List<Posting> findByPurchaseOrderPosition(PurchaseOrderPosition position);

    List<Posting> findByProjectAndBudgetTerritory(Project project, Country budgetTerritory);

    List<Posting> findByProject(Project project);

    boolean existsByProjectAndBudgetTerritory(Project project, Country country);
}