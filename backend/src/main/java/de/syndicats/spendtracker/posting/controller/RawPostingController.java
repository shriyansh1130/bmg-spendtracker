package de.syndicats.spendtracker.posting.controller;

import de.syndicats.spendtracker.posting.entity.RawPostingSyncTask;
import de.syndicats.spendtracker.posting.services.RawPostingSynchronizationTaskService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("api/raw-postings")
@AllArgsConstructor
public class RawPostingController {

    private RawPostingSynchronizationTaskService rawPostingSynchronizationTaskService;

    @RequestMapping(value = "synchronize", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public RawPostingSyncTask createPosFromRawPostings() {
        RawPostingSyncTask syncTask = rawPostingSynchronizationTaskService.createSyncTask();
        return syncTask;
    }
}
