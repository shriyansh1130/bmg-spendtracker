package de.syndicats.spendtracker.posting.repository;

import de.syndicats.spendtracker.posting.entity.RawPosting;
import de.syndicats.spendtracker.posting.entity.RawPostingProcessStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "raw-postings", path = "raw-postings")
public interface RawPostingRepository extends JpaRepository<RawPosting, Long> {

    void deleteByUniqueKey(String uniqueKey);

    List<RawPosting> findByUniqueKey(String uniqueKey);

    List<RawPosting> findAllByProcessStatus(RawPostingProcessStatus processStatus);

    Page<RawPosting> findByProcessStatus(@Param("processStatus") RawPostingProcessStatus processStatus, @Param("pageable") Pageable pageable);

}