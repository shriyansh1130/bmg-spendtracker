package de.syndicats.spendtracker.posting.services;

import de.syndicats.spendtracker.posting.entity.RawPosting;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrder;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import de.syndicats.spendtracker.purchaseorder.repository.PurchaseOrderRepository;
import de.syndicats.spendtracker.purchaseorder.util.PurchaseOrderUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class RawPostingPoMatchingService {

    private PurchaseOrderRepository purchaseOrderRepository;

    @Transactional
    public Optional<PurchaseOrderPosition> findMatchingPoLine(RawPosting posting) {
        String positionDelimiter = PurchaseOrderUtil.PO_NUMBER_LINE_DELIMITER;
        String[] poNumberParts = posting.getPoNumber().split(positionDelimiter);
        if (poNumberParts.length != 2) {
            // made up Po Line - No Match
            return Optional.empty();
        }

        Optional<PurchaseOrder> po = purchaseOrderRepository.findByPurchaseOrderNumber(poNumberParts[0]);
        if (po.isPresent()) {
            int poLineIndex = Integer.parseInt(poNumberParts[1]) - 1;
            List<PurchaseOrderPosition> positions = po.get().getPositions();
            if (positions.size() > poLineIndex) {
                return Optional.of(positions.get(poLineIndex));
            }
        }
        return Optional.empty();
    }
}

