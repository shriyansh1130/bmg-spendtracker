package de.syndicats.spendtracker.posting.entity;

import de.syndicats.spendtracker.application.entity.AbstractBaseModel;
import de.syndicats.spendtracker.importer.data.ImportResult;
import de.syndicats.spendtracker.importer.data.ImportStatus;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "raw_posting_sync_task")
@Entity
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class RawPostingSyncTask extends AbstractBaseModel {

    @Enumerated(EnumType.STRING)
    private RawPostingSyncStatus syncStatus;

    private int processedItems;

    private int totalItems;

    private int matchedPo;

    private int unmatchedPo;

    @Builder.Default
    @ElementCollection(targetClass = String.class)
    @Fetch(FetchMode.SELECT)
    @Lob
    private List<String> errorMessages = new ArrayList<>();

    public void addMatchedPo() {
        this.addProcessedItem();
        this.matchedPo++;
    }

    public void addUnmatchedPo() {
        this.addProcessedItem();
        this.unmatchedPo++;
    }

    private void addProcessedItem() {
        this.processedItems++;
    }

    public void addErrorMessage(String msg) {
        this.errorMessages.add(msg);
    }


}
