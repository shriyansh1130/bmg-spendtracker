package de.syndicats.spendtracker.posting.entity;

import de.syndicats.spendtracker.application.entity.AbstractBaseModel;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

/**
 * Represents a raw entry from sap import to be further processed within the system
 */
@Table(name = "raw_sap_statement")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Getter
@Setter
public class RawPosting extends AbstractBaseModel{
    private String uniqueKey;
    private String glSpendTypeCode;
    private String glSapIonCode;
    private String sapClientCode;
    private String amountInLocalCurrency;
    private String currencyCode;
    private String postingDate;
    private String text;
    private String yearMonth;
    private String year;
    private String documentNo;
    private String itemNo;
    private String sapCompanyCode;
    private String sapTerritoryCode;
    private String budgetTerritoryCode;
    private String poNumber;
    private String fileType;
    private String fileName;
    private String fileImportDate;
    private String glAssignment;
    private String glOrderType;
    private String glCostCtr;
    private String glOrder;
    private String royaltyCostDescription;
    private String projectId;

    @Enumerated(EnumType.STRING)
    private RawPostingProcessStatus processStatus;



}
