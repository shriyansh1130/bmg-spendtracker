package de.syndicats.spendtracker.posting.repository;

import de.syndicats.spendtracker.posting.entity.RawPostingSyncStatus;
import de.syndicats.spendtracker.posting.entity.RawPostingSyncTask;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "raw-posting-sync-tasks", path = "raw-posting-sync-tasks")
public interface RawPostingSyncTaskRepository extends JpaRepository<RawPostingSyncTask, Long> {
    boolean existsBySyncStatus(@Param("syncStatus") RawPostingSyncStatus rawPostingSyncStatus);

    Page<RawPostingSyncTask> findAllBySyncStatus(@Param("rawPostingSyncStatus") RawPostingSyncStatus rawPostingSyncStatus, Pageable pageable);
}