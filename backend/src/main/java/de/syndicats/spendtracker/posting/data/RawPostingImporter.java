package de.syndicats.spendtracker.posting.data;

import de.syndicats.spendtracker.importer.data.AbstractCSVManager;
import de.syndicats.spendtracker.importer.data.ImportResult;
import de.syndicats.spendtracker.posting.entity.RawPosting;
import de.syndicats.spendtracker.posting.entity.RawPostingProcessStatus;
import de.syndicats.spendtracker.posting.repository.RawPostingRepository;
import de.syndicats.spendtracker.posting.services.PostingValidationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static de.syndicats.spendtracker.posting.data.PostingCsvUtil.*;

@Component("rawPostingImporter")
@AllArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
@SuppressWarnings("squid:S1075")
public class RawPostingImporter extends AbstractCSVManager<RawPosting, Long> {

    private RawPostingRepository rawPostingRepository;
    private PostingValidationService postingValidationService;

    private static final String[] HEADERS = {
            UNIQUE_KEY,
            GL_SPEND_TYPE_CODE,
            GL_SAP_ION_CODE,
            SAP_CLIENT_CODE,
            AMOUNT_IN_LOCAL_CURRENCY,
            CURRENCY_CODE,
            POSTING_DATE,
            TEXT,
            YEAR_MONTH,
            YEAR,
            DOCUMENT_NO,
            ITEM_NO,
            SAP_COMPANY_CODE,
            SAP_TERRITORY_CODE,
            BUDGET_TERRITORY_CODE,
            PO_NUMBER,
            FILE_TYPE,
            FILE_NAME,
            FILE_IMPORT_DATE,
            GL_ASSIGNMENT,
            GL_ORDER_TYPE,
            GL_COST_CTR,
            GL_ORDER,
            ROYALTY_COST_DESCRIPTION,
            PROJECT_ID

    };


    @Override
    protected String[] getHeaders() {
        return HEADERS;
    }

    @Override
    protected RawPosting createEntityFromRecord(CSVRecord record) {
        return createRawPostingFromRecord(record);
    }

    @Override
    protected boolean validateAgainstAllRecords(CSVRecord recordToValidate, List<CSVRecord> allRecords, ImportResult result, int line) {
        ///check for multiple keys
        if (allRecords.stream().filter((record) -> !record.equals(recordToValidate)).anyMatch(record -> recordToValidate.get(UNIQUE_KEY).equals(record.get(UNIQUE_KEY)))) {
            result.addErrorMessage(String.format("Line %d: %s is not unique in file", line, UNIQUE_KEY));
            return false;
        }
        return true;
    }


    @Override
    protected boolean validateRecord(CSVRecord recordToValidate, ImportResult result, int line) {
        List<String> errors = new ArrayList<>();
        PostingCsvUtil.validatePostingRecord(recordToValidate, errors);
        if (errors.size() > 0) {
            result.addErrorMessage(String.join(" | ", errors));
            result.incrementErrorCount();
            return false;
        }

        return true;
    }

    @Override
    protected String getEntityName() {
        return "Raw SAP Posting";
    }

    @Override
    protected String getInstanceIdentifier(RawPosting entity) {
        return entity.getUniqueKey();
    }

    @Override
    protected boolean shouldDelete(CSVRecord currentRecord) {
        return false;
    }

    @Override
    protected Long getEntityId(CSVRecord currentRecord) {
        return null;
    }

    @Override
    protected RawPosting updateEntityFromRecord(RawPosting posting, CSVRecord currentRecord) {
        throw new RuntimeException("Raw Postings should never be updated, but only deleted and created");
    }

    @Override
    protected String getIdAsString(Long aLong) {
        return null;
    }

    @Override
    protected String getExportSortField() {
        return "uniqueKey";
    }

    @Override
    protected Iterable<?> getExportRow(RawPosting p) {
        return Arrays.asList(
                p.getUniqueKey(),
                p.getGlSpendTypeCode(),
                p.getGlSapIonCode(),
                p.getAmountInLocalCurrency(),
                p.getCurrencyCode(),
                p.getPostingDate(),
                p.getText(),
                p.getYearMonth(),
                p.getYear(),
                p.getDocumentNo(),
                p.getItemNo(),
                p.getSapCompanyCode(),
                p.getSapTerritoryCode(),
                p.getBudgetTerritoryCode(),
                p.getPoNumber(),
                p.getFileType(),
                p.getFileName(),
                p.getFileImportDate(),
                p.getGlAssignment(),
                p.getGlOrderType(),
                p.getGlCostCtr(),
                p.getGlOrder(),
                p.getRoyaltyCostDescription(),
                p.getProjectId()
        );
    }

    @Override
    protected boolean validateEntity(RawPosting entity, int lineNumber, ImportResult result) {
        List<String> errors = new ArrayList<>();
        List<String> warnings = new ArrayList<>();
        postingValidationService.validateRawPosting(entity, errors, warnings);
        if (warnings.size() > 0) {
            result.addWarningMessage(createLineMessage(lineNumber, warnings));
        }
        if (errors.size() > 0) {
            result.addErrorMessage(createLineMessage(lineNumber, errors));
            result.incrementErrorCount();
            return false;
        } else {
            return super.validateEntity(entity, lineNumber, result);
        }
    }

    private String createLineMessage(int lineNumber, List<String> messages) {
        return String.format("Line %d: %s", lineNumber, String.join(" | ", messages));
    }

    @Override
    protected void beforeCreate(RawPosting entityToSave) {
        //always delete before creation
        ((RawPostingRepository) this.entityRepository).deleteByUniqueKey(entityToSave.getUniqueKey());
        super.beforeCreate(entityToSave);
    }

    private RawPosting createRawPostingFromRecord(CSVRecord record) {
        return RawPosting.builder()
                .uniqueKey(record.get(UNIQUE_KEY))
                .glSpendTypeCode(record.get(GL_SPEND_TYPE_CODE))
                .glSapIonCode(record.get(GL_SAP_ION_CODE))
                .sapClientCode(record.get(SAP_CLIENT_CODE))
                .amountInLocalCurrency(record.get(AMOUNT_IN_LOCAL_CURRENCY))
                .currencyCode(record.get(CURRENCY_CODE))
                .postingDate(record.get(POSTING_DATE))
                .text(record.get(TEXT))
                .yearMonth(record.get(YEAR_MONTH))
                .year(record.get(YEAR))
                .documentNo(record.get(DOCUMENT_NO))
                .itemNo(record.get(ITEM_NO))
                .sapCompanyCode(record.get(SAP_COMPANY_CODE))
                .sapTerritoryCode(record.get(SAP_TERRITORY_CODE))
                .budgetTerritoryCode(record.get(BUDGET_TERRITORY_CODE))
                .poNumber(record.get(PO_NUMBER))
                .fileType(record.get(FILE_TYPE))
                .fileName(record.get(FILE_NAME))
                .fileImportDate(record.get(FILE_IMPORT_DATE))
                .glAssignment(record.get(GL_ASSIGNMENT))
                .glOrderType(record.isSet(GL_ORDER_TYPE) ? record.get(GL_ORDER_TYPE) : "")
                .glCostCtr(record.isSet(GL_COST_CTR) ? record.get(GL_COST_CTR) : "")
                .glOrder(record.isSet(GL_ORDER) ? record.get(GL_ORDER) : "")
                .royaltyCostDescription(record.get(ROYALTY_COST_DESCRIPTION))
                .projectId(record.get(PROJECT_ID))
                .processStatus(RawPostingProcessStatus.UNPROCESSED)
                .build();
    }
}