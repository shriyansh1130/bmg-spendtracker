package de.syndicats.spendtracker.posting.data;

import java.text.NumberFormat;
import java.util.List;
import org.apache.commons.csv.CSVRecord;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public final class PostingCsvUtil {
    public static final String UNIQUE_KEY = "UniqueKey";
    public static final String GL_SPEND_TYPE_CODE = "GL-SpendTypeCode";
    public static final String GL_SAP_ION_CODE = "GL-SAP-IONCode";
    public static final String AMOUNT_IN_LOCAL_CURRENCY = "AmountInLocalCurrency";
    public static final String CURRENCY_CODE = "CurrencyCode";
    public static final String POSTING_DATE = "PostingDate";
    public static final String TEXT = "Text";
    public static final String YEAR_MONTH = "Year/month";
    public static final String YEAR = "Year";
    public static final String DOCUMENT_NO = "DocumentNo";
    public static final String ITEM_NO = "ItemNo";
    public static final String SAP_CLIENT_CODE = "SAP-Client-Code";
    public static final String SAP_COMPANY_CODE = "SAPCompanyCode";
    public static final String SAP_TERRITORY_CODE = "SAPTerritoryCode";
    public static final String BUDGET_TERRITORY_CODE = "BudgetTerritoryCode";
    public static final String PO_NUMBER = "PO-Number";
    public static final String FILE_TYPE = "FileType";
    public static final String FILE_NAME = "FileName";
    public static final String FILE_IMPORT_DATE = "FileImportDate";
    public static final String GL_ASSIGNMENT = "GL-Assignment";
    public static final String GL_ORDER_TYPE = "GL-OrderType";
    public static final String GL_COST_CTR = "GL-CostCtr";
    public static final String GL_ORDER = "GL-Order";
    public static final String ROYALTY_COST_DESCRIPTION = "RoyaltyCostDescription";
    public static final String PROJECT_ID = "Project ID";



    public static void validatePostingRecord(CSVRecord record, List<String> errors) {
        validateUniqueKey(record,errors);
        validateSpendTypeCode(record, errors);
        validateSapIonCode(record, errors);
        validateSapClientCode(record, errors);
        validateAmountInLocalCurrency(record, errors);
        validateCurrencyCode(record, errors);
        validatePostingDate(record, errors);
        validateText(record, errors);
        validateYearMonth(record, errors);
        validateYear(record, errors);
        validateDocumentNo(record, errors);
        validateItemNo(record, errors);
        validateSapCompanyCode(record, errors);
        validateSapTerritoryCode(record, errors);
        validateBudgetTerritoryCode(record, errors);
        validatePoNumber(record, errors);
        validateFileType(record, errors);
        validateFilename(record, errors);
        validateFileImportDate(record, errors);
        validateGlAssignment(record, errors);
        validateGlOrderType(record, errors);
        validateGlOrder(record, errors);
        validateGlCostCtr(record, errors);
        validateRoyaltyCostDescription(record, errors);
        validateProjectId(record, errors);
    }

    private static void validateProjectId(CSVRecord record, List<String> errors) {
        if (isMandatoryAndSet(record, PROJECT_ID, errors)) {

        }
    }

    private static void validateRoyaltyCostDescription(CSVRecord record, java.util.List<String> errors) {
        if (record.isSet(ROYALTY_COST_DESCRIPTION)) {
            validateMaxLength(record, ROYALTY_COST_DESCRIPTION, 50, errors);
        }
    }

    private static void validateGlCostCtr(CSVRecord record, java.util.List<String> errors) {
        /* Not Mandatory anymore
        if ( "PL".equals(record.get(FILE_TYPE)) && isMandatoryAndSet(record, GL_COST_CTR, errors)) {
            validateMaxLength(record, GL_COST_CTR, 8, errors);
        }
        */
    }

    private static void validateGlOrder(CSVRecord record, java.util.List<String> errors) {
         /* Not Mandatory anymore
        if ( "PL".equals(record.get(FILE_TYPE)) && isMandatoryAndSet(record, GL_ORDER, errors)) {
            validateMaxLength(record, GL_ORDER, 20, errors);
        }
        */
    }

    private static void validateGlOrderType(CSVRecord record, java.util.List<String> errors) {
         /* Not Mandatory anymore
        if ( "PL".equals(record.get(FILE_TYPE)) && isMandatoryAndSet(record, GL_ORDER_TYPE, errors)) {
            validateMaxLength(record, GL_ORDER_TYPE, 4, errors);
        }
        */
    }

    private static void validateGlAssignment(CSVRecord record, java.util.List<String> errors) {
        if (isMandatoryAndSet(record, GL_ASSIGNMENT, errors)) {
            validateMaxLength(record, GL_ASSIGNMENT, 20, errors);
        }
    }

    private static void validateFileImportDate(CSVRecord record, java.util.List<String> errors) {
        if (record.isSet(FILE_IMPORT_DATE)) {
            validateDateFormat(record, FILE_IMPORT_DATE, "yyyy-MM-dd", errors);
        }
    }

    private static void validateFilename(CSVRecord record, java.util.List<String> errors) {
        if (isMandatoryAndSet(record, FILE_NAME, errors)) {
            //validateMaxLength(record, FILE_NAME, 20, errors);
        }
    }



    private static void validateFileType(CSVRecord record, java.util.List<String> errors) {
        if (isMandatoryAndSet(record, FILE_TYPE, errors)) {
            String fileType = record.get(FILE_TYPE);
            if (!("PL".equals(fileType) || "BS".equals(fileType))) {
                errors.add(FILE_TYPE + ": value is unexpected (expected: [PL,BS]");
            }
        }
    }

    private static void validatePoNumber(CSVRecord record, java.util.List<String> errors) {
        //if (isMandatoryAndSet(record, PO_NUMBER, errors)) {}
    }

    private static void validateSapTerritoryCode(CSVRecord record, java.util.List<String> errors) {
        if (isMandatoryAndSet(record, SAP_TERRITORY_CODE, errors)) {
            validateMaxLength(record, SAP_TERRITORY_CODE, 2, errors);
        }
    }

    private static void validateBudgetTerritoryCode(CSVRecord record, java.util.List<String> errors) {
        if (isMandatoryAndSet(record, BUDGET_TERRITORY_CODE, errors)) {
            validateMaxLength(record, BUDGET_TERRITORY_CODE, 2, errors);
        }
    }

    private static void validateSapCompanyCode(CSVRecord record, java.util.List<String> errors) {
        if (isMandatoryAndSet(record, SAP_COMPANY_CODE, errors)) {
            validateMaxLength(record, SAP_COMPANY_CODE, 4, errors);
        }
    }

    private static void validateItemNo(CSVRecord record, java.util.List<String> errors) {
        if (isMandatoryAndSet(record, ITEM_NO, errors)) {
            validateMaxLength(record, ITEM_NO, 3, errors);
        }
    }

    private static void validateDocumentNo(CSVRecord record, java.util.List<String> errors) {
        if (isMandatoryAndSet(record, DOCUMENT_NO, errors)) {
            validateMaxLength(record, DOCUMENT_NO, 8, errors);
        }
    }

    private static void validateYear(CSVRecord record, java.util.List<String> errors) {
        if (isMandatoryAndSet(record, YEAR, errors)){
            validateMaxLength(record, YEAR, 4, errors);
            validateDateFormat(record, YEAR, "yyyy", errors);
        }
    }

    private static void validateYearMonth(CSVRecord record, java.util.List<String> errors) {
        String pattern = "yyyy/MM";
        if (isMandatoryAndSet(record, YEAR_MONTH, errors)) {
            validateMaxLength(record, YEAR_MONTH, 7, errors);
            validateDateFormat(record, YEAR_MONTH, pattern, errors);
        }
    }

    private static void validateDateFormat(CSVRecord record, String fieldname, String pattern, java.util.List<String> errors) {
        String dateString = record.get(fieldname);
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
            formatter.parse(dateString);
        } catch (DateTimeParseException dtpe) {
            errors.add(fieldname + ": not able to parse " + dateString + " (pattern: " + pattern + ")" );
        }
    }

    private static void validateText(CSVRecord record, java.util.List<String> errors) {
        if (isMandatoryAndSet(record, TEXT, errors)){
            validateMaxLength(record, TEXT, 75, errors);
        }
    }

    private static void validatePostingDate(CSVRecord record, java.util.List<String> errors) {
        String pattern = "yyyy-MM-dd";
        if (record.isSet(POSTING_DATE)) {
            validateDateFormat(record, POSTING_DATE, pattern, errors);
        }
    }

    private static void validateCurrencyCode(CSVRecord record, java.util.List<String> errors) {
        if (isMandatoryAndSet(record, CURRENCY_CODE, errors)) {
            validateMaxLength(record, CURRENCY_CODE, 3, errors);
        }
    }

    private static void validateAmountInLocalCurrency(CSVRecord record, java.util.List<String> errors) {
        if (isMandatoryAndSet(record, AMOUNT_IN_LOCAL_CURRENCY, errors)) {
            try {
                String amountValue = record.get(AMOUNT_IN_LOCAL_CURRENCY).replace(",", ".");
                BigDecimal bd = new BigDecimal(amountValue);
                if (bd.compareTo(new BigDecimal("999999999.99")) > 0) {
                    errors.add(AMOUNT_IN_LOCAL_CURRENCY + " is greater than 999999999.99. Value: " + amountValue);
                }
            } catch(NumberFormatException nfe) {
                errors.add(AMOUNT_IN_LOCAL_CURRENCY+  ": " +nfe.getMessage());
            }
        }
    }

    private static void validateSapIonCode(CSVRecord record, java.util.List<String> errors) {
        if (isMandatoryAndSet(record, GL_SAP_ION_CODE, errors)) {
            validateMaxLength(record, GL_SAP_ION_CODE, 12, errors);
        }
    }

    private static void validateSapClientCode(CSVRecord record, java.util.List<String> errors) {
        isMandatoryAndSet(record, SAP_CLIENT_CODE, errors);
    }

    private static void validateSpendTypeCode(CSVRecord record, java.util.List<String> errors) {
        if (isMandatoryAndSet(record, GL_SPEND_TYPE_CODE, errors)) {
            validateMaxLength(record, GL_SPEND_TYPE_CODE, 10, errors);
        }
    }

    private static void validateUniqueKey(CSVRecord record, java.util.List<String> errors) {
        if (isMandatoryAndSet(record, UNIQUE_KEY, errors)) {
            validateMaxLength(record, UNIQUE_KEY, 20, errors);
        }
    }

    private static void validateMaxLength(CSVRecord record, String fieldname, int maxLength, java.util.List<String> errors) {
        if (record.get(fieldname).length() > maxLength) {
            errors.add(fieldname + " is longer than maxlength of " + maxLength);
        }
    }

    private static boolean isMandatoryAndSet(CSVRecord record, String fieldname, java.util.List<String> errors) {
        if (record.get(fieldname) == null || record.get(fieldname).isEmpty()) {
            errors.add(fieldname + " is not set");
            return false;
        } else {
            return true;
        }
    }
}
