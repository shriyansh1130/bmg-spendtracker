package de.syndicats.spendtracker.posting.services;

import de.syndicats.spendtracker.exchangerate.services.ExchangeRateService;
import de.syndicats.spendtracker.posting.entity.*;
import de.syndicats.spendtracker.posting.repository.PostingRepository;
import de.syndicats.spendtracker.posting.repository.RawPostingRepository;
import de.syndicats.spendtracker.posting.repository.RawPostingSyncTaskRepository;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
@Slf4j
public class RawPostingSynchronizationService {

    private RawPostingPoMatchingService rawPostingPoMatchingService;
    private RawPostingRepository rawPostingRepository;
    private PostingRepository postingRepository;
    private PostingService postingService;
    private RawPostingSyncTaskRepository rawPostingSyncTaskRepository;
    private ExchangeRateService exchangeRateService;

    @Async
    public void synchronizeRawPostings(RawPostingSyncTask rawPostingSyncTask, List<RawPosting> toProcess) {
        log.info("Synchronization task started...");
        try {
            toProcess.forEach(rawPosting -> {
                try {
                    RawPosting processedRawpostingResult = this.syncRawPosting(rawPosting);
                    switch (processedRawpostingResult.getProcessStatus()) {
                        case PO_MATCHED:
                            rawPostingSyncTask.addMatchedPo();
                            break;
                        case PO_NOT_MATCHED:
                            rawPostingSyncTask.addUnmatchedPo();
                            break;
                    }
                } catch (Exception e) {
                    String errorMessage = String.format("Error synchronizing RawPosting with uniqueKey: %s; Message: %s", rawPosting.getUniqueKey(), e.getMessage());
                    log.error(errorMessage, e);
                    rawPostingSyncTask.addErrorMessage(errorMessage);
                } finally {
                    rawPostingSyncTaskRepository.saveAndFlush(rawPostingSyncTask);
                }
            });
            postingService.closePosIfNecessary();
        } catch (Exception e) {
            log.error("Error while synchronizing Raw-Postings", e);
        } finally {
            RawPostingSyncTask task = rawPostingSyncTaskRepository.findById(rawPostingSyncTask.getId()).get();
            task.setSyncStatus(RawPostingSyncStatus.DONE);
            rawPostingSyncTaskRepository.save(task);
            log.info("Synchronization task finished.");
        }
    }

    private RawPosting syncRawPosting(RawPosting rawPosting) {
        Optional<PurchaseOrderPosition> optionalPosition = rawPostingPoMatchingService.findMatchingPoLine(rawPosting);

        Posting posting = postingService.createPostingFromRawPosting(rawPosting);

        if (optionalPosition.isPresent()) {
            log.debug("Found Matching PO-Position. Creating Posting.");

            posting.setPurchaseOrderPosition(optionalPosition.get());
            posting.setAmountInTargetCurrency(exchangeRateService.calculateAmountInTargetCurrency(posting.getCurrency(), posting.getAmountInLocalCurrency(), optionalPosition.get().getPurchaseOrder().getCurrency()));
            rawPosting.setProcessStatus(RawPostingProcessStatus.PO_MATCHED);
        } else {
            log.debug("No Matching PO-Position found. Creating Posting.");

            rawPosting.setProcessStatus(RawPostingProcessStatus.PO_NOT_MATCHED);
        }
        Posting persistedPosting = postingRepository.save(posting);
        RawPosting saved = rawPostingRepository.save(rawPosting);
        return saved;
    }
}
