package de.syndicats.spendtracker.posting.entity;

import de.syndicats.spendtracker.application.entity.AbstractBaseModel;
import de.syndicats.spendtracker.budget.entity.BudgetSpendType;
import de.syndicats.spendtracker.company.entity.Company;
import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.currency.entity.Currency;
import de.syndicats.spendtracker.project.entity.Project;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Table(name = "sap_posting")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Posting extends AbstractBaseModel {

    @NonNull
    @Column(unique = true)
    private String uniqueKey;

    @NonNull
    @ManyToOne
    @JoinColumn(name = "budget_spend_type_id", referencedColumnName = "id")
    private BudgetSpendType budgetSpendType;

    @ManyToOne
    @JoinColumn(name = "currency_id", referencedColumnName = "id", nullable = false)
    @NonNull
    private Currency currency;

    @ManyToOne
    @JoinColumn(name = "company_id", referencedColumnName = "id", nullable = false)
    @NonNull
    private Company company;

    @ManyToOne
    @JoinColumn(name = "sap_territory_id", referencedColumnName = "id", nullable = false)
    @NonNull
    private Country sapTerritory;

    @ManyToOne
    @JoinColumn(name = "budget_territory_id", referencedColumnName = "id", nullable = false)
    @NonNull
    private Country budgetTerritory;

    @ManyToOne
    private PurchaseOrderPosition purchaseOrderPosition;

    @ManyToOne
    @JoinColumn(name = "project_id", referencedColumnName = "id", nullable = false)
    @NonNull
    private Project project;

    private String glSapIon;

    private String sapClientCode;

    @NonNull
    private BigDecimal amountInLocalCurrency;

    private BigDecimal amountInTargetCurrency;

    @NonNull
    private LocalDate postingDate;

    @NonNull
    private String text;

    @NonNull
    private String year;

    @NonNull
    private String yearMonth;

    @NonNull
    private String documentNo;

    @NonNull
    private String itemNo;

    @NonNull
    private String fileType;

    @NonNull
    private String fileName;

    @NonNull
    private LocalDate fileImportDate;

    @NonNull
    private String poNumber;

    @NonNull
    private String glAssignment;

    private String glOrderType;

    private String glCostCtr;

    private String glOrder;

    private String royaltyCostDescription;

}
