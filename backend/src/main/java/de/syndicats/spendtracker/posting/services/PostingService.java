package de.syndicats.spendtracker.posting.services;

import de.syndicats.spendtracker.budget.repository.BudgetSpendTypeRepository;
import de.syndicats.spendtracker.company.repository.CompanyRepository;
import de.syndicats.spendtracker.country.repository.CountryRepository;
import de.syndicats.spendtracker.currency.repository.CurrencyRepository;
import de.syndicats.spendtracker.exchangerate.services.ExchangeRateService;
import de.syndicats.spendtracker.posting.entity.Posting;
import de.syndicats.spendtracker.posting.entity.RawPosting;
import de.syndicats.spendtracker.posting.repository.PostingRepository;
import de.syndicats.spendtracker.project.repository.ProjectRepository;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrder;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderStatus;
import de.syndicats.spendtracker.purchaseorder.repository.PurchaseOrderRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
@Slf4j
@Transactional
public class PostingService {
    private CountryRepository countryRepository;
    private CurrencyRepository currencyRepository;
    private ProjectRepository projectRepository;
    private BudgetSpendTypeRepository budgetSpendTypeRepository;
    private CompanyRepository companyRepository;
    private PostingRepository postingRepository;
    private PurchaseOrderRepository purchaseOrderRepository;

    private ExchangeRateService exchangeRateService;
    private PostingValidationService postingValidationService;


    public Posting createPostingFromRawPosting(RawPosting rawPosting) {
        handleAlreadyExistingPosting(rawPosting.getUniqueKey());
        DateTimeFormatter isoDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        Posting posting = Posting.builder()
                .uniqueKey(rawPosting.getUniqueKey())
                .budgetSpendType(this.budgetSpendTypeRepository.findByBudgetSpendTypeCode(rawPosting.getGlSpendTypeCode()).get())
                .amountInLocalCurrency(new BigDecimal(rawPosting.getAmountInLocalCurrency().replace(",", ".")))
                .currency(this.currencyRepository.findByCurrencyCode(rawPosting.getCurrencyCode()))
                .company(this.companyRepository.findBySapCompanyCode(rawPosting.getSapCompanyCode()))
                .sapTerritory(this.countryRepository.findByCountryCode(rawPosting.getSapTerritoryCode()))
                .budgetTerritory(this.countryRepository.findByCountryCode(rawPosting.getBudgetTerritoryCode()))
                .project(this.projectRepository.findByUniqueCode(rawPosting.getProjectId()).get())
                .glSapIon(rawPosting.getGlSapIonCode())
                .sapClientCode(rawPosting.getSapClientCode())
                .postingDate(LocalDate.parse(rawPosting.getPostingDate(), isoDateFormatter))
                .text(rawPosting.getText())
                .year(rawPosting.getYear())
                .yearMonth(rawPosting.getYearMonth())
                .documentNo(rawPosting.getDocumentNo())
                .itemNo(rawPosting.getItemNo())
                .fileType(rawPosting.getFileType())
                .fileName(rawPosting.getFileName())
                .fileImportDate(LocalDate.parse(rawPosting.getFileImportDate(), isoDateFormatter))
                .poNumber(rawPosting.getPoNumber())
                .glAssignment(rawPosting.getGlAssignment())
                .glOrderType(rawPosting.getGlOrderType())
                .glCostCtr(rawPosting.getGlCostCtr())
                .glOrder(rawPosting.getGlOrder())
                .royaltyCostDescription(rawPosting.getRoyaltyCostDescription())
                .build();

        return posting;
    }

    private void handleAlreadyExistingPosting(String uniqueKey) {
        Optional<Posting> posting = postingRepository.findByUniqueKey(uniqueKey);
        posting.ifPresent(posting1 -> postingRepository.delete(posting1));
        //TODO: Rollback Logic if side-effects are triggered by PO creation? -> Maybe in OnDeleteHandler?
    }

    public void closePosIfNecessary() {
        log.info("Start checking for PurchaseOrder to close because of zero open po amount.");
        List<Posting> postingsWithMatchedPosition = postingRepository.findByPurchaseOrderPositionIsNotNull();
        postingsWithMatchedPosition.stream()
                .map(posting -> posting.getPurchaseOrderPosition().getPurchaseOrder())
                .filter(purchaseOrder -> purchaseOrder.getStatus().equals(PurchaseOrderStatus.Open))
                .forEach(this::closePoIfNecessary);
        log.info("Completed checking for PurchaseOrder to close because of zero open po amount.");

    }

    private void closePoIfNecessary(PurchaseOrder po) {
        if (po.getStatus().equals(PurchaseOrderStatus.Open)) {
            boolean hasNoOpenPoLeft = po.getPositions().stream().allMatch(this::hasZeroOpenPo);
            if (hasNoOpenPoLeft) {
                log.info(String.format("Closing Purchaseorder %s. All lines have zero open po amount.", po.getPurchaseOrderNumber()));
                po.setStatus(PurchaseOrderStatus.Closed);
                purchaseOrderRepository.save(po);
            }
        }
    }

    private boolean hasZeroOpenPo(PurchaseOrderPosition purchaseOrderPosition) {
        return purchaseOrderPosition.getOpenPo().compareTo(BigDecimal.ZERO) == 0;
    }

    public List<String> getErrorsForPurchaseOrderPosition(PurchaseOrderPosition position) {
        List<String> warnings = new ArrayList<>();
        List<Posting> matchingPostings = postingRepository.findByPurchaseOrderPosition(position);
        for (Posting posting: matchingPostings){
            this.postingValidationService.validatePostingForMatching(position,posting, warnings);
        }
        return warnings;
    }
}
