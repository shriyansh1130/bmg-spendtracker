package de.syndicats.spendtracker.posting.services;

import de.syndicats.spendtracker.budget.entity.BudgetSpendType;
import de.syndicats.spendtracker.budget.repository.BudgetSpendTypeRepository;
import de.syndicats.spendtracker.company.entity.Company;
import de.syndicats.spendtracker.company.repository.CompanyRepository;
import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.country.repository.CountryRepository;
import de.syndicats.spendtracker.currency.entity.Currency;
import de.syndicats.spendtracker.currency.repository.CurrencyRepository;
import de.syndicats.spendtracker.exchangerate.entity.ExchangeRate;
import de.syndicats.spendtracker.exchangerate.repository.ExchangeRateRepository;
import de.syndicats.spendtracker.posting.entity.Posting;
import de.syndicats.spendtracker.posting.entity.RawPosting;
import de.syndicats.spendtracker.posting.repository.RawPostingRepository;
import de.syndicats.spendtracker.project.entity.Project;
import de.syndicats.spendtracker.project.repository.ProjectRepository;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;


@Service
@AllArgsConstructor
@Slf4j
public class PostingValidationService {

    private ProjectRepository projectRepository;
    private BudgetSpendTypeRepository budgetSpendTypeRepository;
    private CountryRepository countryRepository;
    private CurrencyRepository currencyRepository;
    private CompanyRepository companyRepository;
    private ExchangeRateRepository exchangeRateRepository;
    private RawPostingPoMatchingService rawPostingPoMatchingService;
    private RawPostingRepository rawPostingRepository;

    public void validateRawPosting(RawPosting rawPosting, List<String> errors, List<String> warnings) {
        Optional<PurchaseOrderPosition> po = rawPostingPoMatchingService.findMatchingPoLine(rawPosting);

        if (po.isPresent()) {
            validateForMatching(po.get(), rawPosting, errors, warnings);
        } else {
            validateForCreation(rawPosting, errors);
        }
    }

    private void validateForMatching(PurchaseOrderPosition purchaseOrderPosition, RawPosting rawPosting, List<String> errors, List<String> warnings) {
        Project currentProject = purchaseOrderPosition.getProject();
        Optional<Project> matchingProject = findMatchingProject(rawPosting);

        if (!matchingProject.isPresent()) {
            errors.add(String.format("Project ‘%s' not found", rawPosting.getProjectId()));
        } else {
            final String glSapIonCode = rawPosting.getGlSapIonCode();
            final String ionSap = currentProject.getIonSap();
            if (!(Objects.equals(glSapIonCode, ionSap) && Objects.equals(rawPosting.getSapClientCode(), currentProject.getSapClientCode()))) {
                warnings.add(String.format("SAP Project differs %s %s from PO %s %s", glSapIonCode, rawPosting.getSapClientCode(), ionSap, currentProject.getAssignment()));
            }
        }

        final String glSpendTypeCode = rawPosting.getGlSpendTypeCode();
        final String poSpendTypeCode = purchaseOrderPosition.getSpendType().getBudgetSpendTypeCode();
        if (!(Objects.equals(glSpendTypeCode, poSpendTypeCode))) {
            warnings.add(String.format("SAP Spend Type differs %s from PO %s", glSpendTypeCode, poSpendTypeCode));
        }


        Optional<BudgetSpendType> matchingSpendType = budgetSpendTypeRepository.findByBudgetSpendTypeCode(rawPosting.getGlSpendTypeCode());
        if (!matchingSpendType.isPresent()) {
            errors.add(String.format("Spend Type (spendTypeCode: %s) not found", rawPosting.getGlSpendTypeCode()));
        } else {
            final String budgetTerritoryCode = rawPosting.getBudgetTerritoryCode();
            final String countryCode = purchaseOrderPosition.getPoCountry().getCountryCode();
            if (!(Objects.equals(budgetTerritoryCode, countryCode))) {
                warnings.add((String.format("SAP Budget Territory differs %s from PO %s", budgetTerritoryCode, countryCode)));
            }
        }

        Optional<Currency> matchingCurrency = Optional.ofNullable(currencyRepository.findByCurrencyCode(rawPosting.getCurrencyCode()));
        if (!matchingCurrency.isPresent()) {
            errors.add(String.format("Currency (CurrencyCode: %s) not found", rawPosting.getCurrencyCode()));
        } else {
            final String sapCurrencyCode = rawPosting.getCurrencyCode();
            final String poCurrencyCode = purchaseOrderPosition.getPurchaseOrder().getCurrency().getCurrencyCode();
            if (!Objects.equals(sapCurrencyCode, poCurrencyCode)) {
                Optional<ExchangeRate> exchangeRate = exchangeRateRepository.findByFromCurrency_CurrencyCodeAndToCurrency_CurrencyCode(sapCurrencyCode, poCurrencyCode);
                if (!exchangeRate.isPresent()) {
                    errors.add(String.format("No Exchange Rate Mapping from (sap): %s to (po): %s ", sapCurrencyCode, poCurrencyCode));
                }
            }
        }
    }

    private void validateForCreation(RawPosting rawPosting, List<String> errors) {
        Optional<Project> matchingProject = findMatchingProject(rawPosting);

        if (!matchingProject.isPresent()) {
            errors.add(String.format("Project ‘%s' not found", rawPosting.getProjectId()));
        }

        Optional<BudgetSpendType> matchingSpendType = budgetSpendTypeRepository.findByBudgetSpendTypeCode(rawPosting.getGlSpendTypeCode());
        if (!matchingSpendType.isPresent()) {
            errors.add(String.format("Spend Type (spendTypeCode: %s) not found", rawPosting.getGlSpendTypeCode()));
        }

        Optional<Country> matchingPoCountry = Optional.ofNullable(countryRepository.findByCountryCode(rawPosting.getBudgetTerritoryCode()));
        if (!matchingPoCountry.isPresent()) {
            errors.add(String.format("Country for budget territory (budgetTerritoryCode: %s) not found", rawPosting.getBudgetTerritoryCode()));
        }

        Optional<Currency> matchingCurrency = Optional.ofNullable(currencyRepository.findByCurrencyCode(rawPosting.getCurrencyCode()));
        if (!matchingCurrency.isPresent()) {
            errors.add(String.format("Currency (CurrencyCode: %s) not found", rawPosting.getCurrencyCode()));
        }


        Optional<Country> matchingCurrentTerritory = Optional.ofNullable(countryRepository.findByCountryCode(rawPosting.getSapTerritoryCode()));
        if (!matchingCurrentTerritory.isPresent()) {
            errors.add(String.format("Country for Current Territory (sapTerritoryCode: %s) not found", rawPosting.getSapTerritoryCode()));
        }

        Optional<Company> matchingCompany = Optional.ofNullable(companyRepository.findBySapCompanyCode(rawPosting.getSapCompanyCode()));
        if (!matchingCompany.isPresent()) {
            errors.add(String.format("Company for (sapCompanyCode: %s) not found", rawPosting.getSapCompanyCode()));
        }
    }


    private Optional<Project> findMatchingProject(RawPosting rawPosting) {
        Optional<Project> optionalProject = projectRepository.findByUniqueCode(rawPosting.getProjectId());
        return optionalProject;
    }


    public List<String> validatePostingForMatching(PurchaseOrderPosition purchaseOrderPosition, Posting posting, List<String> warnings) {
        Project currentProject = purchaseOrderPosition.getProject();

        final String glSapIonCode = posting.getGlSapIon();
        final String ionSap = currentProject.getIonSap();

        if (!(Objects.equals(glSapIonCode, ionSap) && Objects.equals(posting.getSapClientCode(), currentProject.getSapClientCode()))) {
            warnings.add(String.format("SAP Project <b>%s %s</b> differs from PO <b>%s %s</b>", glSapIonCode, posting.getSapClientCode(), ionSap, currentProject.getAssignment()));
        }

        final String glSpendTypeCode = posting.getBudgetSpendType().getBudgetSpendTypeCode();
        final String poSpendTypeCode = purchaseOrderPosition.getSpendType().getBudgetSpendTypeCode();
        if (!(Objects.equals(glSpendTypeCode, poSpendTypeCode))) {
            warnings.add(String.format("SAP Spend Type <b>%s</b> differs from PO <b>%s</b>", glSpendTypeCode, poSpendTypeCode));
        }

        final String budgetTerritoryCode = posting.getBudgetTerritory().getCountryCode();
        final String countryCode = purchaseOrderPosition.getPoCountry().getCountryCode();
        if (!(Objects.equals(budgetTerritoryCode, countryCode))) {
            warnings.add((String.format("SAP Budget Territory <b>%s</b> differs from PO <b>%s</b>", budgetTerritoryCode, countryCode)));
        }

        final String postingSapCompanyCode = posting.getCompany().getSapCompanyCode();
        final String positionROSapCompanyCode = purchaseOrderPosition.getProject().getRepertoireOwnerCompany().getSapCompanyCode();
        if (!(Objects.equals(postingSapCompanyCode, positionROSapCompanyCode))) {
            warnings.add((String.format("SAP Company Code <b>%s</b> differs from PO lines repertoire owners SAP Company Code <b>%s</b>", postingSapCompanyCode, positionROSapCompanyCode)));
        }

        return warnings;
    }

}
