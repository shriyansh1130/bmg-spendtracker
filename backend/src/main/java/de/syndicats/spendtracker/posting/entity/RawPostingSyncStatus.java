package de.syndicats.spendtracker.posting.entity;

public enum RawPostingSyncStatus {
    IN_PROGRESS, DONE
}
