package de.syndicats.spendtracker.posting.entity;

public enum RawPostingProcessStatus {
    UNPROCESSED, PO_MATCHED, PO_NOT_MATCHED
}
