package de.syndicats.spendtracker.posting.services;

import de.syndicats.spendtracker.posting.entity.RawPosting;
import de.syndicats.spendtracker.posting.entity.RawPostingProcessStatus;
import de.syndicats.spendtracker.posting.entity.RawPostingSyncStatus;
import de.syndicats.spendtracker.posting.entity.RawPostingSyncTask;
import de.syndicats.spendtracker.posting.repository.PostingRepository;
import de.syndicats.spendtracker.posting.repository.RawPostingRepository;
import de.syndicats.spendtracker.posting.repository.RawPostingSyncTaskRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class RawPostingSynchronizationTaskService {

    private RawPostingPoMatchingService rawPostingPoMatchingService;
    private RawPostingRepository rawPostingRepository;
    private PostingRepository postingRepository;
    private PostingService postingService;
    private RawPostingSyncTaskRepository rawPostingSyncTaskRepository;
    private RawPostingSynchronizationService rawPostingSynchronizationService;

    @Transactional
    public RawPostingSyncTask createSyncTask() {
        if (rawPostingSyncTaskRepository.existsBySyncStatus(RawPostingSyncStatus.IN_PROGRESS)) {
            throw new RuntimeException("RawPosting Synchronisation is already in progress. No additional Synchronisation possible.");
        }
        List<RawPosting> toProcess = rawPostingRepository.findAllByProcessStatus(RawPostingProcessStatus.UNPROCESSED);

        RawPostingSyncTask rawPostingSyncTask = rawPostingSyncTaskRepository.save(RawPostingSyncTask.builder()
                .syncStatus(RawPostingSyncStatus.IN_PROGRESS)
                .totalItems(toProcess.size())
                .build());
        rawPostingSynchronizationService.synchronizeRawPostings(rawPostingSyncTask, toProcess);

        return rawPostingSyncTask;
    }


}
