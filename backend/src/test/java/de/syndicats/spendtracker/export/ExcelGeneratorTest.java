package de.syndicats.spendtracker.export;

import de.syndicats.spendtracker.budget.entity.BudgetCategory;
import de.syndicats.spendtracker.budget.entity.BudgetSpendType;
import de.syndicats.spendtracker.budget.entity.BudgetSubCategory;
import de.syndicats.spendtracker.company.entity.Company;
import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.currency.entity.Currency;
import de.syndicats.spendtracker.project.entity.Project;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrder;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderStatus;
import de.syndicats.spendtracker.purchaseorder.service.ExcelGenerator;
import de.syndicats.spendtracker.vendor.entity.Vendor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import java.io.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Simple, functional test
 */
public class ExcelGeneratorTest {

    @Test
    public void testCreateExcel() throws Exception {
        // prepare test case
        List<PurchaseOrderPosition> purchaseOrderPositions = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            addProject(purchaseOrderPositions, i);
        }
        PurchaseOrder purchaseOrder1 = createPurchaseOrder(purchaseOrderPositions);
        PurchaseOrder purchaseOrder2 = createPurchaseOrder(purchaseOrderPositions);

        List<PurchaseOrder> purchaseOrders = new ArrayList<PurchaseOrder>();
        purchaseOrders.add(purchaseOrder1);
        purchaseOrders.add(purchaseOrder2);

        // create excel
        String sheetName = "Purchase Order Positions";
        CompletableFuture<File> future = ExcelGenerator.createExcel(ExcelGenerator.PurchaseOrderExcelContext.builder()
                .sheetName(sheetName)
                .purchaseOrderPositions(purchaseOrderPositions)
                .targetFile(File.createTempFile("ExcelExport_PurchaseOrderPositions_", ".xlsx"))
                .build());

        // verify
        File file = future.get();
        assertThat(file).exists();
        assertThat(file.length() > 1000).isTrue();
        assertThatWorkbook(file, (wb) -> {
            assertThat(wb.getNumberOfSheets()).isEqualTo(1);
            assertThat(wb.getSheet(sheetName)).isNotNull();
            assertThat(wb.getSheet(sheetName).getLastRowNum() == 10).isTrue();
        });

        file.delete();
    }

    @Test
    public void testCreateExcelWithNoPositions() throws Exception {
        // prepare test case
        List<PurchaseOrderPosition> purchaseOrderPositions = new ArrayList<>();
        // create excel
        String sheetName = "Purchase Order Positions";
        CompletableFuture<File> future = ExcelGenerator.createExcel(ExcelGenerator.PurchaseOrderExcelContext.builder()
                .sheetName(sheetName)
                .purchaseOrderPositions(purchaseOrderPositions)
                .targetFile(File.createTempFile("ExcelExport_PurchaseOrderPositions_", ".xlsx"))
                .build());

        // verify
        File file = future.get();
        assertThat(file).exists();
        assertThat(file.length() > 1000).isTrue();
        assertThatWorkbook(file, (wb) -> {
            assertThat(wb.getNumberOfSheets()).isEqualTo(1);
            assertThat(wb.getSheet(sheetName)).isNotNull();
            assertThat(wb.getSheet(sheetName).getLastRowNum() == 0).isTrue();
        });

        file.delete();
    }

    private static PurchaseOrder createPurchaseOrder(List<PurchaseOrderPosition> purchaseOrderPositions) {
        PurchaseOrder purchaseOrder = PurchaseOrder.builder()
                .purchaseOrderNumber("purchaseOrderNumber")
                .status(PurchaseOrderStatus.Open)
                .currency(
                        Currency.builder().currencyCode("EUR").currencyName("currencyName").currencySymbol("$").build())
                .country(
                        Country.builder().countryCode("country_countryCode").countryName("country_countryName").build())
                .vendor(Vendor.builder()
                        .accountNumber(new BigDecimal("0"))
                        .name1("name1")
                        .reconciliationAccountNumber("A0")
                        .city("city")
                        .company(Company.builder()
                                .companyCode("companyCode")
                                .companyName("companyName")
                                .sapCompanyCode("sapCompanyCode")
                                .country(Country.builder()
                                        .countryCode("country_countryCode")
                                        .countryName("country_countryName")
                                        .build())
                                .build())
                        .build())
                .positions(purchaseOrderPositions)
                .build();
            purchaseOrderPositions.forEach(purchaseOrderPosition -> purchaseOrderPosition.setPurchaseOrder(purchaseOrder));
        return purchaseOrder;
    }

    private static void addProject(List<PurchaseOrderPosition> purchaseOrderPositions, int i) {
        Project project = Project.builder()
                .projectName("project_projectName" + i)
                .sapClientCode("project_sapClientCode" + i)
                .assignment("project_assignment" + i)
                .dealType("project_dealType" + i)
                .ionSap("project_ionSap" + i)
                .label("project_label" + i)
                .repertoireOwnerCompany(Company.builder()
                        .companyCode("companyCode")
                        .companyName("companyName")
                        .sapCompanyCode("sapCompanyCode")
                        .country(Country.builder()
                                .countryCode("country_countryCode")
                                .countryName("country_countryName")
                                .build())
                        .build())
                .segment("project_segment")
                .priority(i)
                .uniqueCode("uniqueCode")
                .build();
        BudgetSpendType budgetSpendType = BudgetSpendType.builder()
                .title("budgetSpentType_title" + i)
                .budgetSpendTypeCode("0")
                .ppdRecoupmentPct(new BigDecimal(0.0))
                .budgetSubCategory(BudgetSubCategory.builder()
                        .title("title")
                        .displayOrder(Long.valueOf(1L))
                        .budgetCategory(BudgetCategory.builder().title("title").displayOrder(0L).build())
                        .build())
                .glAccount("Gl_account")
                .build();
        PurchaseOrderPosition purchaseOrderPosition = PurchaseOrderPosition.builder()
                .project(project)
                .price(new BigDecimal("10.00"))
                .quantity(new BigDecimal("10.00"))
                .description("purchaseOrderPosition_description" + i)
                .serviceMonth(LocalDateTime.now())
                .spendType(budgetSpendType)
                .build();

        purchaseOrderPositions.add(purchaseOrderPosition);
    }

    private static void assertThatWorkbook(File targetFile, Consumer<XSSFWorkbook> assertConditions) {
        try (XSSFWorkbook wb = getExcelWorkbook(targetFile)) {
            assertConditions.accept(wb);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private static XSSFWorkbook getExcelWorkbook(File targetFile) {
        try (InputStream is = new FileInputStream(targetFile)) {
            return new XSSFWorkbook(OPCPackage.open(is));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        } catch (InvalidFormatException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

}
