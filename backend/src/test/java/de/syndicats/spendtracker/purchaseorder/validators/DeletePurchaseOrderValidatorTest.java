package de.syndicats.spendtracker.purchaseorder.validators;

import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrder;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderStatus;
import de.syndicats.spendtracker.purchaseorder.repository.PurchaseOrderRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import javax.transaction.Transactional;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@Transactional
public class DeletePurchaseOrderValidatorTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }


    @Test
    public void givenWac_whenServletContext_thenItProvidesValidator() {
        ServletContext servletContext = wac.getServletContext();

        Assert.assertNotNull(servletContext);
        Assert.assertTrue(servletContext instanceof MockServletContext);
        Assert.assertNotNull(wac.getBean("beforeDeletePurchaseOrderValidator"));
    }


    @Test
    public void shouldDeleteWhenDraftStatus() throws Exception {
        Optional<PurchaseOrder> po = purchaseOrderRepository.findById(1l);
        Assert.assertTrue(po.isPresent());
        po.get().setStatus(PurchaseOrderStatus.Draft);
        this.mockMvc.perform(
                delete("/api/purchase-orders/1"))
                .andExpect(status().isNoContent())
        ;
        po = purchaseOrderRepository.findById(1l);
        Assert.assertFalse(po.isPresent());
    }

    @Test
    public void shouldNotDeleteWhenOpenStatus() throws Exception {
        Optional<PurchaseOrder> po = purchaseOrderRepository.findById(1l);
        Assert.assertTrue(po.isPresent());
        po.get().setStatus(PurchaseOrderStatus.Open);
        this.mockMvc.perform(
                delete("/api/purchase-orders/1"))
        .andExpect(status().isBadRequest());
        po = purchaseOrderRepository.findById(1l);
        Assert.assertTrue(po.isPresent());
    }

    @Test
    public void shouldNotDeleteWhenClosedStatus() throws Exception {
        Optional<PurchaseOrder> po = purchaseOrderRepository.findById(1l);
        Assert.assertTrue(po.isPresent());
        po.get().setStatus(PurchaseOrderStatus.Closed);
        this.mockMvc.perform(
                delete("/api/purchase-orders/1"))
                .andExpect(status().isBadRequest());
        po = purchaseOrderRepository.findById(1l);
        Assert.assertTrue(po.isPresent());
    }


}