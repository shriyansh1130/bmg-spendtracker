package de.syndicats.spendtracker.purchaseorder.entity;

import de.syndicats.spendtracker.budget.entity.BudgetSpendType;
import de.syndicats.spendtracker.posting.entity.Posting;
import de.syndicats.spendtracker.project.entity.Project;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.closeTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PurchaseOrderPositionTest {

    public static final BigDecimal PRICE = new BigDecimal("10.50");
    public static final BigDecimal QUANTITY = new BigDecimal("10");
    public static final BigDecimal COMPANY_THRESHOLD = new BigDecimal("0.95"); // current hard coded Company Threshold
    private PurchaseOrderPosition purchaseOrderPosition;
    private PurchaseOrder purchaseOrderMock = mock(PurchaseOrder.class);
    private BudgetSpendType budgetSpendTypeMock = mock(BudgetSpendType.class);
    private Project projectMock = mock(Project.class);
    private List<Posting> postings = new ArrayList<>();

    @Before
    public void setUp() {
        when(purchaseOrderMock.getStatus()).thenReturn(PurchaseOrderStatus.Open);
        this.purchaseOrderPosition = PurchaseOrderPosition.builder()
                .purchaseOrder(purchaseOrderMock)
                .spendType(budgetSpendTypeMock)
                .description("Description")
                .project(projectMock)
                .serviceMonth(LocalDateTime.now())
                .price(PRICE)
                .quantity(QUANTITY)
                .postings(postings)
                .build();
    }


    @Test
    public void getOpenPoWithNoPostings() {
        assertThat(this.purchaseOrderPosition.getOpenPo(), closeTo(new BigDecimal("105"), BigDecimal.ZERO));
    }

    @Test
    public void getOpenPoWithPostings() {
        Posting postingMock = mock(Posting.class);
        when(postingMock.getAmountInTargetCurrency()).thenReturn(new BigDecimal("10"));
        postings.add(postingMock);
        assertThat(this.purchaseOrderPosition.getOpenPo(), closeTo(new BigDecimal("95"), BigDecimal.ZERO));
    }

    @Test
    public void getOpenPoWithPostingsAndCompanyThresholdReached() {
        Posting postingMock = mock(Posting.class);
        BigDecimal thresholdValue = QUANTITY.multiply(PRICE).multiply(COMPANY_THRESHOLD);
        BigDecimal expectedOpen = QUANTITY.multiply(PRICE).subtract(thresholdValue);
        when(postingMock.getAmountInTargetCurrency()).thenReturn(thresholdValue);
        postings.add(postingMock);
        assertThat(this.purchaseOrderPosition.getOpenPo(), closeTo(expectedOpen, BigDecimal.ZERO));
    }


    @Test
    public void getOpenPoWithPostingsAndOverCompanyThreshold() {
        Posting postingMock = mock(Posting.class);
        BigDecimal justAboveThreshold = QUANTITY.multiply(PRICE).multiply(COMPANY_THRESHOLD).add(new BigDecimal("0.01"));
        when(postingMock.getAmountInTargetCurrency()).thenReturn(justAboveThreshold);
        postings.add(postingMock);
        assertThat(this.purchaseOrderPosition.getOpenPo(), closeTo(BigDecimal.ZERO, BigDecimal.ZERO));
    }


    @Test
    public void getVarianceWithoutPostings() {
        assertThat(this.purchaseOrderPosition.getVariance(), closeTo(new BigDecimal("105"), BigDecimal.ZERO));
    }

    @Test
    public void getVarianceWithPostings() {
        Posting postingMock = mock(Posting.class);
        when(postingMock.getAmountInTargetCurrency()).thenReturn(new BigDecimal("10"));
        postings.add(postingMock);
        assertThat(this.purchaseOrderPosition.getVariance(), closeTo(new BigDecimal("95"), BigDecimal.ZERO));
    }

    @Test
    public void getVarianceWithPostingsAndCompanyThresholdReached() {
        Posting postingMock = mock(Posting.class);
        BigDecimal thresholdValue = QUANTITY.multiply(PRICE).multiply(COMPANY_THRESHOLD);
        BigDecimal expectedOpen = QUANTITY.multiply(PRICE).subtract(thresholdValue);
        when(postingMock.getAmountInTargetCurrency()).thenReturn(thresholdValue);
        postings.add(postingMock);
        assertThat(this.purchaseOrderPosition.getVariance(), closeTo(expectedOpen, BigDecimal.ZERO));
    }

    @Test
    public void getVarianceWithPostingsAndOverCompanyThreshold() {
        Posting postingMock = mock(Posting.class);
        BigDecimal justAboveThreshold = QUANTITY.multiply(PRICE).multiply(COMPANY_THRESHOLD).add(new BigDecimal("0.01"));
        BigDecimal expectedVariance = QUANTITY.multiply(PRICE).subtract(justAboveThreshold);
        when(postingMock.getAmountInTargetCurrency()).thenReturn(justAboveThreshold);
        postings.add(postingMock);
        assertThat(this.purchaseOrderPosition.getVariance(), closeTo(expectedVariance, BigDecimal.ZERO));
    }

    @Test
    public void getPaidWithoutPostings() {
        assertThat(this.purchaseOrderPosition.getPaid(), closeTo(BigDecimal.ZERO, BigDecimal.ZERO));
    }

    @Test
    public void getPaidWithPostings() {
        Posting postingMock = mock(Posting.class);
        when(postingMock.getAmountInTargetCurrency()).thenReturn(new BigDecimal("100"));
        postings.add(postingMock);
        assertThat(this.purchaseOrderPosition.getPaid(), closeTo(new BigDecimal("100"), BigDecimal.ZERO));
    }

}