package de.syndicats.spendtracker.purchaseorder.service;

import de.syndicats.spendtracker.budget.entity.BudgetSpendType;
import de.syndicats.spendtracker.project.entity.Project;
import de.syndicats.spendtracker.project.entity.ProjectRecoupmentPercentage;
import de.syndicats.spendtracker.project.entity.ProjectRecoupmentPercentageId;
import de.syndicats.spendtracker.project.repository.ProjectRecoupmentPercentageRepository;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PurchaseOrderPositionServiceTest {

    @Mock
    private ProjectRecoupmentPercentageRepository projectRecoupmentPercentageRepositoryMock;

    @InjectMocks
    private PurchaseOrderPositionService underTest;

    private PurchaseOrderPosition positionMock = mock(PurchaseOrderPosition.class);
    private BudgetSpendType budgetSpendTypeMock = mock(BudgetSpendType.class);
    private ProjectRecoupmentPercentage projectRecoupmentPercentageMock = mock(ProjectRecoupmentPercentage.class);
    private Project projectMock = mock(Project.class);


    @Before
    public void setUp() {
        when(positionMock.getSpendType()).thenReturn(budgetSpendTypeMock);
        when(positionMock.getProject()).thenReturn(projectMock);
        when(budgetSpendTypeMock.getPpdRecoupmentPct()).thenReturn(new BigDecimal("75.00"));
        when(projectRecoupmentPercentageMock.getRecoupmentPercentage()).thenReturn(new BigDecimal("50.00"));
    }

    @Test
    public void shouldUseProjectRecoupmentIfExists() {
        when(projectRecoupmentPercentageRepositoryMock.findById(new ProjectRecoupmentPercentageId(projectMock, budgetSpendTypeMock)))
                .thenReturn(Optional.of(projectRecoupmentPercentageMock));
        assertThat(underTest.getRecoupmentPct(positionMock), is(new BigDecimal("50.00")));
    }


    @Test
    public void shouldUseBudgetSpendTypeRecoupmentIfNoProjectRecoupmentPctExists() {
        when(projectRecoupmentPercentageRepositoryMock.findById(new ProjectRecoupmentPercentageId(projectMock, budgetSpendTypeMock)))
                .thenReturn(Optional.empty());
        assertThat(underTest.getRecoupmentPct(positionMock), is(new BigDecimal("75.00")));
    }

}