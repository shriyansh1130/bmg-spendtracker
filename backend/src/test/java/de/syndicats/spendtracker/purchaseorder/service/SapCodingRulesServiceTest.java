package de.syndicats.spendtracker.purchaseorder.service;

import de.syndicats.spendtracker.budget.entity.BudgetSpendType;
import de.syndicats.spendtracker.company.entity.Company;
import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.project.entity.Project;
import de.syndicats.spendtracker.project.service.ProjectRecoupmentPercentageService;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrder;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderCoding;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderCodingType;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import de.syndicats.spendtracker.vendor.entity.Vendor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SapCodingRulesServiceTest {

    private static final String PROJECTNAME = "PROJECTNAME";
    private static final String PO_NUMBER = "123456";
    private static final String PO_LINE_DELIMITER = "#";
    private static final String PO_LINE_FIRST = "1";
    private static final int ORDER_NUMBER_FIRST = 0;
    private static final String SAPCLIENTCODE = "SAPCLIENTCODE";
    private static final String INVOICE_VALUE_LABEL = "Invoice Value";
    private static final String VENDORNAME1 = "VENDORNAME1";
    private static final String SPENDTYPECODE = "SPENDTYPECODE";
    private static final String POCOUNTRYCODE = "US";
    private static final String CURRENTERRITORYCODE = "DE";
    private static final String OTHERCOUNTRYCODE = "OTHERCOUNTRYCODE";
    private static final String DESCRIPTION = "Some Description";
    private static final String LONG_DESCRIPTION = "Some Long long long long long long long Description";
    private static final String ANOTHER_DEAL_TYPE = "ANOTHER DEAL TYPE";
    private static final String TRADITIONAL_MODEL = "TRADITIONAL MODEL";
    private static final String GLACCOUNT = "GLACCOUNT";
    private static final String SAPION = "SAPION";
    private static final String SAPCOMPANYCODE = "SAPCOMPANYCODE";
    private static final String NON_CAT_COSTCTR = "5080";
    private static final String NON_CAT_ORDER_TYPE = "NEWR";


    @Mock
    private PurchaseOrderPositionService purchaseOrderPositionServiceMock;

    @Mock
    private ProjectRecoupmentPercentageService projectRecoupmentPercentageService;

    @InjectMocks
    private SapCodingRulesService sapCodingRulesService;

    private PurchaseOrderPosition positionMock = mock(PurchaseOrderPosition.class);
    private PurchaseOrder purchaseOrderMock = mock(PurchaseOrder.class);
    private Country poCountryMock = mock(Country.class);
    private Country otherCountryMock = mock(Country.class);
    private Country currentTerritoryMock = mock(Country.class);
    private Project projectMock = mock(Project.class);
    private Company repertoireOwnerMock = mock(Company.class);
    private Vendor vendorMock = mock(Vendor.class);
    private BudgetSpendType spendTypeMock = mock(BudgetSpendType.class);


    @Before
    public void setUp() {
        when(positionMock.getPurchaseOrder()).thenReturn(purchaseOrderMock);
        when(positionMock.getProject()).thenReturn(projectMock);
        when(positionMock.getSpendType()).thenReturn(spendTypeMock);
        when(positionMock.getOrderNumber()).thenReturn(ORDER_NUMBER_FIRST);
        when(positionMock.getDescription()).thenReturn(DESCRIPTION);
        when(positionMock.getPoCountry()).thenReturn(poCountryMock);

        when(projectMock.getSapClientCode()).thenReturn(SAPCLIENTCODE);
        when(projectMock.getProjectName()).thenReturn(PROJECTNAME);
        when(projectMock.getRepertoireOwnerCompany()).thenReturn(repertoireOwnerMock);
        when(projectMock.getIonSap()).thenReturn(SAPION);

        when(purchaseOrderMock.getVendor()).thenReturn(vendorMock);
        when(purchaseOrderMock.getPurchaseOrderNumber()).thenReturn(PO_NUMBER);
        when(purchaseOrderMock.getCountry()).thenReturn(currentTerritoryMock);

        when(vendorMock.getName1()).thenReturn(VENDORNAME1);

        when(spendTypeMock.getBudgetSpendTypeCode()).thenReturn(SPENDTYPECODE);
        when(spendTypeMock.getGlAccount()).thenReturn(GLACCOUNT);

        when(poCountryMock.getCountryCode()).thenReturn(POCOUNTRYCODE);

        when(repertoireOwnerMock.getSapCompanyCode()).thenReturn(SAPCOMPANYCODE);
    }

    @Test
    public void createRechargeCoding() {
        when(repertoireOwnerMock.getCountry()).thenReturn(otherCountryMock);
        PurchaseOrderCoding result = sapCodingRulesService.createCoding(positionMock);

        assertThat(result.getCodingType(), is(PurchaseOrderCodingType.RECHARGE));
        assertThat(result.getRecoupableAccount(), is(""));
        assertThat(result.getRecoupableCustomerAccount(), is(""));
        assertThat(result.getRecoupableAssignment(), is(PROJECTNAME));
        assertThat(result.getRecoupableText(), is(SPENDTYPECODE + POCOUNTRYCODE + "-" + VENDORNAME1 + "-" + DESCRIPTION));
        assertThat(result.getRecoupableReferenceKey2(), is(PO_NUMBER + PO_LINE_DELIMITER + PO_LINE_FIRST));
        assertThat(result.getRecoupableReferenceKey3(), is(SAPCLIENTCODE));
        assertThat(result.getRecoupableAmount(), is("100%"));
    }


    @Test
    public void createRechargeCodingDescriptionCut() {
        when(positionMock.getDescription()).thenReturn(LONG_DESCRIPTION);
        PurchaseOrderCoding result = sapCodingRulesService.createCoding(positionMock);
        assertThat(result.getRecoupableText(), is(not(SPENDTYPECODE + POCOUNTRYCODE + "-" + VENDORNAME1 + "-" + LONG_DESCRIPTION)));
        assertThat(result.getRecoupableText().length(), is(50));
    }


    @Test
    public void createNonTraditionalModel() {
        when(repertoireOwnerMock.getCountry()).thenReturn(currentTerritoryMock);
        when(projectMock.getDealType()).thenReturn(ANOTHER_DEAL_TYPE);

        PurchaseOrderCoding result = sapCodingRulesService.createCoding(positionMock);
        assertThat(result.getCodingType(), is(PurchaseOrderCodingType.FULL_RECOUPABILITY));
        assertThat(result.getRecoupableAccount(), is(SAPCLIENTCODE));
        assertThat(result.getRecoupableAssignment(), is(SPENDTYPECODE + POCOUNTRYCODE));
        assertThat(result.getRecoupableText(), is(VENDORNAME1 + "-" + DESCRIPTION));
        assertThat(result.getRecoupableReferenceKey2(), is(PO_NUMBER + PO_LINE_DELIMITER + PO_LINE_FIRST));
        assertThat(result.getRecoupableAmount(), is("100%"));
    }

    @Test
    public void createNonTraditionalModelDescriptionCut() {
        when(positionMock.getDescription()).thenReturn(LONG_DESCRIPTION);
        when(repertoireOwnerMock.getCountry()).thenReturn(currentTerritoryMock);
        when(projectMock.getDealType()).thenReturn(ANOTHER_DEAL_TYPE);

        PurchaseOrderCoding result = sapCodingRulesService.createCoding(positionMock);

        assertThat(result.getRecoupableText(), is(not(VENDORNAME1 + "-" + LONG_DESCRIPTION)));
        assertThat(result.getRecoupableText().length(), is(50));
    }

    @Test
    public void createTraditionalModelUnrecoupable() {
        when(repertoireOwnerMock.getCountry()).thenReturn(currentTerritoryMock);
        when(projectMock.getDealType()).thenReturn(TRADITIONAL_MODEL);
        when(purchaseOrderPositionServiceMock.getRecoupmentPct(positionMock)).thenReturn(new BigDecimal("0.00"));

        PurchaseOrderCoding result = sapCodingRulesService.createCoding(positionMock);

        assertThat(result.getCodingType(), is(PurchaseOrderCodingType.ZERO_RECOUPABILITY));
        assertThat(result.getNonRecoupableAccount(), is(GLACCOUNT));
        assertThat(result.getNonRecoupableAssignment(), is(SAPCLIENTCODE));
        assertThat(result.getNonRecoupableOrder(), is(SAPION));
        assertThat(result.getNonRecoupableReferenceKey2(), is(PO_NUMBER + PO_LINE_DELIMITER + PO_LINE_FIRST));
        assertThat(result.getNonRecoupableReferenceKey3(), is(SPENDTYPECODE + POCOUNTRYCODE));
        assertThat(result.getNonRecoupableCostCenter(), is(SAPCOMPANYCODE + NON_CAT_COSTCTR));
        assertThat(result.getNonRecoupableOrderType(), is(NON_CAT_ORDER_TYPE));
        assertThat(result.getNonRecoupableText(), is(VENDORNAME1 + "-" + DESCRIPTION));
        assertThat(result.getNonRecoupableAmount(), is("100%"));
    }


    @Test
    public void createTraditionalModelCombined() {
        when(repertoireOwnerMock.getCountry()).thenReturn(currentTerritoryMock);
        when(projectMock.getDealType()).thenReturn(TRADITIONAL_MODEL);
        when(purchaseOrderPositionServiceMock.getRecoupmentPct(positionMock)).thenReturn(new BigDecimal("75.00"));

        PurchaseOrderCoding result = sapCodingRulesService.createCoding(positionMock);

        assertThat(result.getCodingType(), is(PurchaseOrderCodingType.MIXED_RECOUPABILITY));
        assertThat(result.getNonRecoupableAccount(), is(GLACCOUNT));
        assertThat(result.getNonRecoupableAssignment(), is(SAPCLIENTCODE));
        assertThat(result.getNonRecoupableOrder(), is(SAPION));
        assertThat(result.getNonRecoupableReferenceKey2(), is(PO_NUMBER + PO_LINE_DELIMITER + PO_LINE_FIRST));
        assertThat(result.getNonRecoupableReferenceKey3(), is(SPENDTYPECODE + POCOUNTRYCODE));
        assertThat(result.getNonRecoupableCostCenter(), is(SAPCOMPANYCODE + NON_CAT_COSTCTR));
        assertThat(result.getNonRecoupableOrderType(), is(NON_CAT_ORDER_TYPE));
        assertThat(result.getNonRecoupableText(), is(VENDORNAME1 + "-" + DESCRIPTION));
        assertThat(result.getNonRecoupableAmount(), is("25%"));
        assertThat(result.getRecoupableAccount(), is(SAPCLIENTCODE));
        assertThat(result.getRecoupableAssignment(), is(SPENDTYPECODE + POCOUNTRYCODE));
        assertThat(result.getRecoupableText(), is(VENDORNAME1 + "-" + DESCRIPTION));
        assertThat(result.getRecoupableReferenceKey2(), is(PO_NUMBER + PO_LINE_DELIMITER + PO_LINE_FIRST));
        assertThat(result.getRecoupableAmount(), is("75%"));


    }





}