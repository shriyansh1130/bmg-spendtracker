package de.syndicats.spendtracker.project.service;

import de.syndicats.spendtracker.budget.entity.BudgetCategory;
import de.syndicats.spendtracker.budget.entity.BudgetSpendType;
import de.syndicats.spendtracker.budget.entity.BudgetSubCategory;
import de.syndicats.spendtracker.budget.repository.BudgetSpendTypeRepository;
import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.currency.entity.Currency;
import de.syndicats.spendtracker.posting.entity.Posting;
import de.syndicats.spendtracker.posting.repository.PostingRepository;
import de.syndicats.spendtracker.project.entity.Project;
import de.syndicats.spendtracker.project.service.dto.ProjectBudgetCategoryDto;
import de.syndicats.spendtracker.project.service.dto.ProjectBudgetContainerDto;
import de.syndicats.spendtracker.project.service.dto.ProjectBudgetSpendTypeDto;
import de.syndicats.spendtracker.project.service.dto.ProjectBudgetSubCategoryDto;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import de.syndicats.spendtracker.purchaseorder.repository.PurchaseOrderPositionRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Sort;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProjectServiceTest {

    @Mock
    private BudgetSpendTypeRepository budgetSpendTypeRepository;

    @Mock
    private PostingRepository postingRepository;

    @Mock
    private PurchaseOrderPositionRepository purchaseOrderPositionRepository;

    @InjectMocks
    private ProjectService projectService;

    Country countryMock = mock(Country.class);
    Posting posting1 = mock(Posting.class);
    Posting posting2 = mock(Posting.class);
    Posting posting3 = mock(Posting.class);

    @Before
    public void setup() {
        Currency currency = mock(Currency.class);
        when(countryMock.getCurrency()).thenReturn(currency);
        BudgetCategory budgetCategory1 = mock(BudgetCategory.class);
        when(budgetCategory1.getTitle()).thenReturn("BudgetCategory 1");

        BudgetSubCategory budgetSubCategory1_1 = mock(BudgetSubCategory.class);
        when(budgetSubCategory1_1.getTitle()).thenReturn("BudgetSubCategory 1.1");
        when(budgetSubCategory1_1.getBudgetCategory()).thenReturn(budgetCategory1);

        BudgetSpendType budgetSpendType1_1_1 = mock(BudgetSpendType.class);
        when(budgetSpendType1_1_1.getTitle()).thenReturn("BudgetSpendType 1.1.1");
        when(budgetSpendType1_1_1.getBudgetSubCategory()).thenReturn(budgetSubCategory1_1);

        BudgetSubCategory budgetSubCategory1_2 = mock(BudgetSubCategory.class);
        when(budgetSubCategory1_2.getTitle()).thenReturn("BudgetSubCategory 1.2");
        when(budgetSubCategory1_2.getBudgetCategory()).thenReturn(budgetCategory1);

        BudgetSpendType budgetSpendType1_2_1 = mock(BudgetSpendType.class);
        when(budgetSpendType1_2_1.getTitle()).thenReturn("BudgetSpendType 1.2.1");
        when(budgetSpendType1_2_1.getBudgetSubCategory()).thenReturn(budgetSubCategory1_2);

        List<BudgetSpendType> budgetSpendTypes = new ArrayList<>();
        budgetSpendTypes.add(budgetSpendType1_1_1);
        budgetSpendTypes.add(budgetSpendType1_2_1);

        when(budgetSpendTypeRepository.findAll(any(Sort.class))).thenReturn(budgetSpendTypes);

        when(posting1.getBudgetSpendType()).thenReturn(budgetSpendType1_1_1);
        when(posting1.getBudgetTerritory()).thenReturn(countryMock);
        when(posting1.getAmountInLocalCurrency()).thenReturn(new BigDecimal(5));
        when(posting1.getCurrency()).thenReturn(currency);

        when(posting2.getBudgetSpendType()).thenReturn(budgetSpendType1_1_1);
        when(posting2.getBudgetTerritory()).thenReturn(countryMock);
        when(posting2.getAmountInLocalCurrency()).thenReturn(new BigDecimal(5));
        when(posting2.getCurrency()).thenReturn(currency);

        when(posting3.getBudgetSpendType()).thenReturn(budgetSpendType1_2_1);
        when(posting3.getBudgetTerritory()).thenReturn(countryMock);
        when(posting3.getAmountInLocalCurrency()).thenReturn(new BigDecimal(10));
        when(posting3.getCurrency()).thenReturn(currency);

        List<Posting> postings = new ArrayList<>();
        postings.add(posting1);
        postings.add(posting2);
        postings.add(posting3);

        when(postingRepository.findByProject(any())).thenReturn(postings);
    }

    @Test
    public void testGetProjectBudgetDtos() {

        ProjectBudgetContainerDto projectBudgetContainerDto = projectService.getProjectBudgetDtos(mock(Project.class), countryMock);

        for (ProjectBudgetCategoryDto projectBudgetDto : projectBudgetContainerDto.getCategoryBudgets()) {
            switch (projectBudgetDto.getTitle()) {
                case "BudgetCategory 1":
                    assertThat(projectBudgetDto.getPaid(), is(new BigDecimal(20)));

                    for (ProjectBudgetSubCategoryDto projectBudgetSubCategoryDto : projectBudgetDto.getChildren()) {
                        assertThat(projectBudgetSubCategoryDto.getPaid(), is(new BigDecimal(10)));

                        for (ProjectBudgetSpendTypeDto projectBudgetSpendTypeDto : projectBudgetSubCategoryDto.getChildren()) {
                            switch (projectBudgetSpendTypeDto.getTitle()) {
                                case "BudgetSpendType 1.1.1":
                                    assertThat(projectBudgetSpendTypeDto.getPaid(), is(new BigDecimal(10)));
                                    break;
                                case "BudgetSpendType 1.2.1":
                                    assertThat(projectBudgetSpendTypeDto.getPaid(), is(new BigDecimal(10)));
                                    break;
                            }
                        }
                    }
            }

        }
    }

    @Test
    public void testGetProjectBudgetOfMatchingPosting() {
        PurchaseOrderPosition positionWithOtherPoCountry = mock(PurchaseOrderPosition.class);
        Country otherCountry = mock(Country.class);
        when(positionWithOtherPoCountry.getPoCountry()).thenReturn(otherCountry);
        when(posting1.getPurchaseOrderPosition()).thenReturn(positionWithOtherPoCountry);

        ProjectBudgetContainerDto projectBudgetContainerDto = projectService.getProjectBudgetDtos(mock(Project.class), countryMock);

        for (ProjectBudgetCategoryDto projectBudgetDto : projectBudgetContainerDto.getCategoryBudgets()) {
            switch (projectBudgetDto.getTitle()) {
                case "BudgetCategory 1":
                    assertThat(projectBudgetDto.getPaid(), is(new BigDecimal(15)));

                    for (ProjectBudgetSubCategoryDto projectBudgetSubCategoryDto : projectBudgetDto.getChildren()) {
                        for (ProjectBudgetSpendTypeDto projectBudgetSpendTypeDto : projectBudgetSubCategoryDto.getChildren()) {
                            switch (projectBudgetSpendTypeDto.getTitle()) {
                                case "BudgetSpendType 1.1.1":
                                    assertThat(projectBudgetSpendTypeDto.getPaid(), is(new BigDecimal(5)));
                                    break;
                                case "BudgetSpendType 1.2.1":
                                    assertThat(projectBudgetSpendTypeDto.getPaid(), is(new BigDecimal(10)));
                                    break;
                            }
                        }
                    }
            }

        }
    }
}
