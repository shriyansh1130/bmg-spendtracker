package de.syndicats.spendtracker.project.entity;

import de.syndicats.spendtracker.budget.entity.BudgetCategory;
import de.syndicats.spendtracker.budget.entity.BudgetSpendType;
import de.syndicats.spendtracker.budget.entity.BudgetSubCategory;
import de.syndicats.spendtracker.budget.repository.BudgetCategoryRepository;
import de.syndicats.spendtracker.budget.repository.BudgetSpendTypeRepository;
import de.syndicats.spendtracker.budget.repository.BudgetSubCategoryRepository;
import de.syndicats.spendtracker.company.entity.Company;
import de.syndicats.spendtracker.company.repository.CompanyRepository;
import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.country.repository.CountryRepository;
import de.syndicats.spendtracker.project.repository.ProjectRecoupmentPercentageRepository;
import de.syndicats.spendtracker.project.repository.ProjectRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@ActiveProfiles("unittest")
@Transactional
public class ProjectRecoupmentPercentageTest {

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    CountryRepository countryRepository;

    @Autowired
    BudgetCategoryRepository budgetCategoryRepository;

    @Autowired
    BudgetSubCategoryRepository budgetSubCategoryRepository;

    @Autowired
    BudgetSpendTypeRepository budgetSpendTypeRepository;

    @Autowired
    ProjectRecoupmentPercentageRepository projectRecoupmentPercentageRepository;

    @Autowired
    Validator validator;

    @Autowired
    EntityManager entityManager;

    @Before
    public void setUp() {
        Country country = Country.builder()
                .countryName("COUNTRY_NAME")
                .countryCode("DE")
                .build();
        countryRepository.save(country);

        Company repertoireOwnerCompany = Company.builder()
                .companyName("COMPANY_NAME")
                .companyCode("CODE")
                .country(country)
                .sapCompanyCode("SAP_COMPANY_CODE")
                .build();
        companyRepository.save(repertoireOwnerCompany);

        BudgetCategory budgetCategory = BudgetCategory.builder()
                .title("TITLE")
                .displayOrder(1l)
                .build();
        budgetCategoryRepository.save(budgetCategory);

        BudgetSubCategory budgetSubCategory = BudgetSubCategory.builder()
                .title("TITLE")
                .displayOrder(1l)
                .budgetCategory(budgetCategory)
                .build();
        budgetSubCategoryRepository.save(budgetSubCategory);

        BudgetSpendType budgetSpendType = BudgetSpendType.builder()
                .title("TITLE")
                .budgetSubCategory(budgetSubCategory)
                .budgetSpendTypeCode("403")
                .glAccount("GL_ACCOUNT")
                .build();
        budgetSpendTypeRepository.save(budgetSpendType);

        Project project = Project.builder()
                .uniqueCode("UNIQUE_CODE")
                .sapClientCode("123456789")
                .projectName("PROJECT_NAME")
                .repertoireOwnerCompany(repertoireOwnerCompany)
                .ionSap("ION_SAP")
                .assignment("ASSIGNMENT")
                .label("LABEL")
                .dealType("DEAL_TYPE")
                .segment("SEGMENT")
                .priority(1)
                .build();
        projectRepository.save(project);
    }

    @Test
    public void constraintsSuccess() {
        Project project = projectRepository.findAll().get(0);
        BudgetSpendType budgetSpendType = budgetSpendTypeRepository.findAll().get(0);

        ProjectRecoupmentPercentageId projectRecoupmentPercentageId = new ProjectRecoupmentPercentageId(project, budgetSpendType);
        ProjectRecoupmentPercentage projectRecoupmentPercentage = new ProjectRecoupmentPercentage(projectRecoupmentPercentageId, BigDecimal.valueOf(100));

        projectRecoupmentPercentageRepository.save(projectRecoupmentPercentage);
    }

    @Test
    public void constraintsFail() {
        Project project = projectRepository.findAll().get(0);
        BudgetSpendType budgetSpendType = budgetSpendTypeRepository.findAll().get(0);

        ProjectRecoupmentPercentageId projectRecoupmentPercentageId = new ProjectRecoupmentPercentageId(project, budgetSpendType);
        ProjectRecoupmentPercentage projectRecoupmentPercentage = new ProjectRecoupmentPercentage(projectRecoupmentPercentageId, BigDecimal.valueOf(101));

        try {
            projectRecoupmentPercentageRepository.save(projectRecoupmentPercentage);
            entityManager.flush(); // flush to trigger Hibernate validation
            Assert.fail("Save with wrong constaints, exception expected.");
        } catch (ConstraintViolationException e) {
            // success
        }
    }

}