package de.syndicats.spendtracker.project.data;

import de.syndicats.spendtracker.budget.entity.BudgetSpendType;
import de.syndicats.spendtracker.budget.repository.BudgetSpendTypeRepository;
import de.syndicats.spendtracker.importer.data.ImportResult;
import de.syndicats.spendtracker.importer.repository.ImportTaskRepository;
import de.syndicats.spendtracker.project.entity.Project;
import de.syndicats.spendtracker.project.entity.ProjectRecoupmentPercentage;
import de.syndicats.spendtracker.project.entity.ProjectRecoupmentPercentageId;
import de.syndicats.spendtracker.project.repository.ProjectRecoupmentPercentageRepository;
import de.syndicats.spendtracker.project.repository.ProjectRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.io.ClassPathResource;

import javax.validation.Validator;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class ProjectBudgetSpendTypesImporterTest {

    private static final String PROJECT_UNIQUE_CODE_1 = "1007430-MR STAGE 1";
    private static final String PROJECT_UNIQUE_CODE_2 = "1007430-MR STAGE 2";
    private static final String PROJECT_UNIQUE_CODE_3 = "1007430-MR STAGE 3";

    @Mock
    private BudgetSpendTypeRepository budgetSpendTypeRepository;

    @Mock
    private ProjectRepository projectRepository;

    @Mock
    private ImportTaskRepository importTaskRepository;

    @Mock
    private ProjectRecoupmentPercentageRepository projectRecoupmentPercentageRepository;

    @Mock
    private Validator validator;

    @InjectMocks
    private ProjectBudgetSpendTypesImporter projectBudgetSpendTypesImporter;

    private Project projectMock1 = mock(Project.class);
    private Project projectMock2 = mock(Project.class);
    private Project projectMock3 = mock(Project.class);
    private BudgetSpendType spendTypeMock = mock(BudgetSpendType.class);
    private ProjectRecoupmentPercentage projectRecoupmentPercentageMock = mock(ProjectRecoupmentPercentage.class);


    @Before
    public void setUp() {
        projectBudgetSpendTypesImporter.setEntityRepository(projectRecoupmentPercentageRepository);
        projectBudgetSpendTypesImporter.setImportTaskRepository(importTaskRepository);
        projectBudgetSpendTypesImporter.setValidator(validator);
        when(projectRepository.findByUniqueCode(PROJECT_UNIQUE_CODE_1)).thenReturn(Optional.of(projectMock1));
        when(projectRepository.findByUniqueCode(PROJECT_UNIQUE_CODE_2)).thenReturn(Optional.of(projectMock2));
        when(projectRepository.findByUniqueCode(PROJECT_UNIQUE_CODE_3)).thenReturn(Optional.of(projectMock3));
        when(importTaskRepository.findById(-1L)).thenReturn(Optional.empty());
        when(budgetSpendTypeRepository.findByBudgetSpendTypeCode(any())).thenReturn(Optional.of(spendTypeMock));
    }

    @Test
    public void shouldImportWithNoErrors() throws IOException {
        InputStream inputStream = new ClassPathResource("ProjectBudgetSpendTypesImporter/test-RecoupPercentages.csv").getInputStream();
        ImportResult result = projectBudgetSpendTypesImporter.importRecords(-1,inputStream, false);
        verify(projectRecoupmentPercentageRepository, times(5)).save(any(ProjectRecoupmentPercentage.class));
        assertThat(result.getErrorCount(), is(0));
        assertThat(result.getInsertedCount(), is(5));
    }

    @Test
    public void shouldImportWithErrorsForNonFoundProject() throws IOException {
        InputStream inputStream = new ClassPathResource("ProjectBudgetSpendTypesImporter/test-RecoupPercentages.csv").getInputStream();
        when(projectRepository.findByUniqueCode(PROJECT_UNIQUE_CODE_3)).thenReturn(Optional.empty());
        ImportResult result = projectBudgetSpendTypesImporter.importRecords(-1,inputStream, false);
        verify(projectRecoupmentPercentageRepository, times(2)).save(any(ProjectRecoupmentPercentage.class));
        assertThat(result.getErrorCount(), is(3));
        assertThat(result.getInsertedCount(), is(2));
        assertThat(result.getErrorMessages().size(), is(3));
        assertThat(result.getErrorMessages().get(0), is("Line 3: Can't import project spend-type, because project: 1007430-MR STAGE 3 was not found"));
        assertThat(result.getErrorMessages().get(1), is("Line 4: Can't import project spend-type, because project: 1007430-MR STAGE 3 was not found"));
        assertThat(result.getErrorMessages().get(2), is("Line 5: Can't import project spend-type, because project: 1007430-MR STAGE 3 was not found"));

    }

    @Test
    public void shouldImportWithErrorsForNonFoundSpendTypes() throws IOException {
        InputStream inputStream = new ClassPathResource("ProjectBudgetSpendTypesImporter/test-RecoupPercentages.csv").getInputStream();
        when(budgetSpendTypeRepository.findByBudgetSpendTypeCode("101")).thenReturn(Optional.empty());
        ImportResult result = projectBudgetSpendTypesImporter.importRecords(-1,inputStream, false);
        verify(projectRecoupmentPercentageRepository, times(4)).save(any(ProjectRecoupmentPercentage.class));
        assertThat(result.getErrorCount(), is(1));
        assertThat(result.getInsertedCount(), is(4));
        assertThat(result.getErrorMessages().size(), is(1));
        assertThat(result.getErrorMessages().get(0), is("Line 1: Can't import project spend-type, because spend type code: 101 was not found"));
    }

    @Test
    public void shouldImportWithErrorsForEmptyRecoupmentPct() throws IOException {
        InputStream inputStream = new ClassPathResource("ProjectBudgetSpendTypesImporter/test-RecoupPercentagesWithEmptyPct.csv").getInputStream();
        ImportResult result = projectBudgetSpendTypesImporter.importRecords(-1,inputStream, false);
        verify(projectRecoupmentPercentageRepository, times(4)).save(any(ProjectRecoupmentPercentage.class));
        assertThat(result.getErrorCount(), is(1));
        assertThat(result.getInsertedCount(), is(4));
        assertThat(result.getErrorMessages().size(), is(1));
        assertThat(result.getErrorMessages().get(0), is("Line 1: Can't import project spend-type, because recoupment pecentage is empty"));
    }

    @Test
    public void shouldUpdateWhenAlreadyExisting() throws IOException {
        InputStream inputStream = new ClassPathResource("ProjectBudgetSpendTypesImporter/test-RecoupPercentages.csv").getInputStream();
        when(projectRecoupmentPercentageRepository.findById(new ProjectRecoupmentPercentageId(projectMock1, spendTypeMock))).thenReturn(Optional.of(projectRecoupmentPercentageMock));
        when(projectRecoupmentPercentageRepository.existsById(new ProjectRecoupmentPercentageId(projectMock1, spendTypeMock))).thenReturn(true);
        ImportResult result = projectBudgetSpendTypesImporter.importRecords(-1,inputStream, false);
        verify(projectRecoupmentPercentageRepository, times(5)).save(any(ProjectRecoupmentPercentage.class));
        assertThat(result.getErrorCount(), is(0));
        assertThat(result.getInsertedCount(), is(4));
        assertThat(result.getUpdatedCount(), is(1));
    }

    @Test
    public void shouldDeleteWhenExistsAndMarkedForDelete() throws IOException {
        InputStream inputStream = new ClassPathResource("ProjectBudgetSpendTypesImporter/test-RecoupPercentagesDelete.csv").getInputStream();
        when(projectRecoupmentPercentageRepository.findById(new ProjectRecoupmentPercentageId(projectMock1, spendTypeMock))).thenReturn(Optional.of(projectRecoupmentPercentageMock));
        ImportResult result = projectBudgetSpendTypesImporter.importRecords(-1,inputStream, false);
        verify(projectRecoupmentPercentageRepository, times(1)).delete(projectRecoupmentPercentageMock);
        assertThat(result.getErrorCount(), is(0));
        assertThat(result.getInsertedCount(), is(0));
        assertThat(result.getUpdatedCount(), is(0));
        assertThat(result.getDeletedCount(), is(1));
    }
}