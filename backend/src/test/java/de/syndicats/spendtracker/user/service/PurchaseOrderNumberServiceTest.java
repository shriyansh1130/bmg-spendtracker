package de.syndicats.spendtracker.user.service;

import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderNumber;
import de.syndicats.spendtracker.purchaseorder.repository.PurchaseOrderNumberRepository;
import de.syndicats.spendtracker.purchaseorder.service.PurchaseOrderNumberService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@TestPropertySource(properties = {"syndicats.devTestDataGen=false", "syndicats.initialDataGen=false"})
public class PurchaseOrderNumberServiceTest {

    @Autowired
    private PurchaseOrderNumberRepository repo;

    @Autowired
    private PurchaseOrderNumberService service;

    @Before
    public void setUp() {
        repo.save(PurchaseOrderNumber.builder()
                .id(PurchaseOrderNumber.Id.builder().countryCode("DE").yearOfCentury(Integer.valueOf(18)).build())
                .sequentialNumberCounter(Long.valueOf(100))
                .build());
    }

    @Test
    public void testCreateNewPurchaseOrderNumber() {
        String newPurchaseOrderNumber = service.createNewPurchaseOrderNumber("DE", Integer.valueOf(18));

        // verify
        assertThat(newPurchaseOrderNumber).isEqualTo("DE1800101");
        assertThat(repo.count()).isEqualTo(1);
    }

    @Test
    public void testCreateNewPurchaseOrderNumber_newYear() {
        String newPurchaseOrderNumber = service.createNewPurchaseOrderNumber("DE", Integer.valueOf(19));

        // verify
        assertThat(newPurchaseOrderNumber).isEqualTo("DE1900000");
    }

    @Test
    public void testCreateNewPurchaseOrderNumber_newCountry() {
        String newPurchaseOrderNumber = service.createNewPurchaseOrderNumber("GB", Integer.valueOf(18));

        // verify
        assertThat(newPurchaseOrderNumber).isEqualTo("GB1800000");
    }
}
