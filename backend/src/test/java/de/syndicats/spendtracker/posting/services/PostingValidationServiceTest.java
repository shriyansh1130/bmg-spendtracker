package de.syndicats.spendtracker.posting.services;

import de.syndicats.spendtracker.budget.entity.BudgetSpendType;
import de.syndicats.spendtracker.budget.repository.BudgetSpendTypeRepository;
import de.syndicats.spendtracker.company.entity.Company;
import de.syndicats.spendtracker.company.repository.CompanyRepository;
import de.syndicats.spendtracker.country.entity.Country;
import de.syndicats.spendtracker.country.repository.CountryRepository;
import de.syndicats.spendtracker.currency.entity.Currency;
import de.syndicats.spendtracker.currency.repository.CurrencyRepository;
import de.syndicats.spendtracker.exchangerate.entity.ExchangeRate;
import de.syndicats.spendtracker.exchangerate.repository.ExchangeRateRepository;
import de.syndicats.spendtracker.posting.entity.Posting;
import de.syndicats.spendtracker.posting.entity.RawPosting;
import de.syndicats.spendtracker.project.entity.Project;
import de.syndicats.spendtracker.project.repository.ProjectRepository;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrder;
import de.syndicats.spendtracker.purchaseorder.entity.PurchaseOrderPosition;
import de.syndicats.spendtracker.purchaseorder.repository.PurchaseOrderRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PostingValidationServiceTest {

    @Mock
    private ProjectRepository projectRepository;

    @Mock
    private PurchaseOrderRepository purchaseOrderRepository;

    @Mock
    private BudgetSpendTypeRepository budgetSpendTypeRepository;

    @Mock
    private CountryRepository countryRepository;

    @Mock
    private CurrencyRepository currencyRepository;

    @Mock
    private CompanyRepository companyRepository;

    @Mock
    private ExchangeRateRepository exchangeRateRepository;

    @Mock
    private RawPostingPoMatchingService rawPostingPoMatchingService;

    @InjectMocks
    private PostingValidationService postingValidationService;



    @Before
    public void setup() {
        Project project = mock(Project.class);
        when(project.getIonSap()).thenReturn("IONSAP");
        when(project.getSapClientCode()).thenReturn("CLIENTCODE");

        when(projectRepository.findByUniqueCode("PROJECTID")).thenReturn(Optional.of(project));

        Company company = mock(Company.class);

        PurchaseOrderPosition poPosition = mock(PurchaseOrderPosition.class);
        when(poPosition.getProject()).thenReturn(project);

        Country country = mock(Country.class);
        when(country.getCountryCode()).thenReturn("DE");

        BudgetSpendType budgetSpendType = mock(BudgetSpendType.class);
        when(budgetSpendType.getBudgetSpendTypeCode()).thenReturn("SPNDTYP");

        when(budgetSpendTypeRepository.findByBudgetSpendTypeCode("SPNDTYP")).thenReturn(Optional.of(budgetSpendType));


        when(rawPostingPoMatchingService.findMatchingPoLine(Mockito.argThat(argument -> argument.getPoNumber().equals("DE1800001-1")))).thenReturn(Optional.of(poPosition));


        PurchaseOrder po = mock(PurchaseOrder.class);
        Currency eurCurrency = mock(Currency.class);
        when(eurCurrency.getCurrencyCode()).thenReturn("EUR");
        when(po.getCurrency()).thenReturn(eurCurrency);

        Currency usdCurrency = mock(Currency.class);
        when(currencyRepository.findByCurrencyCode("EUR")).thenReturn(eurCurrency);
        when(currencyRepository.findByCurrencyCode("USD")).thenReturn(usdCurrency);
        when(poPosition.getPurchaseOrder()).thenReturn(po);
        when(poPosition.getPoCountry()).thenReturn(country);
        when(poPosition.getSpendType()).thenReturn(budgetSpendType);

        ExchangeRate exchangeRate = mock(ExchangeRate.class);
        when(exchangeRateRepository.findByFromCurrency_CurrencyCodeAndToCurrency_CurrencyCode("USD", "EUR")).thenReturn(Optional.of(exchangeRate));
    }

    @Test
    public void testValidateRawPostingSuccess() {
        RawPosting rawPosting = RawPosting.builder()
                .poNumber("DE1800001-1")
                .glSapIonCode("IONSAP")
                .sapClientCode("CLIENTCODE")
                .currencyCode("EUR")
                .budgetTerritoryCode("DE")
                .glSpendTypeCode("SPNDTYP")
                .sapCompanyCode("SAPCOMPANYCODE")
                .projectId("PROJECTID")
                .build();

        List<String> warnings = new ArrayList<>();
        List<String> errors = new ArrayList<>();
        postingValidationService.validateRawPosting(rawPosting, errors, warnings);

        assertThat(errors.size(), is(0));
    }


    @Test
    public void testValidateRawPostingError() {
        RawPosting rawPosting = RawPosting.builder()
                .poNumber("DE1800001-1").glSapIonCode("IONSAP")
                .sapClientCode("CLIENTCODE")
                .currencyCode("EUR")
                .budgetTerritoryCode("UK")
                .glSpendTypeCode("SPNDTYP")
                .sapCompanyCode("SAPCOMPANYCODE")
                .build();

        List<String> warnings = new ArrayList<>();
        List<String> errors = new ArrayList<>();
        postingValidationService.validateRawPosting(rawPosting, errors, warnings);

        assertThat(warnings.size(), is(1));
    }

    @Test
    public void testExchangeRateValid() {
        RawPosting rawPosting = RawPosting.builder()
                .poNumber("DE1800001-1")
                .glSapIonCode("IONSAP")
                .sapClientCode("CLIENTCODE")
                .currencyCode("USD")
                .budgetTerritoryCode("DE")
                .glSpendTypeCode("SPNDTYP")
                .sapCompanyCode("SAPCOMPANYCODE")
                .projectId("PROJECTID")
                .build();

        List<String> warnings = new ArrayList<>();
        List<String> errors = new ArrayList<>();
        postingValidationService.validateRawPosting(rawPosting, errors, warnings);

        assertThat(errors.size(), is(0));
    }

    @Test
    public void testExchangeRateError() {
        RawPosting rawPosting = RawPosting.builder()
                .poNumber("DE1800001-1")
                .glSapIonCode("IONSAP")
                .sapClientCode("CLIENTCODE")
                .currencyCode("PLN")
                .budgetTerritoryCode("UK")
                .glSpendTypeCode("SPNDTYP")
                .sapCompanyCode("SAPCOMPANYCODE")
                .projectId("PROJECTID")
                .build();

        List<String> warnings = new ArrayList<>();
        List<String> errors = new ArrayList<>();
        postingValidationService.validateRawPosting(rawPosting, errors, warnings);

        assertThat(warnings.size(), is(1));
    }


    @Test
    public void testPostingValidationForMatchingSuccess() {
        PurchaseOrderPosition position = mock(PurchaseOrderPosition.class);
        Posting posting = mock(Posting.class);
        Project project = mock(Project.class);
        BudgetSpendType spendType = mock(BudgetSpendType.class);
        Country country = mock(Country.class);
        Company company = mock(Company.class);

        when(spendType.getBudgetSpendTypeCode()).thenReturn("SPENDTYPECODE");
        when(company.getSapCompanyCode()).thenReturn("SAPCOMPANYCODE");

        when(country.getCountryCode()).thenReturn("COUNTRYCODE");

        when(project.getIonSap()).thenReturn("IONSAP");
        when(position.getProject()).thenReturn(project);

        when(posting.getGlSapIon()).thenReturn("IONSAP");

        when(posting.getSapClientCode()).thenReturn("SAPCLIENTCODE");
        when(project.getSapClientCode()).thenReturn("SAPCLIENTCODE");

        when(posting.getBudgetTerritory()).thenReturn(country);
        when(position.getPoCountry()).thenReturn(country);

        when(posting.getBudgetSpendType()).thenReturn(spendType);
        when(position.getSpendType()).thenReturn(spendType);

        when(project.getRepertoireOwnerCompany()).thenReturn(company);
        when(posting.getCompany()).thenReturn(company);

        List<String> warnings = new ArrayList<>();
        postingValidationService.validatePostingForMatching(position, posting, warnings);

        assertThat(warnings.size(), is(0));
    }

    @Test
    public void testPostingValidationForMatchingIonFail() {
        PurchaseOrderPosition position = mock(PurchaseOrderPosition.class);
        Posting posting = mock(Posting.class);
        Project project = mock(Project.class);
        BudgetSpendType spendType = mock(BudgetSpendType.class);
        Country country = mock(Country.class);
        Company company = mock(Company.class);

        when(spendType.getBudgetSpendTypeCode()).thenReturn("SPENDTYPECODE");
        when(company.getSapCompanyCode()).thenReturn("SAPCOMPANYCODE");

        when(country.getCountryCode()).thenReturn("COUNTRYCODE");

        when(project.getIonSap()).thenReturn("IONSAP");
        when(position.getProject()).thenReturn(project);

        when(posting.getGlSapIon()).thenReturn("FOOBAR");

        when(posting.getSapClientCode()).thenReturn("SAPCLIENTCODE");

        when(posting.getBudgetTerritory()).thenReturn(country);
        when(position.getPoCountry()).thenReturn(country);

        when(posting.getBudgetSpendType()).thenReturn(spendType);
        when(position.getSpendType()).thenReturn(spendType);

        when(project.getRepertoireOwnerCompany()).thenReturn(company);
        when(posting.getCompany()).thenReturn(company);

        List<String> warnings = new ArrayList<>();
        postingValidationService.validatePostingForMatching(position, posting, warnings);

        assertThat(warnings.size(), is(1));
        assertThat(warnings.get(0), containsString("SAP Project"));
    }

    @Test
    public void testPostingValidationForMatchingClientCodeFail() {
        PurchaseOrderPosition position = mock(PurchaseOrderPosition.class);
        Posting posting = mock(Posting.class);
        Project project = mock(Project.class);
        BudgetSpendType spendType = mock(BudgetSpendType.class);
        Country country = mock(Country.class);
        Company company = mock(Company.class);

        when(spendType.getBudgetSpendTypeCode()).thenReturn("SPENDTYPECODE");
        when(company.getSapCompanyCode()).thenReturn("SAPCOMPANYCODE");

        when(country.getCountryCode()).thenReturn("COUNTRYCODE");

        when(project.getIonSap()).thenReturn("IONSAP");
        when(position.getProject()).thenReturn(project);

        when(posting.getGlSapIon()).thenReturn("IONSAP");

        when(posting.getSapClientCode()).thenReturn("SAPCLIENTCODE");
        when(project.getSapClientCode()).thenReturn("FOOBAR");

        when(posting.getBudgetTerritory()).thenReturn(country);
        when(position.getPoCountry()).thenReturn(country);

        when(posting.getBudgetSpendType()).thenReturn(spendType);
        when(position.getSpendType()).thenReturn(spendType);

        when(project.getRepertoireOwnerCompany()).thenReturn(company);
        when(posting.getCompany()).thenReturn(company);

        List<String> warnings = new ArrayList<>();
        postingValidationService.validatePostingForMatching(position, posting, warnings);

        assertThat(warnings.size(), is(1));
        assertThat(warnings.get(0), containsString("SAP Project"));
    }

    @Test
    public void testPostingValidationForMatchingSpendTypeFail() {
        PurchaseOrderPosition position = mock(PurchaseOrderPosition.class);
        Posting posting = mock(Posting.class);
        Project project = mock(Project.class);
        BudgetSpendType spendType = mock(BudgetSpendType.class);
        BudgetSpendType spendType2 = mock(BudgetSpendType.class);
        Country country = mock(Country.class);
        Company company = mock(Company.class);

        when(spendType.getBudgetSpendTypeCode()).thenReturn("SPENDTYPECODE");
        when(spendType2.getBudgetSpendTypeCode()).thenReturn("FOOBAR");
        when(company.getSapCompanyCode()).thenReturn("SAPCOMPANYCODE");

        when(country.getCountryCode()).thenReturn("COUNTRYCODE");

        when(project.getIonSap()).thenReturn("IONSAP");
        when(position.getProject()).thenReturn(project);

        when(posting.getGlSapIon()).thenReturn("IONSAP");

        when(posting.getSapClientCode()).thenReturn("SAPCLIENTCODE");
        when(project.getSapClientCode()).thenReturn("SAPCLIENTCODE");

        when(posting.getBudgetTerritory()).thenReturn(country);
        when(position.getPoCountry()).thenReturn(country);

        when(posting.getBudgetSpendType()).thenReturn(spendType);
        when(position.getSpendType()).thenReturn(spendType2);

        when(project.getRepertoireOwnerCompany()).thenReturn(company);
        when(posting.getCompany()).thenReturn(company);

        List<String> warnings = new ArrayList<>();
        postingValidationService.validatePostingForMatching(position, posting, warnings);

        assertThat(warnings.size(), is(1));
        assertThat(warnings.get(0), containsString("SAP Spend Type"));
    }

    @Test
    public void testPostingValidationForMatchingCountryFail() {
        PurchaseOrderPosition position = mock(PurchaseOrderPosition.class);
        Posting posting = mock(Posting.class);
        Project project = mock(Project.class);
        BudgetSpendType spendType = mock(BudgetSpendType.class);
        Country country = mock(Country.class);
        Country country2 = mock(Country.class);
        Company company = mock(Company.class);

        when(spendType.getBudgetSpendTypeCode()).thenReturn("SPENDTYPECODE");
        when(company.getSapCompanyCode()).thenReturn("SAPCOMPANYCODE");

        when(country.getCountryCode()).thenReturn("COUNTRYCODE");
        when(country2.getCountryCode()).thenReturn("FOOBAR");

        when(project.getIonSap()).thenReturn("IONSAP");
        when(position.getProject()).thenReturn(project);

        when(posting.getGlSapIon()).thenReturn("IONSAP");

        when(posting.getSapClientCode()).thenReturn("SAPCLIENTCODE");
        when(project.getSapClientCode()).thenReturn("SAPCLIENTCODE");

        when(posting.getBudgetTerritory()).thenReturn(country);
        when(position.getPoCountry()).thenReturn(country2);

        when(posting.getBudgetSpendType()).thenReturn(spendType);
        when(position.getSpendType()).thenReturn(spendType);

        when(project.getRepertoireOwnerCompany()).thenReturn(company);
        when(posting.getCompany()).thenReturn(company);

        List<String> warnings = new ArrayList<>();
        postingValidationService.validatePostingForMatching(position, posting, warnings);

        assertThat(warnings.size(), is(1));
        assertThat(warnings.get(0), containsString("SAP Budget Territory"));
    }

    @Test
    public void testPostingValidationForMatchingSapCompanyCodeFail() {
        PurchaseOrderPosition position = mock(PurchaseOrderPosition.class);
        Posting posting = mock(Posting.class);
        Project project = mock(Project.class);
        BudgetSpendType spendType = mock(BudgetSpendType.class);
        Country country = mock(Country.class);
        Company company = mock(Company.class);
        Company company2 = mock(Company.class);

        when(spendType.getBudgetSpendTypeCode()).thenReturn("SPENDTYPECODE");
        when(company.getSapCompanyCode()).thenReturn("SAPCOMPANYCODE");
        when(company2.getSapCompanyCode()).thenReturn("FOOBAR");

        when(country.getCountryCode()).thenReturn("COUNTRYCODE");

        when(project.getIonSap()).thenReturn("IONSAP");
        when(position.getProject()).thenReturn(project);

        when(posting.getGlSapIon()).thenReturn("IONSAP");

        when(posting.getSapClientCode()).thenReturn("SAPCLIENTCODE");
        when(project.getSapClientCode()).thenReturn("SAPCLIENTCODE");

        when(posting.getBudgetTerritory()).thenReturn(country);
        when(position.getPoCountry()).thenReturn(country);

        when(posting.getBudgetSpendType()).thenReturn(spendType);
        when(position.getSpendType()).thenReturn(spendType);

        when(project.getRepertoireOwnerCompany()).thenReturn(company2);
        when(posting.getCompany()).thenReturn(company);

        List<String> warnings = new ArrayList<>();
        postingValidationService.validatePostingForMatching(position, posting, warnings);

        assertThat(warnings.size(), is(1));
        assertThat(warnings.get(0), containsString("SAP Company Code"));
    }
}