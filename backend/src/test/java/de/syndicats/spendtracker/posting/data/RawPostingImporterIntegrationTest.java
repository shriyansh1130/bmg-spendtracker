package de.syndicats.spendtracker.posting.data;

import de.syndicats.spendtracker.posting.entity.RawPosting;
import de.syndicats.spendtracker.posting.repository.RawPostingRepository;
import de.syndicats.spendtracker.posting.services.PostingValidationService;
import de.syndicats.spendtracker.purchaseorder.repository.PurchaseOrderRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;


//TODO: fix test
@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@Transactional
public class RawPostingImporterIntegrationTest {

    @Autowired
    private RawPostingRepository rawPostingRepository;

    @Mock
    private PostingValidationService postingValidationService;

    private RawPostingImporter rawPostingImporter;

    @Mock
    private PurchaseOrderRepository purchaseOrderRepository;

    @Before
    public void setup() {
        //rawPostingImporter = new RawPostingImporter(rawPostingRepository, postingValidationService);
    }


    //@Test
    public void testInsertValid() throws IOException {
        InputStream inputStream = new ClassPathResource("RawPostingImporter/testSapData-valid.csv").getInputStream();
        rawPostingImporter.importRecords(-1,inputStream, false);
        assertThat(rawPostingRepository.findAll().size(), is(5));
    }

    //@Test
    public void testDeleteExistingUniqueKeys() throws IOException {
        RawPosting rawPosting = RawPosting.builder().uniqueKey("2016541771000000072").text("foobar").build();
        rawPostingRepository.save(rawPosting);

        InputStream inputStream = new ClassPathResource("RawPostingImporter/testSapData-valid.csv").getInputStream();
        rawPostingImporter.importRecords(-1,inputStream, false);
        assertThat(rawPostingRepository.findAll().size(), is(5));
        assertThat(rawPostingRepository.findByUniqueKey("2016541771000000072").size(), is(1));
        assertThat(rawPostingRepository.findByUniqueKey("2016541771000000072").get(0).getText(), not("foobar"));
    }

    @Test
    public void testValidateMatching() {

    }

}