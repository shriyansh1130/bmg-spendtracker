package de.syndicats.spendtracker.posting.data;

import de.syndicats.spendtracker.importer.data.ImportResult;
import de.syndicats.spendtracker.importer.repository.ImportTaskRepository;
import de.syndicats.spendtracker.posting.repository.RawPostingRepository;
import de.syndicats.spendtracker.posting.services.PostingValidationService;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

//TODO: fix test
//@RunWith(MockitoJUnitRunner.class)
public class RawPostingImporterTest {

    @Mock
    private PostingValidationService postingValidationService;

    @Mock
    private RawPostingRepository rawPostingRepository;

    @Mock
    private ImportTaskRepository importTaskRepository;

    @InjectMocks
    private RawPostingImporter rawPostingImporterToTest;


    @Before
    public void setUp() {


    }

    // @Test
    public void testImportRecordsNonUnique() throws IOException {
        InputStream inputStream = new ClassPathResource("RawPostingImporter/testSapData-nonUnique.csv").getInputStream();
        ImportResult result = rawPostingImporterToTest.importRecords(-1,inputStream, false);
        assertThat(result.getErrorCount(), is(2));
        assertThat(result.getErrorMessages().get(0), is("Line 4: UniqueKey is not unique"));
        assertThat(result.getInsertedCount(), is(3));
    }

    //@Test
    public void testImportRecordsValid() throws IOException {
        InputStream inputStream = new ClassPathResource("RawPostingImporter/testSapData-valid.csv").getInputStream();
        ImportResult result = rawPostingImporterToTest.importRecords(-1,inputStream, false);
        assertThat(result.getErrorCount(), is(0));
        assertThat(result.getErrorMessages().size(), is(0));
        assertThat(result.getInsertedCount(), is(5));
    }


    //@Test
    public void testImportRecordsMissingMandatory() throws IOException {
        InputStream inputStream = new ClassPathResource("RawPostingImporter/testSapData-missingMandatory.csv").getInputStream();
        ImportResult result = rawPostingImporterToTest.importRecords(-1,inputStream, false);
        assertThat(result.getErrorCount(), is(1));
        assertThat(result.getErrorMessages().get(0).split("\\|").length, is(19));
        assertThat(result.getInsertedCount(), is(0));
    }


    // @Test
    public void testImportRecordsMissingMandatoryForPlFileType() throws IOException {
        InputStream inputStream = new ClassPathResource("RawPostingImporter/testSapData-missingMandatoryForPL.csv").getInputStream();
        ImportResult result = rawPostingImporterToTest.importRecords(-1,inputStream, false);
        assertThat(result.getErrorCount(), is(1));
        assertThat(result.getErrorMessages().get(0).split("\\|").length, is(1));
        assertThat(result.getInsertedCount(), is(4));
    }
}