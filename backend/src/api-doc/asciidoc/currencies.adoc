[[resources-currencies]]
== Currencies

[[resources-currencies-api-doc-example-get]]
=== Get a currency

`GET /api/currencies/{id}`

==== Authorization

include::{snippets}/api-doc-currencies-test/api-doc-example-get/auto-authorization.adoc[]

==== Path parameters

include::{snippets}/api-doc-currencies-test/api-doc-example-get/auto-path-parameters.adoc[]

==== Request fields

include::{snippets}/api-doc-currencies-test/api-doc-example-get/auto-request-fields.adoc[]

==== Example request/response

include::{snippets}/api-doc-currencies-test/api-doc-example-get/curl-request.adoc[]
include::{snippets}/api-doc-currencies-test/api-doc-example-get/http-response.adoc[]


[[resources-currencies-api-doc-example-get-all]]
=== Get all currencies

`GET /api/currencies`

==== Authorization

include::{snippets}/api-doc-currencies-test/api-doc-example-get-all/auto-authorization.adoc[]

==== Request fields

include::{snippets}/api-doc-currencies-test/api-doc-example-get-all/auto-request-fields.adoc[]

==== Example request/response

include::{snippets}/api-doc-currencies-test/api-doc-example-get-all/curl-request.adoc[]
include::{snippets}/api-doc-currencies-test/api-doc-example-get-all/http-response.adoc[]
