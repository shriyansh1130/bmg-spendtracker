import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LogoutComponent} from "./auth/logout/logout.component";
import {AccessDeniedComponent} from "./auth/access-denied/access-denied.component";
import {OktaCallbackComponent} from "@okta/okta-angular";

const routes: Routes = [
  {path: 'login/callback', component: OktaCallbackComponent},
  {path: 'logout', component: LogoutComponent},
  {path: 'accessDenied', component: AccessDeniedComponent},
  {path: '', redirectTo: '/projects', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
