import {Component, OnInit} from '@angular/core';
import {SpendTrackerStoreService} from './core/services/spend-tracker-store.service';
import {MatSelectChange} from '@angular/material';
import {Router} from '@angular/router';
import {OktaAuthService} from "@okta/okta-angular";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'app';
  isAuthenticated: boolean;

  constructor(public storeService: SpendTrackerStoreService, private route: Router, private oktaAuth: OktaAuthService, private http: HttpClient) {
  }

  onUserDropdownChanged(selection: MatSelectChange) {
    switch (selection.value) {
      case 'logout':
        this.route.navigate(['logout']);
        break;
    }
  }

  async ngOnInit() {
    this.isAuthenticated = await this.oktaAuth.isAuthenticated();
    if (this.isAuthenticated) {
      this.fetchUserData();
    } else {
      this.oktaAuth.$authenticationState.subscribe(value => value && this.fetchUserData());
    }
  }

  private fetchUserData() {
    this.http.get('api/users/me').subscribe(response => {
      this.storeService.setUser(response);
      this.isAuthenticated = true;
    });
  }
}
