import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {PurchaseOrderPosition} from "../purchase-order.model";
import {MAT_DIALOG_DATA, MatDialog} from "@angular/material";
import {ResourceService} from "../../resource/resource.service";
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'bst-purchase-order-position-notes',
  templateUrl: './purchase-order-position-notes.component.html',
  styleUrls: ['./purchase-order-position-notes.component.scss']
})
export class PurchaseOrderPositionNotesComponent implements OnInit {

  @Input() purchaseOrderPosition: PurchaseOrderPosition;

  constructor() { }

  ngOnInit() {

  }

}

@Component({
  selector: 'bst-purchase-order-position-notes-dialog',
  template: `
    <bst-purchase-order-position-notes
      [purchaseOrderPosition]="purchaseOrderPosition"></bst-purchase-order-position-notes>`
})
export class PurchaseOrderPositionNotesDialog implements OnInit {
  purchaseOrderPosition: PurchaseOrderPosition;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    this.purchaseOrderPosition = data.purchaseOrderPosition;
  }

  ngOnInit() {
  }
}

@Component({
  selector: 'bst-purchase-order-position-notes-button',
  template: `
    <button mat-icon-button (click)="showModal($event)">
      <mat-icon *ngIf="notesExist" matBadgePosition="after"
                matBadgeSize="small">speaker_notes
      </mat-icon>
      <mat-icon *ngIf="!notesExist" matBadgePosition="after"
                matBadgeSize="small">note
      </mat-icon>
    </button>`,
})

export class PurchaseOrderPositionNotesButton implements OnInit {

  @Input() purchaseOrderPosition: PurchaseOrderPosition;
  @Input() group: FormGroup;
  @Input() index: number;
  @Output() submit: EventEmitter<any> = new EventEmitter();
  notesExist = false;

  constructor(public dialog: MatDialog, public resourceService: ResourceService) {
  }

  ngOnInit() {
      if(this.purchaseOrderPosition.notes && this.purchaseOrderPosition.notes.length > 0){
        this.notesExist = true;
      }
  }

  showModal(event) {
    event.preventDefault();
    event.stopPropagation();

    let dialogRef = this.dialog.open(PurchaseOrderPositionNotesDialog, {
      panelClass: 'info-panel',
      height: '400px',
      width: '600px',
      data: {
        purchaseOrderPosition: this.purchaseOrderPosition
      },
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      let g: FormGroup = <FormGroup>this.group.get('positions.'+this.index);
      g.patchValue(this.purchaseOrderPosition);
    });
  }
}
