import {Component, Inject, Input, OnInit} from '@angular/core';
import {PurchaseOrderPosition} from "@app/purchase-order/purchase-order.model";
import {MAT_DIALOG_DATA, MatDialog} from "@angular/material";
import {PurchaseOrderPositionService} from "@app/purchase-order/services/purchase-order-position.service";

@Component({
  selector: 'bst-purchase-order-position-warnings',
  templateUrl: './purchase-order-position-warnings.component.html',
  styleUrls: ['./purchase-order-position-warnings.component.scss']
})
export class PurchaseOrderPositionWarningsDialog implements OnInit {

  @Input() warnings: Array<string>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    this.warnings = data.warnings;
  }
  ngOnInit() {
  }

}

@Component({
  selector: 'bst-purchase-order-position-warnings-button',
  template: `
    <button *ngIf="warningsExist" mat-icon-button [matTooltip]=warningsTooltip (click)="showModal($event)">
      <mat-icon color="warn" matBadgePosition="after"
                matBadgeSize="small">warning
      </mat-icon>
    </button>`,
})

export class PurchaseOrderPositionWarningsButton implements OnInit {
  @Input() purchaseOrderPosition: PurchaseOrderPosition;
  warningsExist = false;
  warnings: string[];
  warningsTooltip: string = '';

  constructor(public dialog: MatDialog, public purchaseOrderPositionService: PurchaseOrderPositionService) {
  }

  ngOnInit() {
    if(this.purchaseOrderPosition && this.purchaseOrderPosition.id){
      this.purchaseOrderPositionService.fetchWarnings(this.purchaseOrderPosition.id).then(response => {
        this.warnings = response;
        if(this.warnings.length > 0){
          this.warningsExist = true;
          for(let warning of this.warnings){
            if(warning.includes('SAP Project')){
              this.warningsTooltip = this.warningsTooltip +' SAP Project,';
            }else if(warning.includes('SAP Spend Type')){
              this.warningsTooltip = this.warningsTooltip +' SAP Spend Type,';
            }else if(warning.includes('SAP Budget Territory')){
              this.warningsTooltip = this.warningsTooltip +' SAP Budget Territory,';
            } else if (warning.includes('SAP Company Code')) {
              this.warningsTooltip = this.warningsTooltip + ' SAP Company Code,';
            }
          }
          this.warningsTooltip = this.warningsTooltip.substr(0, this.warningsTooltip.length-1);
        }
      })
    }
  }

  showModal(event) {
    event.preventDefault();
    event.stopPropagation();

    let dialogRef = this.dialog.open(PurchaseOrderPositionWarningsDialog, {
      panelClass: 'info-panel',
      data: {
        warnings: this.warnings
      },
      autoFocus: false
    });
  }
}

