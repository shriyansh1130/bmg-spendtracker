import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PurchaseOrderRoutingModule} from './purchase-order-routing.module';
import {BstMaterialModule} from "../bst-material/bst-material.module";
import {PurchaseOrderDetailsComponent} from './purchase-order-details/purchase-order-details.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";
import {CreatePurchaseOrderStoreService} from './services/create-purchase-order-store.service';
import {FlexLayoutModule} from "@angular/flex-layout";
import {PurchaseOrderCreateComponentWrapper} from "./purchase-order-details/purchase-order-create-wrapper.component";
import {PurchaseOrderPositionFormComponent} from './purchase-order-details/components/purchase-order-position-form/purchase-order-position-form.component';
import {PurchaseOrderPositionDialogComponent} from './purchase-order-details/components/purchase-order-position-dialog/purchase-order-position-dialog.component';
import {CanActivateCreatePurchaseOrder} from "./purchase-order-details/purchase-order-create.guard";
import {PurchaseOrderHeaderFormComponent} from './purchase-order-details/components/purchase-order-header-form/purchase-order-header-form.component';
import {PurchaseOrderPositionService} from "./services/purchase-order-position.service";
import {PurchaseOrderPositionListComponent} from './purchase-order-details/components/purchase-order-position-list/purchase-order-position-list.component';
import {PurchaseOrderService} from "./services/purchase-order.service";
import {PurchaseOrderFormInvalidDialog} from "./purchase-order-details/purchase-order-form-invalid-dialog.component";
import {PurchaseOrderSummaryComponent} from './purchase-order-summary/purchase-order-summary.component';
import {ExportService} from "../shared/services/export.service";
import {PurchaseOrderEditComponentWrapper} from "./purchase-order-details/purchase-order-edit-wrapper.component";
import {PurchaseOrderButtonBarComponent} from './purchase-order-details/components/purchase-order-button-bar/purchase-order-button-bar.component';
import {CurrencyModule} from "../currency/currency.module";
import {ForceAutocompleteDirective} from "../shared/directives/force-autocomplete.directive";
import {ConfirmationDialogComponent} from "../shared/confirmation-dialog/confirmation-dialog.component";
import {PurchaseOrderPositionSpendTypeComponent} from './purchase-order-position-spend-type/purchase-order-position-spend-type.component';
import {PurchaseOrderSummaryStoreService} from "./services/purchase-order-summary-store.service";
import {
  PurchaseOrderPositionAttachmentsButton,
  PurchaseOrderPositionAttachmentsComponent,
  PurchaseOrderPositionAttachmentsDialog
} from './purchase-order-position-attachments/purchase-order-position-attachments.component';
import {DropzoneModule} from "ngx-dropzone-wrapper";
import {PurchaseOrderPositionAttachmentService} from "./services/purchase-order-position-attachment.service";
import {
  PurchaseOrderPositionNotesButton,
  PurchaseOrderPositionNotesComponent,
  PurchaseOrderPositionNotesDialog
} from './purchase-order-position-notes/purchase-order-position-notes.component';
import {AuthModule} from "../auth/auth.module";
import {
  PurchaseOrderPositionCodingRulesComponentFullRecoupability,
  PurchaseOrderPositionCodingRulesComponentMixedRecoupability,
  PurchaseOrderPositionCodingRulesComponentNonRecoupable,
  PurchaseOrderPositionCodingRulesComponentRecharge,
} from './purchase-order-details/components/purchase-order-position-coding-rules/purchase-order-position-coding-rules.component';
import {
  PurchaseOrderPositionWarningsButton,
  PurchaseOrderPositionWarningsDialog
} from './purchase-order-position-warnings/purchase-order-position-warnings.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    BstMaterialModule,
    CurrencyModule,
    PurchaseOrderRoutingModule,
    SharedModule,
    DropzoneModule,
    AuthModule
  ],
  declarations: [
    PurchaseOrderDetailsComponent,
    PurchaseOrderCreateComponentWrapper,
    PurchaseOrderEditComponentWrapper,
    PurchaseOrderPositionFormComponent,
    PurchaseOrderPositionDialogComponent,
    PurchaseOrderHeaderFormComponent,
    PurchaseOrderPositionListComponent,
    PurchaseOrderFormInvalidDialog,
    PurchaseOrderSummaryComponent,
    PurchaseOrderButtonBarComponent,
    ForceAutocompleteDirective,
    PurchaseOrderPositionSpendTypeComponent,
    PurchaseOrderPositionAttachmentsButton,
    PurchaseOrderPositionAttachmentsDialog,
    PurchaseOrderPositionAttachmentsComponent,
    PurchaseOrderPositionNotesComponent,
    PurchaseOrderPositionNotesButton,
    PurchaseOrderPositionNotesDialog,
    PurchaseOrderPositionCodingRulesComponentFullRecoupability,
    PurchaseOrderPositionCodingRulesComponentRecharge,
    PurchaseOrderPositionCodingRulesComponentNonRecoupable,
    PurchaseOrderPositionCodingRulesComponentMixedRecoupability,
    PurchaseOrderPositionWarningsButton,
    PurchaseOrderPositionWarningsDialog
  ],
  providers: [
    CreatePurchaseOrderStoreService,
    CanActivateCreatePurchaseOrder,
    PurchaseOrderPositionService,
    PurchaseOrderPositionAttachmentService,
    PurchaseOrderService,
    ExportService,
    PurchaseOrderSummaryStoreService
  ],
  entryComponents: [
    PurchaseOrderFormInvalidDialog,
    ConfirmationDialogComponent,
    PurchaseOrderPositionAttachmentsDialog,
    PurchaseOrderPositionNotesDialog,
    PurchaseOrderPositionWarningsDialog,
    PurchaseOrderPositionCodingRulesComponentFullRecoupability,
    PurchaseOrderPositionCodingRulesComponentRecharge,
    PurchaseOrderPositionCodingRulesComponentNonRecoupable,
    PurchaseOrderPositionCodingRulesComponentMixedRecoupability,
  ]
})
export class PurchaseOrderModule { }
