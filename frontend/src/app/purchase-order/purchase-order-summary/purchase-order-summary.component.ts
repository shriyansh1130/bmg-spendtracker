import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import {fromEvent, merge} from "rxjs";
import {debounceTime, distinctUntilChanged, tap} from "rxjs/operators";
import {MatCheckboxChange, MatSort, PageEvent} from "@angular/material";
import {PurchaseOrderPositionTableDataSource} from "./purchase-order-position-table-data-source";
import {PurchaseOrderPositionService} from "../services/purchase-order-position.service";
import {ExportService} from "../../shared/services/export.service";
import {AppConfigService} from '../../core/services/app-config.service';
import {PurchaseOrderPosition} from "../purchase-order.model";
import {Router} from "@angular/router";
import {CustomPaginatorComponent} from "../../shared/custom-paginator/custom-paginator.component";
import {combineLatest} from "rxjs/index";
import {take} from "rxjs/internal/operators";
import {PurchaseOrderSummaryStoreService} from "../services/purchase-order-summary-store.service";

@Component({
  selector: 'bst-purchase-order-summary',
  templateUrl: './purchase-order-summary.component.html',
  styleUrls: ['./purchase-order-summary.component.scss']
})
export class PurchaseOrderSummaryComponent implements OnInit, AfterViewInit {

  dataSource: PurchaseOrderPositionTableDataSource;
  pageChanged = new EventEmitter<number>();

  @ViewChildren(CustomPaginatorComponent) paginators: QueryList<CustomPaginatorComponent>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filterStatusInput') filterStatusInput: ElementRef;
  @ViewChild('filterProjectInput') filterProjectInput: ElementRef;
  @ViewChild('filterSapCodeInput') filterSapCodeInput: ElementRef;
  @ViewChild('filterSpendTypeInput') filterSpendTypeInput: ElementRef;
  @ViewChild('filterPoNumberInput') filterPoNumberInput: ElementRef;
  @ViewChild('filterVendorInput') filterVendorInput: ElementRef;
  @ViewChild('filterCountryInput') filterCountryInput: ElementRef;
  @ViewChild('filterCreatedByInput') filterCreatedByInput: ElementRef;

  columnsToDisplay = [
    'select',
    'purchaseOrder.status',
    'project.projectName',
    'spendType.title',
    'purchaseOrder.purchaseOrderNumber',
    'description',
    'purchaseOrder.vendor.name1',
    'purchaseOrder.country.countryName',
    'total',
    'paid',
    'openPo',
    'createdBy'
  ];

  public selectedPurchaseOrderPositionIds: string[] = [];

  constructor(public appConfig: AppConfigService,
              public purchaseOrderPositionService: PurchaseOrderPositionService,
              private exportService: ExportService,
              private router: Router,
              public  storeService: PurchaseOrderSummaryStoreService
  ) {
  }

  ngOnInit() {
    this.dataSource = new PurchaseOrderPositionTableDataSource(this.appConfig, this.purchaseOrderPositionService);

    combineLatest(
      this.storeService.statusFilter,
      this.storeService.spendTypeFilter,
      this.storeService.createdByFilter,
      this.storeService.projectFilter,
      this.storeService.vendorFilter,
      this.storeService.purchaseOrderNumberFilter,
      this.storeService.territoryFilter
    ).pipe(
      take(1)
    ).subscribe(([status, spendType, createdBy, projectName, vendorName, poNumber, territoryName]) =>
      this.dataSource.loadPurchaseOrderPositions(status, spendType, createdBy, poNumber, projectName, territoryName, vendorName));
  }

  ngAfterViewInit() {
    fromEvent(document.querySelectorAll('.po-filter'), 'keyup')
      .pipe(
        debounceTime(this.appConfig.filterInputDebounceInMillis),
        distinctUntilChanged(),
        tap(() => {
          this.applyFilter();
          this.goToPageZero();
          this.loadPurchaseOrderPositionsPage();
        })
      ).subscribe();

    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.goToPageZero());

    merge(this.sort.sortChange, this.pageChanged)
      .pipe(
        tap(() => {
          this.loadPurchaseOrderPositionsPage()
        })
      ).subscribe();
  }

  loadPurchaseOrderPositionsPage() {
    this.clearSelectedPurchaseOrderPositionIds();

    this.dataSource.loadPurchaseOrderPositions(
      this.filterStatusInput.nativeElement.value,
      this.filterSpendTypeInput.nativeElement.value,
      this.filterCreatedByInput.nativeElement.value,
      this.filterPoNumberInput.nativeElement.value,
      this.filterProjectInput.nativeElement.value,
      this.filterCountryInput.nativeElement.value,
      this.filterVendorInput.nativeElement.value,
      this.sort.active,
      this.sort.direction,
      this.paginators.first.pageIndex,
      this.paginators.first.pageSize);
  }

  downloadOrders() {
    this.exportService.downloadOrders(this.selectedPurchaseOrderPositionIds);
  }

  gotToPurchaseOrderDetails(position: PurchaseOrderPosition) {
    position.resolveOneAssociation('purchaseOrder').then((pos) => {
      this.router.navigate(['orders', (<PurchaseOrderPosition>pos).purchaseOrder.id])
    })
  }

  changePage(event: PageEvent) {
    this.clearSelectedPurchaseOrderPositionIds();
    this.paginators.forEach((p) => {
      p.pageIndex = event.pageIndex, p.pageSize = event.pageSize
    });
    this.pageChanged.emit(event.pageIndex);
  }

  private goToPageZero() {
    this.clearSelectedPurchaseOrderPositionIds();
    this.paginators.forEach(p => p.pageIndex = 0);
  }

  private applyFilter() {
    this.storeService.filterStatus(this.filterStatusInput.nativeElement.value);
    this.storeService.filterSpendType(this.filterSpendTypeInput.nativeElement.value);
    this.storeService.filterCreatedBy(this.filterCreatedByInput.nativeElement.value);
    this.storeService.filterProject(this.filterProjectInput.nativeElement.value);
    this.storeService.filterPurchaseOrderNumber(this.filterPoNumberInput.nativeElement.value);
    this.storeService.filterSpendType(this.filterSpendTypeInput.nativeElement.value);
    this.storeService.filterTerritory(this.filterCountryInput.nativeElement.value);
    this.storeService.filterVendor(this.filterVendorInput.nativeElement.value);
    this.storeService.filterCreatedBy(this.filterCreatedByInput.nativeElement.value);
  }

  onSelectPurchaseOrderPosition($event: MatCheckboxChange, purchaseOrderPosition: PurchaseOrderPosition) {
    if ($event.checked) {
      this.selectedPurchaseOrderPositionIds.push(purchaseOrderPosition.id);
    } else {
      this.selectedPurchaseOrderPositionIds.splice(this.selectedPurchaseOrderPositionIds.indexOf(purchaseOrderPosition.id), 1);
    }
  }

  selectAll($event: MatCheckboxChange) {
    if ($event.checked) {
      this.dataSource.getCurrentPurchaseOrderPositions().map((p) => this.selectedPurchaseOrderPositionIds.push(p.id));
    } else {
      this.clearSelectedPurchaseOrderPositionIds();
    }
  }

  clearSelectedPurchaseOrderPositionIds(): void {
    this.selectedPurchaseOrderPositionIds.length = 0;
  }

}
