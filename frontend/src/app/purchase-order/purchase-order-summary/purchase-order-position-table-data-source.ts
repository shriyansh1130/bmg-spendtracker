import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {BehaviorSubject, Observable, of} from "rxjs";
import {catchError, finalize, map} from "rxjs/operators";
import {PurchaseOrderPosition} from "../purchase-order.model";
import {PurchaseOrderPositionService} from "../services/purchase-order-position.service";
import {PageableCollection} from "../../resource/pageable-collection";
import {AppConfigService} from '../../core/services/app-config.service';

export class PurchaseOrderPositionTableDataSource implements DataSource<PurchaseOrderPosition> {

  private _purchaseOrderPositionPageSubject = new BehaviorSubject<PageableCollection<PurchaseOrderPosition>>(new PageableCollection());
  private _loadingSubject = new BehaviorSubject<boolean>(false);

  public readonly totalPurchaseOrderPositionCount$ = this._purchaseOrderPositionPageSubject.asObservable().pipe(
    map((p) => p.page && p.page.totalElements || 0)
  );
  public readonly loading$ = this._loadingSubject.asObservable();

  constructor(private appConfig: AppConfigService, private purchaseOrderPositionService: PurchaseOrderPositionService) {

  }

  connect(collectionViewer: CollectionViewer): Observable<PurchaseOrderPosition[]> {
    return this._purchaseOrderPositionPageSubject.asObservable().pipe(
      map((page) => this.mapPageToArrayAndCalculateValues(page)))
  }

  private mapPageToArrayAndCalculateValues(page: PageableCollection<PurchaseOrderPosition>): PurchaseOrderPosition[] {
    if (page.elements && page.elements.length) {
      return page.elements.map((pos: PurchaseOrderPosition) => Object.assign(new PurchaseOrderPosition(), pos, {
        lineNumber: pos.orderNumber + 1
      }));
    } else {
      return [];
    }
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this._purchaseOrderPositionPageSubject.complete();
    this._loadingSubject.complete();
  }

  public loadPurchaseOrderPositions(filterStatus = '%', filterSpendType = '%', filterCreatedBy = '%', filterPoNumber = '%', filterProjectName = '%', filterCountryName = '%', filterVendorName = '%', sortField = 'project.projectName', sortDirection = 'desc', page = 0, pageSize: number = this.appConfig.defaultPageSize) {
    this._loadingSubject.next(true);

    this.purchaseOrderPositionService.find(filterStatus, filterSpendType, filterCreatedBy, filterPoNumber, filterProjectName, filterCountryName, filterVendorName, page, pageSize, sortField, sortDirection).pipe(
      catchError(() => of([])),
      finalize(() => this._loadingSubject.next(false))
    )
      .subscribe(purchaseOrderPositions => {
        this._purchaseOrderPositionPageSubject.next(<PageableCollection<PurchaseOrderPosition>>purchaseOrderPositions)
      });

  }

  public getCurrentPurchaseOrderPositions(): PurchaseOrderPosition[] {
    return this._purchaseOrderPositionPageSubject.value.elements;
  }

}
