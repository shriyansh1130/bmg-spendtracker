import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {PurchaseOrderCreateSelection, SpendTrackerStoreService} from "../../core/services/spend-tracker-store.service";
import {Observable} from "rxjs";
import { map } from 'rxjs/operators';

@Injectable()
export class CanActivateCreatePurchaseOrder implements CanActivate {
  constructor(private storeService: SpendTrackerStoreService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.storeService.purchaseOrderCreationSelection.pipe(
      map(
      (selection: PurchaseOrderCreateSelection) => {
        const valid = (selection.country != null && selection.project != null) && !!(selection.country.id && selection.project.id);
        if (!valid) {
          this.router.navigate(['/projects']);
        }
        return valid;
      }));
  }
}
