import {Component, OnDestroy, OnInit} from '@angular/core';
import {SpendTrackerStoreService} from "../../core/services/spend-tracker-store.service";
import {CurrencyService} from "../../currency/currency.service";
import {BudgetSpendTypeService} from "../../budget-category/services/budget-spend-type.service";
import {PurchaseOrder, PurchaseOrderPosition} from "../purchase-order.model";
import {Country} from "../../country/country.model";
import {MatSnackBar} from "@angular/material";
import {Currency} from "../../currency/currency.model";
import {BehaviorSubject} from "rxjs";
import {Router} from "@angular/router";
import {PurchaseOrderService} from "../services/purchase-order.service";
import {IPurchaseOrderDto} from "../interfaces";
import {takeWhile} from "rxjs/operators";


@Component({
  selector: 'bst-purchase-order-create-wrapper',
  template: `
    <bst-purchase-order-details *ngIf="!loading"
                                [currencies]="currencies"
                                [purchaseOrder]="purchaseOrder | async"
                                (save)="save($event)"
    ></bst-purchase-order-details>`
})
export class PurchaseOrderCreateComponentWrapper implements OnInit, OnDestroy {
  currencies: Currency[];
  purchaseOrderNumber = 'DRAFT'; //TODO: PO-Number needs to be created when Workflow is clear
  loading = false;
  private alive = true;

  private _purchaseOrder = new BehaviorSubject<PurchaseOrder>(new PurchaseOrder());

  public purchaseOrder = this._purchaseOrder.asObservable();

  constructor(public spendTrackerStoreService: SpendTrackerStoreService,
              public currencyService: CurrencyService,
              public spendTypeService: BudgetSpendTypeService,
              private purchaseOrderService: PurchaseOrderService,
              private snackbar: MatSnackBar,
              private router: Router) {
  }

  ngOnInit() {
    this.loading = true;
    this.currencyService.currencies.subscribe(currencies => this.currencies = currencies);
    this.spendTrackerStoreService.purchaseOrderCreationSelection.pipe(
      takeWhile(() => this.alive)
    ).subscribe(({country, currency, project}) => {
        const po = new PurchaseOrder(this.purchaseOrderNumber, 'Draft');
        po.country = country;
        po.positions = [new PurchaseOrderPosition(project, null, 0, 1)];
        if (currency) {
          po.currency = currency;
          this._purchaseOrder.next(po);
          this.loading = false;
        } else {
          country.resolveOneAssociation('currency')
            .then((country) => {
              po.currency = (<Country>country).currency;
              this._purchaseOrder.next(po);
              this.loading = false;
            });
        }
      }
    )
  }

  save(changePurchaseOrder: IPurchaseOrderDto) {
    this.purchaseOrderService.upsert(changePurchaseOrder)
      .then((created) => {
        this.showMessage("Purchase Order saved.");
        this.router.navigate(['orders', created.id])
      })
      .catch(error => this.showMessage(`Purchase Order could not be saved: ${error}`));
  }

  showMessage(msg: string) {
    this.snackbar.open(msg, null, {duration: 1000});
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
