import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Project} from "../../../../project/project.model";
import {BudgetSpendType} from "../../../../budget-category/budget-category.model";
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {Observable, timer} from "rxjs";
import {ProjectService} from "../../../../project/services/project.service";
import {debounce, filter, map, startWith, switchMap} from "rxjs/operators";
import {BudgetSpendTypeService} from "../../../../budget-category/services/budget-spend-type.service";
import {AppConfigService} from '../../../../core/services/app-config.service';
import {Country} from "../../../../country/country.model";
import * as moment from "moment";
import {PurchaseOrderPosition, PurchaseOrderPositionAttachment} from "../../../purchase-order.model";
import {intlDecimalFormatValidator} from "../../../../shared/validators/intl-decimal-format-validator";
import {CountryService} from "../../../../country/services/country.service";
import {compareResources} from "../../../../resource/resource.helper";

const monthNamesLong = ["JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"];
const monthNamesShort = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];

export function parseDateString(dateString: string): string {
  if (dateString) {
    let regex = new RegExp([' ', '-', '/', '\\\.'].join('|'), 'g');
    let split = dateString.split(regex);
    let month = split[0].toLowerCase();
    let monthString;
    let monthInt = parseInt(month);

    if (!isNaN(monthInt)) {
      monthString = monthNamesShort[monthInt - 1];
    } else {
      let index = monthNamesShort.findIndex(value => value.toLowerCase() === month);
      index = index == -1 ? monthNamesLong.indexOf(month) : index;
      monthString = monthNamesShort[index];
    }

    let year = parseInt(split[1]);
    year = year < 100 ? year + 2000 : year;
    if (!monthString || !year) {
      return null
    } else {
      return monthString + '-' + year;
    }
  } else {
    return moment().format('MMM-YYYY');
  }
}

export function validateServiceMonth(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    let dateValid = parseDateString(control.value);
    const notInPast = moment(dateValid, "MMM-YYYY").isSameOrAfter(moment().format("YYYY-MM-01"));
    return dateValid && notInPast ? null : {'date': {value: control.value}};
  };
}


@Component({
  selector: 'bst-purchase-order-position-form',
  templateUrl: './purchase-order-position-form.component.html',
  styleUrls: ['./purchase-order-position-form.component.scss']
})
export class PurchaseOrderPositionFormComponent implements OnInit {
  @Input() purchaseOrderPosition: PurchaseOrderPosition;
  @Input() position: number;
  @Input() country: Country;
  @Input() referenceProject: Project;
  @Input() disabled = false;
  @Output() submit: EventEmitter<any> = new EventEmitter();
  @Output() cancel: EventEmitter<any> = new EventEmitter();
  positionForm: FormGroup;
  filteredProjects: Observable<Project[]>;
  filteredSpendTypes: Observable<BudgetSpendType[]>;
  dateFormControl;
  today: Date = new Date();
  compareResources = compareResources;
  deletedAttachments = [];
  addedAttachments = [];

  constructor(private appConfig: AppConfigService,
              private fb: FormBuilder,
              private projectService: ProjectService,
              private spendTypeService: BudgetSpendTypeService,
              public countryService: CountryService,
  ) {
  }

  ngOnInit() {
    this.countryService.loadPoCountries();
    this.createPositionFormGroup();
    if (this.position !== 0) {
      this.registerProjectFilter();
    }

    this.registerSpendTypeFilter();
    this.dateFormControl = this.positionForm.get('serviceMonth');

    if (!this.dateFormControl.value) {
      this.dateFormControl.setValue(monthNamesShort[this.today.getMonth()] + '-' + this.today.getFullYear());
    }
    this.dateFormControl.setValidators(validateServiceMonth());
  }

  createPositionFormGroup() {
    this.positionForm =  this.fb.group({
      id: [this.purchaseOrderPosition.id],
      project: [this.purchaseOrderPosition.project || '', Validators.required],
      spendType: [this.purchaseOrderPosition.spendType || '', Validators.required],
      poCountry: [this.purchaseOrderPosition.poCountry || this.country],
      price: [this.purchaseOrderPosition.price || '', [Validators.required, Validators.max(999999999.999), Validators.min(0.01), intlDecimalFormatValidator(3)]],
      quantity: [ this.purchaseOrderPosition.quantity|| 1 , [Validators.required, Validators.min(1), Validators.max(999999), intlDecimalFormatValidator(0)]],
      description: [this.purchaseOrderPosition.description || '', Validators.required],
      serviceMonth: [this.purchaseOrderPosition.serviceMonth || '', Validators.required],
      attachments: [this.purchaseOrderPosition.attachments || []]
    });

    if (this.disabled) {
      this.positionForm.disable();
    }
  }

  private registerProjectFilter() {
    this.filteredProjects = this.positionForm.get('project').valueChanges.pipe(
      filter(value => typeof value === 'string'),
      debounce(() => timer(this.appConfig.filterInputDebounceInMillis)),
      startWith(this.purchaseOrderPosition.project ? this.purchaseOrderPosition.project.projectName : ''),
      switchMap((value) => this.projectService.findValidProjects(this.country.id, this.referenceProject.id, value)),
      map(value => value.elements)
    )
  }

  private registerSpendTypeFilter() {
    this.filteredSpendTypes = this.positionForm.get('spendType').valueChanges.pipe(
      filter(value => typeof value === 'string'),
      debounce(() => timer(this.appConfig.filterInputDebounceInMillis)),
      startWith(this.purchaseOrderPosition.spendType ? this.purchaseOrderPosition.spendType.title :''),
      switchMap(value => this.spendTypeService.findByCodeOrTitle(value)),
      map(value => value.elements)
    )
  }

  onSubmit(event) {
    event.preventDefault();
    event.stopPropagation();
    if (this.positionForm.status === 'VALID') {
      this.dateFormControl.setValue(parseDateString(this.dateFormControl.value));
      this.deletedAttachments.forEach(value => value.delete());
      this.submit.emit(Object.assign(this.positionForm.getRawValue(), {attachments: this.purchaseOrderPosition.attachments}));
    }
  }

  onCancel(event) {
    event.preventDefault();
    event.stopPropagation();
    this.cancel.emit();
  }

  deleteAttachment(attachment: PurchaseOrderPositionAttachment) {
    this.deletedAttachments.push(attachment);
  }

  displayProject(project?: Project) {
    return project ? project.projectName : undefined;
  }

  displaySpendType(spendType?: BudgetSpendType) {
    if(spendType){
      return spendType.budgetSpendTypeCode + ' - ' + spendType.title;
    }else{
      return undefined;
    }
  }

  public getValueErrorMessage() {
    let formField = this.positionForm.get('price');
    return formField.hasError('required') ? 'A price is required' :
      formField.hasError('intlDecimalFormat') ? 'Price must be a valid decimal 123.45, 3 decimals max.' :
        formField.hasError('min') ? 'Price must be > 0' :
          formField.hasError('max') ? 'Price must be < 999999999.999' :
            '';
  }
}
