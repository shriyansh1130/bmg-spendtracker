import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {PurchaseOrderPositionDialogData} from "../../../interfaces";


@Component({
  selector: 'bst-purchase-order-position-dialog',
  templateUrl: './purchase-order-position-dialog.component.html',
  styleUrls: ['./purchase-order-position-dialog.component.scss']
})
export class PurchaseOrderPositionDialogComponent {
  public static readonly RESULT_SAVE = "SAVE";


  constructor(public dialogRef: MatDialogRef<PurchaseOrderPositionDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: PurchaseOrderPositionDialogData) {
  }

  onSubmit(positionFormValue) {
    this.data.positionForm.patchValue(positionFormValue);
    this.dialogRef.close(PurchaseOrderPositionDialogComponent.RESULT_SAVE);
  }

  onCancel() {
    this.dialogRef.close();
  }

}


