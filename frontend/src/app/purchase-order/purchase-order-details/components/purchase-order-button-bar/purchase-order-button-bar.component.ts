import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Status} from "../../../interfaces";

@Component({
  selector: 'bst-purchase-order-button-bar',
  templateUrl: './purchase-order-button-bar.component.html',
  styleUrls: ['./purchase-order-button-bar.component.scss']
})
export class PurchaseOrderButtonBarComponent implements OnInit {
  @Output() new: EventEmitter<void> = new EventEmitter();
  @Output() save: EventEmitter<void> = new EventEmitter();
  @Output() submit : EventEmitter<void> = new EventEmitter();
  @Output() close: EventEmitter<void> = new EventEmitter();
  @Input() currentStatus: Status;


  constructor() { }

  ngOnInit() {

  }

}
