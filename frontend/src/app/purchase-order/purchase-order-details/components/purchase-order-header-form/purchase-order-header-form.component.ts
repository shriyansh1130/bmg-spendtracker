import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {Country} from "../../../../country/country.model";
import {Vendor} from "../../../../vendor/vendor.model";
import {Currency} from "../../../../currency/currency.model";
import {Resource} from "../../../../resource/resource";
import {PurchaseOrder} from "../../../purchase-order.model";
import {debounce, filter, map, switchMap} from "rxjs/operators";
import {timer, Observable} from "rxjs";
import {Project} from "../../../../project/project.model";
import {AppConfigService} from "../../../../core/services/app-config.service";
import {VendorService} from "../../../../vendor/vendor.service";
import {MatDialog, MatDialogConfig} from "@angular/material";
import {ConfirmationDialogComponent} from "../../../../shared/confirmation-dialog/confirmation-dialog.component";
import {VendorDetailsFormComponent} from "../../../../vendor/vendor-details-form/vendor-details-form.component";
import {VendorDetailsDialogComponent} from "../../../../vendor/vendor-details-form/vendor-details-dialog.component";
import {RolesService} from "../../../../auth/roles.service";

@Component({
  selector: 'bst-purchase-order-header-form',
  templateUrl: './purchase-order-header-form.component.html',
  styleUrls: ['./purchase-order-header-form.component.scss']
})
export class PurchaseOrderHeaderFormComponent implements OnInit, OnChanges{
  @Input('group') poForm: FormGroup;
  @Input() disabled = false;
  @Input() purchaseOrderNumber: string;
  @Input() country: Country;
  @Input() project: Project;
  @Input() currencies: Currency[];
  @Input() purchaseOrder: PurchaseOrder;
  @Output() onDelete: EventEmitter<void> = new EventEmitter<void>();
  filteredVendors: Observable<Vendor[]>;
  dialogConfigConfirm = new MatDialogConfig<any>();
  vendorEditable = true;
  deletable = false;

  constructor(private appConfig: AppConfigService,private vendorService: VendorService, private dialog: MatDialog, private roleService: RolesService) {

  }

  ngOnInit() {
    this.handleVendorEditable();
    if (this.vendorEditable) {
      this.registerVendorFilter();
    }
    this.checkPoDeletable();
  }

  ngOnChanges() {
    this.handleVendorEditable();
    this.checkPoDeletable();
  }

  checkPoDeletable() {
    this.roleService.hasRole('bmg-spendtracker-user').then(isEditor => {
      this.deletable = this.purchaseOrder.id && this.purchaseOrder.status == 'Draft' && isEditor;
    })
  }

  compareResources(r1: Resource, r2: Resource): boolean {
    return r1 && r2 ? r1.id === r2.id : r1 === r2;
  }

  private registerVendorFilter() {
    this.filteredVendors = this.poForm.get('vendor').valueChanges.pipe(
      filter(value => typeof value === 'string'),
      debounce(() => timer(this.appConfig.filterInputDebounceInMillis)),
      switchMap((value) => this.vendorService.validVendors(this.country.id, this.project.id, value)),
      map(value =>  value.elements)
    )
  }

  displayVendor(vendor?: Vendor) {
    return vendor ? vendor.name1 : undefined;
  }

  deletePurchaseOrder() {
    this.dialogConfigConfirm.data = {title: "Confirm Purchase Order deletion", description: "Would you like to delete this Purchase Order?"};
    let deleteConfirm = this.dialog.open(ConfirmationDialogComponent, this.dialogConfigConfirm);
    deleteConfirm.afterClosed().subscribe(async result => {
      if(result) {
        this.onDelete.emit();
      }
    });
  }

  openVendorDialog() {
    const vendorDialog = this.dialog.open(VendorDetailsDialogComponent, {
      data: {
        vendor: this.poForm.get('vendor').value,
        country: this.purchaseOrder.country,
        company: this.purchaseOrder.positions[0].project.repertoireOwnerCompany
      },
      disableClose: true,
      width: "600px"
    });

    vendorDialog.afterClosed().subscribe(data => {
      if (data) {
        this.poForm.get('vendor').setValue(data);
      }
    });

  }

  private handleVendorEditable() {
    if (this.purchaseOrder.status !== "Draft") {
      this.vendorEditable = false;
      this.poForm.get('vendor').disable();
    }
  }
}
