import {Component, Inject, OnInit} from '@angular/core';
import {PurchaseOrder, PurchaseOrderCodingRules, PurchaseOrderPosition} from "../../../purchase-order.model";
import {MAT_DIALOG_DATA} from "@angular/material";
import {PurchaseOrderPositionService} from "@app/purchase-order/services/purchase-order-position.service";

@Component({
  selector: 'bst-purchase-order-position-coding-rules-full-recoupability',
  styles: ['b {white-space:pre}'],
  template: `
    <mat-dialog-content>
      <mat-list>
        <h3  fxLayoutAlign="center center"> Line Number: {{poLine}} (Recoupablity % = 100%)</h3>
        <mat-list-item><b>Account: </b>{{codingRules.recoupableAccount}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Assignment: </b>{{codingRules.recoupableAssignment}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Text: </b>{{codingRules.recoupableText}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Reference Key 2: </b>{{codingRules.recoupableReferenceKey2}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Amount: </b>{{codingRules.recoupableAmount}}</mat-list-item>
      </mat-list>
    </mat-dialog-content>`
})
export class PurchaseOrderPositionCodingRulesComponentFullRecoupability {

  purchaseOrderPosition: PurchaseOrderPosition;
  purchaseOrder: PurchaseOrder;
  codingRules: PurchaseOrderCodingRules;
  poLine: number;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    this.purchaseOrderPosition = data.purchaseOrderPosition;
    this.purchaseOrder = data.purchaseOrder;
    this.codingRules = data.codingRules;
    this.poLine = data.poLine;
  }
}

@Component({
  selector: 'bst-purchase-order-position-coding-rules-recharge',
  styles: ['b {white-space:pre}'],
  template: `
    <mat-dialog-content>
      <mat-list>
        <h3 fxLayoutAlign="center center"> Line Number: {{poLine}} (Recoupablity % = 100% (RECHARGE))</h3>
        <mat-list-item><b>Account: </b>{{codingRules.recoupableAccount}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Customer Account: </b>{{codingRules.recoupableCustomerAccount}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Assignment: </b>{{codingRules.recoupableAssignment}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Text: </b>{{codingRules.recoupableText}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Reference Key 2: </b>{{codingRules.recoupableReferenceKey2}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Reference Key 3: </b>{{codingRules.recoupableReferenceKey3}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Amount: </b>{{codingRules.recoupableAmount}}</mat-list-item>
      </mat-list>
    </mat-dialog-content>`
})
export class PurchaseOrderPositionCodingRulesComponentRecharge {
  purchaseOrderPosition: PurchaseOrderPosition;
  purchaseOrder: PurchaseOrder;
  codingRules: PurchaseOrderCodingRules;
  poLine: number;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    this.purchaseOrderPosition = data.purchaseOrderPosition;
    this.purchaseOrder = data.purchaseOrder;
    this.poLine = data.poLine;
    this.codingRules = data.codingRules;
  }
}

@Component({
  selector: 'bst-purchase-order-position-coding-rules-non-recoupable',
  styles: ['b {white-space:pre}'],
  template: `
    <mat-dialog-content>
      <mat-list>
        <h3 fxLayoutAlign="center center"> Line Number: {{poLine}} (Recoupablity % = 0%)</h3>
        <mat-list-item><b>Account: </b>{{codingRules.nonRecoupableAccount}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Assignment: </b>{{codingRules.nonRecoupableAssignment}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Order: </b>{{codingRules.nonRecoupableOrder}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Reference Key 2: </b>{{codingRules.nonRecoupableReferenceKey2}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Reference Key 3: </b>{{codingRules.nonRecoupableReferenceKey3}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Cost Ctr: </b>{{codingRules.nonRecoupableCostCenter}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Order Type: </b>{{codingRules.nonRecoupableOrderType}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Text: </b>{{codingRules.nonRecoupableText}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Amount: </b>{{codingRules.nonRecoupableAmount}}</mat-list-item>
      </mat-list>
    </mat-dialog-content>`
})
export class PurchaseOrderPositionCodingRulesComponentNonRecoupable {

  purchaseOrderPosition: PurchaseOrderPosition;
  purchaseOrder: PurchaseOrder;
  codingRules: PurchaseOrderCodingRules;
  poLine: number;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    this.purchaseOrderPosition = data.purchaseOrderPosition;
    this.purchaseOrder = data.purchaseOrder;
    this.poLine = data.poLine;
    this.codingRules = data.codingRules;
  }
}

@Component({
  selector: 'bst-purchase-order-position-coding-rules-ny-no-zero-no-hundred',
  styles: ['b {white-space:pre}'],
  template: `
    <mat-dialog-content>
      <h3 fxLayoutAlign="center center"> Line Number: {{poLine}} (0% < Recoupablity % <100%)</h3>
      <mat-card><input matInput placeholder="Please Enter Invoice Value" (input)="onInvoiceValueChange($event.target.value)"></mat-card>
      <mat-list>
        <h4>Non-Recoupable</h4>
        <mat-divider></mat-divider>
        <mat-list-item><b>Account: </b>{{nrAccount}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Assignment: </b>{{nrAssignment}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Order: </b>{{nrOrder}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Reference Key 2: </b>{{nrReferenceKey2}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Reference Key 3: </b>{{nrReferenceKey3}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Cost Ctr: </b>{{nrCostCtr}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Order Type: </b>{{nrOrderType}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Text: </b>{{nrText}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Amount: </b>{{nrAmount}}</mat-list-item>
        <mat-divider></mat-divider>
        <h4>Recoupable</h4>
        <mat-divider></mat-divider>
        <mat-list-item><b>Account: </b>{{rAccount}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Assignment: </b>{{rAssignment}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Text: </b>{{rText}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Reference Key 2: </b>{{rReferenceKey2}}</mat-list-item>
        <mat-divider></mat-divider>
        <mat-list-item><b>Amount: </b>{{rAmount}}</mat-list-item>
      </mat-list>
    </mat-dialog-content>`
})
export class PurchaseOrderPositionCodingRulesComponentMixedRecoupability implements OnInit {

  purchaseOrderPosition: PurchaseOrderPosition;
  purchaseOrder: PurchaseOrder;
  codingRules: PurchaseOrderCodingRules;
  poLine: number;
  recoupementPercentage: number;
  invoiceValue: string;
  nrAccount: string;
  nrAssignment: string;
  nrOrder: string;
  nrReferenceKey2: string;
  nrReferenceKey3: string;
  nrCostCtr: string;
  nrOrderType: string;
  nrText: string;

  nrAmount: string;
  rAccount: string;
  rAssignment: string;
  rText: string;
  rReferenceKey2: string;

  rAmount: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private purchaseOrderPositionService: PurchaseOrderPositionService) {
    this.purchaseOrderPosition = data.purchaseOrderPosition;
    this.purchaseOrder = data.purchaseOrder;
    this.poLine = data.poLine;
    this.codingRules = data.codingRules;
    this.recoupementPercentage = this.codingRules.recoupmentPct;
  }

  ngOnInit() {
    this.nrAccount = this.codingRules.nonRecoupableAccount;
    this.nrAssignment = this.codingRules.nonRecoupableAssignment;
    this.nrOrder = this.codingRules.nonRecoupableOrder;
    this.nrReferenceKey2 = this.codingRules.nonRecoupableReferenceKey2;
    this.nrReferenceKey3 = this.codingRules.nonRecoupableReferenceKey3;
    this.nrCostCtr = this.codingRules.nonRecoupableCostCenter;
    this.nrOrderType = this.codingRules.nonRecoupableOrderType;
    this.nrText = this.codingRules.nonRecoupableText;
    this.nrAmount = '-';
    this.rAccount = this.codingRules.recoupableAccount;
    this.rAssignment = this.codingRules.recoupableAssignment;
    this.rText = this.codingRules.recoupableText;
    this.rReferenceKey2 = this.codingRules.recoupableReferenceKey2;
    this.rAmount = '-';
  }

  onInvoiceValueChange(value) {
    value = parseFloat(value);
    if(Number.isNaN(value)){
      this.nrAmount = '-';
      this.rAmount = '-';
    }else{
        this.nrAmount = (parseFloat(value)*(1-this.recoupementPercentage/100)).toFixed(2);
        this.rAmount = (value-parseFloat(this.nrAmount)).toFixed(2);
    }
  }
}


