import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {PurchaseOrderPositionService} from "../../../services/purchase-order-position.service";
import {PurchaseOrder, PurchaseOrderCodingRules, PurchaseOrderPosition} from "../../../purchase-order.model";
import {MatDialog} from "@angular/material";
import {
  PurchaseOrderPositionCodingRulesComponentFullRecoupability,
  PurchaseOrderPositionCodingRulesComponentMixedRecoupability,
  PurchaseOrderPositionCodingRulesComponentNonRecoupable,
  PurchaseOrderPositionCodingRulesComponentRecharge
} from "../purchase-order-position-coding-rules/purchase-order-position-coding-rules.component";

@Component({
  selector: 'bst-purchase-order-position-list',
  templateUrl: './purchase-order-position-list.component.html',
  styleUrls: ['./purchase-order-position-list.component.scss']
})
export class PurchaseOrderPositionListComponent implements OnChanges {

  @Input() purchaseOrder;
  @Input() positions;
  @Input() group;
  @Input() disabled = false;
  @Output() addPosition: EventEmitter<void> = new EventEmitter();
  @Output() editPosition: EventEmitter<number> = new EventEmitter();
  @Output() removePosition: EventEmitter<number> = new EventEmitter();
  @Output() afterModalClose = new EventEmitter();


  columnsToDisplay = [
    'position',
    'project',
    'spendType',
    'price',
    'quantity',
    'total',
    'description',
    'serviceMonth',
    'actions'
  ];

  constructor(public purchaseOrderPositionService: PurchaseOrderPositionService, public dialog: MatDialog) {
  }

  ngOnChanges(changes: SimpleChanges): void {
      this.positions = this.positions.map((pos: PurchaseOrderPosition) => Object.assign({}, pos, {
        total: this.purchaseOrderPositionService.calcTotal(pos.price, pos.quantity)
      }) )
  }

  handleDelete(index: number, event: Event): void {
    event.stopPropagation();
    this.removePosition.emit(index);
  }

  showCodingRules(event, index, purchaseOrder, purchaseOrderPosition) {
    event.preventDefault();
    event.stopPropagation();
    this.purchaseOrderPositionService.fetchCodingRules(purchaseOrderPosition.id).then(res => {
      let dialogToOpen = this.chooseDialog(purchaseOrder, purchaseOrderPosition, res);
      let dialogRef = this.dialog.open(dialogToOpen, {
        panelClass: 'info-panel',
        data: {
          purchaseOrder: purchaseOrder,
          purchaseOrderPosition: purchaseOrderPosition,
          poLine: index + 1,
          codingRules: res
        },
        autoFocus: false
      });
    });
  }

  chooseDialog(purchaseOrder: PurchaseOrder, purchaseOrderPosition: PurchaseOrderPosition, codingRules: PurchaseOrderCodingRules): any {
    if (codingRules.codingType === "RECHARGE") {
      return PurchaseOrderPositionCodingRulesComponentRecharge
    } else if (codingRules.codingType === "ZERO_RECOUPABILITY") {
      return PurchaseOrderPositionCodingRulesComponentNonRecoupable
    } else if (codingRules.codingType === "MIXED_RECOUPABILITY") {
      return PurchaseOrderPositionCodingRulesComponentMixedRecoupability;
    } else if (codingRules.codingType === "FULL_RECOUPABILITY") {
      return PurchaseOrderPositionCodingRulesComponentFullRecoupability;
    } else {
      console.error("No valid codingType: " + codingRules.codingType)
    }
  }
}
