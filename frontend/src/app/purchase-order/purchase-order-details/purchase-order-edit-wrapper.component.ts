import {Component, OnInit} from '@angular/core';
import {CreatePurchaseOrderStoreService} from "../services/create-purchase-order-store.service";
import {CurrencyService} from "../../currency/currency.service";
import {BudgetSpendTypeService} from "../../budget-category/services/budget-spend-type.service";
import {ActivatedRoute, Router} from "@angular/router";
import {PurchaseOrderService} from "../services/purchase-order.service";
import {PurchaseOrder} from "../purchase-order.model";
import {PurchaseOrderPositionService} from "../services/purchase-order-position.service";
import {MatSnackBar} from "@angular/material";
import {IPurchaseOrderDto} from "../interfaces";
import {Currency} from "../../currency/currency.model";
import {SpendTrackerStoreService} from "../../core/services/spend-tracker-store.service";
import {RolesService} from "../../auth/roles.service";


@Component({
  selector: 'bst-purchase-order-edit-wrapper',
  template: `
    <ng-container *ngIf="loading">
      <bst-loading-spinner-overlay></bst-loading-spinner-overlay>
    </ng-container>
    <bst-purchase-order-details
      [currencies]="currencies"
      [purchaseOrder]="currentPurchaseOrder"
      (save)="save($event)"
      (onDelete)="deletePurchaseOrder()"
      (onCreateNew)="createNewFromCurrent()"
      [disabled]="!editable"
    ></bst-purchase-order-details>`
})
export class PurchaseOrderEditComponentWrapper implements OnInit {
  currencies: Currency[];
  currentPurchaseOrder: PurchaseOrder = new PurchaseOrder();
  loading = false;
  editable = false;

  constructor(public purchaseOrderCreateStore: CreatePurchaseOrderStoreService,
              private purchaseOrderService: PurchaseOrderService,
              private purchaseOrderPositionService: PurchaseOrderPositionService,
              public currencyService: CurrencyService,
              public spendTypeService: BudgetSpendTypeService,
              private route: ActivatedRoute,
              private snackbar: MatSnackBar,
              private router: Router,
              private spendTrackerStoreService: SpendTrackerStoreService,
              private roleService: RolesService
              ) {
  }

  ngOnInit(): void {
    this.loading = true;
    this.currencyService.currencies.subscribe(currencies => this.currencies = currencies);
    this.route.params.subscribe(params => {
      if (params.id) {
        this.loadPurchaseOrder(params.id);
      }
    });
  }


  loadPurchaseOrder(id:  string): void {
    this.purchaseOrderService.findById(id)
      .then(po => {
        this.currentPurchaseOrder = po;
        this.purchaseOrderCreateStore.selectPurchaseOrder(po); // select on store to trigger reevaluation of valid Vendors / Countries
        this.loading = false;
        this.roleService.hasRole('bmg-spendtracker-user').then(result => {
          this.editable = result && this.currentPurchaseOrder.status !== 'Closed'
        });

      });
  }

  save(changePurchaseOrder: IPurchaseOrderDto) {
    this.purchaseOrderService.upsert(changePurchaseOrder)
      .then(() => {
        this.showMessage("Purchase Order saved.");
        this.loadPurchaseOrder(changePurchaseOrder.id);
      })
      .catch(error => this.showMessage(`Purchase Order could not be saved: ${error.error}`));
  }

  private showMessage(msg: string) {
    this.snackbar.open(msg, null, {duration: 1000});
  }

  deletePurchaseOrder() {
    this.purchaseOrderService.delete(this.currentPurchaseOrder)
      .then(() => {
        this.showMessage("Purchase Order deleted");
        this.router.navigate(['projects']);
      })
      .catch(() => this.showMessage("Purchase Order could not be deleted"));
  }

  createNewFromCurrent() {
    this.spendTrackerStoreService.selectProject(this.currentPurchaseOrder.positions[0].project);
    this.spendTrackerStoreService.selectCountry(this.currentPurchaseOrder.country);
    this.spendTrackerStoreService.selectCurrency(this.currentPurchaseOrder.currency);
    this.router.navigate(['orders', 'create']);
  }

  isNotEditable() {

  }
}
