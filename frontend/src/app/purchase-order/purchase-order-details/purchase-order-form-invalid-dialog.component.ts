import {Component, Inject} from "@angular/core";
import {MAT_DIALOG_DATA} from "@angular/material";

@Component({
  selector: 'bst-purchase-order-form-invalid-dialog',
  template: `<p>Purchase Order could not be saved. Errors exist in the following fields:</p>
    <ul>
      <li *ngFor="let error of errorMessages">
        {{error}}
      </li>
    </ul>
   `
})
export class PurchaseOrderFormInvalidDialog {
  errorMessages = [];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    this.errorMessages = [];
    Object.keys(data.errors).forEach((field) => {
      if (field != 'positions') {
        this.errorMessages.push(`Field ${field}`)
      }
    });
    if (data.errors.positions) {
      Object.keys(data.errors.positions).forEach((position) => {
        const currentPosition = data.errors.positions[position];
        this.errorMessages.push(`Line #${+position+1}: ${Object.keys(currentPosition).join(", ")}`)
      })
    }

  }
}
