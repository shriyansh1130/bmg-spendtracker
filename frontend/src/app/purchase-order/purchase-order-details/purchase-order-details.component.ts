import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatDialog, MatDialogConfig} from "@angular/material";
import {PurchaseOrderPositionDialogComponent} from "./components/purchase-order-position-dialog/purchase-order-position-dialog.component";
import {PurchaseOrder, PurchaseOrderPosition} from "../purchase-order.model";
import {Currency} from "../../currency/currency.model";
import {PurchaseOrderFormInvalidDialog} from "./purchase-order-form-invalid-dialog.component";
import {IPurchaseOrderDto, IPurchaseOrderForm} from "../interfaces";
import {intlDecimalFormatValidator} from "@app/shared/validators/intl-decimal-format-validator";
import {ConfirmationDialogComponent} from "../../shared/confirmation-dialog/confirmation-dialog.component";

@Component({
  selector: 'bst-purchase-order-details',
  templateUrl: './purchase-order-details.component.html',
  styleUrls: ['./purchase-order-details.component.scss']
})
export class PurchaseOrderDetailsComponent implements OnChanges {
  @Input() currencies: Currency[];
  @Input() purchaseOrder: PurchaseOrder;
  @Input() disabled = false;
  @Output() save: EventEmitter<IPurchaseOrderDto> = new EventEmitter();
  @Output() onDelete: EventEmitter<void> = new EventEmitter<void>();
  @Output() onCreateNew: EventEmitter<void> = new EventEmitter<void>();
  poForm: FormGroup;
  positions = [];
  dialogConfigConfirm = new MatDialogConfig<any>();

  constructor(private fb: FormBuilder,
              public dialog: MatDialog) {
    this.createEmptyForm();
  }

  createEmptyForm() {
    this.poForm = this.fb.group({
      id: [],
      purchaseOrderNumber: [],
      status: ['Draft', Validators.required],
      country: ['', Validators.required],
      vendor: ['', Validators.required],
      currency: ['', Validators.required],
      positions: this.fb.array([])
    });
    this.handleChange();
  }

  addPosition(position?) {
    const positionControls = <FormArray>this.poForm.controls['positions'];
    const newIndex: number = positionControls.length;
    if (!position) {
      position = this.createNewPosition();
    }
    positionControls.push(this.createPositionFormGroup(position));
    this.editPosition(newIndex);
  }

  private createNewPosition() {
    const p = new PurchaseOrderPosition();
    p.quantity = 1;
    p.description = this.positions[0].description;
    p.project = this.positions[0].project;
    p.spendType = this.positions[0].spendType;
    p.poCountry = this.poForm.get('country').value;
    p.attachments = [];
    p.notes = '';
    return p;
  }

  createPositionFormGroup(position) {
    debugger;
    const group = this.fb.group({
      id: [position.id],
      project: [position.project || '', Validators.required],
      spendType: [position.spendType || '', Validators.required],
      poCountry: [position.poCountry || this.poForm.get('country').value],
      price: [position.price || 0, [Validators.required, Validators.max(999999999.999), Validators.min(0.01), intlDecimalFormatValidator(3)]],
      quantity: [1.00, [Validators.required, Validators.min(1.00), Validators.max(999999), intlDecimalFormatValidator(0)]],
      description: [position.description || '', Validators.required],
      serviceMonth: [position.serviceMonth || '', Validators.required],
      attachments: [position.attachments || []],
      notes: [position.notes || ''],
      deletable: [position.deletable]
    });
    return group;
  }

  ngOnChanges() {
    if (this.purchaseOrder && this.purchaseOrder.positions) {
      this.resetFormFromState();
    }
  }

  private handleChange() {
    this.poForm.get('positions').valueChanges.subscribe(value => {
      this.positions = value;
    });
  }


  editPosition(index) {
    const dialogRef = this.dialog.open(PurchaseOrderPositionDialogComponent, {
      data: {
        disabled: this.disabled,
        positionForm: this.poForm.get(`positions.${index}`),
        purchaseOrderPosition: this.positions[index],
        position: index,
        country: this.purchaseOrder.country,
        referenceProject: this.purchaseOrder.positions[0].project
      },
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === PurchaseOrderPositionDialogComponent.RESULT_SAVE) {
        this.savePurchaseOrder();
      }
    });
  }

  removePosition(index) {
    const control = <FormArray>this.poForm.controls['positions'];
    if(this.poForm.get('status').value == 'Open' && this.poForm.get('purchaseOrderNumber').value != 'DRAFT'){
      this.dialogConfigConfirm.data = {title: 'Delete', description: 'Are you sure you want to delete this line item? The vendor can not be paid for this line once deleted.'};
      let deleteConfirm = this.dialog.open(ConfirmationDialogComponent, this.dialogConfigConfirm);
      deleteConfirm.afterClosed().subscribe(async result => {
        if(result) {
          control.removeAt(index);
          this.savePurchaseOrder();
        }
      });
    }else{
      control.removeAt(index);
    }
  }

  savePurchaseOrder() {
    if (this.poForm.status === 'VALID') {
      this.save.emit(this.getPurchaseOrderDto(this.poForm.getRawValue()));
    } else {
      this.dialog.open(PurchaseOrderFormInvalidDialog, {data: {errors: this.getAllErrors(this.poForm)}});
    }
  }


  getAllErrors(form: FormGroup | FormArray): { [key: string]: any; } | null {
    let hasError = false;
    const result = Object.keys(form.controls).reduce((acc, key) => {
      const control = form.get(key);
      const errors = (control instanceof FormGroup || control instanceof FormArray)
        ? this.getAllErrors(control)
        : control.errors;
      if (errors) {
        acc[key] = errors;
        hasError = true;
      }
      return acc;
    }, {} as { [key: string]: any; });
    return hasError ? result : null;
  }

  private resetFormFromState() {
    if (this.purchaseOrder && this.purchaseOrder.positions) {
      this.poForm.setControl('positions', this.fb.array(this.derivePositionFormControls()));
      this.poForm.reset(this.purchaseOrder);
      this.positions = this.poForm.getRawValue().positions;
      this.handleChange();

      if (this.purchaseOrder.status !== 'Draft') {
        this.poForm.get('currency').disable();
      }

      if (this.disabled) {
        this.disableForm();
      }
    }
  }

  private derivePositionFormControls(): FormGroup[] {
    const posGroups = this.purchaseOrder.positions.map((p) => this.createPositionFormGroup(p));
    return posGroups;
  }

  private getPurchaseOrderDto(form: IPurchaseOrderForm): IPurchaseOrderDto {
    return {
      id: form.id,
      purchaseOrderNumber: form.purchaseOrderNumber,
      status: form.status,
      countryId: form.country.id,
      vendorId: form.vendor.id,
      currencyId: form.currency.id,
      positions: form.positions.map(p => ({
        id: p.id,
        projectId: p.project.id,
        spendTypeId: p.spendType.id,
        price: p.price,
        quantity: p.quantity,
        description: p.description,
        serviceMonth: p.serviceMonth,
        poCountryId: p.poCountry.id,
        attachments: p.attachments.map(position => position.uuid),
        notes: p.notes,
      }))


    }
  }

  saveAsClosed() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        title: "Confirm Close Purchase Order",
        description: "Are you sure you wish to close this PO?, once closed it will no longer be editable."
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.poForm.get('status').setValue('Closed');
        this.savePurchaseOrder();
      }
    });

  }

  saveAsOpen() {
    this.poForm.get('status').setValue('Open');
    this.savePurchaseOrder();
  }

  disableForm() {
    this.poForm.disable();
  }

  createNewFromCurrent() {
    this.onCreateNew.emit();
  }
}




