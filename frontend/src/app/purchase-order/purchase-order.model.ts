import {Resource} from "../resource/resource";
import {BudgetSpendType} from "../budget-category/budget-category.model";
import {Country} from "../country/country.model";
import {Currency} from "../currency/currency.model";
import {Project} from "../project/project.model";
import {Vendor} from "../vendor/vendor.model";

export class PurchaseOrder extends Resource {
  public static PATH: string = "api/purchase-orders";

  constructor(
    public purchaseOrderNumber?:string,
    public status?: string,
    public country?: Country,
    public currency?: Currency,
    public positions?: PurchaseOrderPosition[],
    public vendor?:Vendor
  ) {
    super();
  }

}

export class PurchaseOrderPosition extends Resource {
  public static PATH: string = "api/purchase-order-positions";

  constructor(
    public project?: Project,
    public spendType?: BudgetSpendType,
    public price?:number,
    public quantity?:number,
    public description?:string,
    public serviceMonth?:string,
    public purchaseOrder?: PurchaseOrder,
    public orderNumber?: number,
    public poCountry?: Country,
    public attachments : PurchaseOrderPositionAttachment[] = [],
    public paid?: number,
    public openPo?: number,
    public notes?: string,
    public deletable?: boolean
  ) {
    super();
  }

}

export class PurchaseOrderPositionAttachment extends Resource {
  public static PATH: string = "api/purchase-order-position-attachments";

  constructor(
    public filename?: string,
    public size?: number,
    public uuid?: string,
    public purchaseOrderPosition?:PurchaseOrderPosition,
    public blob?: any) {
    super();
  }

}

export interface PurchaseOrderPositionInvoiceStatus {
  invoiced: number,
  variance: number,
  openPo: number
}

export interface PurchaseOrderCodingRules {
  codingType: 'RECHARGE' | 'ZERO_RECOUPABILITY' | 'MIXED_RECOUPABILITY' | 'FULL_RECOUPABILITY';
  recoupmentPct: number;
  recoupableAccount: string;
  recoupableCustomerAccount: string;
  recoupableAssignment: string;
  recoupableText: string;
  recoupableReferenceKey2: string;
  recoupableReferenceKey3: string;
  recoupableAmount: string;
  nonRecoupableAccount: string;
  nonRecoupableAssignment: string;
  nonRecoupableOrder: string;
  nonRecoupableReferenceKey2: string;
  nonRecoupableReferenceKey3: string;
  nonRecoupableCostCenter: string;
  nonRecoupableOrderType: string;
  nonRecoupableText: string;
  nonRecoupableAmount: string;
}
