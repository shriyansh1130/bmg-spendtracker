import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {PurchaseOrder} from "../purchase-order.model";
import {SpendTrackerStoreService} from "../../core/services/spend-tracker-store.service";

@Injectable()
export class CreatePurchaseOrderStoreService {

  private _currentPurchaseOrder: BehaviorSubject<PurchaseOrder> = new BehaviorSubject(new PurchaseOrder());

  constructor(private spendtrackerStore: SpendTrackerStoreService) {

  }

  public selectPurchaseOrder(purchaseorder: PurchaseOrder) {
    this.spendtrackerStore.selectProject(purchaseorder.positions[0].project);
    this.spendtrackerStore.selectCountry(purchaseorder.country);
    this._currentPurchaseOrder.next(purchaseorder)
  }
}
