import { Injectable } from '@angular/core';
import {ResourceService} from "../../resource/resource.service";
import {PurchaseOrderPosition, PurchaseOrderPositionAttachment} from "../purchase-order.model";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class PurchaseOrderPositionAttachmentService {

  constructor(private resourceService: ResourceService, private httpClient: HttpClient) { }

  findAttachmentsByPurchaseOrderPosition(purchaseOrderPosition: PurchaseOrderPosition) {
    return this.resourceService.findManyByUrl('/api/purchase-order-position-attachments/search/findAllByPurchaseOrderPosition_Id',
      PurchaseOrderPositionAttachment,
      {
        projection: 'purchaseOrderPositionAttachmentProjection',
        purchaseOrderPositionId: purchaseOrderPosition.id
      })
  }

  downloadByUuid(uuid: string) {
    const fileUrl = `/api/purchase-order-position-attachments/download`;
    this.httpClient.get(fileUrl, {observe: 'response', responseType: 'blob', params: {uuid: uuid}})
      .subscribe(resp => {
        const contentDispositionHeader = resp.headers.get('Content-Disposition');
        const result = contentDispositionHeader.split(';')[1].trim().split('=')[1];
        const fileName = result.replace(/"/g, '');
        const objectUrl = window.URL.createObjectURL(resp.body);
        const anchor = document.createElement("a");
        document.body.appendChild(anchor);
        anchor.href = objectUrl;
        anchor.download = fileName;
        anchor.click();

        setTimeout(() => {
          window.URL.revokeObjectURL(objectUrl);
          document.body.removeChild(anchor);
        }, 0);
      });
  }
}
