import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {ResourceService} from "../../resource/resource.service";
import {PageableCollection} from "../../resource/pageable-collection";
import {PurchaseOrder} from "../purchase-order.model";
import {HttpClient} from "@angular/common/http";
import {IPurchaseOrderDto} from "../interfaces";

@Injectable()
export class PurchaseOrderService {

  private _purchaseOrders: BehaviorSubject<PageableCollection<PurchaseOrder>> = new BehaviorSubject(new PageableCollection());

  public readonly purchaseOrders: Observable<PageableCollection<PurchaseOrder>> = this._purchaseOrders.asObservable();

  constructor(private resourceService: ResourceService, private http: HttpClient) {
      this.loadInitial();
  }

  private loadInitial(){
    this.resourceService.findMany(PurchaseOrder).then((result) =>this._purchaseOrders.next(result))
  }

  public save(order: PurchaseOrder) {
    const positionPromises = order.positions.map((pos) => this.resourceService.save(pos)) ;

    return Promise.all(positionPromises).then(positions => {
        order.positions = positions;
        return this.resourceService.save(order);
    });
  }

  public findById(id: string) {
    return this.resourceService.findById(id, PurchaseOrder, {projection: 'purchaseOrderProjection'});
  }

  public upsert(order: IPurchaseOrderDto) {
    return this.http.post("/api/orders/upsert", order).toPromise().then(created => this.resourceService.mapResourceFromResponse(created));
  }

  public delete(purchaseOrder: PurchaseOrder) {
    return this.resourceService.delete(purchaseOrder);
  }


}
