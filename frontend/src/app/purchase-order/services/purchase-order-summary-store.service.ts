import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";

const FILTER_STATUS_SESSION_KEY = "PurchaseOrderSummaryStoreService_filterStatus";
const FILTER_PROJECT_NAME_SESSION_KEY = "PurchaseOrderSummaryStoreService_filterProjectName";
const FILTER_SAP_CLIENT_CODE_SESSION_KEY = "PurchaseOrderSummaryStoreService_filterSapClientCode";
const FILTER_SPEND_TYPE_SESSION_KEY = "PurchaseOrderSummaryStoreService_filterSpendType";
const FILTER_PO_NUMBER_SESSION_KEY = "PurchaseOrderSummaryStoreService_filterPurchaseOrderNumber";
const FILTER_VENDOR_NAME_SESSION_KEY = "PurchaseOrderSummaryStoreService_filterVendorName";
const FILTER_TERRITORY_NAME_SESSION_KEY = "PurchaseOrderSummaryStoreService_filterTerritoryName";
const FILTER_CREATED_BY_SESSION_KEY = "PurchaseOrderSummaryStoreService_filterCreatedBy";


@Injectable()
export class PurchaseOrderSummaryStoreService {

  constructor() {
    sessionStorage.getItem(FILTER_STATUS_SESSION_KEY) && this.filterVendor(sessionStorage.getItem(FILTER_STATUS_SESSION_KEY));
    sessionStorage.getItem(FILTER_PROJECT_NAME_SESSION_KEY) && this.filterProject(sessionStorage.getItem(FILTER_PROJECT_NAME_SESSION_KEY));
    sessionStorage.getItem(FILTER_SAP_CLIENT_CODE_SESSION_KEY) && this.filterSapCode(sessionStorage.getItem(FILTER_SAP_CLIENT_CODE_SESSION_KEY));
    sessionStorage.getItem(FILTER_SPEND_TYPE_SESSION_KEY) && this.filterSapCode(sessionStorage.getItem(FILTER_SPEND_TYPE_SESSION_KEY));
    sessionStorage.getItem(FILTER_PO_NUMBER_SESSION_KEY) && this.filterPurchaseOrderNumber(sessionStorage.getItem(FILTER_PO_NUMBER_SESSION_KEY));
    sessionStorage.getItem(FILTER_VENDOR_NAME_SESSION_KEY) && this.filterVendor(sessionStorage.getItem(FILTER_VENDOR_NAME_SESSION_KEY));
    sessionStorage.getItem(FILTER_TERRITORY_NAME_SESSION_KEY) && this.filterTerritory(sessionStorage.getItem(FILTER_TERRITORY_NAME_SESSION_KEY));
    sessionStorage.getItem(FILTER_CREATED_BY_SESSION_KEY) && this.filterTerritory(sessionStorage.getItem(FILTER_CREATED_BY_SESSION_KEY));
  }

  private _statusFilter: BehaviorSubject<string> = new BehaviorSubject<string>("");
  private _projectNameFilter: BehaviorSubject<string> = new BehaviorSubject<string>("");
  private _sapCodeFilter: BehaviorSubject<string> = new BehaviorSubject<string>("");
  private _spendTypeFilter: BehaviorSubject<string> = new BehaviorSubject<string>("");
  private _purchaseOrderNumberFilter: BehaviorSubject<string> = new BehaviorSubject<string>("");
  private _vendorFilter: BehaviorSubject<string> = new BehaviorSubject<string>("");
  private _territoryFilter: BehaviorSubject<string> = new BehaviorSubject<string>("");
  private _createdByFilter: BehaviorSubject<string> = new BehaviorSubject<string>("");


  public readonly statusFilter: Observable<string> = this._statusFilter.asObservable();
  public readonly projectFilter : Observable<string> = this._projectNameFilter.asObservable();
  public readonly sapCodeFilter : Observable<string> = this._sapCodeFilter.asObservable();
  public readonly spendTypeFilter: Observable<string> = this._spendTypeFilter.asObservable();
  public readonly purchaseOrderNumberFilter : Observable<string> = this._purchaseOrderNumberFilter.asObservable();
  public readonly territoryFilter : Observable<string> = this._territoryFilter.asObservable();
  public readonly vendorFilter : Observable<string> = this._vendorFilter.asObservable();
  public readonly createdByFilter: Observable<string> = this._createdByFilter.asObservable();

  filterStatus(status) {
    this._statusFilter.next(status);
    sessionStorage.setItem(FILTER_STATUS_SESSION_KEY, status);
  }

  filterProject(projectName) {
    this._projectNameFilter.next(projectName);
    sessionStorage.setItem(FILTER_PROJECT_NAME_SESSION_KEY, projectName);
  }

  filterSapCode(sapCode) {
    this._sapCodeFilter.next(sapCode);
    sessionStorage.setItem(FILTER_SAP_CLIENT_CODE_SESSION_KEY, sapCode);
  }

  filterSpendType(status) {
    this._spendTypeFilter.next(status);
    sessionStorage.setItem(FILTER_SPEND_TYPE_SESSION_KEY, status);
  }

  filterPurchaseOrderNumber(poNumber) {
    this._purchaseOrderNumberFilter.next(poNumber);
    sessionStorage.setItem(FILTER_PO_NUMBER_SESSION_KEY, poNumber);
  }

  filterVendor(vendorName) {
    this._vendorFilter.next(vendorName);
    sessionStorage.setItem(FILTER_VENDOR_NAME_SESSION_KEY, vendorName);
  }

  filterTerritory(territoryName) {
    this._territoryFilter.next(territoryName);
    sessionStorage.setItem(FILTER_TERRITORY_NAME_SESSION_KEY, territoryName);
  }

  filterCreatedBy(createdBy) {
    this._createdByFilter.next(createdBy);
    sessionStorage.setItem(FILTER_CREATED_BY_SESSION_KEY, createdBy);
  }

}
