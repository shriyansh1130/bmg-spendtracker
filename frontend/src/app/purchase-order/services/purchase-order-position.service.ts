import {Injectable} from '@angular/core';
import {ResourceService} from "../../resource/resource.service";
import {PurchaseOrderCodingRules, PurchaseOrderPosition} from "../purchase-order.model";
import {from as fromPromise} from "rxjs";
import {HttpClient} from "@angular/common/http";


@Injectable()
export class PurchaseOrderPositionService {

  constructor(private resourceService: ResourceService, private httpClient: HttpClient) {
  }

  public calcTotal(price: number, quantity: number): number {
    return (Number(price) * Number(quantity));
  }

  find(status: string, spendType: string, createdBy: string, poNumber: string, projectName: string, country: string, vendor: string, page = 0, size = 25, fieldName = "project.projectName", sort = 'desc') {
    return fromPromise(this.resourceService.findManyByUrl(
      '/api/purchase-order-positions/summary',
      PurchaseOrderPosition, {
        status: status,
        spendType: spendType,
        createdBy: createdBy,
        poNumber: poNumber,
        projectName: projectName,
        countryName: country,
        vendorName: vendor,
        page,
        size,
        sort: `${fieldName},${sort}`,
      projection: 'purchaseOrderPositionSummaryProjection'
    }));
  }

  fetchWarnings(positionId: string): Promise<Array<string>> {
    if (positionId === null || positionId === undefined) {
      console.error("Tried to fetch warnings without position id");
      return null;
    }
    return this.httpClient.get<Array<string>>(`/api/purchase-order-positions/${positionId}/warnings`).toPromise();
  }

  fetchCodingRules(positionId: string): Promise<PurchaseOrderCodingRules> {
    if (positionId === null || positionId === undefined) {
      console.error("Tried to fetch Recoupment Percentage without position id");
      return null;
    }
    return this.httpClient.get<PurchaseOrderCodingRules>(`/api/purchase-order-positions/${positionId}/coding-rules`).toPromise();
  }
}
