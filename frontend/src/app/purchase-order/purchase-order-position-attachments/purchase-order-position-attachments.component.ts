import {
  Component, EventEmitter, Inject, Input, OnChanges, OnInit, Output, SimpleChanges,
  ViewChild
} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatSnackBar} from "@angular/material";
import {PurchaseOrderPosition, PurchaseOrderPositionAttachment} from "../purchase-order.model";
import {DropzoneComponent, DropzoneConfig, DropzoneConfigInterface} from "ngx-dropzone-wrapper";
import {ResourceService} from "../../resource/resource.service";
import {PurchaseOrderPositionAttachmentService} from "../services/purchase-order-position-attachment.service";
import {OktaAuthService} from "@okta/okta-angular";


@Component({
  selector: 'bst-purchase-order-position-attachments',
  templateUrl: './purchase-order-position-attachments.component.html',
  styleUrls: ['./purchase-order-position-attachments.component.scss']
})
export class PurchaseOrderPositionAttachmentsComponent implements OnInit {
  config: DropzoneConfigInterface;
  dropzoneReady = false;
  //attachments = [];
  @Input() purchaseOrderPosition: PurchaseOrderPosition;
  @Input() instantDelete: false;
  @Output() onDeleteAttempt = new EventEmitter();

  @ViewChild(DropzoneComponent) componentRef?: DropzoneComponent;

  constructor(private resourceService: ResourceService,
              private positionAttachmentService: PurchaseOrderPositionAttachmentService,
              private snackBar: MatSnackBar,
              private oktaAuth: OktaAuthService
              ) {

  }

  ngOnInit() {
    this.config = {
      url: `/api/purchase-order-positions/${this.purchaseOrderPosition && this.purchaseOrderPosition.id ? this.purchaseOrderPosition.id : 'unassigned'}/attachments`,
      maxFilesize: 10,
      timeout: 0,
      maxFiles: 1,
      withCredentials: true,
    };
    this.addAuthHeaders().then(()=> this.dropzoneReady = true);
    this.loadAttachments();
  }

  async addAuthHeaders() {
    this.config.headers = {
      "Authorization": "Bearer " + await this.oktaAuth.getAccessToken()
    };
  }

  onUploadSuccess($event) {
    const addedAttachment = this.resourceService.mapResourceFromResponse($event[1]);
    this.purchaseOrderPosition.attachments.push(addedAttachment)
  }

  onUploadError($event) {
    this.snackBar.open($event[1], null,{
      duration: 2000,
    });
  }

  removePreview($event) {
    this.componentRef.directiveRef.reset();
  }

  private loadAttachments() {
    if (this.purchaseOrderPosition && this.purchaseOrderPosition.id) {
      this.positionAttachmentService.findAttachmentsByPurchaseOrderPosition(this.purchaseOrderPosition).then(attachments => this.purchaseOrderPosition.attachments = attachments.elements);
    }
  }

  deleteAttachment(attachment: PurchaseOrderPositionAttachment, $event) {
    $event.preventDefault();
    $event.stopPropagation();

    if (this.instantDelete) {
      attachment.delete().then(() => {
        this.purchaseOrderPosition.attachments.splice(this.purchaseOrderPosition.attachments.indexOf(attachment), 1);
      })
    } else {
      this.purchaseOrderPosition.attachments.splice(this.purchaseOrderPosition.attachments.indexOf(attachment), 1);
      this.onDeleteAttempt.emit(attachment);
    }
  }

  showAttachment(attachment: PurchaseOrderPositionAttachment) {
    this.positionAttachmentService.downloadByUuid(attachment.uuid);
  }
}


@Component({
  selector: 'bst-purchase-order-position-attachments-dialog',
  template: `
    <bst-purchase-order-position-attachments
      [purchaseOrderPosition]="purchaseOrderPosition"
      instantDelete="true"    
    ></bst-purchase-order-position-attachments>`,
})
export class PurchaseOrderPositionAttachmentsDialog implements OnInit {
  purchaseOrderPosition: PurchaseOrderPosition;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    this.purchaseOrderPosition = data.purchaseOrderPosition;
  }

  ngOnInit() {
  }
}


@Component({
  selector: 'bst-purchase-order-position-attachments-button',
  template: `
    <button mat-icon-button (click)="showModal($event)">
      <mat-icon [matBadgeHidden]="attachmentCount === 0" [matBadge]="attachmentCount" matBadgePosition="after"
                matBadgeSize="small">attach_file
      </mat-icon>
    </button>`,
})
export class PurchaseOrderPositionAttachmentsButton implements OnInit {
  @Input() purchaseOrderPosition: PurchaseOrderPosition;
  attachmentCount = 0;


  constructor(public dialog: MatDialog, private attachmentService: PurchaseOrderPositionAttachmentService) {
  }

  ngOnInit() {
    if (this.purchaseOrderPosition.attachments) {
      if (this.purchaseOrderPosition.attachments.length > 0) {
        this.attachmentCount = this.purchaseOrderPosition.attachments.length;
      }
    } else {
      if (this.purchaseOrderPosition.id) {
        this.loadAttachments();
      }
    }
  }

  loadAttachments(){
    this.attachmentService.findAttachmentsByPurchaseOrderPosition(this.purchaseOrderPosition).then(attachments => {
      this.attachmentCount = attachments.elements.length;
    })
  }

  showModal(event) {
    event.preventDefault();
    event.stopPropagation();

    let dialogRef = this.dialog.open(PurchaseOrderPositionAttachmentsDialog, {
      data: {
        purchaseOrderPosition: this.purchaseOrderPosition
      },
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      this.attachmentCount = this.purchaseOrderPosition.attachments.length;
    });
  }

}
