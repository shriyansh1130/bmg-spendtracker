import {Component, Input, OnInit} from '@angular/core';
import {Country} from "../../country/country.model";
import {BudgetSpendType} from "../../budget-category/budget-category.model";

@Component({
  selector: 'bst-purchase-order-position-spend-type',
  templateUrl: './purchase-order-position-spend-type.component.html',
  styleUrls: ['./purchase-order-position-spend-type.component.scss']
})
export class PurchaseOrderPositionSpendTypeComponent implements OnInit {
  @Input() country: Country;
  @Input() spendType: BudgetSpendType

  constructor() { }

  ngOnInit() {
  }

}
