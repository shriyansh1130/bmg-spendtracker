import {FormGroup} from "@angular/forms";
import {Currency} from "../currency/currency.model";
import {Country} from "../country/country.model";
import {Project} from "../project/project.model";
import {Vendor} from "../vendor/vendor.model";
import {BudgetSpendType} from "../budget-category/budget-category.model";
import {PurchaseOrderPosition, PurchaseOrderPositionAttachment} from "./purchase-order.model";

export interface IPurchaseOrderPositionDto {
  id: string,
  projectId: string,
  spendTypeId: string,
  price: number,
  quantity: number,
  description: string,
  serviceMonth: string,
  attachments: string[],
  notes: string
}

export interface IPurchaseOrderDto {
  id: string,
  purchaseOrderNumber: string,
  status: string,
  countryId: string,
  vendorId: string ,
  currencyId: string,
  positions: IPurchaseOrderPositionDto[],
}

export interface PurchaseOrderPositionDialogData {
  positionForm: FormGroup;
  purchaseOrderPosition: PurchaseOrderPosition;
  position: number,
  country: Country,
  referenceProject: Project,
  disabled: boolean
}

export interface IPurchaseOrderForm {
  id: string,
  purchaseOrderNumber: string,
  status: Status,
  country: Country,
  vendor: Vendor,
  currency: Currency,
  positions: IPurchaseOrderPositionForm[],
}

export interface IPurchaseOrderPositionForm {
  id: string,
  project: Project,
  spendType: BudgetSpendType,
  poCountry: Country,
  price: number,
  quantity: number,
  description: string,
  serviceMonth: string,
  attachments: PurchaseOrderPositionAttachment[],
  notes: string;
}

export type Status = 'Draft' | 'Closed' | 'Open';


