import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PurchaseOrderCreateComponentWrapper} from "./purchase-order-details/purchase-order-create-wrapper.component";
import {CanActivateCreatePurchaseOrder} from "./purchase-order-details/purchase-order-create.guard";
import {PurchaseOrderSummaryComponent} from "./purchase-order-summary/purchase-order-summary.component";
import {PurchaseOrderEditComponentWrapper} from "./purchase-order-details/purchase-order-edit-wrapper.component";
import {OktaAuthGuard} from "@okta/okta-angular";
import {RoleGuardService} from "../auth/role-guard.service";
import {RawPostingImportComponent} from "../raw-posting/raw-posting-import/raw-posting-import.component";

const routes: Routes = [
  {
    path: 'orders/create',
    component: PurchaseOrderCreateComponentWrapper ,
    canActivate: [CanActivateCreatePurchaseOrder, OktaAuthGuard, RoleGuardService],
    data: {
      expectedRoles: ['bmg-spendtracker-user']
    }
  },
  {
    path: 'orders/:id',
    component: PurchaseOrderEditComponentWrapper,
    canActivate: [OktaAuthGuard, RoleGuardService],
    data: {
      expectedRoles: ['bmg-spendtracker-admin', 'bmg-spendtracker-user', 'bmg-spendtracker-readonly']
    }
  },
  {
    path: 'orders',
    component: PurchaseOrderSummaryComponent,
    canActivate: [OktaAuthGuard,  RoleGuardService],
    data: {
      expectedRoles: ['bmg-spendtracker-admin', 'bmg-spendtracker-user', 'bmg-spendtracker-readonly']
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseOrderRoutingModule { }
