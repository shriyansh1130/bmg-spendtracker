import {Inject, Injectable, InjectionToken} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Resource} from "./resource";
import {Page} from "./page";
import {PageableCollection} from "./pageable-collection";

const HEADERS = new HttpHeaders().set('Content-Type', 'application/json');

const HEADERS_URI_LIST = new HttpHeaders().set('Content-Type', 'text/uri-list');

export let MANAGED_RESOURCES = new InjectionToken('managed.resources');

@Injectable()
export class ResourceService {

  constructor(@Inject(MANAGED_RESOURCES) private managedResources: (new () => Resource)[], private http: HttpClient) {

  }

  public findById<T extends Resource>(id: string, resourceType: new () => T, queryConfig?: any): Promise<T> {
    let baseUrl = (<any>resourceType).PATH;
    return this.findByUri(baseUrl.concat('/', id), queryConfig);
  }

  public findByUri<T extends Resource>(uri: string, queryConfig?: any): Promise<T> {
    let queryParams = queryConfig ? queryConfig : {};
    let resource: T;
    return this.http.get(uri, {headers: HEADERS, params: queryParams})
      .toPromise()
      .then(response => resource = this.mapResourceFromResponse<T>(response));
  }


  public findMany<T extends Resource>(resourceType: new () => T, queryConfig?: any, page?: Page): Promise<PageableCollection<T>> {
    let baseUrl = (<any>resourceType).PATH;
    return this.findManyByUrl(baseUrl, resourceType, queryConfig, page);
  }

  public findManyByUrl<T extends Resource>(url: string, resourceType: new () => T, queryConfig?: any, page?: Page): Promise<PageableCollection<T>> {

    let queryParams = queryConfig ? queryConfig : {};

    if (page) {
      queryParams['page'] = page.pageNumber;
      queryParams['size'] = page.pageSize;
    }

    return <Promise<PageableCollection<T>>> this.http.get(url, {headers: HEADERS, params: queryParams})
      .toPromise()
      .then(response => {
        let pageableCollection = new PageableCollection();
        pageableCollection.queryConfig = queryConfig;

        if (response['page']) {
          let pageResponse = response['page'];
          let page = new Page(
            pageResponse['number'],
            pageResponse['size'],
            pageResponse['totalPages'],
            pageResponse['totalElements']
          );
          pageableCollection.page = page;
        }

        let resources = [] as T[];
        if (response ['_embedded']) {
          let embeddedResponse = response['_embedded'];
          Object.keys(embeddedResponse)
            .forEach(key => embeddedResponse[key]
              .forEach(embeddedElement => resources.push(this.mapResourceFromResponse(embeddedElement))));
        }

        pageableCollection.elements = resources;
        pageableCollection.resourceType = resourceType;
        pageableCollection.resourceService = this;

        return pageableCollection;
      });
  }


  public createResource<T extends Resource>(resourceType: new () => T): T {
    let resource = new resourceType();
    resource.resourceService = this;
    return resource;
  }

  public save<T extends Resource>(resource: T): Promise<T> {
    let requestBody = this.mapRequestBodyFromResource(resource);
    if (!resource.uri) {
      return <Promise<T>> this.http.post(resource.path(), requestBody, {headers: HEADERS})
        .toPromise()
        .then(response => this.mapResourceFromResponse(response));
    } else {
      return <Promise<T>> this.http.patch(resource.uri, requestBody, {headers: HEADERS})
        .toPromise()
        .then(response => this.mapResourceFromResponse(response));
    }
  }

  public saveAssociation<T extends Resource>(resource: T, association: string): Promise<T> {

    let associationUrl = resource.getLink(association);
    let elements = resource[association];
    let lineSeperatedUris = elements.map(e => e['uri'])
      .join('\n');

    return this.http.put(associationUrl, lineSeperatedUris, {headers: HEADERS_URI_LIST})
      .toPromise()
      .then(response => resource);
  }

  public delete<T extends Resource>(resource: T): Promise<T> {
    return this.http.delete<any>(resource.uri)
      .toPromise()
      .then(response => Promise.resolve(resource));
  }


  private mapRequestBodyFromResource<T extends Resource>(resource: T): string {
    let requestBody = JSON.stringify(resource, (key, value) => {
      if (key !== '' && value instanceof Resource) return value.uri; //leaves root out
      else if (key.startsWith("_")) return undefined; //omit private/technically params
      else return value;
    });
    return requestBody;
  }


  public mapResourceFromResponse<T extends Resource>(response: any): T {
    //Adding Resourcedata
    let resourceTypeNew = this.getResourceType(response);

    let resource = new resourceTypeNew();


    Object.keys(response)
      .forEach(key => {
          if (key !== '_links') {
            let value = response[key];
            if (value instanceof Array) {
              value = value.map(item => {
                if (this.isResource(item)) {
                  return this.mapResourceFromResponse(item);
                } else {
                  return item;
                }
              });
            } else if (this.isResource(value)) {
              value = this.mapResourceFromResponse(value);
            }
            resource[key] = value;
          }
        }
      );

    //Adding all links
    let links = response['_links'];
    Object.keys(links)
      .forEach(link => {
        resource.addLink(link, links[link]['href']);
      });

    resource.uri = links['self']['href'];
    resource.resourceService = this;

    return <T> resource;
  }

  private getResourceType(response: any) {
    if (response['_links'] && response['_links']['self']) {

      let uri = response['_links']['self']['href'];
      if (uri.indexOf('{') > -1) {
        uri = uri.substr(0, uri.indexOf('{'));
      }

      let resourceType = this.managedResources.filter(resource => uri.indexOf((<any>resource).PATH) > -1);
      if (resourceType.length > 0) {
        return resourceType[0];
      } else {
        console.error('No registered Resource found for URI', uri);
      }
    } else {
      console.error('Cannot convert response to resource:', response);
    }
  }

  private isResource(object: any): boolean {
    return (object && object['_links'] && object['_links']['self']);
  }

}

