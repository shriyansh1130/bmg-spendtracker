import {Resource} from "./resource";

export const compareResources = (r1: Resource, r2: Resource): boolean => {
  return r1 && r2 ? r1.id === r2.id : r1 === r2;
}
