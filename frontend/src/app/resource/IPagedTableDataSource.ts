import {Resource} from './resource';
import {Observable} from 'rxjs';
import {DataSource} from "@angular/cdk/collections";

export interface IPagedTableDataSource<T extends Resource> extends DataSource<T> {
  readonly loading$:Observable<boolean>;

  connect(collectionViewer): Observable<T[]>;
  disconnect(collectionViewer): void;
}
