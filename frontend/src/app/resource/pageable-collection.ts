import {Page} from "./page";
import {Resource} from "./resource";
import {ResourceService} from "./resource.service";

export class PageableCollection<T extends Resource> {

  private _page: Page;
  private _elements: T[];

  private _queryConfig: any;

  private _resourceType: new () => T;
  private _resourceService: ResourceService;

  constructor() {
  }

  public set resourceService(_resourceService: ResourceService) {
    this._resourceService = _resourceService;
  }

  public set resourceType(_resourceType: new () => T) {
    this._resourceType = _resourceType;
  }

  public get page(): Page {
    return this._page;
  }

  public set page(_page: Page) {
    this._page = _page;
  }

  public get elements(): T[] {
    return this._elements;
  }

  public set elements(_resources: T[]) {
    this._elements = _resources;
  }

  public get queryConfig(): string {
    return this._queryConfig;
  }

  public set queryConfig(_queryConfig: string) {
    this._queryConfig = _queryConfig;
  }

  public first(): Promise<PageableCollection<T>> {
    let firstPage = this.page.first();
    return this._resourceService.findMany(this._resourceType, this.queryConfig, firstPage);
  }

  public last(): Promise<PageableCollection<T>> {
    let lastPage = this.page.last();
    return this._resourceService.findMany(this._resourceType, this.queryConfig, lastPage);
  }

  public next(): Promise<PageableCollection<T>> {
    if (this.page.hasNext()) {
      let nextPage = this.page.next();
      return this._resourceService.findMany(this._resourceType, this.queryConfig, nextPage);
    } else {
      return null;
    }
  }

  public previous(): Promise<PageableCollection<T>> {
    if (this.page.hasPrevious()) {
      let previousPage = this.page.previous();
      return this._resourceService.findMany(this._resourceType, this.queryConfig, previousPage);
    } else {
      return null;
    }
  }

  public getPage(pageNumber: number): Promise<PageableCollection<T>> {
    let newPage = this.page.getPage(pageNumber);
    if (newPage) {
      return this._resourceService.findMany(this._resourceType, this.queryConfig, newPage);
    } else {
      return null;
    }
  }

}
