export class Page {

  pageSize: number;
  pageNumber: number;
  totalElements: number;
  totalPages: number;


  constructor(pageNumber: number,
              pageSize: number,
              totalPages: number,
              totalElements?: number) {
    this.pageNumber = pageNumber;
    this.pageSize = pageSize;
    this.totalPages = totalPages;
    this.totalElements = totalElements;
  }

  public hasNext(): boolean {
    return this.pageNumber + 1 < this.totalPages;
  }

  public hasPrevious(): boolean {
    return this.pageNumber - 1 < 0;
  }

  public getPage(pageNumber: number) {
    if (-1 < pageNumber && pageNumber < this.totalPages) {
      return new Page(pageNumber, this.pageSize, this.totalPages, this.totalElements);
    } else {
      return null;
    }
  }

  public first(): Page {
    return new Page(0, this.pageSize, this.totalPages, this.totalElements);
  }

  public last(): Page {
    return new Page(this.totalPages - 1, this.pageSize, this.totalPages, this.totalElements);
  }

  public next(): Page {
    if (this.hasNext()) {
      return new Page(this.pageNumber + 1, this.pageSize, this.totalPages, this.totalElements);
    } else {
      return null;
    }
  }

  public previous(): Page {
    if (this.hasPrevious()) {
      return new Page(this.pageNumber - 1, this.pageSize, this.totalPages, this.totalElements);
    } else {
      return null;
    }
  }

}
