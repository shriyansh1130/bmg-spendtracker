import {ResourceService} from "./resource.service";

export class Resource {
  public static PATH: string = "UNKNOWN";

  public path(): string {
    const resourceType = <typeof Resource>this.constructor;
    return resourceType.PATH;
  }

  //All the links
  private _links: string[] = [];

  //Identifier
  private _uri: string;
  private _id: string;

  private _resourceService: ResourceService;

  public hasLink(rel: string): boolean {
    return this._links[rel];
  }

  public getLink(rel: string): string {
    return this._links[rel];
  }

  public addLink(rel: string, href: string) {
    if (href.indexOf('{') > -1) {
      this._links[rel] = href.substr(0, href.indexOf('{'));
    } else {
      this._links[rel] = href;
    }
  }

  public removeLink(rel: string) {
    delete this._links[rel];
  }

  public set resourceService(_resourceService: ResourceService) {
    this._resourceService = _resourceService;
  }

  public get uri(): string {
    return this._uri;
  }

  public set uri(_uri: string) {
    if (_uri.indexOf('{') > -1) {
      this._uri = _uri.substr(0, _uri.indexOf('{'));
    } else {
      this._uri = _uri;
    }

    let uriParts = this._uri.split('/');
    this._id = uriParts[uriParts.length - 1];
  }

  public get id(): string {
    return this._id;
  }

  public set id(_id: string) {
    this._id = _id;
  }

  public removeElement<T extends Resource>(association: string, element: T): Promise<Resource> {
    if (!this[association]) {
      this.resolveAssociation(association, Resource);
    }
    let collection = this[association] as Array<T>;
    let index = collection.map(el => el.uri)
      .indexOf(element.uri);
    if (index > -1) {
      collection.splice(index, 1);
    }
    return this.saveAssociation(association);
  }

  public addElement<T extends Resource>(association: string, element: T): Promise<Resource> {
    if (!this[association]) {
      this.resolveAssociation(association, Resource);
    }
    let collection = this[association] as Array<T>;
    collection.push(element);
    return this.saveAssociation(association);
  }

  public saveAssociation(association: string): Promise<Resource> {
    return this._resourceService.saveAssociation(this, association);
  }

  /**
   * resolves an array-like association (findMany)
   */
  public resolveAssociation<T extends Resource>(association: string, resourceType: new () => T, queryConfig?: any): Promise<Resource> {
    let associationUrl = this.getLink(association);
    return this._resourceService.findManyByUrl(associationUrl, resourceType, queryConfig)
      .then(collection => this[association] = collection.elements)
      .then(e => Promise.resolve(this));
  }

  /**
   * resolves a single association (findOne)
   */
  public resolveOneAssociation<T extends Resource>(association: string, queryConfig?: any): Promise<Resource> {
    return this._resourceService.findByUri(this.getLink(association), queryConfig)
      .then(value => {
        this[association] = value;
      })
      .then(e => Promise.resolve(this));
  }

  public save(): Promise<Resource> {
    return this._resourceService.save(this);
  }

  public delete(): Promise<Resource> {
    return this._resourceService.delete(this);
  }

}
