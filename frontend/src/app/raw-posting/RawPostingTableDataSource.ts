import {catchError, finalize, map} from "rxjs/operators";
import {AppConfigService} from "@app/core/services/app-config.service";
import {CollectionViewer} from "@angular/cdk/collections";
import {IPagedTableDataSource} from "@app/resource/IPagedTableDataSource";
import {BehaviorSubject, Observable, of} from "rxjs/index";
import {PageableCollection} from "@app/resource/pageable-collection";
import {RawPosting} from "@app/raw-posting/raw-posting.model";
import {RawPostingService} from "@app/raw-posting/raw-posting.service";

export class RawPostingTableDataSource implements IPagedTableDataSource<RawPosting> {
  private _rawPostingPageSubject = new BehaviorSubject<PageableCollection<RawPosting>>(new PageableCollection());
  private _loadingSubject = new BehaviorSubject<boolean>(false);
  public readonly loading$ = this._loadingSubject.asObservable();

  public readonly totalRawPostingCount$ = this._rawPostingPageSubject.asObservable().pipe(
    map((p) => p.page && p.page.totalElements || 0)
  );

  constructor(private appConfig: AppConfigService, private rawPostingService: RawPostingService) {

  }

  connect(collectionViewer: CollectionViewer): Observable<RawPosting[]> {
    return this._rawPostingPageSubject.asObservable().pipe(
      map((page) => page.elements)
    );
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this._rawPostingPageSubject.complete();
    this._loadingSubject.complete();
  }

  public loadRawPostings(sortField = 'uniqueKey', sortDirection = 'desc', page = 0, pageSize: number = this.appConfig.defaultPageSize) {
    this._loadingSubject.next(true);

    this.rawPostingService.loadRawPostingsWithPagination(page, pageSize, sortField, sortDirection).pipe(
      catchError(() => of([])),
      finalize(() => this._loadingSubject.next(false))
    )
      .subscribe(rawPostings => {
        this._rawPostingPageSubject.next(<PageableCollection<RawPosting>> rawPostings)
      });
  }
}
