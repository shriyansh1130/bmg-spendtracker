import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {RawPosting, RawPostingSyncTask} from "./raw-posting.model";
import {PageableCollection} from "../resource/pageable-collection";
import {ResourceService} from "../resource/resource.service";
import {AppConfigService} from "@app/core/services/app-config.service";
import {from as fromPromise, Observable} from "rxjs";

@Injectable()
export class RawPostingService {

  constructor(private http: HttpClient, private resourceService: ResourceService, private appConfig: AppConfigService) {
  }

  public loadRawPostingsWithPagination(page = 0, size = this.appConfig.defaultPageSize, fieldName = "uniqueKey", sort = 'desc'): Observable<PageableCollection<RawPosting>> {
    return fromPromise(this.resourceService.findManyByUrl('api/raw-postings/search/findByProcessStatus', RawPosting, {
      processStatus: 'UNPROCESSED',
      page: page,
      size: size,
      sort: `${fieldName},${sort}`
    }));
  }

  public getRawPostingSyncTask(id): Observable<RawPostingSyncTask> {
    return fromPromise(this.resourceService.findById(id,RawPostingSyncTask));
  }

  public getCurrentActiveSyncTask(): Promise<RawPostingSyncTask> {
    return this.resourceService.findManyByUrl('api/raw-posting-sync-tasks/search/findAllBySyncStatus',
      RawPostingSyncTask,
      {
        rawPostingSyncStatus: 'IN_PROGRESS'
      }).then(value => {
      return value.elements[0]
    }) //Pre-Condition: Only one sync task should be in progress -> we expect only one result
  }

}
