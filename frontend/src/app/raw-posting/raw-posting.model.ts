import {Resource} from "../resource/resource";

export class RawPosting extends Resource {
  public static PATH: string = "api/raw-postings";

  constructor(
    uniqueKey?: string,
    poNumber?: string,
    text?: string,
    projectId?: string,
  ) {
    super();
  }
}

export class RawPostingSyncTask extends Resource {
  public static PATH: string = "api/raw-posting-sync-tasks";

  constructor(
    public syncStatus?: string,
    public processedItems?: number,
    public totalItems?: number,
    public  matchedPo?: number,
    public  unmatchedPo?: number,
    public  errorMessages?: string[],
  ) {
    super();
  }
}
