import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {OktaAuthGuard} from "@okta/okta-angular";
import {RoleGuardService} from "../auth/role-guard.service";
import {RawPostingImportComponent} from "../raw-posting/raw-posting-import/raw-posting-import.component";
import {RawPostingsSyncComponent} from "./raw-postings-sync/raw-postings-sync.component";

const routes: Routes = [
  {
    path: 'raw-postings/import',
    component: RawPostingImportComponent,
    canActivate: [OktaAuthGuard,  RoleGuardService],
    data: {
      expectedRoles: ['bmg-spendtracker-admin']
    }
  },
  {
    path: 'raw-postings',
    component: RawPostingsSyncComponent,
    canActivate: [OktaAuthGuard,  RoleGuardService],
    data: {
      expectedRoles: ['bmg-spendtracker-admin']
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RawPostingRoutingModule { }
