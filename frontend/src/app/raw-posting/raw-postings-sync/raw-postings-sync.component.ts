import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import {MatDialog, MatDialogConfig, MatSnackBar, MatSort, PageEvent} from "@angular/material";
import {RawPostingService} from "../raw-posting.service";
import {AppConfigService} from "@app/core/services/app-config.service";
import {RawPostingTableDataSource} from "@app/raw-posting/RawPostingTableDataSource";
import {merge} from "rxjs/index";
import {CustomPaginatorComponent} from "@app/shared/custom-paginator/custom-paginator.component";
import {tap} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
import {TimerObservable} from "rxjs-compat/observable/TimerObservable";
import 'rxjs/add/operator/takeWhile';
import {RawPostingSyncTask} from "@app/raw-posting/raw-posting.model";
import {RawPostingSyncResultDialogComponent} from "@app/shared/raw-posting-sync-result-dialog/raw-posting-sync-result-dialog.component";

@Component({
  selector: 'bst-raw-postings-sync',
  templateUrl: './raw-postings-sync.component.html',
  styleUrls: ['./raw-postings-sync.component.scss']
})
export class RawPostingsSyncComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChildren(CustomPaginatorComponent) paginators: QueryList<CustomPaginatorComponent>;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: RawPostingTableDataSource;
  pageChanged = new EventEmitter<number>();
  displayedColumns: string[] = ['uniqueKey', 'poNumber', 'projectId', 'text'];
  public syncInProgress: boolean;
  private interval: number;
  public syncTask: RawPostingSyncTask;
  public progress: number;

  constructor(public appConfig: AppConfigService,
              private dialog: MatDialog,
              private rawPostingService: RawPostingService,
              private http: HttpClient,
              private snackbar: MatSnackBar) {
    this.syncInProgress = false;
    this.interval = 1000;
    this.progress = 0;
  }

  async ngOnInit() {
    this.dataSource = new RawPostingTableDataSource(this.appConfig, this.rawPostingService);
    this.dataSource.loadRawPostings();
    const currentTaskInProgress = await this.rawPostingService.getCurrentActiveSyncTask();
    if (currentTaskInProgress) {
      this.startSyncTimer(currentTaskInProgress.id);
    }

  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.goToPageZero());
    merge(this.sort.sortChange, this.pageChanged)
      .pipe(
        tap(() => this.loadRawPostingsPage())
      ).subscribe();
  }

  startSyncTimer(id) {
    this.syncInProgress = true;
    this.progress = 0;
    TimerObservable.create(0, this.interval)
      .takeWhile(() => this.syncInProgress)
      .subscribe(() => {
        this.rawPostingService.getRawPostingSyncTask(id)
          .subscribe((data) => {
            this.syncTask = data;
            this.progress = this.calculateProgress();
            if (this.syncTask.totalItems == this.syncTask.processedItems) {
              this.syncInProgress = false;
              this.loadRawPostingsPage();
            }
          });
      });
  }

  calculateProgress(): number {
    let numberOfErrors: number;
    if (this.syncTask.errorMessages) {
      numberOfErrors = this.syncTask.errorMessages.length;
    }
    return ((this.syncTask.processedItems + numberOfErrors) / this.syncTask.totalItems) * 100;

  }

  ngOnDestroy() {
    this.syncInProgress = false;
  }

  loadRawPostingsPage() {
    this.dataSource.loadRawPostings(this.sort.active, this.sort.direction, this.paginators.first.pageIndex, this.paginators.first.pageSize);
  }

  changePage(event: PageEvent) {
    this.paginators.forEach((p) => {
      p.pageIndex = event.pageIndex, p.pageSize = event.pageSize
    });
    this.pageChanged.emit(event.pageIndex);
  }

  private goToPageZero() {
    this.paginators.forEach(p => p.pageIndex = 0);
  }

  syncPos() {
    this.http.post('/api/raw-postings/synchronize', {}).toPromise()
      .then(response => {
        this.startSyncTimer(response['id']);
      })
      .catch(error => this.snackbar.open("Error: " + error.error.message));
  }

  openSyncResultDialog() {
    let syncResultData = new MatDialogConfig<any>();
    syncResultData.data = this.syncTask;
    this.dialog.open(RawPostingSyncResultDialogComponent, syncResultData);
  }
}
