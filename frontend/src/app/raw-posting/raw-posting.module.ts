import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ExportService} from "../shared/services/export.service";
import {ConfirmationDialogComponent} from "../shared/confirmation-dialog/confirmation-dialog.component";
import {AuthModule} from "../auth/auth.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {FlexLayoutModule} from "@angular/flex-layout";
import {BstMaterialModule} from "../bst-material/bst-material.module";
import {SharedModule} from "../shared";
import { RawPostingsSyncComponent } from './raw-postings-sync/raw-postings-sync.component';
import {RawPostingService} from "./raw-posting.service";
import {RawPostingRoutingModule} from "./raw-posting-routing.module";
import {RawPostingImportComponent} from "./raw-posting-import/raw-posting-import.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    BstMaterialModule,
    RawPostingRoutingModule,
    SharedModule,
    AuthModule
  ],
  declarations: [
    RawPostingImportComponent,
    RawPostingsSyncComponent
  ],
  providers: [
    ExportService,
    RawPostingService
  ],
  entryComponents: [
    ConfirmationDialogComponent,
  ]
})
export class RawPostingModule { }
