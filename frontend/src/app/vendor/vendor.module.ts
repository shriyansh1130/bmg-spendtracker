import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {VendorRoutingModule} from './vendor-routing.module';
import {VendorService} from './vendor.service';
import {SelectVendorDialog, VendorDetailsFormComponent} from './vendor-details-form/vendor-details-form.component';
import {ReactiveFormsModule} from "@angular/forms";
import {BstMaterialModule} from "../bst-material/bst-material.module";
import {FlexLayoutModule} from "@angular/flex-layout";
import {VendorDetailsDialogComponent} from "./vendor-details-form/vendor-details-dialog.component";
import {VendorImportComponent} from "./vendor-import/vendor-import.component";
import {SharedModule} from "../shared";

@NgModule({
  imports: [
    CommonModule,
    VendorRoutingModule,
    ReactiveFormsModule,
    BstMaterialModule,
    FlexLayoutModule,
    SharedModule
  ],
  declarations: [VendorDetailsFormComponent, VendorDetailsDialogComponent, SelectVendorDialog, VendorImportComponent],
  providers: [VendorService],
  entryComponents: [VendorDetailsDialogComponent, SelectVendorDialog]
})
export class VendorModule { }
