import {Injectable} from '@angular/core';
import {ResourceService} from "../resource/resource.service";
import {BehaviorSubject, from as fromPromise, Observable} from "rxjs";
import {PageableCollection} from "../resource/pageable-collection";
import {map} from "rxjs/operators";
import {Vendor} from "./vendor.model";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class VendorService {

  private _vendors: BehaviorSubject<PageableCollection<Vendor>> = new BehaviorSubject(new PageableCollection());

  public readonly vendors: Observable<Vendor[]> = this._vendors.asObservable().pipe(
    map(value => value.elements)
  );

  constructor(private resourceService: ResourceService, private http: HttpClient) {
    this.loadInitial();
  }

  public loadInitial() {
    this.resourceService.findMany(Vendor).then((result) => this._vendors.next(result))
  }

  public validVendors(countryId, projectId, name?): Observable<PageableCollection<Vendor>> {
    return fromPromise(this.resourceService.findManyByUrl(
      '/api/vendors/search/validVendors',
      Vendor,
      {countryId: countryId, projectId: projectId, name: name || "%"}));
  }

  public vendorsInCountry(country, name?): Observable<PageableCollection<Vendor>> {
    return fromPromise(this.resourceService.findManyByUrl('/api/vendors/search/findByCompany_Country_IdAndName1IgnoreCaseContaining',
      Vendor,
      {countryId: country.id, name1: name || "%", projection: 'vendorProjection'}
    ))
  }

  public findByVatRegistrationNumberAndSapCompanyCode(vatRegistrationNumber: string, sapCompanyCode): Promise<Vendor[]> {
    return this.resourceService.findManyByUrl('/api/vendors/search/findByVatRegistrationNumberAndCompany_SapCompanyCode', Vendor, {
      vatRegistrationNumber,
      sapCompanyCode
    }).then((value: PageableCollection<Vendor>) => value.elements)
  }

  saveVendor(vendor: Vendor) {
    return this.resourceService.save(vendor);
  }

  findSimilarVendors(vendorName: string, street: string, postcode: string, city: string, linkedCompanyId: string): Promise<any>{
    return this.http.get(
      '/api/vendors/search/similarVendors',
      {
        params: {
          vendorName,
          street,
          postcode,
          city,
          linkedCompanyId
        }
      }).toPromise();
  }
}
