import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import {ConfirmationDialogComponent} from "../../shared/confirmation-dialog/confirmation-dialog.component";
import {MAT_DIALOG_DATA, MatAutocompleteSelectedEvent, MatDialog, MatDialogRef} from "@angular/material";
import {VendorService} from "../vendor.service";
import {Vendor} from "../vendor.model";
import {Company} from "../../company/company.model";
import {CompanyService} from "../../company/services/company.service";
import {Country} from "../../country/country.model";
import {CountryService} from "../../country/services/country.service";
import {Observable, timer} from "rxjs";
import {debounce, filter, map, switchMap} from "rxjs/operators";
import {AppConfigService} from "../../core/services/app-config.service";

function emailOrEmptyValidator() {
  function emailOrEmpty(control: AbstractControl): ValidationErrors | null {
    return control.value === '' ? null : Validators.email(control);
  }
}

@Component({
  selector: 'bst-vendor-details-form',
  templateUrl: './vendor-details-form.component.html',
  styleUrls: ['./vendor-details-form.component.scss']
})
export class VendorDetailsFormComponent implements OnInit {
  vendorForm: FormGroup;
  filteredVendors: Observable<Vendor[]>;
  linkedCompany: Company;

  @Input() vendor: any;
  @Input() company: Company;
  @Input() country: Country;
  @Output() saved: EventEmitter<any> = new EventEmitter();
  @Output() cancel: EventEmitter<any> = new EventEmitter();

  constructor(private fb: FormBuilder,
              private dialog: MatDialog,
              private vendorService: VendorService,
              private companyService: CompanyService,
              private countryService: CountryService,
              private appConfig: AppConfigService) {
  }

  async ngOnInit() {
    this.createForm();
    this.registerVendorFilter();
    this.linkedCompany = this.country.id == this.company.country.id ? this.company : await this.companyService.findInternationalCompany(this.country);
  }

  private registerVendorFilter() {
    this.filteredVendors = this.vendorForm.get('vendorName').valueChanges.pipe(
      filter(value => typeof value === 'string'),
      debounce(() => timer(this.appConfig.filterInputDebounceInMillis)),
      switchMap((value) => this.vendorService.vendorsInCountry(this.country, value)),
      map(value => value.elements)
    )
  }

  private createForm() {
    this.vendorForm = this.fb.group({
      vendorName: [this.vendor, Validators.required],
      group: [''],
      accountNumber: [''],
      vatRegistrationNumber: [''],
      contactName: [''],
      street: [''],
      houseNumber: [''],
      city: [''],
      region: [''],
      postcode: [''],
      phoneNumber: ['', Validators.pattern('^[0-9]+')],
      email: ['', emailOrEmptyValidator()]
    })
  }

  async onSubmit() {
    const vendorFormData = this.vendorForm.getRawValue();
    // perform duplicate checks
    // --- Based on VAT
    const vatRegControl = this.vendorForm.get('vatRegistrationNumber');
    if (vatRegControl.value) {
      const vendorWithSameVat = await this.findVendorForVatNrAndSapCompanyCode(vatRegControl.value, this.linkedCompany.sapCompanyCode);
      if (vendorWithSameVat) {
        const shouldSelect = await this.askForSelection(vendorWithSameVat);
        if (shouldSelect) {
          this.saved.emit(vendorWithSameVat);
          return;
        } else {
          // else mark vatNumber as invalid
          vatRegControl.setErrors({exists: true});
          vatRegControl.markAsTouched();
        }
      }
    }

    // --- matching Criteria
    const vendorsMatchingCriteria = await this.findSimilarVendors();
    if (vendorsMatchingCriteria.length > 0) {
      const selected = await this.askForMultiSelection(vendorsMatchingCriteria);
      if (selected) {
        this.saved.emit(selected);
        return;
      } else {
        const currentFormVendorName = typeof vendorFormData.vendorName === 'object' ? vendorFormData.vendorName.name1 : vendorFormData.vendorName;
        if (vendorsMatchingCriteria.some((v) => v.name1 === currentFormVendorName)) {
          this.vendorForm.get('vendorName').setErrors({namenotunique: true});
          this.vendorForm.get('vendorName').markAsTouched();
        }
      }

    }
    if (this.vendorForm.status === 'VALID') {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        data: {
          title: "Confirm Add Vendor",
          description: "Are you sure you wish to create the Vendor?"
        }
      });

      dialogRef.afterClosed().subscribe(async result => {
        if (result) {

          const vendor = await this.createVendor(this.vendorForm.getRawValue());
          this.vendorService.saveVendor(vendor).then((v) => {
            this.saved.emit(v)
          });
        }
      });
    }
  }

  private async findVendorForVatNrAndSapCompanyCode(vatRegNr: string, sapCompanyCode: string) {
    try {
      const vendorsWithVatRegNr = await this.vendorService.findByVatRegistrationNumberAndSapCompanyCode(vatRegNr, sapCompanyCode);
      if (vendorsWithVatRegNr.length > 0) {
        // we expect to only have one result here.
        // ignore multiple results as these would only exist in case of wrong data.
        return vendorsWithVatRegNr[0];
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  async createVendor(rawValue: any) {
    return Object.assign(new Vendor(), {
      name1: this.extractVendorName(rawValue),
      accountNumber: rawValue.accountNumber,
      vatRegistrationNumber: rawValue.vatRegistrationNumber,
      contactName: rawValue.contactName,
      street: rawValue.street,
      houseNumber: rawValue.houseNumber,
      city: rawValue.city,
      region: rawValue.region,
      postcode: rawValue.postcode,
      group: rawValue.group,
      company: this.linkedCompany,
      country: this.country,
      phoneNumber: rawValue.phoneNumber,
      email: rawValue.email
    })
  }

  private extractVendorName(rawValue: any) {
    return typeof rawValue.vendorName === 'object' ? rawValue.vendorName.name1 : rawValue.vendorName;
  }

  displayVendor(vendor?: Vendor) {
    if (vendor && vendor.name1) {
      return vendor.name1;
    }else{
      return this.vendorForm.get('vendorName').value;
    }
  }

  onCancel() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        title: "Confirm Cancel Add Vendor",
        description: "Are you sure you wish to cancel the creation of Vendor?"
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.cancel.emit();
      }
    });
  }

  onVendorSelect($event: MatAutocompleteSelectedEvent) {
    const selectedvendor: Vendor = $event.option.value;
    if (this.shouldSelect(selectedvendor)) {
      this.saved.emit(selectedvendor);
      return;
    }
    // group and accountNumber should not be copied
    this.vendorForm.patchValue(Object.assign({}, selectedvendor, {group: '', accountNumber: ''}))
  }

  private shouldSelect(selectedvendor: Vendor): boolean {
    if (selectedvendor.company.id === this.company.id) {
      //ST-80: automatically select when company matches linked company on vendor
      return true;
    }
  }

  private async askForSelection(vendor) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        title: "VAT Registration Number already exists",
        description: `The VAT details of this vendor match an existing vendor, do you want to use this Vendor?
                <ul>
                    <li>${vendor.name1}</li>                    
                    <li>${vendor.vatRegistrationNumber}</li>
                    ${vendor.street && '<li>' + vendor.street + '</li>'}
                    ${vendor.city && '<li>' + vendor.city + '</li>'}                  
                </ul>`
      }
    });

    return dialogRef.afterClosed().toPromise()
  }

  private async askForMultiSelection(vendors) {
    const dialogRef = this.dialog.open(SelectVendorDialog, {
      data: {
        vendors
      },
      autoFocus: false,
    });

    return dialogRef.afterClosed().toPromise()
  }

  private findSimilarVendors() {
    const vendorData = this.vendorForm.getRawValue();

    return this.vendorService.findSimilarVendors(this.extractVendorName(vendorData), vendorData.street, vendorData.postcode, vendorData.city, this.linkedCompany.id);
  }
}


@Component({
  selector: 'bst-select-vendor-dialog',
  template: `<h3 mat-dialog-title>
    Matching Vendors found
  </h3>

  <mat-dialog-content>
    <mat-selection-list #vendors (selectionChange)="handleSelection($event)">
      <mat-list-option *ngFor="let vendor of data.vendors" [value]="vendor">
        {{vendor.name1}} ({{vendor.street}}, {{vendor.postcode}})
      </mat-list-option>
      <mat-list-option [value]="null">
        none of the above
      </mat-list-option>
    </mat-selection-list>
  </mat-dialog-content>

  <mat-dialog-actions fxLayout="row" fxLayoutAlign="space-between center">
    <button mat-button [mat-dialog-close]="false">Cancel</button>
    <button mat-button color="accent" [mat-dialog-close]="selectedVendor" [disabled]="!selectionMade">Ok</button>
  </mat-dialog-actions>
  `,
})
export class SelectVendorDialog {
  selectedVendor;
  selectionMade = false;

  constructor(public dialogRef: MatDialogRef<SelectVendorDialog>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  handleSelection(event) {
    if (event.option.selected) {
      event.source.deselectAll();
      event.option._setSelected(true);
      this.selectionMade = true;
      this.selectedVendor = event.option.value;
    }
  }
}
