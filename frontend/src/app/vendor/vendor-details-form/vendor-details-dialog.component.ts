import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {Country} from "../../country/country.model";
import {Company} from "../../company/company.model";
import {Vendor} from "../vendor.model";

export interface VendorDetailsDialogData {
  vendor: string,
  country: Country,
  company: Company
}

@Component({
  selector: 'bst-vendor-details-dialog',
  template: `
    <mat-dialog-content>
      <bst-vendor-details-form
        (cancel)="onCancel()"
        (saved)="onSaved($event)"
        [vendor]="data.vendor"
        [company]="data.company"
        [country]="data.country"
      ></bst-vendor-details-form>
    </mat-dialog-content>`
})
export class VendorDetailsDialogComponent {

  constructor(public dialogRef: MatDialogRef<VendorDetailsDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: VendorDetailsDialogData) {
  }

  onCancel() {
    this.dialogRef.close();
  }

  onSaved(vendor) {
    this.dialogRef.close(vendor);
  }
}


