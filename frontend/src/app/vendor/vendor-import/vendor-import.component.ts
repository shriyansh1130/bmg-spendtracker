import {Component} from '@angular/core';

@Component({
  selector: 'bst-vendor-import',
  template: `<bst-import-records [displayName]="'Vendors'" [importerName]="'vendor'"></bst-import-records>`
})
export class VendorImportComponent {
  constructor() {

  }
}
