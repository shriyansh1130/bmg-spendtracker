import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {VendorDetailsFormComponent} from "./vendor-details-form/vendor-details-form.component";
import {OktaAuthGuard} from "@okta/okta-angular";
import {VendorImportComponent} from "./vendor-import/vendor-import.component";

const routes: Routes = [
  {path: 'vendors/create', component: VendorDetailsFormComponent , canActivate: [OktaAuthGuard]},
  {path: 'vendors/import', component: VendorImportComponent , canActivate: [OktaAuthGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorRoutingModule { }
