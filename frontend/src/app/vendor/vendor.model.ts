import {Resource} from "../resource/resource";
import {Company} from "../company/company.model";
import {Country} from "../country/country.model";

export class Vendor extends Resource {
  public static PATH: string = "api/vendors";

  constructor(
    public accountNumber?: number,
    public name1?: string,
    public name2?: string,
    public name3?: string,
    public contactName?: string,
    public street?: string,
    public houseNumber?: string,
    public city?: string,
    public region?: string,
    public postcode?: string,
    public vatRegistrationNumber?: string,
    public reconciliationAccountNumber?:number,
    public company?:Company,
    public country?:Country,
    public group?: string,
    public phone?: string,
    public email?: string
  ) {
    super();
  }

}
