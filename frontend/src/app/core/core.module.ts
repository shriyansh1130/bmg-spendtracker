import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SpendTrackerStoreService} from './services/spend-tracker-store.service';
import {AppConfigService} from './services/app-config.service';
import {VersionFooterComponent} from "./version-footer/version-footer.component";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    VersionFooterComponent
  ],
  exports: [
    VersionFooterComponent
  ],
  providers: [SpendTrackerStoreService, AppConfigService]
})
export class CoreModule { }
