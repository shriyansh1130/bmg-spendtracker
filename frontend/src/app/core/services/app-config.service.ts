import {Injectable} from '@angular/core';
import {MatSnackBarConfig} from '@angular/material';

/**
 * Service to globalize various settings (default values)
 */
@Injectable()
export class AppConfigService {

  private _snackbarConfig;
  public get snackbarConfig(): MatSnackBarConfig {
    if(!this._snackbarConfig) {
      this._snackbarConfig = new MatSnackBarConfig();
      this._snackbarConfig.duration = 2000;
    }
    return this._snackbarConfig;
  }

  public readonly defaultPageSize = 100;
  public readonly choosablePageSizes = [100, 250, 500];

  public readonly filterInputDebounceInMillis = 300;

  constructor() { }

}
