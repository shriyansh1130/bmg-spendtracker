import {Injectable} from '@angular/core';
import {Project} from "../../project/project.model";
import {Country} from "../../country/country.model";
import {BehaviorSubject, Observable, combineLatest} from "rxjs";
import {CountryService} from "../../country/services/country.service";
import {User} from '../../auth/user.model';
import {Currency} from "../../currency/currency.model";
import {OktaAuthService} from "@okta/okta-angular";



export interface SpendTrackerStore {
  selectedProject: Project,
  selectedCountry: Country,
  user: User
}

export interface PurchaseOrderCreateSelection {
  country: Country,
  currency: Currency,
  project: Project
}

@Injectable()
export class SpendTrackerStoreService {

  constructor(public countryService: CountryService, private oktaAuth: OktaAuthService) {

  }

  private _selectedCountry: BehaviorSubject<Country> = new BehaviorSubject(null);
  private _selectedProject: BehaviorSubject<Project> = new BehaviorSubject(null);
  private _selectedCurrency: BehaviorSubject<Project> = new BehaviorSubject(null);

  private _user: BehaviorSubject<User> = new BehaviorSubject(null);


  public readonly selectedCountry: Observable<Country> = this._selectedCountry.asObservable();

  public readonly selectedProject: Observable<Project> = this._selectedProject.asObservable();

  public readonly selectedCurrency: Observable<Currency> = this._selectedCurrency.asObservable();

  public readonly user: Observable<User> = this._user.asObservable();

  public readonly purchaseOrderCreationSelection: Observable<PurchaseOrderCreateSelection> = combineLatest(
    this._selectedCountry,
    this._selectedCurrency,
    this._selectedProject,
    (country, currency, project) => {
      return {
        country,
        currency,
        project
      }
    }
  );

  selectCountry(country: Country) {
   this._selectedCountry.next(country);
  }
  selectProject(project: Project) {
    this._selectedProject.next(project);
  }

  selectCurrency(currency: Currency){
    this._selectedCurrency.next(currency);
  }

  setUser(user: any) {
    this._user.next(user);
  }

  userNonEmpty():boolean {
    return !this._user.getValue();
  }



  resetSelections() {
    //this._selectedCountry.next(null);
    this._selectedProject.next(null);
    this._selectedCurrency.next(null);

  }
}
