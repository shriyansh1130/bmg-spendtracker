import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {OktaAuthService} from "@okta/okta-angular/dist/okta/services/okta.service";

@Component({
  selector: 'bst-version-footer',
  templateUrl: './version-footer.component.html',
  styleUrls: ['./version-footer.component.scss']
})
export class VersionFooterComponent implements OnInit {
  public appVersion: string;

  constructor(private http: HttpClient, private authService: OktaAuthService) { }

  ngOnInit() {
    this.authService.isAuthenticated().then(value => {
      if (value) {
          this.fetchVersion();
      } else {
        this.authService.$authenticationState.subscribe(value => {
          if (value) {
            this.fetchVersion();
          }
        });
      }
    })
  }

  fetchVersion() {
    this.http.get('/actuator/info').toPromise()
      .then((info: {version: string}) => this.appVersion = info.version)
  }
}
