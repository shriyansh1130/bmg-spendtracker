import {Resource} from "../resource/resource";
import {Country} from "../country/country.model";
import {Company} from "../company/company.model";

export class Project extends Resource {

  public static PATH: string = "api/projects";

  constructor(
    public projectName?: string,
    public sapClientCode?:string,
    public ionSap?: string,
    public label?: string,
    public assignment?: string,
    public dealType?: string,
    public segment?: string,
    public repertoireOwnerCompany?: Company
   ){
    super();
  }




}
