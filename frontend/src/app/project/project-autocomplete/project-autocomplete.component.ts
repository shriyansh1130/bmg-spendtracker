import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Observable} from "rxjs/Rx";
import {Project} from "../project.model";
import {debounce, filter, map, startWith, switchMap} from "rxjs/operators";
import {timer} from "rxjs/index";
import {FormControl} from "@angular/forms";
import {ProjectService} from "../services/project.service";
import {AppConfigService} from "../../core/services/app-config.service";
import {MatAutocompleteSelectedEvent} from "@angular/material";

@Component({
  selector: 'bst-project-autocomplete',
  templateUrl: './project-autocomplete.component.html',
  styleUrls: ['./project-autocomplete.component.scss']
})
export class ProjectAutocompleteComponent implements OnInit {
  @Input() project: Project;
  @Output() projectSelected: EventEmitter<Project> = new EventEmitter();

  filteredOptions: Observable<Project[]>;

  projectControl = new FormControl();

  constructor(private appConfig: AppConfigService,
              private projectService: ProjectService,) {
  }

  ngOnInit() {
    this.registerProjectFilter();
  }

  displayFn(project?: Project): string | undefined {
    return project ? project.projectName : undefined;
  }

  onOptionSelected($event: MatAutocompleteSelectedEvent) {
    this.projectSelected.emit(<Project>$event.option.value);
  }

  private registerProjectFilter() {
    this.filteredOptions = this.projectControl.valueChanges.pipe(
      filter(value => typeof value === 'string'),
      debounce(() => timer(this.appConfig.filterInputDebounceInMillis)),
      startWith(this.project ? this.project.projectName : ''),
      switchMap((value) => this.projectService.findByProjectName(value)),
      map(value => value.elements)
    )
  }
}
