import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ProjectRoutingModule} from './project-routing.module';
import {ProjectListComponent} from './project-list/project-list.component';
import {ProjectService} from './services/project.service';
import {BstMaterialModule} from "../bst-material/bst-material.module";
import {SharedModule} from "../shared/shared.module";
import {SpendTrackerStoreService} from "../core/services/spend-tracker-store.service";
import {FlexLayoutModule} from "@angular/flex-layout";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ProjectListStoreService} from "./services/project-list-store.service";
import {AuthModule} from "../auth/auth.module";
import {ProjectImportComponent} from "./project-import/project-import.component";
import {ProjectOverviewComponent} from './project-overview/project-overview.component';
import {ProjectOverviewPositionDialogComponent} from './project-overview-position-dialog/project-overview-position-dialog.component';
import {ProjectAutocompleteComponent} from './project-autocomplete/project-autocomplete.component';
import {CountryModule} from "../country/country.module";

@NgModule({
  imports: [
    CommonModule,
    BstMaterialModule,
    ProjectRoutingModule,
    SharedModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    AuthModule,
    CountryModule
  ],
  declarations: [ProjectListComponent, ProjectImportComponent, ProjectOverviewComponent, ProjectOverviewPositionDialogComponent, ProjectAutocompleteComponent],
  providers: [
    ProjectService,
    SpendTrackerStoreService,
    ProjectListStoreService]
})
export class ProjectModule { }
