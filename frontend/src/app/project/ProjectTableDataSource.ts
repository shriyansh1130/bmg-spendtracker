import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {BehaviorSubject, Observable, of} from "rxjs";
import {Project} from "./project.model";
import {ProjectService} from "./services/project.service";
import {catchError, finalize, map} from "rxjs/operators";
import {PageableCollection} from "../resource/pageable-collection";
import {AppConfigService} from '../core/services/app-config.service';

export class ProjectTableDataSource implements DataSource<Project> {

  private _projectPageSubject = new BehaviorSubject<PageableCollection<Project>>(new PageableCollection());
  private _loadingSubject = new BehaviorSubject<boolean>(false);

  public readonly totalProjectCount$ = this._projectPageSubject.asObservable().pipe(
    map((p) =>  p.page && p.page.totalElements || 0)
  );
  public readonly loading$ = this._loadingSubject.asObservable();

  constructor(private appConfig:AppConfigService, private projectService: ProjectService) {

  }

  connect(collectionViewer: CollectionViewer): Observable<Project[]> {
    return this._projectPageSubject.asObservable().pipe(
      map((page) => page.elements )
    )
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this._projectPageSubject.complete();
    this._loadingSubject.complete();
  }

  public loadProjects(
    filterProjectName = '%',
    filterSapClientCode = '%',
    sortField='projectName',
    sortDirection = 'asc',
    page = 0, pageSize: number = this.appConfig.defaultPageSize) {
    this._loadingSubject.next(true);
    this.projectService.findByProjectNameAndSapClientCode(filterProjectName, filterSapClientCode, page, pageSize, sortField, sortDirection).pipe(
      catchError(() => of([])),
      finalize(() => this._loadingSubject.next(false))
    )
    .subscribe(projects => {
      this._projectPageSubject.next(<PageableCollection<Project>>projects)
    });
  }
}
