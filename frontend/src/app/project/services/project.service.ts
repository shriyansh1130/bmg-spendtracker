import {Injectable} from '@angular/core';
import {BehaviorSubject, from as fromPromise, Observable} from "rxjs";
import {Project} from "../project.model";
import {ResourceService} from "../../resource/resource.service";
import {PageableCollection} from "../../resource/pageable-collection";
import {map} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
import {AppConfigService} from "@app/core/services/app-config.service";

@Injectable()
export class ProjectService {

  private _projects: BehaviorSubject<PageableCollection<Project>> = new BehaviorSubject(new PageableCollection());

  public readonly projects: Observable<Project[]> = this._projects.asObservable().pipe(
    map(value => value.elements)
  );

  constructor(private resourceService: ResourceService, private http: HttpClient, private appConfig: AppConfigService) {
    this.loadInitialProjects();
  }

  private loadInitialProjects() {
    this.resourceService.findMany(Project).then((result) => this._projects.next(result))
  }

  public findByProjectNameAndSapClientCode(name: string, sapClientCode: string, page = 0, size = this.appConfig.defaultPageSize, fieldName = "projectName", sort = 'asc'): Observable<PageableCollection<Project>> {
    return fromPromise(this.resourceService.findManyByUrl(
      '/api/projects/search/findByProjectNameIgnoreCaseContainingAndSapClientCodeIgnoreCaseContaining',
      Project,
      {projectName: name, sapClientCode: sapClientCode, page: page, size: size, sort: `${fieldName},${sort}`}));
  }

  public findValidProjects(countryId, projectId, name?): Observable<PageableCollection<Project>> {
    return fromPromise(this.resourceService.findManyByUrl(
      '/api/projects/search/validProjects',
      Project,
      {countryId: countryId, projectId: projectId, name: name || "%"}));
  }

  public fetchProjectBudget(projectId, countryId) {
    return this.http.get<any>('/api/projects/budgets', {
      params: {
        projectId,
        countryId
      }
    })
  }

  public findByProjectName(name: string): Observable<PageableCollection<Project>> {
    return fromPromise(this.resourceService.findManyByUrl(
      '/api/projects/search/findByProjectNameIgnoreCaseContaining',
      Project,
      {projectName: name}));
  }

}
