import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, combineLatest} from "rxjs";

const FILTER_PROJECT_NAME_SESSION_KEY = "ProjectListStoreService_filterProjectName";
const FILTER_SAP_CLIENT_CODE_SESSION_KEY = "ProjectListStoreService_filterSapClientCode";


@Injectable()
export class ProjectListStoreService {

  constructor() {
    sessionStorage.getItem(FILTER_PROJECT_NAME_SESSION_KEY) && this.filterProject(sessionStorage.getItem(FILTER_PROJECT_NAME_SESSION_KEY));
    sessionStorage.getItem(FILTER_SAP_CLIENT_CODE_SESSION_KEY) && this.filterSapCode(sessionStorage.getItem(FILTER_SAP_CLIENT_CODE_SESSION_KEY));
  }
  private _projectNameFilter: BehaviorSubject<string> = new BehaviorSubject<string>("");
  private _sapCodeFilter: BehaviorSubject<string> = new BehaviorSubject<string>("");

  public readonly projectFilter : Observable<string> = this._projectNameFilter.asObservable();
  public readonly sapCodeFilter : Observable<string> = this._sapCodeFilter.asObservable();

  filterProject(projectName) {
    this._projectNameFilter.next(projectName);
    sessionStorage.setItem(FILTER_PROJECT_NAME_SESSION_KEY, projectName);
  }

  filterSapCode(sapCode) {
    this._sapCodeFilter.next(sapCode);
    sessionStorage.setItem(FILTER_SAP_CLIENT_CODE_SESSION_KEY, sapCode);
  }
}
