import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProjectListComponent} from "./project-list/project-list.component";
import {OktaAuthGuard} from "@okta/okta-angular";
import {RoleGuardService} from "../auth/role-guard.service";
import {ProjectImportComponent} from "./project-import/project-import.component";
import {ProjectOverviewComponent} from "@app/project/project-overview/project-overview.component";

const routes: Routes = [
  {
    path: 'projects',
    component: ProjectListComponent,
    canActivate: [OktaAuthGuard, RoleGuardService],
    data: {
      expectedRoles: ['bmg-spendtracker-admin', 'bmg-spendtracker-user', 'bmg-spendtracker-readonly']
    }
  },

  {
    path: 'projects/import',
    component: ProjectImportComponent,
    canActivate: [OktaAuthGuard, RoleGuardService],
    data: {
      expectedRoles: ['bmg-spendtracker-admin']
    }
  },
  {
    path: 'projects/:id/overview',
    component: ProjectOverviewComponent,
    canActivate: [OktaAuthGuard, RoleGuardService],
    data: {
      expectedRoles: ['bmg-spendtracker-user', 'bmg-spendtracker-readonly'],
      excludedRoles: ['bmg-spendtracker-user-restricted']
    }
  },
  {
    path: 'project-overview',
    component: ProjectOverviewComponent,
    canActivate: [OktaAuthGuard, RoleGuardService],
    data: {
      expectedRoles: ['bmg-spendtracker-admin', 'bmg-spendtracker-user', 'bmg-spendtracker-readonly'],
      excludedRoles: ['bmg-spendtracker-user-restricted']
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule {
}
