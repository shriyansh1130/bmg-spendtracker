import {Component} from '@angular/core';

@Component({
  selector: 'bst-project-import',
  template: `<bst-import-records [displayName]="'Projects'" [importerName]="'project'"></bst-import-records>`
})
export class ProjectImportComponent {
  constructor() {

  }
}
