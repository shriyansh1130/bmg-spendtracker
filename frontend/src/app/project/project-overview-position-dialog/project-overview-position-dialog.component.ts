import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig} from "@angular/material";

@Component({
  selector: 'bst-project-overview-position-dialog',
  templateUrl: './project-overview-position-dialog.component.html',
  styleUrls: ['./project-overview-position-dialog.component.scss']
})
export class ProjectOverviewPositionDialogComponent implements OnInit {


  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {


  }

  ngOnInit() {
  }



}
