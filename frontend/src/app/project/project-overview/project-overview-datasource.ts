import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {BehaviorSubject, Observable, of} from "rxjs";
import {catchError, finalize} from "rxjs/operators";
import {AppConfigService} from "@app/core/services/app-config.service";
import {ProjectService} from "../services/project.service";

function expandAll(budgets: ProjectBudget[]) {
  budgets.forEach(value => {
      value.visible = true;
      if (Array.isArray(value.children)) {
        value.isParent = true;
        value.collapsed = false;
      }
    }
  );
}

export class ProjectOverviewDatasource implements DataSource<ProjectBudget> {

  private _budgets = new BehaviorSubject<ProjectBudget[]>([]);
  private _total = new BehaviorSubject<ProjectBudgetTotal>({});
  private _loadingSubject = new BehaviorSubject<boolean>(false);

  public readonly loading$ = this._loadingSubject.asObservable();
  public readonly budgets$ = this._budgets.asObservable();
  public readonly total$ = this._total.asObservable();

  constructor(private appConfig: AppConfigService, private projectService: ProjectService) {

  }

  connect(collectionViewer: CollectionViewer): Observable<ProjectBudget[]> {
    return this._budgets.asObservable();
  }

  flattenBudgetArray(array) {
    let budgets: ProjectBudget[] = [];
    array.forEach((projectBudget) => this.flattenProjectBudget(projectBudget, budgets, 1));
    return budgets;
  }

  flattenProjectBudget(projectBudget: ProjectBudget, dataSets, level) {
    projectBudget.level = level;
    dataSets.push(projectBudget);
    if (projectBudget.children && projectBudget.children.length && projectBudget.children.length > 0) {
      projectBudget.children.forEach(projectBudget => this.flattenProjectBudget(projectBudget, dataSets, level + 1))
    }
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this._budgets.complete();
    this._loadingSubject.complete();
  }

  public loadBudgets(projectId, countryId) {
    this._loadingSubject.next(true);
    this.projectService.fetchProjectBudget(projectId, countryId).pipe(
      catchError(() => of({categoryBudgets: []})),
      finalize(() => this._loadingSubject.next(false))
    )
      .subscribe((budget: ProjectBudgetTotal) => {
        const flattenedArray = [];
        flattenedArray.push({
          title: 'Total',
          paid: budget.paid,
          openPo: budget.openPo,
          committedSpend: budget.committedSpend,
          level: 'header'
        });
        flattenedArray.push(...this.flattenBudgetArray(budget.categoryBudgets));
        expandAll(flattenedArray);
        this._budgets.next(flattenedArray);
        this._total.next(budget);
      });
  }

  public toggleExpand(budget: ProjectBudget, collapse: boolean) {
    if (budget.isParent) {
      budget.collapsed = collapse;
      budget.children.forEach((child) => {
        child.visible = !collapse;
        this.toggleExpand(child, collapse);
      });

    }
  }
}

interface ProjectBudget {
  title: string,
  paid?: number,
  openPo?: number,
  committedSpend?: number,
  level?: number,
  visible?: boolean,
  collapsed?: boolean,
  isParent?: boolean,
  children: ProjectBudget[],
}

interface ProjectBudgetTotal {
  paid?: number,
  openPo?: number,
  committedSpend?: number
  categoryBudgets?: ProjectBudget[]
}
