import {Component, OnInit} from '@angular/core';
import {ProjectOverviewDatasource} from "@app/project/project-overview/project-overview-datasource";
import {AppConfigService} from "@app/core/services/app-config.service";
import {BudgetCategoryService} from "@app/budget-category/services/budget-category.service";
import {ProjectService} from "../services/project.service";
import {Country} from "../../country/country.model";
import {ActivatedRoute} from "@angular/router";
import {CountryService} from "../../country/services/country.service";

@Component({
  selector: 'bst-project-overview',
  templateUrl: './project-overview.component.html',
  styleUrls: ['./project-overview.component.scss']
})
export class ProjectOverviewComponent implements OnInit {
  selectedProjectId;
  selectedCountry;
  dataExists = false;

  columnsToDisplay = ['title', 'paid', 'openPO', 'committedSpend'];
  dataSource: ProjectOverviewDatasource;

  constructor(private appConfigService: AppConfigService,
              private projectService: ProjectService,
              private route: ActivatedRoute,
              private countryService: CountryService) {
  }

  ngOnInit() {
    this.dataSource = new ProjectOverviewDatasource(this.appConfigService, this.projectService);
    this.route.params.subscribe(value => {
      if (value.id) {
        this.selectedProjectId = value.id;
        this.findByCountryName("").toPromise().then(result => {
          if (result.elements.length > 0) {
            this.dataExists = true;
          }
        })
      }
    });
  }

  toggle(row) {
    if (row.isParent) {
      this.dataSource.toggleExpand(row, !row.collapsed);
    }
  }

  selectCountry(country: Country) {
    this.selectedCountry = country;
    this.loadBudget();
  }

  private loadBudget() {
    if (this.selectedProjectId && this.selectedCountry) {
      this.dataSource.loadBudgets(this.selectedProjectId, this.selectedCountry.id);
    }
  }

  findByCountryName(countryName: string) {
    return this.countryService.findByCountryNameForProjectWithData(countryName, this.selectedProjectId);
  }
}
