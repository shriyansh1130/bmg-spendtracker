import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import {MatCheckboxChange, MatDialog, MatSort, PageEvent} from "@angular/material";
import {Project} from "../project.model";
import {ProjectService} from "../services/project.service";
import {CountryService} from "../../country/services/country.service";
import {SpendTrackerStoreService} from "../../core/services/spend-tracker-store.service";
import {ProjectTableDataSource} from "../ProjectTableDataSource";
import {debounceTime, distinctUntilChanged, tap} from "rxjs/operators";
import {merge, fromEvent, combineLatest} from "rxjs";
import {AppConfigService} from '../../core/services/app-config.service';
import {CustomPaginatorComponent} from "../../shared/custom-paginator/custom-paginator.component";
import {takeWhile} from 'rxjs/operators';
import {Country} from "@app/country/country.model";
import {take} from "rxjs/internal/operators";
import {ProjectListStoreService} from "../services/project-list-store.service";
const COUNTRY_SESSION_KEY = "selectedCountryId";

@Component({
  selector: 'bst-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements AfterViewInit, OnInit, OnDestroy {

  dataSource: ProjectTableDataSource;
  columnsToDisplay = ['select', 'projectName', 'repertoireOwner', 'sapClientCode', 'sapIon'];
  initialSelection = [];
  allowMultiSelect = false;
  readyToCreate: boolean;
  alive: boolean = true;
  pageChanged = new EventEmitter<number>();
  selectedProject: Project;
  selectedCountry: Country;


  @ViewChildren(CustomPaginatorComponent) paginators: QueryList<CustomPaginatorComponent>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filterProjectName') filterProjectName: ElementRef;
  @ViewChild('filterSapClientCode') filterSapClientCode: ElementRef;


  constructor(public appConfig: AppConfigService,
              public storeService: SpendTrackerStoreService,
              public projectService: ProjectService,
              public countryService: CountryService,
              public dialog: MatDialog,
              public projectListStoreService: ProjectListStoreService
  ) {

  }

  ngAfterViewInit() {

    merge(fromEvent(this.filterProjectName.nativeElement, 'keyup'), fromEvent(this.filterSapClientCode.nativeElement, 'keyup'))
      .pipe(
        takeWhile(() => this.alive),
        debounceTime(this.appConfig.filterInputDebounceInMillis),
        distinctUntilChanged(),
        tap(() => {
          this.applyFilter();
          this.goToPageZero();
          this.loadProjectsPage();
        })
      )
      .subscribe();

    // reset the paginator after sorting
    this.sort.sortChange
      .pipe(
        takeWhile(() => this.alive))
      .subscribe(() => this.goToPageZero());

    merge(this.sort.sortChange, this.pageChanged)
      .pipe(
        takeWhile(() => this.alive),
        tap(() => this.loadProjectsPage())
      ).subscribe();
  }

  ngOnInit(): void {
    this.countryService.loadCountries();
    this.storeService.resetSelections();
    this.dataSource = new ProjectTableDataSource(this.appConfig, this.projectService);

    if(sessionStorage.getItem(COUNTRY_SESSION_KEY)){
      this.countryService.findByCountryCode(sessionStorage.getItem(COUNTRY_SESSION_KEY)).subscribe(country => this.storeService.selectCountry(country));
    }

    combineLatest(this.projectListStoreService.projectFilter, this.projectListStoreService.sapCodeFilter).pipe(
      take(1)
    ).subscribe(([projectName, sapCode]) => this.dataSource.loadProjects(projectName, sapCode));

    combineLatest(this.storeService.selectedProject, this.storeService.selectedCountry).pipe(
      takeWhile(() => this.alive)
    )
      .subscribe(([project, country]) => {
        this.selectedCountry = country || this.selectedCountry;
        this.selectedProject = project || this.selectedProject;
        this.readyToCreate = (country != null && project != null);
      })
  }

  compareCountries(c1: Country, c2: Country) {
    return c1 && c2 && c1.countryCode === c2.countryCode;
  }

  onCountrySelectChange($event) {
    const country = $event.value;
    this.storeService.selectCountry(country);
    sessionStorage.setItem(COUNTRY_SESSION_KEY, country.countryCode);
  }

  onProjectChange(project: Project) {
    this.storeService.selectProject(project);
  }

  loadProjectsPage() {
    this.dataSource.loadProjects(
      this.filterProjectName.nativeElement.value,
      this.filterSapClientCode.nativeElement.value,
      this.sort.active,
      this.sort.direction,
      this.paginators.first.pageIndex,
      this.paginators.first.pageSize);
  }

  ngOnDestroy() {
    this.alive = false;
  }

  changePage(event: PageEvent) {
    this.paginators.forEach((p) => {
      p.pageIndex = event.pageIndex, p.pageSize = event.pageSize
    });
    this.pageChanged.emit(event.pageIndex);
  }

  private goToPageZero() {
    this.paginators.forEach(p => p.pageIndex = 0);
  }

  onCheckBoxchange($event: MatCheckboxChange, project: Project) {
    if ($event.checked) {
      this.onProjectChange(project);
    } else {
      this.onProjectChange(null);
    }
  }

  private applyFilter() {
    this.projectListStoreService.filterSapCode(this.filterSapClientCode.nativeElement.value);
    this.projectListStoreService.filterProject(this.filterProjectName.nativeElement.value);
  }
}
