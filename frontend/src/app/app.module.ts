import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {APP_INITIALIZER, NgModule} from '@angular/core';

import {BstMaterialModule} from "./bst-material/bst-material.module";
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {PurchaseOrderModule} from "./purchase-order/purchase-order.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "./shared/shared.module";
import {ResourceModule} from "./resource/resource.module";
import {ProjectModule} from "./project/project.module";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {StartupService} from "./startup.service";
import {OKTA_CONFIG, OktaAuthGuard, OktaAuthModule, OktaAuthService} from "@okta/okta-angular/dist";
import {TokenInterceptor} from "./auth/token.interceptor";
import {LogoutComponent} from "./auth/logout/logout.component";
import {AccessDeniedComponent} from "./auth/access-denied/access-denied.component";
import {BudgetCategoryModule} from "./budget-category/budget-category.module";
import {ExchangeRateModule} from './exchange-rate/exchange-rate.module';
import {ExchangeRateService} from './exchange-rate/services/exchange-rate.service';
import {ExchangeRate} from './exchange-rate/exchange-rate.model';
import {PurchaseOrderPositionDialogComponent} from "./purchase-order/purchase-order-details/components/purchase-order-position-dialog/purchase-order-position-dialog.component";
import {CountryModule} from "./country/country.module";
import {Country} from "./country/country.model";
import {MANAGED_RESOURCES} from "./resource/resource.service";
import {BudgetCategory, BudgetSpendType, BudgetSubCategory} from "./budget-category/budget-category.model";
import {Project} from "./project/project.model";
import {CurrencyModule} from "./currency/currency.module";
import {Currency} from "./currency/currency.model";
import {Company} from "./company/company.model";
import {VendorModule} from "./vendor/vendor.module";
import {Vendor} from "./vendor/vendor.model";
import {
  PurchaseOrder,
  PurchaseOrderPosition,
  PurchaseOrderPositionAttachment
} from "./purchase-order/purchase-order.model";
import {User} from './auth/user.model';
import {CompanyModule} from "./company/company.module";
import {CoreModule} from "./core";
import {AuthModule} from "./auth/auth.module";
import {ImportTask} from "./shared/import-task/import-task.model";
import {RawPostingModule} from "@app/raw-posting/raw-posting.module";
import {RawPosting, RawPostingSyncTask} from "./raw-posting/raw-posting.model";
import {ProjectOverviewPositionDialogComponent} from "@app/project/project-overview-position-dialog/project-overview-position-dialog.component";

export function initConfig(startupService: StartupService): Function {
  return () => startupService.load();
}

@NgModule({
  declarations: [
    AppComponent,
    LogoutComponent,
    AccessDeniedComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ResourceModule,
    ReactiveFormsModule,
    CoreModule,
    ProjectModule,
    CountryModule,
    CompanyModule,
    CurrencyModule,
    VendorModule,
    PurchaseOrderModule,
    BudgetCategoryModule,
    ExchangeRateModule,
    RawPostingModule,
    AppRoutingModule,
    FormsModule,
    BstMaterialModule,
    SharedModule,
    AuthModule,
    OktaAuthModule
  ],
  entryComponents: [
    PurchaseOrderPositionDialogComponent,
    ProjectOverviewPositionDialogComponent
  ],
  providers: [
    StartupService,
    {
      provide: APP_INITIALIZER,
      useFactory: initConfig,
      deps: [StartupService],
      multi: true
    },
    {
      provide: OKTA_CONFIG,
      useFactory: (startupService: StartupService) => {
        return startupService.config;
      },
      deps: [StartupService]
    },
    OktaAuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    OktaAuthService,
    OktaAuthGuard,
    {
      provide: MANAGED_RESOURCES, useValue: [
        User,
        Country,
        Project,
        Vendor,
        Currency,
        Company,
        ExchangeRate,
        BudgetCategory,
        BudgetSubCategory,
        BudgetSpendType,
        PurchaseOrder,
        PurchaseOrderPosition,
        PurchaseOrderPositionAttachment,
        ImportTask,
        RawPosting,
        RawPostingSyncTask,
      ],
    },
    ExchangeRateService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
