import {Resource} from "../resource/resource";
import {Currency} from "../currency/currency.model";

export class Country extends Resource {

  public static PATH: string = "api/countries";

  constructor(
    public countryName?: string,
    public countryCode?: string,
    public currency?: Currency,
    public sapCountry?: boolean,
    public poCountry?: boolean,
    public affiliateBudget?: string
  ){
    super();
  }
}
