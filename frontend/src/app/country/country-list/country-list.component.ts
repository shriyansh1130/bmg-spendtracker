import {AfterViewInit, Component, EventEmitter, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {MatSort, PageEvent} from "@angular/material";
import {CountryService} from "../services/country.service";
import {AppConfigService} from "../../core/services/app-config.service";
import {CountryTableDataSource} from "../CountryTableDataSource";
import {tap} from "rxjs/operators";
import {merge} from "rxjs";
import {CustomPaginatorComponent} from "../../shared/custom-paginator/custom-paginator.component";

@Component({
  selector: 'bst-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.scss']
})
export class CountryListComponent implements AfterViewInit, OnInit {

  //columnDict= [['countryName', 'Country Name', true], ['countryCode', 'Country Code', true]];
  dataSource: CountryTableDataSource;
  pageChanged = new EventEmitter<number>();
  @ViewChildren(CustomPaginatorComponent) paginators:QueryList<CustomPaginatorComponent>;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public appConfig:AppConfigService, private countryService: CountryService) { }

  ngOnInit() {
    this.dataSource = new CountryTableDataSource(this.appConfig, this.countryService);
    this.dataSource.loadCountries();
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.goToPageZero());

    merge(this.sort.sortChange, this.pageChanged)
      .pipe(
        tap(() => this.loadCountriesPage())
      ).subscribe();
  }

  loadCountriesPage() {
    this.dataSource.loadCountries(this.sort.active ,this.sort.direction, this.paginators.first.pageIndex, this.paginators.first.pageSize);
  }

  changePage(event: PageEvent) {
    this.paginators.forEach((p) => {p.pageIndex = event.pageIndex, p.pageSize = event.pageSize});
    this.pageChanged.emit(event.pageIndex);
  }

  private goToPageZero() {
    this.paginators.forEach(p => p.pageIndex = 0);
  }
}
