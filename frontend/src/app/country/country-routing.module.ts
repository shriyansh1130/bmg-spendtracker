import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CountryListComponent} from "./country-list/country-list.component";
import {OktaAuthGuard} from "@okta/okta-angular";
import {CountryImportComponent} from "./country-import/country-import.component";


const routes: Routes = [
  {path: 'countries', component: CountryListComponent, canActivate: [OktaAuthGuard]},
  {path: 'countries/import', component: CountryImportComponent, canActivate: [OktaAuthGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CountryRoutingModule { }
