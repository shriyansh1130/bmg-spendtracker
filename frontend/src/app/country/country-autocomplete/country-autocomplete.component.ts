import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Project} from "../../project/project.model";
import {Country} from "../country.model";
import {FormControl} from "@angular/forms";
import {AppConfigService} from "../../core/services/app-config.service";
import {CountryService} from "../services/country.service";
import {debounce, filter, map, startWith, switchMap} from "rxjs/operators";
import {timer} from "rxjs/index";
import {MatAutocompleteSelectedEvent} from "@angular/material";
import {Observable} from "rxjs/Rx";
import {PageableCollection} from "../../resource/pageable-collection";

@Component({
  selector: 'bst-country-autocomplete',
  templateUrl: './country-autocomplete.component.html',
  styleUrls: ['./country-autocomplete.component.scss']
})
export class CountryAutocompleteComponent implements OnInit {
  @Input() country: Country;
  @Input() findByValue: (countryName: string) => Observable<PageableCollection<Country>>;

  @Output() countrySelected: EventEmitter<Project> = new EventEmitter();

  countryControl = new FormControl();
  filteredOptions: Observable<Country[]>;

  constructor(private appConfig: AppConfigService, private countryService: CountryService) {
  }

  ngOnInit() {
    this.registerCountryFilter();
  }

  displayFn(country?: Country): string | undefined {
    return country ? country.countryName : undefined;
  }

  onOptionSelected($event: MatAutocompleteSelectedEvent) {
    this.countrySelected.emit(<Country>$event.option.value);
  }

  private registerCountryFilter() {
    this.filteredOptions = this.countryControl.valueChanges.pipe(
      filter(value => typeof value === 'string'),
      debounce(() => timer(this.appConfig.filterInputDebounceInMillis)),
      startWith(this.country ? this.country.countryName : ''),
      switchMap((value) => this.findByValue(value)),
      map(value => value.elements)
    );
  }

}
