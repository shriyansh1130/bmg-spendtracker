import {PageableCollection} from "../resource/pageable-collection";
import {BehaviorSubject, Observable, of} from "rxjs";
import {AppConfigService} from "../core/services/app-config.service";
import {CollectionViewer} from "@angular/cdk/collections";
import {catchError, finalize, map} from "rxjs/operators";
import {CountryService} from "../country/services/country.service";
import {Country} from "../country/country.model";
import {IPagedTableDataSource} from "../resource/IPagedTableDataSource";

export class CountryTableDataSource implements IPagedTableDataSource<Country> {
  private _countryPageSubject = new BehaviorSubject<PageableCollection<Country>>(new PageableCollection());
  private _loadingSubject = new BehaviorSubject<boolean>(false);
  public readonly loading$ = this._loadingSubject.asObservable();

  public readonly totalCountryCount$ = this._countryPageSubject.asObservable().pipe(
    map((p) =>  p.page && p.page.totalElements || 0)
  );

  constructor(private appConfig:AppConfigService, private countryService: CountryService) {

  }

  connect(collectionViewer: CollectionViewer): Observable<Country[]> {
    return this._countryPageSubject.asObservable().pipe(
      map((page) => page.elements )
    );
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this._countryPageSubject.complete();
    this._loadingSubject.complete();
  }

  public loadCountries(sortField='countryName', sortDirection = 'desc', page = 0, pageSize:number = this.appConfig.defaultPageSize) {
    this._loadingSubject.next(true);

    this.countryService.loadCountriesWithPagination(page, pageSize, sortField, sortDirection).pipe(
      catchError(() => of([])),
      finalize(() => this._loadingSubject.next(false))
    )
      .subscribe(countries => {
        for(let country of (<PageableCollection<Country>>countries).elements){
          if (country.currency != null) {
            country.resolveOneAssociation('currency');
          }
        }

        this._countryPageSubject.next(<PageableCollection<Country>>countries)
      });
  }
}
