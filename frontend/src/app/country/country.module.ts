import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CountryRoutingModule} from './country-routing.module';
import {CountryService} from './services/country.service';
import {SharedModule} from "../shared";
import {CountryListComponent} from './country-list/country-list.component';
import {BstMaterialModule} from "../bst-material/bst-material.module";
import {CountryImportComponent} from './country-import/country-import.component';
import {AuthModule} from "../auth/auth.module";
import {CountryAutocompleteComponent} from './country-autocomplete/country-autocomplete.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    BstMaterialModule,
    CountryRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AuthModule
  ],
  declarations: [CountryListComponent, CountryImportComponent, CountryAutocompleteComponent],
  providers: [
    CountryService,
  ],
  exports: [CountryAutocompleteComponent]
})
export class CountryModule { }
