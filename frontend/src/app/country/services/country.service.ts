import {Injectable} from '@angular/core';
import {BehaviorSubject, from as fromPromise, Observable} from "rxjs";
import {ResourceService} from "../../resource/resource.service";
import {PageableCollection} from "../../resource/pageable-collection";
import {map} from "rxjs/operators";
import {Country} from "../country.model";
import {AppConfigService} from "@app/core/services/app-config.service";

@Injectable()
export class CountryService {

  private _countries: BehaviorSubject<PageableCollection<Country>> = new BehaviorSubject(new PageableCollection());
  private _sapCountries: BehaviorSubject<PageableCollection<Country>> = new BehaviorSubject(new PageableCollection());
  private _poCountries: BehaviorSubject<PageableCollection<Country>> = new BehaviorSubject(new PageableCollection());

  public readonly countries: Observable<Country[]> = this._countries.asObservable().pipe(
    map(value => value.elements || [])
  );

  public readonly sapCountries: Observable<Country[]> = this._sapCountries.asObservable().pipe(
    map(value => value.elements || [])
  );


  public readonly poCountries: Observable<Country[]> = this._poCountries.asObservable().pipe(
    map(value => value.elements || [])
  );


  constructor(private resourceService: ResourceService, private appConfig: AppConfigService) {
  }


  loadCountries() {
    this.resourceService.findMany(Country, {projection: 'countryProjection'}).then(value => {
      return this._countries.next(value);
    });

    this.resourceService.findManyByUrl('/api/countries/search/findBySapCountryIsTrue', Country, {
      projection: 'countryProjection',
      size: 100,
      sort: 'countryName,asc'
    }).then(value => {
      return this._sapCountries.next(value);
    });

    this.loadPoCountries();
  }

  loadPoCountries() {
    this.resourceService.findManyByUrl('/api/countries/search/findByPoCountryIsTrue', Country, {
      projection: 'countryProjection',
      size: 100,
      sort: 'countryName,asc'
    }).then(value => {
      return this._poCountries.next(value);
    });
  }

  save(country: Country): Observable<Country> {
    return fromPromise(this.resourceService.save(country).then(value => {
      this.loadCountries();
      return value;
    }));
  }

  public loadCountriesWithPagination(page = 0, size = this.appConfig.defaultPageSize, fieldName = "countryName", sort = 'desc'): Observable<PageableCollection<Country>> {
    return fromPromise(this.resourceService.findMany(Country,
      {projection: 'countryProjection', page: page, size: size, sort: `${fieldName},${sort}`}));
  }

  findById(id: string): Observable<Country> {
    return fromPromise(this.resourceService.findById(id, Country));
  }

  findByCountryCode(code: string): Observable<Country> {
    return fromPromise(this.resourceService.findByUri('/api/countries/search/findByCountryCode', {countryCode: code}));
  }

  getById(id: string): Promise<Country> {
    return this.resourceService.findById(id, Country);
  }


  findByCountryName(countryName: string): Observable<PageableCollection<Country>> {
    return fromPromise(this.resourceService.findManyByUrl('/api/countries/search/findByCountryNameIgnoreCaseContaining', Country, {
      countryName: countryName
    }));
  }

  findByCountryNameForProjectWithData(countryName: string, projectId: number): Observable<PageableCollection<Country>> {
    return fromPromise(this.resourceService.findManyByUrl('/api/countries/search/findForProjectWithData', Country, {
      countryName,
      projectId
    }));
  }
}
