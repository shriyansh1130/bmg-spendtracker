import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {OktaConfig} from "@okta/okta-angular/dist/okta/models/okta.config";


interface StartupConfig extends OktaConfig{
  env: string
}

@Injectable()
export class StartupService {
  private startupConfig: StartupConfig;
  constructor(private http: HttpClient ) { }

  load(): Promise<any> {
    this.startupConfig = null;

    //Pre Angular bootstraping
    return fetch("/config")
      .then(res => res.json())
      .then(data => this.startupConfig = data)
      .catch((err: any) => {
        console.error(err);
        return Promise.resolve();
      });
  }

  get config() {
    return this.startupConfig;
  }
}
