import * as decode from "jwt-decode";
import {OktaAuthService} from "@okta/okta-angular";
import {Injectable} from "@angular/core";

@Injectable()
export class RolesService {
  constructor(private oktaAuth: OktaAuthService) {
  }

  async hasRole(role: string): Promise<boolean> {
    // decode the token to get its payload
    const groups = await this.currentRoles();
    if (groups.indexOf(role) !== -1) {
      return true;
    }
    return false;
  }

  async hasOneRoleOf(roles: string[]): Promise<boolean> {
    const groups = await this.currentRoles();
    return groups.some((g) => roles.indexOf(g) != -1)
  }

  async currentRoles() {
    const token = await this.oktaAuth.getAccessToken();
    if (token) {
      const tokenPayload: { groups: string[] } = decode(token);
      return tokenPayload.groups;
    }
    return [];
  }
}
