import {Resource} from "../resource/resource";
import {ICurrency} from '../currency/currency.model';

export class User extends Resource {

  public static PATH: string = "api/users";

  constructor(
    public userId?: string,
    public fullname?:string,
    public email?: string,
    public defaultCurrency?: ICurrency,
  ){
    super();
  }
}


