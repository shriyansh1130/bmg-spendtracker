import {OktaAuthService} from "@okta/okta-angular";
import {ActivatedRouteSnapshot, CanActivate, Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {RolesService} from "./roles.service";

@Injectable()
export class RoleGuardService implements CanActivate {
  constructor( public router: Router, private oktaAuth: OktaAuthService, private roleService: RolesService) {}
  canActivate(route: ActivatedRouteSnapshot): Promise<boolean> {
    // this will be passed from the route config
    // on the data property
    const expectedRoles = route.data.expectedRoles || [];
    const excludedRoles = route.data.excludedRoles || [];

    return Promise.all([this.roleService.hasOneRoleOf(expectedRoles), this.roleService.hasOneRoleOf(excludedRoles)]).then(function ([hasExpected, hasExcluded]) {
      return hasExpected && !hasExcluded;
    });
  }
}
