import {Component, OnInit, ViewEncapsulation} from "@angular/core";
import {Router} from "@angular/router";
import {OktaAuthService} from "@okta/okta-angular";

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LogoutComponent implements OnInit {

  constructor(private router: Router, private oktaAuth: OktaAuthService) {
  }

  ngOnInit() {
    this.oktaAuth.logout()
      .then(() => this.router.navigate(['/']))
      .catch(() => this.router.navigate(['/']));
  }

}
