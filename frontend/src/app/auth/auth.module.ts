import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RoleGuardService} from "./role-guard.service";
import {RolesService} from "./roles.service";
import {hasRoleDirective} from "./hasRole.directive";
import {excludeRoleDirective} from "./excludeRole.directive";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    hasRoleDirective, excludeRoleDirective
  ],
  exports: [
    hasRoleDirective, excludeRoleDirective
  ],
  providers: [RoleGuardService, RolesService]
})
export class AuthModule { }
