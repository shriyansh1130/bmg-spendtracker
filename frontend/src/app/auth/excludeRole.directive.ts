import {Directive, Input, TemplateRef, ViewContainerRef} from "@angular/core";
import {RolesService} from "./roles.service";

@Directive({selector: '[excludeRole]'})
export class excludeRoleDirective {
  constructor(private templateRef: TemplateRef<any>,
              private viewContainer: ViewContainerRef,
              private roleService: RolesService) {
  }

  @Input() set excludeRole(inputRoles: string | string[]) {
    let roles = [];
    if (typeof inputRoles === 'string') {
      roles = inputRoles.split(',');
    }

    if (typeof inputRoles === 'object' && inputRoles.hasOwnProperty('length')) {
      roles = inputRoles;
    }
    this.roleService.hasOneRoleOf(roles).then(result => {
      if (!result) {
        // If condition is true add template to DOM
        this.viewContainer.createEmbeddedView(this.templateRef);
      } else {
        // Else remove template from DOM
        this.viewContainer.clear();
      }
    });
  }
}
