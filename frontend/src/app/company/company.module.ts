import {NgModule} from '@angular/core';

import {CompanyRoutingModule} from './company-routing.module';
import {CompanyListComponent} from './company-list/company-list.component';
import {SharedModule} from "../shared";
import {CompanyService} from "./services/company.service";
import {BstMaterialModule} from "../bst-material/bst-material.module";
import {CommonModule} from "@angular/common";
import {CompanyImportComponent} from './company-import/company-import.component';
import {AuthModule} from "../auth/auth.module";

@NgModule({
  imports: [
    CompanyRoutingModule,
    SharedModule,
    BstMaterialModule,
    CommonModule,
    AuthModule
  ],
  declarations: [CompanyListComponent, CompanyImportComponent],
  providers: [
    CompanyService,
  ]
})
export class CompanyModule { }
