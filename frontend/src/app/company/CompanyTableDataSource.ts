import {PageableCollection} from "../resource/pageable-collection";
import {BehaviorSubject, Observable, of} from "rxjs";
import {AppConfigService} from "../core/services/app-config.service";
import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {catchError, finalize, map} from "rxjs/operators";
import {Company} from "../company/company.model";
import {CompanyService} from "./services/company.service";
import {IPagedTableDataSource} from "../resource/IPagedTableDataSource";

export class CompanyTableDataSource implements DataSource<Company>, IPagedTableDataSource<Company> {

  private _companyPageSubject = new BehaviorSubject<PageableCollection<Company>>(new PageableCollection());
  private _loadingSubject = new BehaviorSubject<boolean>(false);

  public readonly totalCompanyCount$ = this._companyPageSubject.asObservable().pipe(
    map((p) =>  p.page && p.page.totalElements || 0)
  );
  public readonly loading$ = this._loadingSubject.asObservable();

  constructor(private appConfig:AppConfigService, private companyService: CompanyService) {

  }

  connect(collectionViewer: CollectionViewer): Observable<Company[]> {
    return this._companyPageSubject.asObservable().pipe(
      map((page) => page.elements )
    );
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this._companyPageSubject.complete();
    this._loadingSubject.complete();
  }

  public loadCompanies(sortField='companyName', sortDirection = 'desc', page = 0, pageSize:number = this.appConfig.defaultPageSize) {
    this._loadingSubject.next(true);
    this.companyService.loadCompaniesWithPagination(page, pageSize, sortField, sortDirection).pipe(
      catchError(() => of([])),
      finalize(() => this._loadingSubject.next(false))
    )
      .subscribe(companies => {
        this._companyPageSubject.next(<PageableCollection<Company>>companies)
      });
  }
}
