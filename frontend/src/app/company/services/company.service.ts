import {Injectable} from '@angular/core';
import {map} from "rxjs/operators";
import {PageableCollection} from "../../resource/pageable-collection";
import {BehaviorSubject, from as fromPromise, Observable} from "rxjs";
import {Company} from "../../company/company.model";
import {ResourceService} from "../../resource/resource.service";
import {Country} from "../../country/country.model";
import {AppConfigService} from "@app/core/services/app-config.service";

@Injectable()
export class CompanyService {

  private _companies: BehaviorSubject<PageableCollection<Company>> = new BehaviorSubject(new PageableCollection());

  public readonly companies: Observable<Company[]> = this._companies.asObservable().pipe(
    map(value => value.elements || [])
  );

  constructor(private resourceService: ResourceService, private appConfig: AppConfigService) {
    this.initialLoad();
  }

  private initialLoad() {
    this.loadCompanies();
  }

  loadCompanies() {
    return this.resourceService.findMany(Company).then(value => {
      return this._companies.next(value);
    });
  }

  save(company: Company): Observable<Company> {
    return fromPromise(this.resourceService.save(company).then(value => {
      this.loadCompanies();
      return value;
    }));
  }

  public loadCompaniesWithPagination(page=0, size=this.appConfig.defaultPageSize, fieldName="companyName", sort='desc' ): Observable<PageableCollection<Company>> {
    return fromPromise(this.resourceService.findMany(Company,
      {page: page, size: size, sort: `${fieldName},${sort}`}));
  }

  public getById(id): Promise<Company> {
    return this.resourceService.findById(id, Company)
  }

  findInternationalCompany(country: Country) {
    return this.resourceService.findByUri('/api/companies/search/findByCountry_IdAndInternationalIsTrue',
      {countryId: country.id}
    )
  }
}
