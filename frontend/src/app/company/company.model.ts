import {Resource} from "../resource/resource";
import {Country} from "../country/country.model";

export class Company extends Resource {

  public static PATH: string = "api/companies";

  constructor(
    public companyName?: string,
    public companyCode?:string,
    public country?: Country,
    public sapCompanyCode?: string,
    public international?: boolean,
  ){
    super();
  }
}
