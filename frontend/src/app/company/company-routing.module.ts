import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CompanyListComponent} from "./company-list/company-list.component";
import {OktaAuthGuard} from "@okta/okta-angular";
import {CompanyImportComponent} from "./company-import/company-import.component";

const routes: Routes = [
  {path: 'companies', component: CompanyListComponent, canActivate: [OktaAuthGuard]},
  {path: 'companies/import', component: CompanyImportComponent, canActivate: [OktaAuthGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyRoutingModule {
}
