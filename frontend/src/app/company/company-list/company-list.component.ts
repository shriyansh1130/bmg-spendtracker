import {AfterViewInit, Component, EventEmitter, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {tap} from "rxjs/operators";
import {merge} from "rxjs";
import {AppConfigService} from "../../core/services/app-config.service";
import {MatSort, PageEvent} from "@angular/material";
import {CompanyTableDataSource} from "../CompanyTableDataSource";
import {CompanyService} from "../services/company.service";
import {CustomPaginatorComponent} from "../../shared/custom-paginator/custom-paginator.component";

@Component({
  selector: 'bst-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.scss']
})
export class CompanyListComponent implements AfterViewInit, OnInit {


  //columnDict= [['countryName', 'Country Name', true], ['countryCode', 'Country Code', true]];
  @ViewChildren(CustomPaginatorComponent) paginators:QueryList<CustomPaginatorComponent>;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: CompanyTableDataSource;
  pageChanged = new EventEmitter<number>();

  constructor(public appConfig:AppConfigService, private companyService: CompanyService) { }

  ngOnInit() {
    this.dataSource = new CompanyTableDataSource(this.appConfig, this.companyService);
    this.dataSource.loadCompanies();
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.goToPageZero());

    merge(this.sort.sortChange, this.pageChanged)
      .pipe(
        tap(() => this.loadCompaniesPage())
      ).subscribe();
  }

  loadCompaniesPage() {
    this.dataSource.loadCompanies(this.sort.active ,this.sort.direction, this.paginators.first.pageIndex, this.paginators.first.pageSize);
  }

  changePage(event: PageEvent) {
    this.paginators.forEach((p) => {p.pageIndex = event.pageIndex, p.pageSize = event.pageSize});
    this.pageChanged.emit(event.pageIndex);
  }

  private goToPageZero() {
    this.paginators.forEach(p => p.pageIndex = 0);
  }
}
