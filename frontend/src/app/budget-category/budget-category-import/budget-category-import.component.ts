import {Component} from '@angular/core';

@Component({
  selector: 'bst-category-import',
  template: `<bst-import-records [displayName]="'Budget-Categories'" [importerName]="'budgetCategory'"></bst-import-records>`
})
export class BudgetCategoryImportComponent {
  constructor() {

  }
}
