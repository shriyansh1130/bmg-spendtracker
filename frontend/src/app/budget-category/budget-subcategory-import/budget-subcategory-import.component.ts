import {Component} from '@angular/core';

@Component({
  selector: 'bst-subcategory-import',
  template: `<bst-import-records [displayName]="'Budget-Sub-Categories'" [importerName]="'budgetSubCategory'"></bst-import-records>`
})
export class BudgetSubcategoryImportComponent {
  constructor() {

  }
}
