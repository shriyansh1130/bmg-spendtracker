import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BudgetSubcategoryListComponent} from './budget-subcategory-list.component';
import {BudgetCategoryService} from "../services/budget-category.service";

describe('BudgetSubcategoryListComponent', () => {
  let component: BudgetSubcategoryListComponent;
  let fixture: ComponentFixture<BudgetSubcategoryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BudgetSubcategoryListComponent ],
      providers: [
        {provide: BudgetCategoryService, useValue: {}},
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BudgetSubcategoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
