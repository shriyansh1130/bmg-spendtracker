import {Component, OnInit} from '@angular/core';
import {BudgetCategoryService} from "../services/budget-category.service";

@Component({
  selector: 'bst-budget-subcategory-list',
  templateUrl: './budget-subcategory-list.component.html',
  styleUrls: ['./budget-subcategory-list.component.scss']
})
export class BudgetSubcategoryListComponent implements OnInit {

  constructor(public budgetCategoryService: BudgetCategoryService) { }

  ngOnInit() {
  }

}
