import {inject, TestBed} from '@angular/core/testing';

import {BudgetSubCategoryService} from './budget-subcategory.service';
import {ResourceService} from "../../resource/resource.service";
import {BudgetCategoryService} from "./budget-category.service";
import {Observable} from "rxjs/Observable";

describe('BudgetSubcategoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BudgetSubCategoryService,
        {provide: ResourceService, useValue: {findMany: () => Promise.resolve([])}},
        {provide: BudgetCategoryService, useValue: {selectedCategory: Observable.of({})}},
      ]
    });
  });

  it('should be created', inject([BudgetSubCategoryService], (service: BudgetSubCategoryService) => {
    expect(service).toBeTruthy();
  }));
});
