import {Injectable} from '@angular/core';
import {Observable, from as fromPromise, BehaviorSubject} from "rxjs";
import {BudgetCategory, BudgetSubCategory} from "../budget-category.model";
import {ResourceService} from "../../resource/resource.service";
import {BudgetCategoryService} from "./budget-category.service";
import {map} from "rxjs/operators";

@Injectable()
export class BudgetSubCategoryService {
  private _subCategories: BehaviorSubject<BudgetSubCategory[]> = new BehaviorSubject([]);
  private _selectedSubCategory: BehaviorSubject<BudgetSubCategory> = new BehaviorSubject(new BudgetSubCategory());

  public readonly subCategories: Observable<BudgetSubCategory[]> = this._subCategories.asObservable().pipe(map(value => value || []));

  public readonly selectedSubCategory: Observable<BudgetSubCategory> = this._selectedSubCategory.asObservable();

  constructor(private resourceService: ResourceService, private budgetCategoryService: BudgetCategoryService) {
      budgetCategoryService.selectedCategory.subscribe(selectedCategory => this.loadForCategory(selectedCategory));
  }

  private loadForCategory(selectedCategory: BudgetCategory) {
    this._subCategories.next(selectedCategory.budgetSubCategories);
  }

  selectSubCategory(subCategory: BudgetSubCategory) {
    this._selectedSubCategory.next(subCategory);
  }

  save(subCategory: BudgetSubCategory) : Observable<BudgetSubCategory> {
      return fromPromise(this.resourceService.save(subCategory).then(value => {
        return this.budgetCategoryService.loadBudgetCategories()
          .then(() =>{return value}).then(() => {return value})
      }))
  }

  changeSelectedSubCategory(change: Partial<BudgetSubCategory>) {
    const current = this._selectedSubCategory.getValue();
    Object.keys(change).forEach(key => current[key] = change[key]);
    this._selectedSubCategory.next(current);
  }

  selectBudgetSubCategoryById(id: string) {
    this.subCategories.subscribe(value => {
      const sub = value.find(sc => sc.id === id);
      if (sub) {
        this._selectedSubCategory.next(sub);
      }
    });
  }

  newBudgetSubCategory() {
    this.budgetCategoryService.selectedCategory.subscribe(
      selectedCat => {
        const toCreate = new BudgetSubCategory();
        toCreate.budgetCategory = selectedCat;
        this._selectedSubCategory.next(toCreate);
      }
    );
  }


}
