import {inject, TestBed} from '@angular/core/testing';

import {BudgetCategoryService} from './budget-category.service';
import {ResourceService} from "../../resource/resource.service";

describe('BudgetCategoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BudgetCategoryService,
        {provide: ResourceService, useValue: {findMany: () => Promise.resolve([])}},
      ]
    });
  });

  it('should be created', inject([BudgetCategoryService], (service: BudgetCategoryService) => {
    expect(service).toBeTruthy();
  }));
});
