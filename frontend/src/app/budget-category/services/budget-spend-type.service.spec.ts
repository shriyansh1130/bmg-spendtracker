import {inject, TestBed} from '@angular/core/testing';

import {BudgetSpendTypeService} from './budget-spend-type.service';
import {ResourceService} from "../../resource/resource.service";
import {BudgetCategoryService} from "./budget-category.service";
import {BudgetSubCategoryService} from "./budget-subcategory.service";
import {Observable} from "rxjs/Observable";
import "rxjs/add/observable/of";

describe('BudgetSpendTypeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BudgetSpendTypeService,
        {provide: ResourceService, useValue: {findMany: () => Promise.resolve([])}},
        {provide: BudgetCategoryService, useValue: {}},
        {provide: BudgetSubCategoryService, useValue: {selectedSubCategory: Observable.of({})}},

      ]
    });
  });

  it('should be created', inject([BudgetSpendTypeService], (service: BudgetSpendTypeService) => {
    expect(service).toBeTruthy();
  }));
});
