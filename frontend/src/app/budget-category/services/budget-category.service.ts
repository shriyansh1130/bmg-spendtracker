import {Injectable} from '@angular/core';
import {BehaviorSubject, from as fromPromise, Observable} from "rxjs";
import {ResourceService} from "../../resource/resource.service";
import {PageableCollection} from "../../resource/pageable-collection";
import {BudgetCategory} from "../budget-category.model";
import {map} from "rxjs/operators";
import {Project} from "@app/project/project.model";

@Injectable()
export class BudgetCategoryService {
  private _budgetCategories: BehaviorSubject<PageableCollection<BudgetCategory>> = new BehaviorSubject(new PageableCollection());
  private _selectedCategory: BehaviorSubject<BudgetCategory> = new BehaviorSubject(new BudgetCategory());

  public readonly budgetCategories: Observable<BudgetCategory[]> = this._budgetCategories.asObservable().pipe(
    map(value => value.elements || [])
  );

  public readonly selectedCategory: Observable<BudgetCategory> = this._selectedCategory.asObservable();

  constructor(private resourceService: ResourceService) {
    this.initialLoad();
  }

  private initialLoad() {
    this.loadBudgetCategories();
  }

  loadBudgetCategories() {
    return this.resourceService.findMany(BudgetCategory, {projection: "budgetCategoryProjection", sort: "displayOrder"}).then(value => {
      return this._budgetCategories.next(value);
    });
  }

  findBudgetCategories(): Observable<PageableCollection<Project>> {
    return fromPromise(this.resourceService.findMany(BudgetCategory, {
      projection: "budgetCategoryProjection",
      sort: "displayOrder"
    }));
  }

  selectBudgetCategory(budgetCategory: BudgetCategory) {
    this._selectedCategory.next(budgetCategory);
  }

  findById(id: string): Observable<BudgetCategory> {
    return fromPromise(this.resourceService.findById(id, BudgetCategory));
  }

  save(budgetCategory: BudgetCategory): Observable<BudgetCategory> {
    return fromPromise(this.resourceService.save(budgetCategory).then(value => {
      this.loadBudgetCategories();
      return value;
    }));
  }

  changeSelectedCategory(change: Partial<BudgetCategory>) {
    const current = this._selectedCategory.getValue();
    Object.keys(change).forEach(key => current[key] = change[key]);
    this._selectedCategory.next(current);

  }

  selectBudgetCategoryById(id: string) {
    this.budgetCategories.subscribe(
      value => {
        const cat = value.find(c => c.id === id);
        if (cat) {
          this._selectedCategory.next(cat);
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  newBudgetCategory() {
    this._selectedCategory.next(new BudgetCategory());
  }
}
