import {Injectable} from '@angular/core';
import {BudgetCategory, BudgetSpendType, BudgetSubCategory} from "../budget-category.model";
import {BehaviorSubject, Observable, from as fromPromise} from "rxjs";
import {map} from "rxjs/operators";
import {ResourceService} from "../../resource/resource.service";
import {BudgetCategoryService} from "./budget-category.service";
import {BudgetSubCategoryService} from "./budget-subcategory.service";
import {PageableCollection} from "../../resource/pageable-collection";
import {PurchaseOrder} from "../../purchase-order/purchase-order.model";

@Injectable()
export class BudgetSpendTypeService {


  private _spendTypes: BehaviorSubject<BudgetSpendType[]> = new BehaviorSubject([]);
  private _selectedSpendType: BehaviorSubject<BudgetSpendType> = new BehaviorSubject(new BudgetSpendType());

  public readonly spendTypes: Observable<BudgetSpendType[]> = this._spendTypes.asObservable().pipe(
    map(value => value || [])
  );

  public readonly selectedSpendType: Observable<BudgetCategory> = this._selectedSpendType.asObservable();

  constructor(private resourceService: ResourceService, private budgetCategoryService : BudgetCategoryService, private budgetSubcategoryService: BudgetSubCategoryService) {
    budgetSubcategoryService.selectedSubCategory.subscribe(selectedSubCategory => this.loadForSubCategory(selectedSubCategory));
  }

  newSpendType() {
    this.budgetSubcategoryService.selectedSubCategory.subscribe(sub => {
      const toCreate = new BudgetSpendType();
      toCreate.budgetSubCategory = sub;
      this._selectedSpendType.next(toCreate);
    });
  }

  private loadForSubCategory(selectedSubCategory: BudgetSubCategory) {
    this._spendTypes.next(selectedSubCategory.budgetSpendTypes);
  }

  selectSpendTypeById(id: string) {
    this.spendTypes.subscribe(value => {
      const spendType = value.find(spendType => spendType.id === id);
      if (spendType) {
        this._selectedSpendType.next(spendType);
      }
    });
  }

  save(spendType: BudgetSpendType) {
    return fromPromise(this.resourceService.save(spendType).then(value => {
      return this.budgetCategoryService.loadBudgetCategories()
        .then(() =>{return value}).then(() => {return value})
    }))
  }

  changeSelectedSpendType(change: Partial<BudgetSpendType>) {
    const current = this._selectedSpendType.getValue();
    Object.keys(change).forEach(key => current[key] = change[key]);
    this._selectedSpendType.next(current);
  }

  getAllSpendTypes() {
    return this.resourceService.findMany(BudgetSpendType);
  }

  public findByCodeOrTitle(codeOrTitle: string): Observable<PageableCollection<BudgetSpendType>> {
    let possibleCode = Number(codeOrTitle);
    if(possibleCode){
      return fromPromise(this.resourceService.findManyByUrl(
        '/api/budget-spend-types/search/findByBudgetSpendTypeCodeContaining',
        PurchaseOrder,
        {budgetSpendTypeCode: possibleCode}));
    }else{
      return fromPromise(this.resourceService.findManyByUrl(
        '/api/budget-spend-types/search/findByTitleIgnoreCaseContaining',
        PurchaseOrder,
        {title: codeOrTitle}));
    }


  }
}
