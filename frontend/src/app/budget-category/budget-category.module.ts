import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {BudgetCategoryRoutingModule} from './budget-category-routing.module';
import {BudgetCategoryDetailComponent} from './budget-category-detail/budget-category-detail.component';
import {BudgetCategoryContainerComponent} from './budget-category-container/budget-category-container.component';
import {BstMaterialModule} from "../bst-material/bst-material.module";
import {BudgetCategoryService} from './services/budget-category.service';
import {ReactiveFormsModule} from "@angular/forms";
import {BudgetSubcategoryDetailComponent} from './budget-subcategory-detail/budget-subcategory-detail.component';
import {BudgetSubcategoryListComponent} from './budget-subcategory-list/budget-subcategory-list.component';
import {BudgetSubCategoryContainerComponent} from "./budget-subcategory-container/budget-subcategory-container.component";
import {SharedModule} from "../shared";
import {SpendtypeContainerComponent} from './spend-type-container/spend-type-container.component';
import {BudgetSubCategoryService} from "./services/budget-subcategory.service";
import {BudgetSpendTypeService} from "./services/budget-spend-type.service";
import {SpendTypeDetailComponent} from './spend-type-detail/spend-type-detail.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BudgetCategoryImportComponent} from "./budget-category-import/budget-category-import.component";
import {BudgetSubcategoryImportComponent} from "./budget-subcategory-import/budget-subcategory-import.component";
import {BudgetSpendTypeImportComponent} from "./budget-spend-type-import/budget-spend-type-import.component";
import {AuthModule} from "../auth/auth.module";
import {ProjectBudgetSpendTypeImportComponent} from "@app/budget-category/project-budget-spend-type-import/project-budget-spend-type-import.component";

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    BstMaterialModule,
    SharedModule,
    BudgetCategoryRoutingModule,
    AuthModule
  ],
  declarations: [
    BudgetCategoryDetailComponent,
    BudgetCategoryContainerComponent,
    BudgetSubcategoryDetailComponent,
    BudgetSubCategoryContainerComponent,
    BudgetSubcategoryListComponent,
    SpendtypeContainerComponent,
    SpendTypeDetailComponent,
    BudgetCategoryImportComponent,
    BudgetSubcategoryImportComponent,
    BudgetSpendTypeImportComponent,
    ProjectBudgetSpendTypeImportComponent
  ],
  providers: [
    BudgetCategoryService,
    BudgetSubCategoryService,
    BudgetSpendTypeService
  ]
})
export class BudgetCategoryModule {
}
