import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {BudgetCategory} from "../budget-category.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'bst-budget-category-detail',
  templateUrl: './budget-category-detail.component.html',
  styleUrls: ['./budget-category-detail.component.scss']
})
export class BudgetCategoryDetailComponent implements OnChanges, OnInit {
  public form: FormGroup;
  newLink: string;
  @Input() budgetCategory: BudgetCategory;
  @Output() change: EventEmitter<SimpleChanges> = new EventEmitter();
  @Output() save: EventEmitter<BudgetCategory> = new EventEmitter();

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute) {
    this.createForm();
    this.handleChange();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.budgetCategory) {
      this.form.patchValue(this.budgetCategory);
    }
  }


  saveCategory() {
    this.save.emit(this.budgetCategory);
  }

  private createForm() {
    this.form = this.formBuilder.group({
      title: ['', Validators.required],
      displayOrder: ['', Validators.required],
    });

  }

  private handleChange() {
    this.form.valueChanges.subscribe((value) => {
      this.change.emit(value);
    })
  }

  ngOnInit() {
    this.route.params.subscribe(params => this.newLink = params.id ? "../": "./")
  }


}
