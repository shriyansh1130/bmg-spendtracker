import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BudgetSpendType} from "../budget-category.model";
import {ActivatedRoute} from "@angular/router";
import {BudgetSubCategoryService} from "../services/budget-subcategory.service";
import {intlDecimalFormatValidator} from "../../shared/validators/intl-decimal-format-validator";

@Component({
  selector: 'bst-spend-type-detail',
  templateUrl: './spend-type-detail.component.html',
  styleUrls: ['./spend-type-detail.component.scss']
})
export class SpendTypeDetailComponent implements OnInit, OnChanges {
  public form: FormGroup;
  newLink: string;
  disabled = true; // ST-69
  @Input() budgetSpendType: BudgetSpendType;
  @Output() change: EventEmitter<Partial<BudgetSpendType>> = new EventEmitter();
  @Output() save: EventEmitter<BudgetSpendType> = new EventEmitter();

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private subCategoryService: BudgetSubCategoryService) {
    this.createForm();
    this.handleChange();

  }

  ngOnInit() {
    this.route.params.subscribe(params => this.newLink = params.id ? "../" : "./");
  }

  ngOnChanges(changes: Partial<BudgetSpendType>): void {
    if (this.budgetSpendType) {
      this.form.patchValue(this.budgetSpendType);
    }
  }

  private createForm() {
    this.form = this.formBuilder.group({
      title: ['', Validators.required],
      budgetSpendTypeCode: [0, Validators.required],
      glAccount: ['', Validators.required],
      ppdRecoupmentPct: [0, [Validators.required, Validators.min(0), Validators.max(100), intlDecimalFormatValidator(2)]]
    });

    if (this.disabled) {
      this.form.disable();
    }
  }

  private handleChange() {
    this.form.valueChanges.subscribe((value) => {
      this.change.emit(value);
    })
  }

  saveSpendType() {
    if (this.form.status === 'VALID') {
      this.save.emit(this.budgetSpendType);
    }
  }

  public getPpdRecoupmentPctErrorMessage() {
    let formField = this.form.get('ppdRecoupmentPct');
    return formField.hasError('required') ? 'A price is required' :
      formField.hasError('intlDecimalFormat') ? 'Value must be a valid decimal 123.45, 2 decimals max.' :
        formField.hasError('min') ? 'Value must be >= 0' :
          formField.hasError('max') ? 'Value must be <= 100' :
          '';
  }
}
