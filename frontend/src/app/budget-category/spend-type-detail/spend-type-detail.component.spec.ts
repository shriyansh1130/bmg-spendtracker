import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SpendTypeDetailComponent} from './spend-type-detail.component';
import {BudgetSubCategoryService} from "../services/budget-subcategory.service";
import {BstMaterialModule} from "../../bst-material/bst-material.module";
import {ActivatedRoute} from "@angular/router";
import {BudgetCategoryService} from "../services/budget-category.service";
import {RouterTestingModule} from "@angular/router/testing";
import {ReactiveFormsModule} from "@angular/forms";
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {BudgetSpendTypeService} from "../services/budget-spend-type.service";

describe('SpendTypeDetailComponent', () => {
  let component: SpendTypeDetailComponent;
  let fixture: ComponentFixture<SpendTypeDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpendTypeDetailComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [ReactiveFormsModule, BstMaterialModule, RouterTestingModule, NoopAnimationsModule],
      providers: [
        {provide: ActivatedRoute, useValue: {params: {subscribe: () => {}}}},
        {provide: BudgetSubCategoryService, useValue: {}},
        {provide: BudgetCategoryService, useValue: {}},
        {provide: BudgetSpendTypeService, useValue: {}},
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpendTypeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
