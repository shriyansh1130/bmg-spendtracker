import {Component} from '@angular/core';

@Component({
  selector: 'bst-spend-typpe-import',
  template: `<bst-import-records [displayName]="'Project-Budget-Spend-Types'" [importerName]="'projectBudgetSpendType'"></bst-import-records>`
})
export class ProjectBudgetSpendTypeImportComponent {
  constructor() {

  }
}
