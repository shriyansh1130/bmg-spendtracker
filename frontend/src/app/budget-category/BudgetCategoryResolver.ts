import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {BudgetCategoryService} from "./services/budget-category.service";
import {of, EMPTY, Observable} from "rxjs";
import {BudgetCategory} from "./budget-category.model";
import {take, map} from 'rxjs/operators';


@Injectable()
export class BudgetCategoryResolver implements Resolve<BudgetCategory> {
  constructor(private budgetCategoryService: BudgetCategoryService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<BudgetCategory> {
    const idParam = route.paramMap.get("id");
    if (idParam != null) {
      if (idParam === 'new') {
        return of(new BudgetCategory());
      } else if (!isNaN(Number(idParam))) {
        return this.budgetCategoryService.findById(idParam).pipe(
          take(1),
          map(budgetCategory => {
            if (budgetCategory) {
              return budgetCategory
            } else {
              this.router.navigate(['budget-categories'])
            }
          }));
      }
    }
    return EMPTY;

  }
}
