import {Component, OnInit} from '@angular/core';
import {BudgetSpendTypeService} from "../services/budget-spend-type.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog, MatDialogConfig, MatSnackBar} from "@angular/material";
import {BudgetSpendType} from "../budget-category.model";
import {BudgetCategoryService} from "../services/budget-category.service";
import {AppConfigService} from '../../core/services/app-config.service';
import {ErrorDialogComponent} from '../../shared/error-dialog/error-dialog.component';
import {ConfirmationDialogComponent} from "../../shared/confirmation-dialog/confirmation-dialog.component";

@Component({
  selector: 'bst-spend-type-container',
  templateUrl: './spend-type-container.component.html',
  styleUrls: ['./spend-type-container.component.scss']
})
export class SpendtypeContainerComponent implements OnInit {

  constructor(public spendTypeService: BudgetSpendTypeService,
              public budgetCategoryService: BudgetCategoryService,
              private appConfig:AppConfigService,
              private router: Router,
              private snackbar: MatSnackBar,
              private route: ActivatedRoute,
              private dialog: MatDialog) {
  }

  dialogConfig = new MatDialogConfig<any>();
  dialogConfigConfirm = new MatDialogConfig<any>();

  ngOnInit() {
    this.route.params.subscribe(value => {
      if (value.id) {
        this.spendTypeService.selectSpendTypeById(value.id);
      } else {
        this.spendTypeService.newSpendType();
      }
    });
  }

  saveSpendType(spendType: BudgetSpendType) {
    this.spendTypeService.save(spendType).subscribe(
      value => {
        this.router.navigate(['spend-types', value.id], {relativeTo: this.route.parent});
        this.snackbar.open(`Successfully saved: ${spendType.title}`, null, this.appConfig.snackbarConfig);
      },
      error => {
        this.dialogConfig.data = {generalError: error, description: "Could not save SpendType"};
        this.dialog.open(ErrorDialogComponent, this.dialogConfig);
      }
    )
  }

  deleteSpendType(spendType: BudgetSpendType) {
    this.dialogConfigConfirm.data = {description: "Would you like to delete this BudgetSpendType?"};
    let deleteConfirm = this.dialog.open(ConfirmationDialogComponent, this.dialogConfigConfirm);
    deleteConfirm.afterClosed().subscribe(async result => {
      if(result) {
        try {
          await spendType.delete();
          this.snackbar.open(`Successfully deleted: ${spendType.title}`, null, this.appConfig.snackbarConfig);
          this.budgetCategoryService.loadBudgetCategories(); // reload data after delete
          this.router.navigate(['budget-categories']);
        } catch (error) {
          this.dialogConfig.data = {generalError: error, description: "Could not delete BudgetSubCategory"};
          this.dialog.open(ErrorDialogComponent, this.dialogConfig);
        }
      }
    });
  }

  handleChange(change: Partial<BudgetSpendType>) {
    this.spendTypeService.changeSelectedSpendType(change);
  }

}
