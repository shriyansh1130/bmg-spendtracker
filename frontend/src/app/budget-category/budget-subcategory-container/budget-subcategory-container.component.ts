import {Component, OnInit} from '@angular/core';
import {BudgetCategoryService} from "../services/budget-category.service";
import {BudgetSubCategoryService} from "../services/budget-subcategory.service";
import {BudgetCategory, BudgetSubCategory} from "../budget-category.model";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog, MatDialogConfig, MatSnackBar} from "@angular/material";
import {AppConfigService} from '../../core/services/app-config.service';
import {ErrorDialogComponent} from '../../shared/error-dialog/error-dialog.component';
import {ConfirmationDialogComponent} from "../../shared/confirmation-dialog/confirmation-dialog.component";

@Component({
  selector: 'bst-budget-subcategory-container',
  templateUrl: './budget-subcategory-container.component.html',
  styleUrls: ['./budget-subcategory-container.component.scss']
})
export class BudgetSubCategoryContainerComponent implements OnInit {

  constructor(public budgetCategoryService: BudgetCategoryService,
              public budgetSubCategoryService: BudgetSubCategoryService,
              private appConfig:AppConfigService,
              private router: Router,
              private snackbar: MatSnackBar,
              private route: ActivatedRoute,
              private dialog: MatDialog) {
  }

  dialogConfigError = new MatDialogConfig<any>();
  dialogConfigConfirm = new MatDialogConfig<any>();

  ngOnInit() {
    this.route.params.subscribe(value => {
      if (value.id) {
        this.budgetSubCategoryService.selectBudgetSubCategoryById(value.id);
      } else {
        this.budgetSubCategoryService.newBudgetSubCategory();
      }
    });

  }

  saveSubCategory(budgetSubCategory: BudgetSubCategory) {
    this.budgetSubCategoryService.save(budgetSubCategory).subscribe(
      value => {
        this.router.navigate(['sub-categories', value.id], {relativeTo: this.route.parent});
        this.snackbar.open(`Successfully saved: ${budgetSubCategory.title}`, null, this.appConfig.snackbarConfig);
      },
      error => this.snackbar.open(`Save Failed!`)
    )
  }

  deleteBudgetSubCategory(budgetSubCategory: BudgetSubCategory) {
    this.dialogConfigConfirm.data = {title: "Delete", description: "Would you like to delete this BudgetSubCategory?"};
    let deleteConfirm = this.dialog.open(ConfirmationDialogComponent, this.dialogConfigConfirm);
    deleteConfirm.afterClosed().subscribe(async result => {
      if(result) {
        try {
          await budgetSubCategory.delete();
          this.snackbar.open(`Successfully deleted: ${budgetSubCategory.title}`, null, this.appConfig.snackbarConfig);
          this.budgetCategoryService.loadBudgetCategories(); // reload data after delete
          this.router.navigate(['budget-categories']);
        } catch (error) {
          this.dialogConfigError.data = {generalError: error, description: "Could not delete BudgetSubCategory"};
          this.dialog.open(ErrorDialogComponent, this.dialogConfigError);
        }
      }
    });
  }

  handleChange(change: Partial<BudgetCategory>) {
    this.budgetSubCategoryService.changeSelectedSubCategory(change);
  }

}
