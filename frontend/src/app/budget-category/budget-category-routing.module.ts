import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BudgetCategoryContainerComponent} from "./budget-category-container/budget-category-container.component";
import {BudgetCategoryResolver} from "./BudgetCategoryResolver";
import {BudgetSubCategoryContainerComponent} from "./budget-subcategory-container/budget-subcategory-container.component";
import {SpendtypeContainerComponent} from "./spend-type-container/spend-type-container.component";
import {OktaAuthGuard} from "@okta/okta-angular";
import {BudgetCategoryImportComponent} from "./budget-category-import/budget-category-import.component";
import {BudgetSubcategoryImportComponent} from "./budget-subcategory-import/budget-subcategory-import.component";
import {BudgetSpendTypeImportComponent} from "./budget-spend-type-import/budget-spend-type-import.component";
import {ProjectBudgetSpendTypeImportComponent} from "@app/budget-category/project-budget-spend-type-import/project-budget-spend-type-import.component";

const routes: Routes = [
  { path: 'budget-categories', component: BudgetCategoryContainerComponent , canActivate: [OktaAuthGuard]},
  { path: 'budget-categories/import', component: BudgetCategoryImportComponent , canActivate: [OktaAuthGuard]},
  {
    path: 'budget-categories/:id', component: BudgetCategoryContainerComponent, canActivate: [OktaAuthGuard],
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'sub-categories'},
      { path: 'sub-categories', component: BudgetSubCategoryContainerComponent , canActivate: [OktaAuthGuard]},
      {
        path: 'sub-categories/:id', component: BudgetSubCategoryContainerComponent, canActivate: [OktaAuthGuard],
        children: [
          { path: '', pathMatch: 'full', redirectTo:  'spend-types' },
          { path: 'spend-types', component: SpendtypeContainerComponent, canActivate: [OktaAuthGuard]},
          { path: 'spend-types/:id', component: SpendtypeContainerComponent, canActivate: [OktaAuthGuard]},
        ]
      }
    ]
  },
  { path: 'budget-sub-categories/import', component: BudgetSubcategoryImportComponent , canActivate: [OktaAuthGuard]},
  { path: 'budget-spend-types/import', component: BudgetSpendTypeImportComponent , canActivate: [OktaAuthGuard]},
  { path: 'project-budget-spend-types/import', component: ProjectBudgetSpendTypeImportComponent , canActivate: [OktaAuthGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [BudgetCategoryResolver]
})
export class BudgetCategoryRoutingModule { }
