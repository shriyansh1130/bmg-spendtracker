import {Component, OnInit} from '@angular/core';
import {BudgetCategory} from "../budget-category.model";
import {BudgetCategoryService} from "../services/budget-category.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog, MatDialogConfig, MatSnackBar} from "@angular/material";
import {AppConfigService} from '../../core/services/app-config.service';
import {ErrorDialogComponent} from '../../shared/error-dialog/error-dialog.component';
import {ConfirmationDialogComponent} from "../../shared/confirmation-dialog/confirmation-dialog.component";

@Component({
  selector: 'bst-budget-category-container',
  templateUrl: './budget-category-container.component.html',
  styleUrls: ['./budget-category-container.component.scss']
})
export class BudgetCategoryContainerComponent implements OnInit {

  constructor(public budgetCategoryService: BudgetCategoryService,
              private appConfig:AppConfigService,
              private router: Router,
              private snackbar: MatSnackBar,
              private route: ActivatedRoute,
              private dialog: MatDialog) { }

  dialogConfig = new MatDialogConfig<any>();
  dialogConfigConfirm = new MatDialogConfig<any>();

  ngOnInit() {
    this.route.params.subscribe(value => {
      if (value.id) {
        this.budgetCategoryService.selectBudgetCategoryById(value.id);
      }else {
        this.budgetCategoryService.newBudgetCategory();
      }
    });
  }

  saveBudgetCategory(budgetCategory: BudgetCategory) {
    this.budgetCategoryService.save(budgetCategory).subscribe(
      value => {
        this.router.navigate(['budget-categories', value.id]);
        this.snackbar.open(`Successfully saved: ${budgetCategory.title}`,null, this.appConfig.snackbarConfig );
      },
      error =>   this.snackbar.open(`Save Failed!` ),
    )
  }

  deleteBudgetCategory(budgetCategory: BudgetCategory) {
    this.dialogConfigConfirm.data = {title: "Delete", description: "Would you like to delete this BudgetCategory?"};
    let deleteConfirm = this.dialog.open(ConfirmationDialogComponent, this.dialogConfigConfirm);
    deleteConfirm.afterClosed().subscribe(async result => {
      if(result) {
        try {
          await budgetCategory.delete();
          this.snackbar.open(`Successfully deleted: ${budgetCategory.title}`,null, this.appConfig.snackbarConfig);
          this.budgetCategoryService.loadBudgetCategories(); // reload data after delete
          this.router.navigate(['budget-categories']);
        } catch(error) {
          this.dialogConfig.data = {generalError: error, description: "Could not delete BudgetCategory"};
          this.dialog.open(ErrorDialogComponent, this.dialogConfig);
        }
      }
    });
  }

  handleChange(change: Partial<BudgetCategory>) {
    this.budgetCategoryService.changeSelectedCategory(change);
  }

}
