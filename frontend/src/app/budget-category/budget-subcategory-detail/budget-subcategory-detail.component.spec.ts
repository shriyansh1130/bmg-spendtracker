import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BudgetSubcategoryDetailComponent} from './budget-subcategory-detail.component';
import {BstMaterialModule} from "../../bst-material/bst-material.module";
import {ActivatedRoute} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {ReactiveFormsModule} from "@angular/forms";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {BudgetSubCategoryService} from "../services/budget-subcategory.service";
import {BudgetCategoryService} from "../services/budget-category.service";

describe('BudgetSubcategoryDetailComponent', () => {
  let component: BudgetSubcategoryDetailComponent;
  let fixture: ComponentFixture<BudgetSubcategoryDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BudgetSubcategoryDetailComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [ReactiveFormsModule, BstMaterialModule, RouterTestingModule, NoopAnimationsModule],
      providers: [
        {provide: ActivatedRoute, useValue: {params: {subscribe: () => {}}}},
        {provide: BudgetSubCategoryService, useValue: {}},
        {provide: BudgetCategoryService, useValue: {}},
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BudgetSubcategoryDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
