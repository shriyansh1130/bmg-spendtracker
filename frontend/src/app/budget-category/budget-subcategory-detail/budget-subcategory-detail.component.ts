import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BudgetSubCategory} from "../budget-category.model";
import {BudgetCategoryService} from "../services/budget-category.service";

@Component({
  selector: 'bst-budget-subcategory-detail',
  templateUrl: './budget-subcategory-detail.component.html',
  styleUrls: ['./budget-subcategory-detail.component.scss']
})
export class BudgetSubcategoryDetailComponent implements OnChanges, OnInit {
  newLink: string;
  public form: FormGroup;
  @Input() budgetSubCategory: BudgetSubCategory;
  @Output() change: EventEmitter<SimpleChanges> = new EventEmitter();
  @Output() save: EventEmitter<BudgetSubCategory> = new EventEmitter();

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, public budgetCategoryService: BudgetCategoryService) {
    this.createForm();
    this.handleChange();
  }

  ngOnInit() {
    this.route.params.subscribe(params => this.newLink = params.id ? "../": "./")
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.budgetSubCategory) {
      this.form.patchValue(this.budgetSubCategory);
    }
  }

  saveSubCategory() {
    this.save.emit(this.budgetSubCategory);
  }

  private createForm() {
    this.form = this.formBuilder.group({
      //budgetCategory: [this.budgetCategoryService.selectedCategory , Validators.required],
      title: ['', Validators.required],
      displayOrder: ['', Validators.required],
    });
  }

  private handleChange() {
    this.form.valueChanges.subscribe((value) => {
      this.change.emit(value);
    })
  }

}
