import {Component} from '@angular/core';

@Component({
  selector: 'bst-spend-typpe-import',
  template: `<bst-import-records [displayName]="'Budget-Spend-Types'" [importerName]="'budgetSpendType'"></bst-import-records>`
})
export class BudgetSpendTypeImportComponent {
  constructor() {

  }
}
