import {Resource} from "../resource/resource";

export class BudgetCategory extends Resource {

  public static PATH: string = "api/budget-categories";

  constructor(
    //public id: string,
    public title?,
    public displayOrder?: number,
    public budgetSubCategories?: BudgetSubCategory[],
    public createdAt?: string,
    public createdBy?: string,
    public modifiedBy?: string,
    public modifiedAt?: string
  ){
    super();
  }
}

export class BudgetSubCategory extends Resource {

  public static PATH: string = "api/budget-sub-categories";

  constructor(
    //public id: string,
    public title?,
    public budgetCategory?: BudgetCategory,
    public displayOrder?: number,
    public budgetSpendTypes?: BudgetSpendType[],
    public createdAt?: string,
    public createdBy?: string,
    public modifiedBy?: string,
    public modifiedAt?: string
  ){
    super();
  }
}

export class BudgetSpendType extends Resource {

  public static PATH: string = "api/budget-spend-types";

  constructor(
    //public id: string,
    public title?,
    public budgetSubCategory?: BudgetSubCategory,
    public budgetSpendTypeCode?: string,
    public ppdRecoupmentPct?: number,
    public displayOrder?: number,
    public glAccount?: string,
    public createdAt?: string,
    public createdBy?: string,
    public modifiedBy?: string,
    public modifiedAt?: string
  ){
    super();
  }
}


