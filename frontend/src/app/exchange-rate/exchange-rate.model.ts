import {Resource} from "../resource/resource";
import {Currency} from '../currency/currency.model';

export class ExchangeRate extends Resource {

  public static PATH: string = "api/exchange-rates";

  constructor(
    //public id: string,
    public exchangeRateName?,
    public exchangeRateValue?: number,
    public fromCurrency?: Currency,
    public toCurrency?: Currency,
    public createdAt?: string,
    public createdBy?: string,
    public modifiedBy?: string,
    public modifiedAt?: string
  ){
    super();
  }
}
