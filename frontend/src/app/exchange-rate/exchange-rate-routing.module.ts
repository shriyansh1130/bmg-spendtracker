import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ExchangeRateComponent} from './exchange-rate.component';
import {OktaAuthGuard} from "@okta/okta-angular";
import {ExchangeRateImportComponent} from "./exchange-rate-import/exchange-rate-import.component";

const routes: Routes = [
  {path: 'exchange-rates', component: ExchangeRateComponent, canActivate: [OktaAuthGuard]},
  {path: 'exchange-rates/import', component: ExchangeRateImportComponent, canActivate: [OktaAuthGuard]},
  {path: 'exchange-rates/:id', component: ExchangeRateComponent, canActivate: [OktaAuthGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExchangeRateRoutingModule {
}
