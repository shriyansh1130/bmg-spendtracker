import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {ExchangeRateService} from '../services/exchange-rate.service';
import {ExchangeRate} from '../exchange-rate.model';
import {Router} from '@angular/router';
import {AppConfigService} from '../../core/services/app-config.service';

@Component({
  selector: 'bst-exchange-rate-modal',
  templateUrl: './exchange-rate-modal.component.html',
  styleUrls: ['./exchange-rate-modal.component.scss']
})
export class ExchangeRateModalComponent implements OnInit {
  private formData: Partial<ExchangeRate>;
  private _formValid = true;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<ExchangeRateModalComponent>,
              private router: Router,
              private appConfig:AppConfigService,
              private exchangeRateService:ExchangeRateService,
              private snackbar:MatSnackBar) {
  }

  ngOnInit() {
  }

  itemChanged(ec:Partial<ExchangeRate>) {
    this.formData = ec;
  }

  get formValid() {
    return this._formValid;
  }

  save() {
    if (this.formValid) {
      let exchangeRate = this.data.item;
      Object.keys(this.formData).forEach(k => {
        exchangeRate[k] = this.formData[k];
      });
      this.exchangeRateService.save(exchangeRate).subscribe(
        saveResult => {
          this.snackbar.open(`Successfully saved: ${saveResult.exchangeRateName}`, null, this.appConfig.snackbarConfig );
          this.router.navigate(['/exchange-rates']);
          this.exchangeRateService.clearExchangeRateSelection();
          this.dialogRef.close(saveResult);
        },
        error => this.snackbar.open('Save Failed!', null, this.appConfig.snackbarConfig )
      )
    }
  }

  statusChanged(status: string) {
    this._formValid = status === 'VALID';
  }
}
