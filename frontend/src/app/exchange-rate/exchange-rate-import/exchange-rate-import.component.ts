import {Component} from '@angular/core';

@Component({
  selector: 'bst-exchange-rate-import',
  template: `<bst-import-records [displayName]="'Exchange-Rates'" [importerName]="'exchangerate'"></bst-import-records>`
})
export class ExchangeRateImportComponent {
  constructor() {

  }
}
