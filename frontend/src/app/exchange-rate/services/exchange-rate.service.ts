import {Injectable} from '@angular/core';
import {ResourceService} from '../../resource/resource.service';
import {ExchangeRate} from '../exchange-rate.model';
import {BehaviorSubject, Observable, from as fromPromise} from 'rxjs';

import {PageableCollection} from '../../resource/pageable-collection';
import {Router} from '@angular/router';
import {AppConfigService} from "@app/core/services/app-config.service";

@Injectable()
export class ExchangeRateService {

  private _selectedExchangeRate = new BehaviorSubject(new ExchangeRate());

  public readonly selectedExchangeRate: Observable<ExchangeRate> = this._selectedExchangeRate.asObservable();

  constructor(private resourceService: ResourceService, private router: Router, private appConfig: AppConfigService) {
  }

  save(exchangeRate: ExchangeRate): Observable<ExchangeRate> {
    return fromPromise(this.resourceService.save(exchangeRate).then(value => {
      return value;
    }));
  }

  clearExchangeRateSelection() {
    this._selectedExchangeRate.next(null);
  }

  create() {
    let created = this.resourceService.createResource(ExchangeRate);
    this._selectedExchangeRate.next(created);
    return created;
  }

  async selectById(id: string) {
    try {
      let exchangeRate = await this.resourceService.findById(id, ExchangeRate);
      this._selectedExchangeRate.next(exchangeRate);
    } catch {
      this.router.navigate(['/exchange-rates']);
    }
  }

  public loadExchangeRatesWithPagination(page=0, size=this.appConfig.defaultPageSize, fieldName="exchangeRateName", sort='desc' ): Observable<PageableCollection<ExchangeRate>> {
    return fromPromise(this.resourceService.findManyByUrl(
      '/api/exchange-rates/search/findByExchangeRateNameIgnoreCaseContaining',
      ExchangeRate,
      {exchangeRateName: name, page: page, size: size, sort: `${fieldName},${sort}`}));
  }

}
