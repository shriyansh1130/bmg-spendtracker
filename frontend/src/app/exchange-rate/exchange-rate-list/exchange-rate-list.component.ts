import {
  AfterViewInit,
  Component,
  EventEmitter, Input,
  OnInit,
  Output,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import {SelectionModel} from "@angular/cdk/collections";
import {ExchangeRateService} from '../services/exchange-rate.service';
import {ExchangeRate} from '../exchange-rate.model';
import {ExchangeRateTableDataSource} from '../ExchangeRateTableDataSource';
import {AppConfigService} from '../../core/services/app-config.service';
import {MatSort, PageEvent} from "@angular/material";
import {merge} from "rxjs";
import {tap} from "rxjs/operators";
import {CustomPaginatorComponent} from "../../shared/custom-paginator/custom-paginator.component";

@Component({
  selector: 'bst-exchange-rate-list',
  templateUrl: './exchange-rate-list.component.html',
  styleUrls: ['./exchange-rate-list.component.scss']
})
export class ExchangeRateListComponent implements OnInit, AfterViewInit {

  @Output() createExchangeRate: EventEmitter<void> = new EventEmitter();
  @Output() selectedItem: EventEmitter<ExchangeRate> = new EventEmitter();
  @Input() editable = false;
  @ViewChildren(CustomPaginatorComponent) paginators:QueryList<CustomPaginatorComponent>;
  @ViewChild(MatSort) sort: MatSort;

  dataSource: ExchangeRateTableDataSource;

  initialSelection = [];
  allowMultiSelect = false;
  selection = new SelectionModel<ExchangeRate>(this.allowMultiSelect, this.initialSelection);
  pageChanged = new EventEmitter<number>();

  constructor(public appConfig:AppConfigService, public exchangeRateService:ExchangeRateService) { }

  ngOnInit() {
    this.dataSource = new ExchangeRateTableDataSource(this.appConfig, this.exchangeRateService);
    this.dataSource.loadExchangeRates();
  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.goToPageZero());

    merge(this.sort.sortChange, this.pageChanged)
      .pipe(
        tap(() => {
          this.loadExchangeRatesPage();
          this.goToPageZero();
        })
      ).subscribe();
  }

  loadExchangeRatesPage() {
    this.dataSource.loadExchangeRates(this.sort.active ,this.sort.direction, this.paginators.first.pageIndex, this.paginators.first.pageSize);
  }

  onSelectRow(rowItem:ExchangeRate) {
    this.selection.toggle(rowItem);
    this.selectedItem.emit(rowItem);
  }

  onCreateExchangerate() {
    this.createExchangeRate.emit();
  }

  changePage(event: PageEvent) {
    this.paginators.forEach((p) => {p.pageIndex = event.pageIndex, p.pageSize = event.pageSize});
    this.pageChanged.emit(event.pageIndex);
  }

  private goToPageZero() {
    this.paginators.forEach(p => p.pageIndex = 0);
  }

}
