import {NgModule} from '@angular/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import {CoreModule} from '../core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared';
import {ExchangeRateRoutingModule} from './exchange-rate-routing.module';
import {ExchangeRateComponent} from './exchange-rate.component';
import {BstMaterialModule} from '../bst-material/bst-material.module';
import {ExchangeRateDetailComponent} from './exchange-rate-detail/exchange-rate-detail.component';
import {ReactiveFormsModule} from "@angular/forms";
import {ExchangeRateListComponent} from './exchange-rate-list/exchange-rate-list.component';
import {ExchangeRateModalComponent} from './exchange-rate-modal/exchange-rate-modal.component';
import {ExchangeRateImportComponent} from "./exchange-rate-import/exchange-rate-import.component";
import {AuthModule} from "../auth/auth.module";

@NgModule({
  imports: [
    CoreModule,
    CommonModule,
    SharedModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    ExchangeRateRoutingModule,
    BstMaterialModule,
    AuthModule
  ],
  declarations: [
    ExchangeRateComponent,
    ExchangeRateDetailComponent,
    ExchangeRateListComponent,
    ExchangeRateModalComponent,
    ExchangeRateImportComponent
  ],
  entryComponents: [
    ExchangeRateModalComponent
  ]
})
export class ExchangeRateModule {
}
