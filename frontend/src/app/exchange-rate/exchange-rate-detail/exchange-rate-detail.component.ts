import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ExchangeRate} from '../exchange-rate.model';
import {ActivatedRoute} from '@angular/router';
import {Currency} from '../../currency/currency.model';
import {CurrencyService} from '../../currency/currency.service';
import {intlDecimalFormatValidator} from "@app/shared/validators/intl-decimal-format-validator";

@Component({
  selector: 'bst-exchange-rate-detail',
  templateUrl: './exchange-rate-detail.component.html',
  styleUrls: ['./exchange-rate-detail.component.scss']
})
export class ExchangeRateDetailComponent implements OnInit, OnChanges {

  public form: FormGroup;
  public currencies;

  @Input() exchangeRate: ExchangeRate;
  @Output() onItemChange: EventEmitter<SimpleChanges> = new EventEmitter();
  @Output() onStatusChange: EventEmitter<SimpleChanges> = new EventEmitter();

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private currencyService: CurrencyService) {
    this.createForm();
    this.handleChange();
  }

  ngOnInit() {
    this.currencyService.currencies.subscribe(currencies => this.currencies = currencies);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.exchangeRate) {
      this.form.patchValue(this.exchangeRate);
    }
  }

  private createForm() {
    this.form = this.formBuilder.group({
      exchangeRateName: ['', Validators.required],
      exchangeRateValue: ['', [Validators.required, Validators.min(0), Validators.max(999.99999), intlDecimalFormatValidator(5)]],
      fromCurrency: [null, Validators.required],
      toCurrency: [null, Validators.required]
    });
  }

  private handleChange() {
    this.form.valueChanges.subscribe((value) => {
      this.onItemChange.emit(value);
    })

    this.form.statusChanges.subscribe(value => {
      this.onStatusChange.emit(value);
    })
  }

  public currencyCompareFn(c1: Currency, c2: Currency): boolean {
    return c1 && c2 ? c1.uri === c2.uri : c1 === c2;
  }

  public getValueErrorMessage() {
    let formField = this.form.get('exchangeRateValue');
    return formField.hasError('required') ? 'A price is required' :
      formField.hasError('intlDecimalFormat') ? 'Value must be a valid decimal 123.45, 5 decimals max.' :
        formField.hasError('min') ? 'Value must be > 0' :
          formField.hasError('max') ? 'Value must be < 999.99999' :
            '';
  }

}
