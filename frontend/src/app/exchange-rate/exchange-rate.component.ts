import {Component, OnInit} from '@angular/core';
import {ExchangeRateService} from './services/exchange-rate.service';
import {ExchangeRate} from './exchange-rate.model';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {ExchangeRateModalComponent} from './exchange-rate-modal/exchange-rate-modal.component';
import {CurrencyService} from '../currency/currency.service';
import {Location} from '@angular/common';

@Component({
  selector: 'bst-exchange-rate',
  templateUrl: './exchange-rate.component.html',
  styleUrls: ['./exchange-rate.component.scss']
})


export class ExchangeRateComponent implements OnInit {
  constructor(public exchangeRateService: ExchangeRateService,
              private currencyService:CurrencyService,
              private router: Router,
              private location: Location,
              private route: ActivatedRoute,
              public dialog: MatDialog) {
  }

  dialogConfig = new MatDialogConfig<any>();

  ngOnInit() {
    this.dialogConfig.width = '550px';
    this.dialogConfig.maxHeight = '600px';

    this.exchangeRateService.selectedExchangeRate.subscribe(ec => {
      if(ec && ec.id) {this.openEditDialog(ec, false);}
    });

    this.route.params.subscribe(value => {
      if (value.id) {
        this.exchangeRateService.selectById(value.id);
      } else {
        this.exchangeRateService.clearExchangeRateSelection();
      }
    });
  }

  openNewDialog() {
    this.dialogConfig.data = {item: this.exchangeRateService.create()};
    this.dialog.open(ExchangeRateModalComponent, this.dialogConfig);
  }

  openEditDialog(exchangeRate:ExchangeRate, withRouteChange:boolean=true) {
    this.dialogConfig.data = {item: exchangeRate};
    if(withRouteChange) {
      this.location.replaceState('/exchange-rates/'+exchangeRate.id)
    }
    let dialogRef = this.dialog.open(ExchangeRateModalComponent, this.dialogConfig);
    dialogRef.beforeClose().subscribe(value => {
      if(!value) {
        // cancelled edit, back to overview.
        this.location.replaceState('/exchange-rates')
      }
    });
  }
}
