import {BehaviorSubject, Observable, of} from "rxjs";
import {PageableCollection} from "../resource/pageable-collection";
import {ExchangeRate} from './exchange-rate.model';
import {ExchangeRateService} from './services/exchange-rate.service';
import {IPagedTableDataSource} from '../resource/IPagedTableDataSource';
import {AppConfigService} from '../core/services/app-config.service';
import {Country} from "../country/country.model";
import {CollectionViewer} from "@angular/cdk/collections";
import {catchError, finalize, map} from "rxjs/operators";

export class ExchangeRateTableDataSource implements IPagedTableDataSource<ExchangeRate> {
  private _exchangeRateSubject = new BehaviorSubject<PageableCollection<ExchangeRate>>(new PageableCollection());
  private _loadingSubject = new BehaviorSubject<boolean>(false);
  public readonly loading$ = this._loadingSubject.asObservable();

  public readonly totalExchangeRateCount$ = this._exchangeRateSubject.asObservable().pipe(
    map((p) =>  p.page && p.page.totalElements || 0)
  );


  constructor(private appConfig:AppConfigService, private exchangeRateService: ExchangeRateService) {

  }

  connect(collectionViewer: CollectionViewer): Observable<ExchangeRate[]> {
    return this._exchangeRateSubject.asObservable().pipe(
      map((page) => page.elements )
    )
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this._exchangeRateSubject.complete();
    this._loadingSubject.complete();
  }

  public loadExchangeRates(sortField='exchangeRateName', sortDirection = 'desc', page = 0, pageSize:number = this.appConfig.defaultPageSize) {
    this._loadingSubject.next(true);

    this.exchangeRateService.loadExchangeRatesWithPagination(page, pageSize, sortField, sortDirection).pipe(
      catchError(() => of([])),
      finalize(() => this._loadingSubject.next(false))
    )
      .subscribe(exchangeRates => {
        this._exchangeRateSubject.next(<PageableCollection<ExchangeRate>>exchangeRates)
      });
  }

}
