import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {BehaviorSubject, Observable, of} from "rxjs";
import {ImportTask} from "./import-task.model";
import {ImportTaskService} from "./import-task.service";
import {PageableCollection} from "../../resource/pageable-collection";
import {catchError, finalize, map} from "rxjs/operators";
import {Project} from "../../project/project.model";
import {Page} from "../../resource/page";

export class ImportTaskDataSource extends DataSource<ImportTask> {

  private _importTaskPageSubject = new BehaviorSubject<PageableCollection<ImportTask>>(new PageableCollection());
  private _loadingSubject = new BehaviorSubject<boolean>(false);

  public readonly totalTaskCount$ = this._importTaskPageSubject.asObservable()
    .pipe(
      map((p) => p.page && p.page.totalElements || 0)
    );

  public readonly loading$ = this._loadingSubject.asObservable();

  constructor(private importTaskService: ImportTaskService) {
    super();
  }

  connect(collectionViewer: CollectionViewer): Observable<ImportTask[]> {
    return this._importTaskPageSubject.asObservable()
      .pipe(
        map(page => page.elements)
      )
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this._importTaskPageSubject.complete();
    this._loadingSubject.complete();
  }

  public loadImportTasks(importerName: String, page?: Page): void {
    this.importTaskService.findByImporterName(importerName, page)
      .pipe(
        catchError(() => of([])),
        finalize(() => this._loadingSubject.next(false))
      )
      .subscribe(projects => {
        this._importTaskPageSubject.next(<PageableCollection<Project>>projects)
      });
  }


}
