import {Resource} from "../../resource/resource";
import {ImportResult} from "./import-result.model";

export class ImportTask extends Resource {

  public static PATH: string = "api/import-tasks";

  constructor(public importerName?: string,
              public errorMessage?: string,
              public importStatus?: string,
              public importResult?: ImportResult,
              public fileName?: string,
              public createdAt?: string) {
    super();
  }


}
