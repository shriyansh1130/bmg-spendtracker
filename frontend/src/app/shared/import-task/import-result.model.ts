export class ImportResult {
  insertedCount: number;
  updatedCount: number;
  deletedCount: number;
  errorCount: number;
  errorMessages: string[];
  warningMessages: string[];
}
