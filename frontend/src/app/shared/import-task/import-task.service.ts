import {Injectable} from "@angular/core";
import {from as fromPromise, Observable} from "rxjs";
import {ResourceService} from "../../resource/resource.service";
import {PageableCollection} from "../../resource/pageable-collection";
import {ImportTask} from "./import-task.model";
import {Page} from "../../resource/page";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class ImportTaskService {

  constructor(private resourceService: ResourceService, private http: HttpClient) {
  }

  findByImporterName(importerName: String, page?: Page): Observable<PageableCollection<ImportTask>> {
    return fromPromise(this.resourceService.findManyByUrl('api/import-tasks/search/findByImporterNameOrderByCreatedAtDesc', ImportTask, {'importerName': importerName}, page));
  }


  exportRecords(importerName: string): void {
    this.http.get(
      "api/export",

      {observe: 'response', responseType: 'blob', params: {importer: importerName}})
      .subscribe(resp => {
        const contentDispositionHeader = resp.headers.get('Content-Disposition');
        const result = contentDispositionHeader.split(';')[1].trim()
          .split('=')[1];
        const fileName = result.replace(/"/g, '');
        const objectUrl = window.URL.createObjectURL(resp.body);
        const anchor = document.createElement("a");
        document.body.appendChild(anchor);
        anchor.href = objectUrl;
        anchor.download = fileName;
        anchor.click();

        setTimeout(() => {
          window.URL.revokeObjectURL(objectUrl);
          document.body.removeChild(anchor);
        }, 0);
      });
  }


}
