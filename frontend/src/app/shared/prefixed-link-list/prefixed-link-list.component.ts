import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'bst-prefixed-link-list',
  templateUrl: './prefixed-link-list.component.html',
  styleUrls: ['./prefixed-link-list.component.scss']
})
export class PrefixedLinkListComponent<T> implements OnInit {

  prefix: string;
  @Input() elements: T[];
  @Input() labelAttribute: string;

  @Output() deleteItem: EventEmitter<any> = new EventEmitter();

  constructor(private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.route.params.subscribe(value => this.prefix = value.id ? "../" : "./")
  }
}
