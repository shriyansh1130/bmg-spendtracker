import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog, MatDialogConfig, MatTableDataSource} from "@angular/material";
import {ImportResult} from "../import-task/import-result.model";
import {UploaderOptions, UploadFile, UploadInput, UploadOutput} from "ngx-uploader";
import {ImportResultDialogComponent} from "../import-result-dialog/import-result-dialog.component";
import {OktaAuthService} from "@okta/okta-angular";
import {ImportTaskService} from "../import-task/import-task.service";
import {ImportTaskDataSource} from "../import-task/import-task.data-source";
import {AppConfigService} from "../../core/services/app-config.service";
import {ImportTask} from "../import-task/import-task.model";
import {Page} from "../../resource/page";

@Component({
  selector: 'bst-import-records',
  templateUrl: './import-records.component.html',
  styleUrls: ['./import-records.component.scss']
})
export class ImportRecordsComponent implements OnInit {


  @Input()
  displayName: string;

  @Input()
  importerName: string;

  @Output()
  complete = new EventEmitter<object>();

  displayedColumns = ['name', 'status'];

  files: UploadFile[] = [];
  importResults: ImportResult[] = [];

  filesDataSource = new MatTableDataSource(this.files);
  importTaskDataSource: ImportTaskDataSource;
  options: UploaderOptions;

  uploadInput: EventEmitter<UploadInput>;

  constructor(private oktaAuth: OktaAuthService, private dialog: MatDialog, private importTaskService: ImportTaskService, public appConfig: AppConfigService) {
    this.options = {concurrency: 1};
    this.files = [];
    this.uploadInput = new EventEmitter<UploadInput>();
    this.importTaskDataSource = new ImportTaskDataSource(this.importTaskService);
  }


  onUploadOutput(output: UploadOutput): void {
    if (output.type === 'addedToQueue' && typeof output.file !== 'undefined') {
      this.files.push(output.file);
      this.filesDataSource = new MatTableDataSource<UploadFile>(this.files);
    } else if (output.type === 'uploading' && typeof output.file !== 'undefined') {
      const index = this.files.findIndex(file => typeof output.file !== 'undefined' && file.id === output.file.id);
      this.files[index] = output.file;
    } else if (output.type == 'done') {
      this.importResults[output.file.id] = output.file.response;
      this.refreshImportTasks();
      this.complete.emit(output.file);
    }
  }

  hasUnimportedFiles(): boolean {
    return this.files.map(file => file.progress.status)
      .filter(status => status !== 2).length === 0;
  }

  exportRecords(): void {
    this.importTaskService.exportRecords(this.importerName);
  }

  changePage(event) {
    let page = new Page(event.pageIndex, event.pageSize, event.length);
    this.refreshImportTasks(page);
  }

  showImportResults(importTask: ImportTask) {
    let importResultData = new MatDialogConfig<any>();
    importResultData.data = importTask;
    this.dialog.open(ImportResultDialogComponent, importResultData);
  }

  importRecords(): void {
    this.oktaAuth.getAccessToken()
      .then(token => {
        const event: UploadInput = {
          type: 'uploadAll',
          url: 'api/import?importer=' + this.importerName,
          method: 'POST',
          headers: {Authorization: `Bearer ${token}`}
        };

        this.uploadInput.emit(event);
      })
  }

  ngOnInit(): void {
    this.refreshImportTasks();
  }

  public refreshImportTasks(page?: Page) {
    this.importTaskDataSource.loadImportTasks(this.importerName, page);
  }


}
