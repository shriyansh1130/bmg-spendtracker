import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {ConfirmationDialogComponent} from "../confirmation-dialog/confirmation-dialog.component";
import {ImportTask} from "../import-task/import-task.model";

@Component({
  selector: 'bst-import-result-dialog',
  templateUrl: './import-result-dialog.component.html',
  styleUrls: ['./import-result-dialog.component.scss']
})
export class ImportResultDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public importTask: ImportTask,
              private dialogRef: MatDialogRef<ConfirmationDialogComponent>) {
  }

  ngOnInit() {
  }

}
