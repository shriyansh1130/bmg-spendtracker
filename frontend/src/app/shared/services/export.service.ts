import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

enum ProgressStatus {
  NONE = -1,
  FILENAME_REQUESTED,
  DONWLOADING,
  COMPLETED
}

const fileUrl = 'api/orders/excel';

@Injectable()
export class ExportService {

  private _excelDownloadProgress = ProgressStatus.NONE;
  set excelDownloadProgress(value) {
    this._excelDownloadProgress = value;
    this.excelDownloadPercent = Math.round((this.excelDownloadMaxSteps / value)*100);
  }
  get excelDownloadProgress() {
    return this._excelDownloadProgress;
  }

  public progressStatus = ProgressStatus;
  public excelDownloadMaxSteps = ProgressStatus.COMPLETED;
  public excelDownloadPercent = 0;

  constructor(private httpClient: HttpClient) { }

  downloadOrders(purchaseOrderPositionIds: string[]) {

    this.excelDownloadProgress = ProgressStatus.FILENAME_REQUESTED;

    this.httpClient.get(fileUrl, {
      observe: 'response',
      responseType: 'blob',
      params: {purchaseOrderId: purchaseOrderPositionIds}
    })
      .subscribe(resp => {
        this.excelDownloadProgress = ProgressStatus.DONWLOADING;

        const contentDispositionHeader = resp.headers.get('Content-Disposition');
        const result = contentDispositionHeader.split(';')[1].trim().split('=')[1];
        const fileName = result.replace(/"/g, '');
        const objectUrl = window.URL.createObjectURL(resp.body);
        const anchor = document.createElement("a");
        document.body.appendChild(anchor);
        anchor.href = objectUrl;
        anchor.download = fileName + '.xlsx';
        anchor.click();

        setTimeout(() => {
          window.URL.revokeObjectURL(objectUrl);
          document.body.removeChild(anchor);
          this.excelDownloadProgress = ProgressStatus.COMPLETED;
        }, 0);

        setTimeout(() => this.excelDownloadProgress = ProgressStatus.NONE, 5000);
      });
  }
}
