import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BstMaterialModule} from "../bst-material/bst-material.module";
import {PrefixedLinkListComponent} from './prefixed-link-list/prefixed-link-list.component';
import {RouterModule} from "@angular/router";
import {ReactiveFormsModule} from "@angular/forms";
import {FlexLayoutModule} from '@angular/flex-layout';
import {ErrorDialogComponent} from './error-dialog/error-dialog.component';
import {LoadingSpinnerOverlayComponent} from './loading-spinner-overlay/loading-spinner-overlay.component';
import {ConfirmationDialogComponent} from './confirmation-dialog/confirmation-dialog.component';
import {CustomPaginatorComponent} from './custom-paginator/custom-paginator.component';
import {ActiveRouteTitleComponent} from './active-route-title/active-route-title.component';
import {NgUploaderModule} from "ngx-uploader";
import {ImportResultDialogComponent} from './import-result-dialog/import-result-dialog.component';
import {ImportRecordsComponent} from './import-records/import-records.component';
import {ImportTaskService} from "./import-task/import-task.service";
import { RawPostingSyncResultDialogComponent } from './raw-posting-sync-result-dialog/raw-posting-sync-result-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FlexLayoutModule,
    BstMaterialModule,
    ReactiveFormsModule, NgUploaderModule
  ],
  providers: [ImportTaskService],
  exports: [PrefixedLinkListComponent, LoadingSpinnerOverlayComponent, CustomPaginatorComponent, ActiveRouteTitleComponent, NgUploaderModule, ImportRecordsComponent],
  declarations: [PrefixedLinkListComponent, RawPostingSyncResultDialogComponent, ErrorDialogComponent, LoadingSpinnerOverlayComponent, ConfirmationDialogComponent, CustomPaginatorComponent, ActiveRouteTitleComponent, ImportResultDialogComponent, ImportRecordsComponent, RawPostingSyncResultDialogComponent],
  entryComponents: [ErrorDialogComponent, RawPostingSyncResultDialogComponent, LoadingSpinnerOverlayComponent, ImportResultDialogComponent]
})
export class SharedModule {
}
