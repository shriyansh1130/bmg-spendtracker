import {AbstractControl, ValidatorFn} from "@angular/forms";

/**
 * Validator that checks for 'international' decimal pattern using a dot as delimiter
 * @param {number} decimals Number of decimals to allow. Defaults to two (2). Pun intended.
 * @returns {ValidatorFn}
 */
export function intlDecimalFormatValidator(decimals: number): ValidatorFn {
  if(decimals == 0){
    return (control: AbstractControl): { [key: string]: any } => {
      let pattern = '^[0-9]+$';
      let regexp = new RegExp(pattern);
      return !regexp.test(control.value) ? {'intlDecimalFormat': {value: control.value}} : null;
    };
  }else{
    return (control: AbstractControl): { [key: string]: any } => {
      let pattern = '^[0-9]+(\\.[0-9]{0,' + (decimals ? Math.round(decimals) : 2) + '})?$';
      let regexp = new RegExp(pattern);
      return !regexp.test(control.value) ? {'intlDecimalFormat': {value: control.value}} : null;
    };
  }
}

