import {Component, Inject} from "@angular/core";
import {MAT_DIALOG_DATA} from "@angular/material";

@Component({
  selector: 'bst-error-dialog',
  templateUrl: 'error-dialog.component.html',
  styleUrls: ['error-dialog.component.scss']
})
export class ErrorDialogComponent {
  errorMessages = [];
  description = "Your last action resulted in the following error:";

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    this.errorMessages = data.errors || [];
    this.description = data.description;
    let gError = data.generalError;
    if(gError) {
      if(gError.hasOwnProperty("message")) {
        this.errorMessages.push(gError.message);
      }
    }
  }
}
