import {Component, OnInit} from '@angular/core';
import {Location} from "@angular/common";
import {NavigationEnd, Router} from "@angular/router";

@Component({
  selector: 'bst-active-route-title',
  templateUrl: './active-route-title.component.html',
  styleUrls: ['./active-route-title.component.scss']
})
export class ActiveRouteTitleComponent implements OnInit {

  title: string = '';

  constructor(private location: Location, private router: Router) {
  }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd){
        this.setTitle(event.url);
      }
    });
  }

  setTitle(url: string) {
    switch(true) {
      case url.startsWith('/orders/create'): {
        this.title = 'Create Purchase Order';
        break;
      }
      case url.startsWith('/orders'): {
        this.title = 'Purchase Orders';
        break;
      }
      case url.startsWith('/budget-categories'): {
        this.title = 'Budget Categories';
        break;
      }
      case url.startsWith('/exchange-rates'): {
        this.title = 'Exchange Rates';
        break;
      }
      case url.startsWith('/projects'): {
        this.title = 'Projects';
        break;
      }
      case url.startsWith('/currencies'): {
        this.title = 'Currencies';
        break;
      }
      case url.startsWith('/companies'): {
        this.title = 'Companies';
        break;
      }
      case url.startsWith('/countries'): {
        this.title = 'Countries';
        break;
      }
      default: {
        this.title = '';
        break;
      }
    }
  }

}
