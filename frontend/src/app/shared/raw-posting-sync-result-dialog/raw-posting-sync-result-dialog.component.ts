import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {ConfirmationDialogComponent} from "@app/shared/confirmation-dialog/confirmation-dialog.component";
import {RawPostingSyncTask} from "@app/raw-posting/raw-posting.model";

@Component({
  selector: 'bst-raw-posting-sync-result-dialog',
  templateUrl: './raw-posting-sync-result-dialog.component.html',
  styleUrls: ['./raw-posting-sync-result-dialog.component.scss']
})
export class RawPostingSyncResultDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public syncTask: RawPostingSyncTask,
              private dialogRef: MatDialogRef<ConfirmationDialogComponent>) {
  }
  ngOnInit() {
  }

}
