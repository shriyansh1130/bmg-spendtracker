import {AfterContentInit, Directive, Host, Input, Self} from '@angular/core';
import {MatAutocomplete, MatAutocompleteSelectedEvent, MatAutocompleteTrigger} from "@angular/material";
import {Subscription} from "rxjs";
import {NgControl} from "@angular/forms";

@Directive({
  selector: '[bstForceAutocomplete]'
})
export class ForceAutocompleteDirective implements AfterContentInit {
  @Input() matAutocomplete: MatAutocomplete;
  @Input() optionKeyName = "id";

  subscription: Subscription;

  constructor(@Host() @Self() private readonly autoCompleteTrigger: MatAutocompleteTrigger, private controlWrapper: NgControl) {
  }

  ngAfterContentInit() {
    if (this.controlWrapper === undefined) {
      throw Error('inputCtrl @Input should be provided ')
    }

    if (this.matAutocomplete === undefined) {
      throw Error('valueCtrl @Input should be provided ')
    }

    this.autoCompleteTrigger.registerOnTouched(() => {
      this.checkForSelection();
      return {}
    });

    setTimeout(() => {
      this.subscribeToClosingActions();
      this.handleSelection();
    }, 0);
  }

  private subscribeToClosingActions(): void {
    if (this.subscription && !this.subscription.closed) {
      this.subscription.unsubscribe();
    }

    this.subscription = this.autoCompleteTrigger.panelClosingActions
      .subscribe((e) => {
          if (!e || !e.source) {
            this.checkForSelection();
          }
        },
        err => this.subscribeToClosingActions(),
        () => this.subscribeToClosingActions());
  }

  private checkForSelection() {
    const selected = this.matAutocomplete.options
      .map(option => option.value)
      .find(option => this.controlWrapper.value && option[this.optionKeyName] === this.controlWrapper.value[this.optionKeyName]);

    if (selected == null) {
      if (this.matAutocomplete.options.length > 0) {
        this.controlWrapper.control.setValue(null);
      } else {
        this.controlWrapper.control.setErrors({'unknown': true});
        this.controlWrapper.control.markAsTouched()
      }
    }
  }

  private handleSelection() {
    this.matAutocomplete.optionSelected.subscribe((e: MatAutocompleteSelectedEvent) => {
      this.controlWrapper.control.setValue(e.option.value);
    });
  }
}
