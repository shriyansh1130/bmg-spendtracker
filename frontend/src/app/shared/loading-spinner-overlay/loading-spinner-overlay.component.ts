import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'bst-loading-spinner-overlay',
  templateUrl: './loading-spinner-overlay.component.html',
  styleUrls: ['./loading-spinner-overlay.component.scss']
})
export class LoadingSpinnerOverlayComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
