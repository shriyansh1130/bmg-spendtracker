import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

export interface ConfirmationDialogData {
  title: string;
  description: string;
}

@Component({
  selector: 'bst-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss']
})
export class ConfirmationDialogComponent implements OnInit {
  public title : string;
  public description: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: ConfirmationDialogData,
              private dialogRef:MatDialogRef<ConfirmationDialogComponent>) {
    this.title = data.title || "Confirm";
    this.description = data.description || "Are you sure?";
  }

  ngOnInit() {
  }

}
