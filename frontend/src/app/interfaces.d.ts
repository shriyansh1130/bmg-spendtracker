export type PurchaseOrderStatus = "Draft" | "Open" | "Closed";

export interface ITerritory {
  id: string,
  name: string,
  code: string,
  currency: ICurrency,
  vatRate: number,
  sapCountry: boolean
}

export interface ICurrency {
  id: string,
  name: string,
  code: string,
  symbol: string,
  available: boolean,
}

export interface IVendor {
  companyCode: string,
  countryCode: string,
  group: string,
  vendorCode: string,
  accountNumber: string,
  name1: string,
  name2: string,
  name3: string,
  street: string,
  houseNumber: string,
  city: string,
  rg: string,
  postCode: string,
  vatRegistrationNumber: string
  reconAcct: string
}

export interface IProject {
  name: string
  sapClientCode: string,
  roCompanyCode: string,
  ionSap: string,
  assignment: string,
  label: string,
  dealType: string,
  roTerritory: string,
  segment: string
}


export interface ICategory {
  id: string,
  title: string,
  displayOrder: number
}

export interface ISubCategory extends ICategory {
  category: ICategory,
}

export interface ISpendType {
  id: string,
  name: string,
  code: string,
  subCategory: ISubCategory,
  ppdRecoubable: boolean,
  ppdRecoupmentPct: number,
}
