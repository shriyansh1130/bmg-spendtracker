import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CurrencyRoutingModule} from './currency-routing.module';
import {CurrencyService} from './currency.service';
import {CurrencyListComponent} from './currency-list/currency-list.component';
import {SharedModule} from "../shared";
import {BstMaterialModule} from "../bst-material/bst-material.module";
import {CurrencyValueDisplayComponent} from './currency-value-display/currency-value-display.component';
import {CurrencyImportComponent} from "./currency-import/currency-import.component";
import {AuthModule} from "../auth/auth.module";

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BstMaterialModule,
    CurrencyRoutingModule,
    AuthModule
  ],
  exports: [CurrencyValueDisplayComponent],
  declarations: [CurrencyListComponent, CurrencyValueDisplayComponent, CurrencyImportComponent],
  providers: [CurrencyService]
})
export class CurrencyModule { }
