import {AfterViewInit, Component, EventEmitter, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {CurrencyService} from "../currency.service";
import {MatSort, PageEvent} from "@angular/material";
import {CurrencyTableDataSource} from '../CurrencyTableDataSource';
import {AppConfigService} from '../../core/services/app-config.service';
import {merge} from "rxjs";
import {tap} from "rxjs/operators";
import {CustomPaginatorComponent} from "../../shared/custom-paginator/custom-paginator.component";

@Component({
  selector: 'bst-currency-list',
  templateUrl: './currency-list.component.html',
  styleUrls: ['./currency-list.component.scss']
})
export class CurrencyListComponent implements OnInit, AfterViewInit {

  @ViewChildren(CustomPaginatorComponent) paginators:QueryList<CustomPaginatorComponent>;
  @ViewChild(MatSort) sort: MatSort;
  dataSource:CurrencyTableDataSource;
  pageChanged = new EventEmitter<number>();


  constructor(public appConfig:AppConfigService, private currencyService: CurrencyService) { }

  ngOnInit() {
    this.dataSource = new CurrencyTableDataSource(this.appConfig, this.currencyService);
    this.dataSource.loadCurrencies();
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.goToPageZero());

    merge(this.sort.sortChange, this.pageChanged)
      .pipe(
        tap(() => this.loadCurrenciesPage())
      ).subscribe();
  }

  loadCurrenciesPage() {
    this.dataSource.loadCurrencies(this.sort.active ,this.sort.direction, this.paginators.first.pageIndex, this.paginators.first.pageSize);
  }

  changePage(event: PageEvent) {
    this.paginators.forEach((p) => {p.pageIndex = event.pageIndex, p.pageSize = event.pageSize});
    this.pageChanged.emit(event.pageIndex);
  }

  private goToPageZero() {
    this.paginators.forEach(p => p.pageIndex = 0);
  }

}
