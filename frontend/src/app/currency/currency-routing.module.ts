import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CurrencyListComponent} from "./currency-list/currency-list.component";
import {OktaAuthGuard} from "@okta/okta-angular";
import {CurrencyImportComponent} from "./currency-import/currency-import.component";

const routes: Routes = [
  {path: 'currencies', component: CurrencyListComponent, canActivate: [OktaAuthGuard]},
  {path: 'currencies/import', component: CurrencyImportComponent, canActivate: [OktaAuthGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CurrencyRoutingModule { }
