import {Injectable} from '@angular/core';
import {ResourceService} from "../resource/resource.service";
import {Currency} from "./currency.model";
import {PageableCollection} from '../resource/pageable-collection';
import {Page} from '../resource/page';
import {Country} from "../country/country.model";
import {BehaviorSubject, Observable, from as fromPromise} from "rxjs";
import {map} from "rxjs/operators";
import {AppConfigService} from "@app/core/services/app-config.service";


@Injectable()
export class CurrencyService {

  private _currencies: BehaviorSubject<PageableCollection<Currency>> = new BehaviorSubject(new PageableCollection());

  public readonly currencies: Observable<Currency[]> = this._currencies.asObservable().pipe(
    map(value => value.elements || [])
  );

  constructor(private resourceService:ResourceService, private appConfig: AppConfigService) {
    this.initialLoad();
  }

  private initialLoad() {
    this.loadCurrencies();
  }


  loadCurrencies() {
    return this.resourceService.findMany(Currency, {sort: 'currencyCode, asc'}).then(value => {
      return this._currencies.next(value);
    });
  }

  public loadCurrenciesWithPagination(page=0, size=this.appConfig.defaultPageSize, fieldName="currencyName", sort='desc' ): Observable<PageableCollection<Currency>> {
    return fromPromise(this.resourceService.findMany(Currency,
      {page: page, size: size, sort: `${fieldName},${sort}`}));
  }

  getAllCountryCurrencies(){
    return this.resourceService.findMany(Country, {projection: 'countryCurrencyProjection'}, new Page(0, 300, 1));
  }

}
