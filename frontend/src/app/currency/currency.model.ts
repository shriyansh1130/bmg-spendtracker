import {Resource} from "../resource/resource";

export class Currency extends Resource implements ICurrency{
  public static PATH: string = "api/currencies";

  constructor(
    public currencyName?: string,
    public currencyCode?: number,
    public currencySymbol?: string,
    public createdBy?: string,
    public modifiedBy?: string,
    public modifiedAt?: string
  ) {
    super();
  }

}

export interface ICurrency {
  currencyName?: string,
  currencyCode?: number,
  currencySymbol?: string,
}
