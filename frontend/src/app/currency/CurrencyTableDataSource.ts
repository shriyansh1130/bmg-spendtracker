import {BehaviorSubject, Observable, of} from "rxjs";
import {PageableCollection} from "../resource/pageable-collection";
import {IPagedTableDataSource} from '../resource/IPagedTableDataSource';
import {AppConfigService} from '../core/services/app-config.service';
import {Currency} from './currency.model';
import {CurrencyService} from './currency.service';
import {CollectionViewer} from "@angular/cdk/collections";
import {catchError, finalize, map} from "rxjs/operators";

export class CurrencyTableDataSource implements IPagedTableDataSource<Currency> {
  private _currencySubject = new BehaviorSubject<PageableCollection<Currency>>(new PageableCollection());
  private _loadingSubject = new BehaviorSubject<boolean>(false);

  public readonly totalCurrencyCount$ = this._currencySubject.asObservable().pipe(
    map((p) =>  p.page && p.page.totalElements || 0)
  );
  public readonly loading$ = this._loadingSubject.asObservable();


  constructor(private appConfig:AppConfigService, private currencyService: CurrencyService) {

  }

  connect(collectionViewer: CollectionViewer): Observable<Currency[]> {
    return this._currencySubject.asObservable().pipe(
      map((page) => page.elements )
    )
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this._currencySubject.complete();
    this._loadingSubject.complete();
  }

  public loadCurrencies(sortField='currencyName', sortDirection = 'desc', page = 0, pageSize:number = this.appConfig.defaultPageSize) {
    this._loadingSubject.next(true);

    this.currencyService.loadCurrenciesWithPagination(page, pageSize, sortField, sortDirection).pipe(
      catchError(() => of([])),
      finalize(() => this._loadingSubject.next(false))
    )
      .subscribe(currencies => {
       this._currencySubject.next(<PageableCollection<Currency>>currencies)
      });
  }

}
