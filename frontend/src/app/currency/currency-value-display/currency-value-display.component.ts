import {Component, Input} from '@angular/core';
import {Currency} from "../currency.model";

@Component({
  selector: 'bst-currency-value-display',
  template: `<span class="bst-currency-display">{{currency? (value | currency:currency.currencyCode:'symbol-narrow':getFormat()) :  (value | number:getFormat()) }}</span> `,
  styles: ['.bst-currency-display { word-wrap: normal; text-align: right}']
})
export class CurrencyValueDisplayComponent {
  @Input() value: number;
  @Input() maxDecimalPlaces: number = 3;
  @Input() currency: Currency;


  constructor() { }

  getFormat() {
    return '1.2-' + this.maxDecimalPlaces;
  }
}
