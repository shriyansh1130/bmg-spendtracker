import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CurrencyValueDisplayComponent} from './currency-value-display.component';

describe('CurrencyValueDisplayComponent', () => {
  let component: CurrencyValueDisplayComponent;
  let fixture: ComponentFixture<CurrencyValueDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrencyValueDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrencyValueDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show 3 decimal places as default if necessary', () => {
    component.value = 0.005;
    fixture.detectChanges();
    const span: HTMLElement = fixture.nativeElement;
    expect(span.textContent).toContain('0.005');
  })

  it('should round to 2 decimal places if max decimals defined as 2', () => {
    component.value = 0.005;
    component.maxDecimalPlaces = 2;
    fixture.detectChanges();
    const span: HTMLElement = fixture.nativeElement;
    expect(span.textContent).toContain('0.01');
  })
});
