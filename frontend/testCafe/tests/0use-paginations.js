import {Selector} from 'testcafe';
import OktaPage from '../pages/okta-page';
import FramingPage from "../pages/framing-page";
import PaginationFragment from "../pages/pagination-fragment";

//Page Objects
const oktaPage = new OktaPage();
const paginationFragment = new PaginationFragment();
const framingPage = new FramingPage();

fixture `BMG Spend Tracker`
  .page `http://localhost:4200`
  .beforeEach( async t => {
    await t.resizeWindow( 1300, 700 );
    await oktaPage.login(t);
  });

test('Test Pagination Page Size Change', async t => {
  await t.setTestSpeed(0.75); //Paginations perform wonky at 1.0 speed if is only a small amount of items
  await framingPage.openPurchaseOrders(t);
  await paginationFragment.autoCheckPageSizeSelects(t);
  await framingPage.openProjects(t);
  await paginationFragment.autoCheckPageSizeSelects(t);
  await framingPage.openExchangeRates(t);
  await paginationFragment.autoCheckPageSizeSelects(t);
  await framingPage.openCurrencies(t);
  await paginationFragment.autoCheckPageSizeSelects(t);
  await framingPage.openCompanies(t);
  await paginationFragment.autoCheckPageSizeSelects(t);
  await framingPage.openCountries(t);
  await paginationFragment.autoCheckPageSizeSelects(t);
  await t.setTestSpeed(1);
});

test('Test Pagination Next Previous', async t => {
  await framingPage.openPurchaseOrders(t);
  await paginationFragment.autoCheckPaginatorRanges(t);
  await framingPage.openProjects(t);
  await paginationFragment.autoCheckPaginatorRanges(t);
  await framingPage.openExchangeRates(t);
  await paginationFragment.autoCheckPaginatorRanges(t);
  await framingPage.openCurrencies(t);
  await paginationFragment.autoCheckPaginatorRanges(t);
  await framingPage.openCompanies(t);
  await paginationFragment.autoCheckPaginatorRanges(t);
  await framingPage.openCountries(t);
  await paginationFragment.autoCheckPaginatorRanges(t);
});

