import { Selector } from 'testcafe';
import OktaPage from '../pages/okta-page';
import ProjectsPage from '../pages/projects-page';
import CreatePoPage from "../pages/create-po-page";
import FramingPage from "../pages/framing-page";
import PurchaseOrdersPage from "../pages/purchase-orders-page";

//Page Objects
const oktaPage = new OktaPage();
const projectsPage = new ProjectsPage();
const purchaseOrdersPage = new PurchaseOrdersPage();
const framingPage = new FramingPage();

fixture `BMG Spend Tracker`
  .page `http://localhost:4200`
  .beforeEach( async t => {
    await t.resizeWindow( 1300, 700 );
    await oktaPage.login(t);
  });

test('Test Projects Page Filters', async t => {
  await framingPage.openProjects(t);
  await projectsPage.filterByProject(t, 'you');
  await projectsPage.testFilterByProject(t, 'you');
  await projectsPage.filterByProject(t, '%');
  await projectsPage.filterBySAPClientCode(t, '2');
  await projectsPage.testFilterBySAPClientCode(t, '2');
});

test('Test Purchase Orders Page Filters', async t => {
  await framingPage.openPurchaseOrders(t);
  await purchaseOrdersPage.filterByProject(t, 'you');
  await purchaseOrdersPage.testFilterByProject(t, 'you');
  await purchaseOrdersPage.filterByProject(t, '%');
  await purchaseOrdersPage.filterByPoNumber(t, '2');
  await purchaseOrdersPage.testFilterByPoNumber(t, '2');
  await purchaseOrdersPage.filterByPoNumber(t, '%');
  await purchaseOrdersPage.filterByTerritory(t, 'un');
  await purchaseOrdersPage.testFilterByTerritory(t, 'un');
  await purchaseOrdersPage.filterByTerritory(t, '%');
  await purchaseOrdersPage.filterByVendor(t, 'e');
  await purchaseOrdersPage.testFilterByVendor(t, 'e');
  await purchaseOrdersPage.filterByVendor(t, '%');
});
