import OktaPage from '../pages/okta-page';
import ProjectsPage from '../pages/projects-page';
import CreatePoPage from "../pages/create-po-page";
import FramingPage from "../pages/framing-page";
import PurchaseOrdersPage from "../pages/purchase-orders-page";

//Page Objects
const oktaPage = new OktaPage();
const projectsPage = new ProjectsPage();
const createPoPage = new CreatePoPage();
const purchaseOrdersPage = new PurchaseOrdersPage();
const framingPage = new FramingPage();

//Variables
const ukProjectWithNonInternationalRO = 'UK-PROJECT-WITH-NON-INTERNATIONAL-REPERTOIRE-OWNER'; // Project of short list with non interantional GB Company
const scandinavianProjectWithScandinavianRO = "PROJECT-WITH-NON-UK-REPERTOIRE-OWNER"; //BMG Rights Management Scandinavia AB -> would use International GB Company for as linked Company for vendor
const territoryVar = 'United Kingdom';

fixture `BMG Spend Tracker`
  .page `http://localhost:4200`
  .beforeEach(async t => {
    await  t.resizeWindow(1300, 700);
    await  oktaPage.login(t);
  });

test('Create a vendor with existing vat reg nr for other company', async t => {
  const vendorName = "Testvendor-" + Date.now();
  const vatNumber = "" + Date.now();
  await framingPage.openProjects(t);
  await projectsPage.checkCreatePoButtonDisabled(t, true);
  await projectsPage.startPoCreation(t, ukProjectWithNonInternationalRO, territoryVar);
  await framingPage.checkHeaderText(t, 'Create Purchase Order');
  await createPoPage.clickAddVendor(t);
  // create Vendor for non International UK Company
  await createPoPage.saveNewVendor(t, vendorName, vatNumber);
  await createPoPage.confirmVendorCreation(t, vendorName, vatNumber);
  await framingPage.openProjects(t);

  await projectsPage.startPoCreation(t, scandinavianProjectWithScandinavianRO, territoryVar);
  // create same vendor for international UK Company -> derived because repertoire owner is no uk company
  // (should work with same VAT but different company)
  await createPoPage.clickAddVendor(t);
  await createPoPage.saveNewVendor(t, vendorName, vatNumber);
  await createPoPage.confirmVendorCreation(t, vendorName, vatNumber);

  // Vendor now exists for Vat number and international uk company -> should not be created
  await createPoPage.clickAddVendor(t);
  await createPoPage.saveNewVendor(t, "anything", vatNumber);
  await createPoPage.confirmExistingVendorSelection(t, vendorName, vatNumber);
});
