import OktaPage from '../pages/okta-page';
import ProjectsPage from '../pages/projects-page';
import CreatePoPage from "../pages/create-po-page";
import FramingPage from "../pages/framing-page";
import PurchaseOrdersPage from "../pages/purchase-orders-page";

//Page Objects
const oktaPage = new OktaPage();
const projectsPage = new ProjectsPage();
const createPoPage = new CreatePoPage();
const purchaseOrdersPage = new PurchaseOrdersPage();
const framingPage = new FramingPage();

//Variables
const projectVar = 'Dead!';
const territoryVar = 'United Kingdom';
const vendorVar = 'We Folk';
const otherProjectVar = 'Brooke Fraser';
const spendTypeVar = ['451', 'Press PR'];
const priceVar = '16';
const quantityVar = '8';
const descriptionVar = 'This is a test description. It has no further purpose.';
const serviceMonthVar = 'Dec-2020';
const invalidQuantityVars = ['0','1.4', '1000000'];

fixture `BMG Spend Tracker`
  .page `http://localhost:4200`
  .beforeEach( async t => {
    await t.resizeWindow( 1300, 700 );
    await oktaPage.login(t);
  });

test('Use the New Po Button to create a PO', async t => {
  await framingPage.openPurchaseOrders(t);
  await purchaseOrdersPage.sortByStatus(t);
  const selectedProject = await purchaseOrdersPage.getProjectInLine(t, 1);
  const selectedTerritory = await purchaseOrdersPage.getTerritoryInLine(t, 1);
  await purchaseOrdersPage.openPo(t,1);
  await framingPage.checkHeaderText(t, 'Purchase Orders');
  await purchaseOrdersPage.createNewPo(t);
  await createPoPage.checkProject(t, selectedProject);
  await createPoPage.checkTerritory(t, selectedTerritory);
  await createPoPage.checkStatus(t, 'Draft');
  await createPoPage.checkPoCurrencyFilled(t);
});


test('Create a PO', async t => {
  await framingPage.openProjects(t);
  await projectsPage.checkCreatePoButtonDisabled(t, true);
  await projectsPage.startPoCreation(t, projectVar, territoryVar);
  await framingPage.checkHeaderText(t, 'Create Purchase Order');
  await createPoPage.checkAddVendorButtonExists(t, true);
  await createPoPage.checkAddPoLineButtonExists(t, true);
  await createPoPage.checkPreFilledFields(t, projectVar, territoryVar);
  await createPoPage.fillInVendor(t, vendorVar);
  await createPoPage.editFirstPoLine(t);
  await createPoPage.tryToSaveUnfilledForm(t);
  await createPoPage.cancelPoLineForm(t);
  for (let invalidQuantity of invalidQuantityVars){
    await createPoPage.editFirstPoLine(t);
    await createPoPage.tryToEnterInvalidQuantity(t, invalidQuantity);
    await createPoPage.cancelPoLineForm(t);
  }
  await createPoPage.editFirstPoLine(t);
  await createPoPage.fillFirstPoLineForm(t, projectVar, spendTypeVar, priceVar, quantityVar, descriptionVar, serviceMonthVar);
  await createPoPage.cancelPoLineForm(t);
  await createPoPage.checkPreFilledFields(t);
  await createPoPage.editPoLine(t, 1);
  await createPoPage.fillFirstPoLineForm(t, projectVar, spendTypeVar, priceVar, quantityVar, descriptionVar, serviceMonthVar);
  await createPoPage.savePoLineForm(t);
  await t.wait(1000); // needs to wait as we now save the whole po and do a page transition
  await createPoPage.saveSuccessfulMessageExist(t);
  await createPoPage.checkLastFilledLine(t, 1);
  await createPoPage.addPoLine(t);
  await createPoPage.fillPoLineForm(t, otherProjectVar, spendTypeVar, priceVar, quantityVar, descriptionVar, serviceMonthVar);
  await createPoPage.savePoLineForm(t);
  await createPoPage.checkLastFilledLine(t, 2);
  await createPoPage.checkBottomButtonExists(t, ['Save', 'Submit', 'Close Purchase Order']);
  await createPoPage.savePo(t);
  await createPoPage.checkNumberOfLinesInTable(t, 2);
  await createPoPage.deleteLine(t, 2);
  await createPoPage.checkNumberOfLinesInTable(t, 1);
  await createPoPage.deleteButtonShouldExist(t, true);
  await createPoPage.checkBottomButtonExists(t, ['Save', 'Submit', 'Close Purchase Order'], true);
  await createPoPage.checkStatus(t, 'Draft');
  await createPoPage.submitPo(t);
  await createPoPage.checkPoNumberNotDraft(t);
  await createPoPage.checkStatus(t, 'Open');
  await createPoPage.deleteButtonShouldExist(t, false);
  await createPoPage.checkBottomButtonExists(t, ['Submit'], false);
  await createPoPage.checkBottomButtonExists(t, ['Save', 'New PO', 'Close Purchase Order'], true);
  await createPoPage.closePo(t);
  await createPoPage.checkStatus(t, 'Closed');
  await createPoPage.checkVendorEnabled(t, false);
  await createPoPage.checkAddVendorButtonExists(t, false);
  await createPoPage.checkAddPoLineButtonExists(t, false);
  await createPoPage.editPoLine(t, 1);
  await createPoPage.checkEditFormFieldsNotEditable(t);
});
