import {Selector} from 'testcafe';
import OktaPage from '../pages/okta-page';
import FramingPage from "../pages/framing-page";
import MaterialTableFragment from "../pages/material-table-fragment";

//Page Objects
const oktaPage = new OktaPage();
const materialTableFragment = new MaterialTableFragment();
const framingPage = new FramingPage();

fixture `BMG Spend Tracker`
  .page `http://localhost:4200`
  .beforeEach( async t => {
    await t.resizeWindow( 1300, 700 );
    await oktaPage.login(t);
  });

test('Check Basic Data exists', async t => {
  await t.setTestSpeed(0.75);
  await framingPage.openPurchaseOrders(t);
  await materialTableFragment.checkNumberOfRowsBiggerThan(t, 2);
  await framingPage.openProjects(t);
  await materialTableFragment.checkNumberOfRowsBiggerThan(t, 2);
  await framingPage.openExchangeRates(t);
  await materialTableFragment.checkNumberOfRowsBiggerThan(t, 2);
  await framingPage.openCurrencies(t);
  await materialTableFragment.checkNumberOfRowsBiggerThan(t, 2);
  await framingPage.openCompanies(t);
  await materialTableFragment.checkNumberOfRowsBiggerThan(t, 2);
  await framingPage.openCountries(t);
  await materialTableFragment.checkNumberOfRowsBiggerThan(t, 2);
  await t.setTestSpeed(1);
});

