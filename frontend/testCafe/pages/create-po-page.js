import {Selector} from 'testcafe';

export default class CreatePoPage {

  constructor() {
    //Variables
    this.projectVar = 'The Legend of Zelda: Symphony of The Goddesses - CD/DVD';
    this.spendTypeVar = ['207', 'Website'];
    this.priceVar = '18';
    this.quantityVar = '2';
    this.descriptionVar = 'This is a test description. It has no further purpose.';
    this.serviceMonthVar = 'AUG-2020';


    //Generic
    this.autocompleteOptions = Selector('mat-option');
    this.confirmationDialogOkButton = Selector('bst-confirmation-dialog button').withText('Ok');
    this.confirmationDialogTitle = Selector('bst-confirmation-dialog .mat-dialog-title');
    this.confirmationDialogText = Selector('bst-confirmation-dialog mat-dialog-content div');
    this.snackbar = Selector('snack-bar-container > simple-snack-bar');

    //Form and Table
    this.poNumber = Selector('bst-purchase-order-header-form input[placeholder="PO #"]');
    this.territory = Selector('bst-purchase-order-header-form input[placeholder="Territory"]');
    this.status = Selector('bst-purchase-order-header-form input[placeholder="Status"]');
    this.poCurrency = Selector('mat-select[formcontrolname="currency"]');
    this.vendorAutocomplete = Selector('input[formcontrolname="vendor"]');
    this.addVendorButton = Selector('.vendor-select button');
    this.poLines = Selector('bst-purchase-order-position-list mat-row');
    this.firstPoLineProject = Selector('bst-purchase-order-position-list mat-row mat-cell.cdk-column-project');
    this.firstPoLine = this.poLines.nth(0);
    this.savePoButton = Selector('bst-purchase-order-button-bar button').withText('Save');
    this.submitPoButton = Selector('bst-purchase-order-button-bar button').withText('Submit');
    this.closePoButton = Selector('bst-purchase-order-button-bar button').withText('Close Purchase Order');
    this.addPoLineButton = Selector('mat-header-cell.mat-column-actions button');
    this.deletePoButton = Selector('bst-purchase-order-header-form button mat-icon').withText('delete_forever');

    //Vendor Dialog
    this.vendorDialogVendorNameAutocomplete = Selector('input[formcontrolname="vendorName"]');
    this.vendorDialogVatRegistrationNumber = Selector('input[formcontrolname="vatRegistrationNumber"]');
    this.vendorDialogSaveButton = Selector('bst-vendor-details-dialog button').withText('Save');


    //Edit Dialog
    this.project = Selector('bst-purchase-order-position-form input[formcontrolname="project"]');
    this.spendType = Selector('bst-purchase-order-position-form input[formcontrolname="spendType"]');
    this.price = Selector('bst-purchase-order-position-form input[formcontrolname="price"]');
    this.quantity = Selector('bst-purchase-order-position-form input[formcontrolname="quantity"]');
    this.description = Selector('bst-purchase-order-position-form textarea[formcontrolname="description"]');
    this.serviceMonth = Selector('bst-purchase-order-position-form input[formcontrolname="serviceMonth"]');
    this.saveEditDialogButton = Selector('button[type="submit"]');
    this.cancelEditDialogButton = Selector('button').withText('Cancel');
    this.dialogErrors = Selector('bst-purchase-order-position-dialog mat-error');

    //Bottom Action Buttons
    this.bottomButton = Selector('bst-purchase-order-button-bar button');

    //Close Confirmation Dialog
    this.okCloseButton = Selector('bst-confirmation-dialog button').withText('Ok');
  }

  async checkPreFilledFields(t, project = 'Dead!', territory = 'United Kingdom') {
    await
      t
        .expect(this.poNumber.getAttribute('ng-reflect-value')).eql('DRAFT')
        .expect(this.poNumber.withAttribute('disabled').exists).ok()
        .expect(this.territory.getAttribute('ng-reflect-value')).eql(territory)
        .expect(this.territory.withAttribute('disabled').exists).ok()
        .expect(this.status.getAttribute('ng-reflect-value')).eql('Draft')
        .expect(this.status.withAttribute('disabled').exists).ok()
        .expect(this.poCurrency.innerText).contains('GBP')
        .expect(this.firstPoLine.find('mat-cell.cdk-column-project').innerText).eql(project)
        .expect(this.firstPoLine.find('mat-cell.cdk-column-quantity').innerText).eql('1')
        .expect(this.firstPoLine.find('mat-cell.cdk-column-description').innerText).eql('')
        .expect(this.firstPoLine.find('mat-cell.cdk-column-serviceMonth').innerText).eql('')
  }

  async checkTerritory(t, territory) {
    await
      t
        .expect(this.territory.getAttribute('ng-reflect-value')).eql(territory)
        .expect(this.territory.withAttribute('disabled').exists).ok();
  }

  async checkProject(t, project) {
    await
      t
        .expect(this.firstPoLineProject.innerText).eql(project);
  }

  async checkStatus(t, status) {
    await
      t
        .expect(this.status.getAttribute('ng-reflect-value')).eql(status)
  }

  async checkPoCurrencyFilled(t) {
    await
      t
        .expect(this.poCurrency.innerText).ok();
  }

  async checkPoNumberNotDraft(t) {
    await
      t
        .expect(this.poNumber.getAttribute('ng-reflect-value')).notEql('Draft')
  }


  async fillInVendor(t, vendor = 'Kirk Lake') {
    await
      t
        .typeText(this.vendorAutocomplete, vendor)
        .click(this.autocompleteOptions.withText(vendor))
        .expect(this.vendorAutocomplete.value).contains(vendor);
  }

  async addPoLine(t) {
    await
      t
        .click(this.addPoLineButton);
  }

  async editFirstPoLine(t) {
    await
      t
        .click(this.poLines.nth(0))
        .expect(this.project.exists).notOk();
  }


  async editPoLine(t, lineNumber = 1) {
    await
      t
        .click(this.poLines.nth(lineNumber - 1));
  }

  async deleteLine(t, lineNumber) {
    await
      t
        .click(this.poLines.nth(lineNumber - 1).find('.mat-column-actions>button>span>mat-icon').withText('delete'));
  }


  async fillFirstPoLineForm(t, project = this.projectVar, spendType = this.spendTypeVar, price = this.priceVar, quantity = this.quantityVar, description = this.descriptionVar, serviceMonth = this.serviceMonthVar) {
    this.setFormVars(project, spendType, price, quantity, description, serviceMonth);
    await
      t
        .typeText(this.spendType, this.spendTypeVar[0])
        .click(this.autocompleteOptions.withText(this.spendTypeVar[0]))
        .typeText(this.price, this.priceVar)
        .selectText(this.quantity)
        .typeText(this.quantity, this.quantityVar)
        .typeText(this.description, this.descriptionVar)
        .selectText(this.serviceMonth)
        .typeText(this.serviceMonth, this.serviceMonthVar)
  }

  async fillPoLineForm(t, project = this.projectVar, spendType = this.spendTypeVar, price = this.priceVar, quantity = this.quantityVar, description = this.descriptionVar, serviceMonth = this.serviceMonthVar) {
    this.setFormVars(project, spendType, price, quantity, description, serviceMonth);
    await
      t
        .selectText(this.project)
        .typeText(this.project, this.projectVar)
        .click(this.autocompleteOptions.withText(this.projectVar))
        .selectText(this.spendType)
        .typeText(this.spendType, this.spendTypeVar[0])
        .click(this.autocompleteOptions.withText(this.spendTypeVar[0]))
        .typeText(this.price, this.priceVar)
        .selectText(this.quantity)
        .typeText(this.quantity, this.quantityVar)
        .selectText(this.description)
        .typeText(this.description, this.descriptionVar)
        .selectText(this.serviceMonth)
        .typeText(this.serviceMonth, this.serviceMonthVar)
  }

  setFormVars(project, spendType, price, quantity, description, serviceMonth) {
    this.projectVar = project;
    this.spendTypeVar = spendType;
    this.priceVar = price;
    this.quantityVar = quantity;
    this.descriptionVar = description;
    this.serviceMonthVar = serviceMonth;
  }

  async checkLastFilledLine(t, lineNumber) {
    await
      t
        .expect(this.poLines.nth(lineNumber - 1).find('mat-cell.cdk-column-project').innerText).eql(this.projectVar)
        .expect(this.poLines.nth(lineNumber - 1).find('mat-cell.cdk-column-spendType').innerText).contains(this.spendTypeVar[1])
        .expect(this.poLines.nth(lineNumber - 1).find('mat-cell.cdk-column-price').innerText).contains(this.priceVar)
        .expect(this.poLines.nth(lineNumber - 1).find('mat-cell.cdk-column-quantity').innerText).eql(this.quantityVar)
        .expect(this.poLines.nth(lineNumber - 1).find('mat-cell.cdk-column-total').innerText).contains((parseFloat(this.priceVar) * parseInt(this.quantityVar)).toString())
        .expect(this.poLines.nth(lineNumber - 1).find('mat-cell.cdk-column-description').innerText).eql(this.descriptionVar)
        .expect(this.poLines.nth(lineNumber - 1).find('mat-cell.cdk-column-serviceMonth').innerText).eql(this.serviceMonthVar)
  }

  async tryToEnterInvalidQuantity(t, quantity) {
    await
      t
        .selectText(this.quantity)
        .typeText(this.quantity, quantity)
        .click(this.saveEditDialogButton)
        .expect(this.dialogErrors.nth(2).innerText).contains('Value must be between 1 and 999999');
  }

  async tryToSaveUnfilledForm(t) {
    await
      t
        .click(this.saveEditDialogButton)
        .expect(this.dialogErrors.nth(0).innerText).contains('Please enter a valid Spend Type')
        .expect(this.dialogErrors.nth(1).innerText).contains('A price is required');
  }

  async savePoLineForm(t) {
    await
      t
        .click(this.saveEditDialogButton);
  }

  async cancelPoLineForm(t) {
    await
      t
        .click(this.cancelEditDialogButton);
  }

  async checkNumberOfLinesInTable(t, numberOfLines) {
    await
      t
        .expect(this.poLines.count).eql(numberOfLines);
  }

  async deleteButtonShouldExist(t, shouldExist = true) {
    await
      t
        .expect(this.deletePoButton.exists).eql(shouldExist);
  }

  async checkVendorEnabled(t, shouldBeEnabled = true) {
    await
      t
        .expect(this.vendorAutocomplete.withAttribute('disabled').exists).notEql(shouldBeEnabled)
  }

  async checkBottomButtonExists(t, buttons, shouldExist = true) {
    for (let button of buttons) {
      await
        t.expect(this.bottomButton.withText(button).exists).eql(shouldExist);
    }
  }

  async checkAddPoLineButtonExists(t, shouldExist = true) {
    await
      t.expect(this.addPoLineButton.exists).eql(shouldExist);
  }

  async checkEditFormFieldsNotEditable(t) {
    await
      t
        .expect(this.spendType.withAttribute('disabled').exists).ok()
        .expect(this.price.withAttribute('disabled').exists).ok()
        .expect(this.quantity.withAttribute('disabled').exists).ok()
        .expect(this.description.withAttribute('disabled').exists).ok()
        .expect(this.serviceMonth.withAttribute('disabled').exists).ok()
        .expect(this.saveEditDialogButton.exists).notOk();
  }

  async checkAddVendorButtonExists(t, shouldExist = true) {
    await
      t.expect(this.addVendorButton.exists).eql(shouldExist);
  }

  async savePo(t) {
    await
      t
        .click(this.savePoButton);
  }

  async submitPo(t) {
    await
      t
        .click(this.submitPoButton);
  }

  async closePo(t) {
    await
      t
        .click(this.closePoButton)
        .click(this.okCloseButton);
  }

  async clickAddVendor(t) {
    await
      t
        .click(this.addVendorButton);
  }

  async fillVendorDialogVendorNameFormField(t, vendorName) {
    await
      t
        .selectText(this.vendorDialogVendorNameAutocomplete)
        .typeText(this.vendorDialogVendorNameAutocomplete, vendorName)
  }

  async fillVendorDialogVatNumberFormField(t, vatNumber) {
    await
      t
        .selectText(this.vendorDialogVatRegistrationNumber)
        .typeText(this.vendorDialogVatRegistrationNumber, vatNumber)
  }

  async saveNewVendor(t, vendorName, vatNumber) {
    this.fillVendorDialogVendorNameFormField(t, vendorName);
    this.fillVendorDialogVatNumberFormField(t, vatNumber);
    await
      t
        .click(this.vendorDialogSaveButton)

  }

  async confirmVendorCreation(t) {
    await
      t.expect(this.confirmationDialogText.textContent).contains("Are you sure you wish to create the Vendor?")
        .click(this.confirmationDialogOkButton)
  };

  async confirmExistingVendorSelection(t) {
    await
      t.expect(this.confirmationDialogTitle.textContent).contains("VAT Registration Number already exists")
        .click(this.confirmationDialogOkButton)
  };


  async fillVendorDialogVendorNameFormFieldWithExisting(t, vendorName) {
    await
      t
        .selectText(this.vendorDialogVendorNameAutocomplete)
        .typeText(this.vendorDialogVendorNameAutocomplete, vendorName)
        .click(this.autocompleteOptions.withText(vendor))
        .expect(this.vendorDialogVendorNameAutocomplete.value).contains(vendorName);
  }

  async saveSuccessfulMessageExist(t) {
    await t
      .expect(this.snackbar.textContent).contains('Purchase Order saved.');
  }
}

