import { Selector } from 'testcafe';

export default class FramingPage {
  constructor() {
    this.loggedInUserPlaceholder = Selector('mat-toolbar mat-placeholder');
    this.headerTextSelector = Selector('mat-toolbar bst-active-route-title');

    //Sidenav Menu Items
    this.menuBudgetCategories = Selector('mat-sidenav mat-nav-list a[routerlink="/budget-categories"]');
    this.menuExchangeRates = Selector('mat-sidenav mat-nav-list a[routerlink="/exchange-rates"]');
    this.menuPurchaseOrders = Selector('mat-sidenav mat-nav-list a[routerlink="/orders"]');
    this.menuProjects = Selector('mat-sidenav mat-nav-list a[routerlink="/projects"]');
    this.menuCurrencies = Selector('mat-sidenav mat-nav-list a[routerlink="/currencies"]');
    this.menuCompanies = Selector('mat-sidenav mat-nav-list a[routerlink="/companies"]');
    this.menuCountries = Selector('mat-sidenav mat-nav-list a[routerlink="/countries"]');

  }

  async checkHeaderText(t, headerText) {
    await t.expect(this.headerTextSelector.innerText).contains(headerText);
  }

  async checkUserLoggedInShown(t, username = 'Florian Meyszies') {
    await t.expect(this.loggedInUserPlaceholder.innerText).contains(username);
  }

  async openBudgetCategories(t) { await t.click(this.menuBudgetCategories); }
  async openExchangeRates(t) { await t.click(this.menuExchangeRates); }
  async openPurchaseOrders(t) { await t.click(this.menuPurchaseOrders); }
  async openProjects(t) { await t.click(this.menuProjects); }
  async openCurrencies(t) { await t.click(this.menuCurrencies); }
  async openCompanies(t) { await t.click(this.menuCompanies); }
  async openCountries(t) { await t.click(this.menuCountries); }


}

