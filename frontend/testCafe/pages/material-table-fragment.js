import { Selector } from 'testcafe';

export default class MaterialTableFragment {
  constructor () {
    this.tableRows = Selector('mat-table mat-row');
  }

  async checkNumberOfRowsBiggerThan(t, numberOfRows){
    await t
      .expect(this.tableRows.count).gte(numberOfRows);
  }

}

