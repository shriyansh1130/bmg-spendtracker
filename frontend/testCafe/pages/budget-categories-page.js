import { Selector } from 'testcafe';

export default class BudgetCategoriesPage {
  constructor () {
    this.budgetCategoryTitleText = '';
    this.budgetSubCategoryTitleText = '';
    this.budgetSpendTypeTitleText = '';
    this.listItems = Selector('bst-budget-category-container mat-list-item');


    //Budget Category Column
    this.budgetCategoryTitle = Selector('bst-budget-category-container bst-budget-category-detail input[formcontrolname="title"]');
    this.budgetCategoryDisplayOrder = Selector('bst-budget-category-container bst-budget-category-detail input[formcontrolname="displayOrder"]');
    this.budgetCategorySaveButton = Selector('bst-budget-category-container bst-budget-category-detail button').withText('Save');//Budget Category Column
    //Budget Sub Category Column
    this.budgetSubCategoryTitle = Selector('bst-budget-category-container bst-budget-subcategory-detail input[formcontrolname="title"]');
    this.budgetSubCategoryDisplayOrder = Selector('bst-budget-category-container bst-budget-subcategory-detail input[formcontrolname="displayOrder"]');
    this.budgetSubCategorySaveButton = Selector('bst-budget-category-container bst-budget-subcategory-detail button').withText('Save');
    //Budget Spend Types
    this.budgetSpendTypeTitle = Selector('bst-budget-category-container bst-spend-type-detail input[formcontrolname="title"]');
    this.budgetSpendTypeCode = Selector('bst-budget-category-container bst-spend-type-detail input[formcontrolname="budgetSpendTypeCode"]');
    this.budgetSpendTypeRecoupPct = Selector('bst-budget-category-container bst-spend-type-detail input[formcontrolname="ppdRecoupmentPct"]');
    this.budgetSpendTypePPDCheckbox = Selector('bst-budget-category-container bst-spend-type-detail mat-checkbox');
    this.budgetSpendTypeSaveButton = Selector('bst-budget-category-container bst-spend-type-detail button').withText('Save');
    //Dialog
    this.dialogOkButton = Selector('bst-confirmation-dialog mat-dialog-actions button').withText('Ok');
  }

  getRndString() {
    return (4747+Math.floor(Math.random()*1000)).toString();
  }

  async createBudgetCategory (t, title = 'Test Budget CategoryASD') {
    let realTitle = title + this.getRndString();
    let realDisplayOrder = this.getRndString();
    this.budgetCategoryTitleText = realTitle;
    await t
      .typeText(this.budgetCategoryTitle, realTitle)
      .typeText(this.budgetCategoryDisplayOrder, realDisplayOrder)
      .click(this.budgetCategorySaveButton);
  }

  async createBudgetSubCategory (t, title = 'Test Budget Sub-Category') {
    let realTitle = title + this.getRndString();
    let realDisplayOrder = this.getRndString();
    this.budgetSubCategoryTitleText = realTitle;
    await t
      .typeText(this.budgetSubCategoryTitle, realTitle)
      .typeText(this.budgetSubCategoryDisplayOrder, realDisplayOrder)
      .click(this.budgetSubCategorySaveButton);
  }

  async createBudgetSpendType (t, title = 'Test Budget Spend Type', code = '4747') {
    let realTitle = title +this.getRndString();
    let realDisplayOrder = this.getRndString();
    this.budgetSpendTypeTitleText = title;
    if(code === '4747'){
      code = this.getRndString();
    }
    await t
      .typeText(this.budgetSpendTypeTitle, realTitle)
      .typeText(this.budgetSpendTypeCode, code)
      .typeText(this.budgetSpendTypeRecoupPct, '1')
      .click(this.budgetSpendTypePPDCheckbox)
      .click(this.budgetSpendTypeSaveButton);
  }

  async checkCreatedItems (t) {
    await t
      .click(this.listItems.withText(this.budgetCategoryTitleText))
      .click(this.listItems.withText(this.budgetSubCategoryTitleText))
      .click(this.listItems.withText(this.budgetSpendTypeTitleText));
  }

  async deleteCreatedItems (t) {
    await t
      .click(this.listItems.withText(this.budgetCategoryTitleText))
      .click(this.listItems.withText(this.budgetSubCategoryTitleText))
      .click(this.listItems.withText(this.budgetSpendTypeTitleText))
      .click(this.listItems.withText(this.budgetSpendTypeTitleText).find('mat-icon'))
      .click(this.dialogOkButton)
      .click(this.listItems.withText(this.budgetSubCategoryTitleText).find('mat-icon'))
      .click(this.dialogOkButton)
      .click(this.listItems.withText(this.budgetCategoryTitleText).find('mat-icon'))
      .click(this.dialogOkButton);
  }

}

