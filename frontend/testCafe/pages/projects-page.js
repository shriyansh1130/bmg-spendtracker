import {Selector} from 'testcafe';

export default class ProjectsPage {
  constructor () {
    //Header
    this.territorySelect = Selector('mat-select[aria-label="Territory"]');
    this.territorySelectOption = Selector('mat-option');
    this.createPoButton = Selector('button[routerlink="/orders/create"]');
    this.projectFilter = Selector('bst-custom-paginator input[placeholder="Filter By Project Name"]');
    this.sapClientCodeFilter = Selector('bst-custom-paginator input[placeholder="Filter By SAP Client Code"]');
    //Pagination
    this.pageSizeSelect = Selector('bst-custom-paginator .bst-custom-paginator-sub-container mat-select .mat-select-value');
    this.selectOptions = Selector('mat-option');
    //Table
    this.projectsInTable = Selector('bst-project-list mat-row');
    this.projectNamesInTable = Selector('bst-project-list mat-row .cdk-column-projectName');
    this.projectSAPClientCodesInTable = Selector('bst-project-list mat-row .cdk-column-sapClientCode');
  }

  async checkPageSizeSelects (t, pageSize) {
    await t
      .expect(this.pageSizeSelect.nth(0).innerText).eql(pageSize)
      .expect(this.pageSizeSelect.nth(1).innerText).eql(pageSize)
  }

  async changePageSizeSelect (t, pageSize, top = true) {
    var y = (top ? 0 : 1);
    await t
      .click(this.pageSizeSelect.nth(y))
      .click(this.selectOptions.withText(pageSize));
  }

  async startPoCreation(t, project = 'Dead!', territory = 'United Kingdom') {
    await this.filterByProject(t, project);
    await t
      .click(this.projectsInTable.withText(project))
      .click(this.territorySelect)
      .click(this.territorySelectOption.withText(territory))
      .click(this.createPoButton);
  }

  async checkCreatePoButtonDisabled(t, shouldBeDisabled) {
    await t.expect(this.createPoButton.withAttribute('disabled').exists).eql(shouldBeDisabled)
  }

  async testFilterByProject(t, projectName) {
    const projectNames = await this.projectNamesInTable();
    const count    = await projectNames.count;

    for (var i = 0; i < count; i++){
      await t
        .expect(projectNameInArray.nth(i).innerText).contains(projectName);
    }
  }

  async testFilterBySAPClientCode(t, sapClientCode) {
    const sapClientCodes = await this.projectSAPClientCodesInTable();
    const count    = await sapClientCodes.count;

    for (var i = 0; i < count; i++){
      await t
        .expect(sapClientCodes.nth(i).innerText).contains(sapClientCode);
    }
  }

  async filterByProject(t, projectName) {
    await t
      .selectText(this.projectFilter)
      .typeText(this.projectFilter, projectName);
  }

  async filterBySAPClientCode(t, sapClientCode) {
    await t
      .selectText(this.sapClientCodeFilter)
      .typeText(this.sapClientCodeFilter, sapClientCode);
  }
}

