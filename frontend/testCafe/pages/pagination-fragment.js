import { Selector } from 'testcafe';

export default class PaginationFragment {
  constructor () {
    this.pageSizeSelect = Selector('bst-custom-paginator .bst-custom-paginator-sub-container mat-select .mat-select-value');
    this.selectOptions = Selector('mat-option');
    this.paginatorNextButton = Selector('bst-custom-paginator .bst-custom-paginator-sub-container .bst-custom-paginator-navigation-next');
    this.paginatorPreviousButton = Selector('bst-custom-paginator .bst-custom-paginator-sub-container .bst-custom-paginator-navigation-previous');
    this.paginatorRangeLabel = Selector('bst-custom-paginator .bst-custom-paginator-sub-container .bst-custom-paginator-range-label');
  }

  async autoCheckPageSizeSelects (t) {
    await this.checkPageSizeSelects(t, '100');
    await this.changePageSizeSelect(t, '250', true);
    await this.checkPageSizeSelects(t, '250');
    await this.changePageSizeSelect(t, '500', false);
    await this.checkPageSizeSelects(t, '500');
  }

  async autoCheckPaginatorRanges (t) {
    var paginatorRangeText = await this.paginatorRangeLabel.nth(0).innerText;
    var totalItems = parseInt(paginatorRangeText.split(' of ')[1]);
    if(totalItems > 200){
      await this.checkPaginatorRanges(t, '1 - 100');
      await this.paginatorNext(t, true);
      await this.checkPaginatorRanges(t, '101 - 200');
      await this.paginatorPrevious(t, true);
      await this.checkPaginatorRanges(t, '1 - 100');
      await this.paginatorNext(t, false);
      await this.checkPaginatorRanges(t, '101 - 200');
      await this.paginatorPrevious(t, false);
      await this.checkPaginatorRanges(t, '1 - 100');
    }
  }

  async checkPageSizeSelects (t, pageSize) {
    await t
      .expect(this.pageSizeSelect.nth(0).innerText).eql(pageSize)
      .expect(this.pageSizeSelect.nth(1).innerText).eql(pageSize);
  }

  async checkPaginatorRanges (t, range) {
    await t
      .expect(this.paginatorRangeLabel.nth(0).innerText).contains(range)
      .expect(this.paginatorRangeLabel.nth(1).innerText).contains(range);
  }

  async paginatorNext (t, top = true) {
    var y = (top ? 0 : 1);
    await t
      .click(this.paginatorNextButton.nth(y));
  }

  async paginatorPrevious (t, top = true) {
    var y = (top ? 0 : 1);
    await t
      .click(this.paginatorPreviousButton.nth(y));
  }

  async changePageSizeSelect (t, pageSize, top = true) {
    var y = (top ? 0 : 1);
    await t
      .click(this.pageSizeSelect.nth(y))
      .click(this.selectOptions.withText(pageSize));
  }
}

