import { Selector } from 'testcafe';

export default class PurchaseOrdersPage {
  constructor () {
    //Header
    this.poNumberFilter = Selector('bst-custom-paginator input[placeholder="Filter By PO #"]');
    this.projectFilter = Selector('bst-custom-paginator input[placeholder="Filter By Project or SAP"]');
    this.territoryFilter = Selector('bst-custom-paginator input[placeholder="Filter By Territory"]');
    this.vendorFilter = Selector('bst-custom-paginator input[placeholder="Filter By Vendor"]');

    //Table
    this.statusHeader = Selector('bst-purchase-order-summary mat-header-row button').withText('Status');
    this.posInTable = Selector('bst-purchase-order-summary mat-table mat-row');
    this.projectNamesInTable = Selector('bst-purchase-order-summary mat-row .cdk-column-project-projectName');
    this.poNumbersInTable = Selector('bst-purchase-order-summary mat-row .cdk-column-purchaseOrder-purchaseOrderNumber');
    this.territoriesInTable = Selector('bst-purchase-order-summary mat-row .cdk-column-purchaseOrder-country-countryName');
    this.vendorsInTable = Selector('bst-purchase-order-summary mat-row .cdk-column-purchaseOrder-vendor-name1');

    //Bottom Buttons
    this.newPoButton = Selector('bst-purchase-order-button-bar button').withText('New PO');
  }

  async sortByStatus (t) {
    await t.click(this.statusHeader);
  }
  async getProjectInLine (t, lineNumber = 1) {
    //await t.debug();
    let projectAndSap = await this.posInTable.nth(lineNumber).find('.cdk-column-project-projectName').innerText;
    let project = projectAndSap.split('(')[0].trim();
    return project;
  }

  async getTerritoryInLine (t, lineNumber = 1) {
    return this.posInTable.nth(lineNumber).find('.cdk-column-purchaseOrder-country-countryName').innerText;
  }

  async createNewPo (t) {
    await t
      .click(this.newPoButton);
  }

  async openPo (t, lineNumber = 1) {
    await t
      .click(this.posInTable.nth(1));
  }

  async filterByPoNumber(t, poNumber) {
    await t
      .selectText(this.poNumberFilter)
      .typeText(this.poNumberFilter, poNumber);
  }

  async filterByProject(t, project) {
    await t
      .selectText(this.projectFilter)
      .typeText(this.projectFilter, project);
  }

  async filterByTerritory(t, territory) {
    await t
      .selectText(this.territoryFilter)
      .typeText(this.territoryFilter, territory);
  }

  async filterByVendor(t, vendor) {
    await t
      .selectText(this.vendorFilter)
      .typeText(this.vendorFilter, vendor);
  }

  async testFilterByProject(t, projectName) {
    const projectNames = await this.projectNamesInTable();
    this.testFilterComparisonHelper(t, projectName, projectNames);
  }

  async testFilterByPoNumber(t, poNumber) {
    const poNumbers = await this.poNumbersInTable();
    this.testFilterComparisonHelper(t, poNumber, poNumbers);
  }

  async testFilterByTerritory(t, territory) {
    const territories = await this.territoriesInTable();
    this.testFilterComparisonHelper(t, territory, territories);
  }

  async testFilterByVendor(t, vendor) {
    const vendors = await this.vendorsInTable();
    this.testFilterComparisonHelper(t, vendor, vendors);
  }

  async testFilterComparisonHelper(t, filterNeedle, filterHaystack) {
    const count    = await filterHaystack.count;
    for (var i = 0; i < count; i++){
      await t
        .expect(filterHaystack.nth(i).innerText).contains(filterNeedle);
    }
  }
}

