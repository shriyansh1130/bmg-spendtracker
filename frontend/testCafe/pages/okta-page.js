import { Selector } from 'testcafe';

export default class OktaPage {
  constructor () {
    this.usernameInput = Selector('#okta-signin-username');
    this.passwordInput = Selector('#okta-signin-password');
    this.submitButton = Selector('#okta-signin-submit');
  }

  async login (t) {
    await t
      .typeText( this.usernameInput,'florian.meyszies@syndicats.de')
      .typeText( this.passwordInput,'Fpjo2018')
      .click( this.submitButton);
  }
}

